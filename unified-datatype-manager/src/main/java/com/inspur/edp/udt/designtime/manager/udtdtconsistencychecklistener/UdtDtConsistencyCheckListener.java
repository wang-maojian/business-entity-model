package com.inspur.edp.udt.designtime.manager.udtdtconsistencychecklistener;

import com.inspur.edp.bef.bizentity.bizentitydtevent.BizEntityDTEventListener;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.entityeventargs.AbstractBeEntityArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.entityeventargs.RemovingEntityEventArgs;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationCollection;
import com.inspur.edp.cef.designtime.api.dtconsistencycheck.ConsistencyCheckEventMessage;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.element.ElementCollection;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.List;

public class UdtDtConsistencyCheckListener extends BizEntityDTEventListener {


    /**
     * 修改节点编号检查当前BE节点的UDT关联和依赖信息
     *
     * @param args
     * @return
     */
    @Override
    public RemovingEntityEventArgs removingEntity(RemovingEntityEventArgs args) {
        return (RemovingEntityEventArgs) udtConsistencyCheck(args);
    }

    /**
     * 对依赖信息进行检查
     *
     * @param args
     * @return
     */
    protected AbstractBeEntityArgs udtConsistencyCheck(AbstractBeEntityArgs args) {
        String returnMessage = getDependencyInfos(args.getMetadataPath(), args.getBeId(), args.getBeEntityId());
        if (returnMessage == null || returnMessage.isEmpty()) {
            return args;
        }
        ConsistencyCheckEventMessage message = new ConsistencyCheckEventMessage(false, returnMessage);
        args.addEventMessage(message);
        return args;
    }

    /**
     * 获取的关联信息
     *
     * @param metadataPath 元数据路径
     * @param beId         元数据ID
     * @return 关联当前BE的关联信息
     */
    protected String getDependencyInfos(String metadataPath, String beId, String beEntityId) {
        MetadataService metadataService = SpringBeanUtils
                .getBean(com.inspur.edp.lcm.metadata.api.service.MetadataService.class);
        List<GspMetadata> gspMetadataList = metadataService
                .getMetadataListByRefedMetadataId(metadataPath, beId);
        StringBuilder strBuilder = new StringBuilder();
        gspMetadataList.forEach(gspMetadata -> {
            if (!gspMetadata.getHeader().getType().equals("UnifiedDataType")) {
                return;
            }
            UnifiedDataTypeDef unifiedDataType = (UnifiedDataTypeDef) metadataService
                    .loadMetadata(gspMetadata.getHeader().getFileName(), gspMetadata.getRelativePath())
                    .getContent();
            if (!getSingleUdtDependence(unifiedDataType, beEntityId) && !getMultiUdtDependence(unifiedDataType, beEntityId))
                return;
            String packageName = projectName(gspMetadata.getRelativePath());
            strBuilder.append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0003",packageName,unifiedDataType.getCode()));
        });
        if (strBuilder.toString() == null || strBuilder.toString().isEmpty()) {
            return null;
        }
        return strBuilder.toString();
    }

    /**
     * 获取工程名称
     *
     * @param metadataPath 元数据路径
     * @return 元数据包名
     */
    protected String projectName(String metadataPath) {
        MetadataProjectService projectService = SpringBeanUtils
                .getBean(com.inspur.edp.lcm.metadata.api.service.MetadataProjectService.class);
        return projectService.getMetadataProjInfo(metadataPath).getName();
    }

    protected boolean getSingleUdtDependence(UnifiedDataTypeDef udtElement, String beEntityId) {
        if (udtElement instanceof SimpleDataTypeDef) {
            GspAssociationCollection gspAssociations = ((SimpleDataTypeDef) udtElement).getChildAssociations();
            if (gspAssociations.isEmpty()) {
                return false;
            }
            return gspAssociations.get(0).getRefObjectID().equals(beEntityId);
        }
        return false;
    }

    protected boolean getMultiUdtDependence(UnifiedDataTypeDef udtElement, String beEntityId) {
        if (!(udtElement instanceof ComplexDataTypeDef))
            return false;
        ElementCollection fields = ((ComplexDataTypeDef) udtElement).getElements();
        if (fields.isEmpty())
            return false;
        for (IGspCommonField field : fields) {
            //遍历多值UDT的每个字段
            if (field.getChildAssociations().isEmpty())
                continue;
            if (field.getChildAssociations().get(0) == null)
                continue;
            if (field.getChildAssociations().get(0).getRefObjectID().equals(beEntityId))
                return true;
        }
        return false;
    }
}
