package com.inspur.edp.udt.designtime.manager.generatecomponent.componentgenerators;


import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.bef.component.base.VoidReturnType;
import com.inspur.edp.bef.component.detailcmpentity.udtvalidation.UDTValidComponent;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;
import com.inspur.edp.udt.designtime.api.utils.UdtUtils;
import com.inspur.edp.udt.designtime.manager.generatecomponent.ComponentClassNameGenerator;
import com.inspur.edp.udt.designtime.manager.generatecomponent.JavaCompCodeNames;

public class CommonValGenerator extends CommonOperationComponentGenerator {

    public static CommonValGenerator getInstance() {
        return new CommonValGenerator();
    }

    private CommonValGenerator() {
    }

    @Override
    protected GspComponent buildComponent() {
        return new UDTValidComponent();
    }

    @Override
    protected void evaluateComponentInfo(GspComponent component, CommonOperation operation,
                                         GspComponent originalComponent) {
        this.originalComponent = originalComponent;
        // 基本信息（没有参数信息）
        evaluateComponentBasicInfo((UDTValidComponent) component, (CommonValidation) operation);
        // 返回值信息
        if (component instanceof UDTValidComponent) {
            ((UDTValidComponent) (component))
                    .getUdtValidMethod().setReturnValue(new VoidReturnType());
        }

    }

    private void evaluateComponentBasicInfo(UDTValidComponent component,
                                            CommonValidation validation) {
        if (!UdtUtils.checkNull(validation.getComponentId())) {
            component.setComponentID(validation.getComponentId());
        }
        component.setComponentCode(validation.getCode());
        component.setComponentName(validation.getName());
        component.setComponentDescription(validation.getDescription());
        component.getUdtValidMethod().setDotnetAssembly(this.assemblyName);

        String suffix = String.format("%1$s%2$s%3$s", this.udtDefCode, ".",
                JavaCompCodeNames.UDTValidationNameSpaceSuffix);
        String packageName = udtAssemblyName;
        packageName = javaModuleImportPackage(packageName);
        packageName = String.format("%1$s%2$s", packageName, suffix.toLowerCase());
        if (this.originalComponent != null) {
            component.getUdtValidMethod()
                    .setDotnetClassName(originalComponent.getMethod().getDotnetClassName());
            component.getMethod().setClassName(
                    javaModuleClassName(originalComponent.getMethod().getDotnetClassName(), packageName));
        } else {
            String classNameSuffix = validation.getCode();
            component.getUdtValidMethod().setDotnetClassName(ComponentClassNameGenerator
                    .generateUDTVldComponentClassName(this.nameSpace, classNameSuffix));
            component.getMethod().setClassName(javaModuleClassName(ComponentClassNameGenerator
                    .generateUDTVldComponentClassName(this.nameSpace, classNameSuffix), packageName));
        }
    }
}