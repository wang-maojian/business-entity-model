package com.inspur.edp.udt.designtime.manager.generatecomponent.componentgenerators;


import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.bef.component.base.VoidReturnType;
import com.inspur.edp.bef.component.detailcmpentity.udtdetermination.UDTDtmComponent;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.udt.designtime.api.utils.UdtUtils;
import com.inspur.edp.udt.designtime.manager.generatecomponent.ComponentClassNameGenerator;
import com.inspur.edp.udt.designtime.manager.generatecomponent.JavaCompCodeNames;

public class CommonDtmGenerator extends CommonOperationComponentGenerator {

    public static CommonDtmGenerator getInstance() {
        return new CommonDtmGenerator();
    }

    private CommonDtmGenerator() {
    }

    @Override
    protected GspComponent buildComponent() {
        return new UDTDtmComponent();
    }

    @Override
    protected void evaluateComponentInfo(GspComponent component, CommonOperation operation,
                                         GspComponent originalComponent) {
        this.originalComponent = originalComponent;
        // 基本信息（没有参数信息）
        evaluateComponentBasicInfo((UDTDtmComponent) component, (CommonDetermination) operation);
        // 返回值信息
        if (component instanceof UDTDtmComponent) {
            ((UDTDtmComponent) component)
                    .getUdtDtmMethod().setReturnValue(new VoidReturnType());
        }

    }

    private void evaluateComponentBasicInfo(UDTDtmComponent component,
                                            CommonDetermination determination) {
        if (!UdtUtils.checkNull(determination.getComponentId())) {
            component.setComponentID(determination.getComponentId());
        }
        component.setComponentCode(determination.getCode());
        component.setComponentName(determination.getName());
        component.setComponentDescription(determination.getDescription());
        component.getUdtDtmMethod().setDotnetAssembly(this.assemblyName);

        String suffix = String.format("%1$s%2$s%3$s", this.udtDefCode, ".",
                JavaCompCodeNames.UDTDeterminationNameSpaceSuffix);
        String packageName = udtAssemblyName;
        packageName = javaModuleImportPackage(packageName);
        packageName = String.format("%1$s%2$s", packageName, suffix.toLowerCase());

        if (this.originalComponent != null) {
            component.getUdtDtmMethod()
                    .setDotnetClassName(originalComponent.getMethod().getDotnetClassName());
            component.getMethod().setClassName(
                    javaModuleClassName(originalComponent.getMethod().getDotnetClassName(), packageName));

        } else {
            String classNameSuffix = udtDefCode + determination.getCode();
            component.getUdtDtmMethod().setDotnetClassName(ComponentClassNameGenerator
                    .generateUDTDtmComponentClassName(this.nameSpace, classNameSuffix));
            component.getMethod().setClassName(javaModuleClassName(ComponentClassNameGenerator
                    .generateUDTDtmComponentClassName(this.nameSpace, classNameSuffix), packageName));
        }
    }
}