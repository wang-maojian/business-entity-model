package com.inspur.edp.udt.designtime.manager.copy;

import com.inspur.edp.metadata.rtcustomization.api.entity.ConfigData;
import com.inspur.edp.metadata.rtcustomization.api.entity.MimicTypeEnum;
import com.inspur.edp.metadata.rtcustomization.spi.ConfigDataSaveSpi;
import com.inspur.edp.udt.designtime.api.nocode.BusinessField;
import com.inspur.edp.udt.designtime.api.nocode.IBusinessFieldService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.List;

/**
 * @Author wangmaojian
 * @create 2023/4/13
 */
public class BusinessFieldConfigDataSaveImpl implements ConfigDataSaveSpi {
    @Override
    public void save(ConfigData configData) {
        if (!configData.getTableName().equals(BusinessFieldConstants.BusinessField_Table_Name)) {
            return;
        }
        if (configData.getEntityList() == null || configData.getEntityList().isEmpty()) {
            return;
        }
        List<BusinessField> businessFieldList = (List<BusinessField>) configData.getEntityList();
        IBusinessFieldService businessFieldService = SpringBeanUtils.getBean(IBusinessFieldService.class);
        for (BusinessField field : businessFieldList) {
            businessFieldService.saveBusinessField(field);
        }
    }

    @Override
    public MimicTypeEnum getMimicTypeEnum() {
        return MimicTypeEnum.COMMON_APP_COPY;
    }
}
