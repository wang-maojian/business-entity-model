package com.inspur.edp.udt.designtime.manager.commonstructure;

import com.inspur.edp.caf.cef.rt.api.CommonStructureInfo;
import com.inspur.edp.caf.cef.rt.spi.EntitySchemaExtension;
import com.inspur.edp.caf.cef.schema.structure.CommonStructure;
import com.inspur.edp.lcm.metadata.api.service.MetadataRTService;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public class UdtEntitySchemaExtension implements EntitySchemaExtension {
    @Override
    public CommonStructure getEntity(String metaId) {
        MetadataRTService service = SpringBeanUtils.getBean(MetadataRTService.class);
        UnifiedDataTypeDef udt = (UnifiedDataTypeDef) service.getMetadata(metaId).getContent();
        return udt;
    }

    @Override
    public CommonStructureInfo getEntitySummary(String s) {
        return null;
    }

    @Override
    public String getEntityType() {
        return "UnifiedDataType";
    }
}
