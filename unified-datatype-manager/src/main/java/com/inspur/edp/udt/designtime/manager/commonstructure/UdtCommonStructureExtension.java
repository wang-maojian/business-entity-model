package com.inspur.edp.udt.designtime.manager.commonstructure;

import com.inspur.edp.caf.cef.dt.spi.CommonStructureSchemaExtension;
import com.inspur.edp.caf.cef.schema.common.CommonStructureContext;
import com.inspur.edp.caf.cef.schema.common.CommonStructureInfo;
import com.inspur.edp.caf.cef.schema.structure.CommonStructure;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;

public class UdtCommonStructureExtension implements CommonStructureSchemaExtension {

    @Override
    public String getEntityType() {
        return "UnifiedDataType";
    }

    @Override
    public CommonStructure getCommonStructure(IMetadataContent iMetadataContent,
                                              CommonStructureContext commonStructureContext) {
        return (UnifiedDataTypeDef) iMetadataContent;
    }

    @Override
    public CommonStructureInfo getCommonStructureSummary(IMetadataContent iMetadataContent) {
        return null;
    }
}
