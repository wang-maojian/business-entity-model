package com.inspur.edp.udt.designtime.manager.generatecomponent;


import com.inspur.edp.lcm.fs.api.IFsService;
import com.inspur.edp.udt.designtime.api.utils.UdtUtils;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public final class ComponentGenUtil {

    public static final String ComponentDir = "component";

    public static String prepareComponentDir(String bePath) {
        IFsService service = SpringBeanUtils.getBean(IFsService.class);
        String path = UdtUtils.getCombinePath(bePath, ComponentDir);
        if (!service.existsAsDir(path)) {
            service.createDir(path);
        }
        return path;
    }

  @Deprecated
  public static String prepareJavaPackageName(String assemblyName) {
    String[] strArray = assemblyName.split("[.]", -1);
    String str = "com.";
    int i;

    for (i = 0; i < strArray.length - 1; i++) {
      str += strArray[i].toLowerCase() + ".";
    }
    str += strArray[i].toLowerCase();
    if (str.startsWith("com.inspur.gsp.common.commonudt")) {
      str = str
          .replace("com.inspur.gsp.common.commonudt", "com.inspur.edp.common.commonudt");
    }
    return str;
  }

  public static String prepareJavaPackageName(String packagePrefix, String assemblyName) {
    String[] strArray = assemblyName.split("[.]", -1);
    String str = packagePrefix +".";
    int i;

    for (i = 0; i < strArray.length - 1; i++) {
      str += strArray[i].toLowerCase() + ".";
    }
    str += strArray[i].toLowerCase();

    if (str.startsWith("com.inspur.gsp.common.commonudt")) {
      str = str
              .replace("com.inspur.gsp.common.commonudt", "com.inspur.edp.common.commonudt");
    }
    return str;
  }
}