package com.inspur.edp.udt.designtime.manager.validate;


import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.collection.CommonDtmCollection;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.element.GspAssociationKey;
import com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
import com.inspur.edp.cef.designtime.api.element.GspEnumValue;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.dbInfo.ColumnMapType;
import com.inspur.edp.udt.designtime.api.entity.element.ElementCollection;
import com.inspur.edp.udt.designtime.api.entity.element.UdtElement;
import com.inspur.edp.udt.designtime.api.entity.validation.UdtTriggerTimePointType;
import com.inspur.edp.udt.designtime.api.entity.validation.ValidationCollection;
import com.inspur.edp.udt.designtime.api.entity.validation.ValidationInfo;
import com.inspur.edp.udt.designtime.api.exception.UdtModelErrorCodeEnum;
import com.inspur.edp.udt.designtime.api.exception.UdtModelException;
import com.inspur.edp.udt.designtime.api.utils.UdtUtils;

import java.text.DateFormat;
import java.util.EnumSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UdtValidater {
    public final void validate(UnifiedDataTypeDef udt) {
        String validateResult = validateUdt(udt);
        if (!validateResult.isEmpty()) {
            //保存前校验不通过，抛异常
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_TEMPLATE_ERROR, validateResult);
        }
    }

    private String validateUdt(UnifiedDataTypeDef udt) {
        String message = validateBasicInfo(udt);
        if (!message.isEmpty()) {
            return message;
        }
        if (udt instanceof ComplexDataTypeDef) {
            message = validateComplexUdt(
                    (ComplexDataTypeDef) udt);
            if (!message.isEmpty()) {
                return message;
            }
        } else if (udt instanceof SimpleDataTypeDef) {
            message = validateSimpleDataTypeDef((SimpleDataTypeDef) udt);
            if (!message.isEmpty()) {
                return message;
            }
        }

        message = validateValidations(udt);
        return message;
    }

    private String validateSimpleDataTypeDef(SimpleDataTypeDef element) {
        StringBuilder sb = new StringBuilder();
        if (element.getObjectType() == GspElementObjectType.Association) {
            //关联字段必须满足ID字段类型
            if (!validateIDElementTypeAndLength(element)) {
                sb.append((MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0004")));
                sb.append("\n");
            }

            if (element.getChildAssociations() == null || element.getChildAssociations().isEmpty()) {
                sb.append((MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0005")));
                sb.append("\n");
            } else {

                for (GspAssociation association : element.getChildAssociations()) {
                    if (association == null || association.getKeyCollection().isEmpty()) {
                        sb.append(((MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0005"))));
                        sb.append("\n");
                    }
                    //处理有时关联字段会丢失的问题
                    if (association != null) {
                        for (GspAssociationKey associationKey : association.getKeyCollection()) {
                            if (associationKey.getSourceElement().isEmpty()
                                    || associationKey.getTargetElement().isEmpty()) {
                                sb.append(((MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0006"))));
                                sb.append("\n");
                            }
                        }
                    }

                }
            }
        } else if (element.getObjectType() == GspElementObjectType.Enum) {
            if (element.getContainEnumValues().isEmpty()) {
                sb.append(((MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0007"))));
                sb.append("\n");
            }
            // 枚举类型字段，非必填
            if (element.getIsRequired()) {
                sb.append(((MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0008"))));
                sb.append("\n");
            }
            //处理Enum值必须为string的问题
            for (GspEnumValue ev : element.getContainEnumValues()) {
                String _value = ev.getValue();
                if (element.getMDataType() == GspElementDataType.Boolean) {
                    try {
                        boolean temp = Boolean.parseBoolean(_value);
                    } catch (Exception e) {
                        sb.append(((MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0009"))));
                        sb.append("\n");
                    }
                } else if (element.getMDataType() == GspElementDataType.String) {
                    int indexLength = String.valueOf(ev.getIndex()).length();
                    int eleLength = element.getLength();
                    if (eleLength < indexLength) {
                        sb.append((MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0010", ev.getName())));
                        sb.append("\n");
                    }
                }
            }
        } else {
            sb.append(
                    validateElementDefaultValue(element.getContainElements().get(0), element.getCode(),
                            MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0011")));
        }
        return sb.toString();
    }

    private String validateBasicInfo(UnifiedDataTypeDef udt) {
        StringBuilder sb = new StringBuilder();
        if (UdtUtils.checkNull(udt.getCode())) {
            sb.append((MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0012", udt.getName())));
        } else {
            // 模型Code限制非法字符
            if (!conformToNaminGspecification(udt.getCode())) {
                sb.append((MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0013", udt.getName())));
                sb.append("\n");
            }
        }

        if (UdtUtils.checkNull(udt.getName())) {
            sb.append((MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0014", udt.getCode())));
        }

        return sb.toString();
    }

    private String validateComplexUdt(ComplexDataTypeDef udt) {
        return validateComplexBasicInfo(udt);

    }

    private String validateComplexBasicInfo(ComplexDataTypeDef udt) {
        StringBuilder sb = new StringBuilder();

        for (IGspCommonField el : udt.getElements()) {

            UdtElement element = (UdtElement) el;
            if (element != null && UdtUtils.checkNull(element.getCode())) {
                sb.append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0015", udt.getName(), element.getName()));
                sb.append("\n");
            } else {
                // 字段Code限制非法字符
                if (element != null && !conformToNaminGspecification(element.getCode())) {
                    sb.append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0016",
                            udt.getName(), element.getName()));
                    sb.append("\n");
                }
            }
            if (element != null && UdtUtils.checkNull(element.getName())) {
                sb.append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0017", udt.getName(), element.getCode()));
                sb.append("\n");
            }

            if (element.getObjectType() == GspElementObjectType.Association) {
                if (udt.getDbInfo().getMappingType() == ColumnMapType.SingleColumn) {
                    throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_UDT_MODEL_0009, element.getName());
                }

                //关联字段必须满足ID字段类型
                if (!validateIDElementTypeAndLength(element)) {
                    sb.append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0018",
                            udt.getName(), element.getName()));
                    sb.append("\n");
                }

                if (element.getChildAssociations() == null || element.getChildAssociations().isEmpty()) {
                    sb.append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0019", udt.getName(),
                            element.getName()));
                    sb.append("\n");
                } else {

                    for (GspAssociation association : element.getChildAssociations()) {
                        if (association == null || association.getKeyCollection().isEmpty()) {
                            sb.append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0020", udt.getName(),
                                    element.getName()));
                            sb.append("\n");
                        }
                        //处理有时关联字段会丢失的问题
                        if (association != null) {
                            for (GspAssociationKey associationKey : association.getKeyCollection()) {
                                if (associationKey.getSourceElement().isEmpty()
                                        || associationKey.getTargetElement().isEmpty()) {
                                    sb.append(MessageI18nUtils.
                                            getMessage("GSP_UDT_MESSAGE_0021", udt.getName(), element.getName()));
                                    sb.append("\n");
                                }
                            }
                        }

                    }
                }
            } else if (element.getObjectType() == GspElementObjectType.Enum) {
                if (element.getContainEnumValues().isEmpty()) {
                    sb.append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0022", udt.getName(),
                            element.getName()));
                    sb.append("\n");
                }
                // 枚举类型字段，非必填
                if (element.getIsRequire()) {
                    sb.append(MessageI18nUtils.getMessage(("GSP_UDT_MESSAGE_0023")));
                    sb.append("\n");
                }
                //处理Enum值必须为string的问题
                for (GspEnumValue ev : element.getContainEnumValues()) {
                    String _value = ev.getValue();
                    if (element.getMDataType() == GspElementDataType.Boolean) {
                        try {
                            boolean temp = Boolean.parseBoolean(_value);
                        } catch (java.lang.Exception e) {
                            sb.append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0024", udt.getName(),
                                    element.getName()));
                            sb.append("\n");
                        }
                    } else if (element.getMDataType() == GspElementDataType.String) {
                        int indexLength = String.valueOf(ev.getIndex()).length();
                        int eleLength = element.getLength();
                        if (eleLength < indexLength) {
                            sb.append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0025", udt.getName(),
                                    udt.getName(), ev.getName()));
                            sb.append("\n");
                        }
                    }
                }

                if (element.getContainEnumValues() != null && !element.getContainEnumValues().isEmpty()) {
                    for (int i = 0; i < element.getContainEnumValues().size() - 1; i++) {

                        GspEnumValue enumValue1 = element.getContainEnumValues().get(i);
                        for (int j = i + 1; j < element.getContainEnumValues().size(); j++) {

                            GspEnumValue enumValue2 = element.getContainEnumValues().get(j);
                            if (enumValue1.getName().equals(enumValue2.getName())) {
                                sb.append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0026", udt.getName(),
                                        element.getName(), enumValue1.getName()));
                                sb.append("\n");
                            }
                        }
                    }
                }
            } else {
                sb.append(((validateElementDefaultValue(element, udt.getCode(), MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0027", udt.getCode(), element.getName())))));
            }
            if (element.getIsUdt()) {
                if (udt.getDbInfo().getMappingType() == ColumnMapType.MultiColumns) {
                    throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_UDT_MODEL_0010, element.getName());
                }
            }
            if (element.getIsUdt() && UdtUtils.checkNull(element.getUdtID())) {
                sb.append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0028", udt.getName(),
                        element.getName()));
                sb.append("\n");
            }
        }

        String message = validateElementCodeRepeat(udt);
        if (!message.isEmpty()) {
            sb.append(message);
        }

        String dtmMessage = validateCommonDeterminations(udt);
        if (!dtmMessage.isEmpty()) {
            sb.append(dtmMessage);
        }
        return sb.toString();
    }

    /**
     * 字段编号重复
     */
    private String validateElementCodeRepeat(ComplexDataTypeDef udt) {
        ElementCollection allElementList = udt.getElements();
        if (allElementList != null && !allElementList.isEmpty()) {
            for (int i = 0; i < allElementList.size() - 1; i++) {
                UdtElement firstElement = (UdtElement) allElementList.getItem(i);
                for (int j = i + 1; j < allElementList.size(); j++) {
                    UdtElement secondElement = (UdtElement) ((allElementList.getItem(j) instanceof UdtElement)
                            ? allElementList.getItem(j) : null);
                    if (secondElement != null && firstElement.getCode().equals(secondElement.getCode())) {
                        return MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0029", firstElement.getName(),
                                secondElement.getName());
                    }
                }
            }
        }
        return "";
    }

    private String validateValidations(UnifiedDataTypeDef udt) {
        StringBuilder sb = new StringBuilder();

        ValidationCollection actions = udt.getValidations();

        String errorMessage = MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0030", udt.getName());
        //①必填项
        sb.append(validateValidationBasicInfo(errorMessage, actions));
        if (sb.length() > 0) {
            return sb.toString();
        }
        //②编号重复
        sb.append(validateValidationCodeRepeat(errorMessage, actions));
        //③校验规则至少选择一种执行时机
        sb.append(validateValidationTriggerTimePoint(errorMessage, actions));

        return sb.toString();
    }

    /**
     * 校验规则至少选择一个执行时机
     */
    private String validateValidationTriggerTimePoint(String errorMessage,
                                                      ValidationCollection operations) {
        StringBuilder sb = new StringBuilder();
        if (operations != null && !operations.isEmpty()) {
            for (ValidationInfo operation : operations) {
                if (operation.getTriggerTimePointType()
                        .equals(EnumSet.of(UdtTriggerTimePointType.None))) {
                    String message = (errorMessage + operation.getName() + MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0031"));
                    sb.append(message);
                    sb.append("\n");
                }
            }
        }
        return sb.toString();
    }

    private String validateValidationBasicInfo(String errorMessage,
                                               ValidationCollection validations) {
        if (validations == null || validations.isEmpty()) {
            return "";
        }

        StringBuilder sb = new StringBuilder();

        for (ValidationInfo val : validations) {
            if (UdtUtils.checkNull(val.getCode())) {
                sb.append(errorMessage).append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0032", val.getName()));
                sb.append("\n");
            } else {
                // 操作Code限制非法字符
                if (!conformToNaminGspecification(val.getCode())) {
                    sb.append(errorMessage).append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0033", val.getName()));
                    sb.append("\n");
                }
            }
            if (UdtUtils.checkNull(val.getName())) {
                sb.append(errorMessage).append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0034", val.getCode()));
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    private String validateValidationCodeRepeat(String errorMessage,
                                                ValidationCollection validations) {
        StringBuilder sb = new StringBuilder();
        if (validations != null && !validations.isEmpty()) {
            for (int i = 0; i < validations.size() - 1; i++) {

                ValidationInfo firstOperation = validations.get(i);
                for (int j = i + 1; j < validations.size(); j++) {

                    ValidationInfo secondOperation = validations.get(j);
                    if (firstOperation.getCode().equals(secondOperation.getCode())) {
                        String message = errorMessage + MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0035", firstOperation.getName(),
                                secondOperation.getName());
                        sb.append("\n");
                        return message;
                    }
                }
            }
        }
        return sb.toString();
    }

    private boolean conformToNaminGspecification(String text) {
        String regex = "^[A-Za-z_][A-Za-z_0-9]*$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        return matcher.matches();
    }

    private boolean validateIDElementTypeAndLength(UdtElement element) {
        if (element == null) {
            return true;
        }
        return element.getMDataType() == GspElementDataType.String && element.getLength() == 36;
    }

    private boolean validateIDElementTypeAndLength(SimpleDataTypeDef element) {
        if (element == null) {
            return true;
        }
        return element.getMDataType() == GspElementDataType.String && element.getLength() == 36;
    }


    private String validateElementDefaultValue(IGspCommonField ele, String objName,
                                               String errorMessage) {
        StringBuilder sb = new StringBuilder();
        String regex = "^-?\\\\d+$";//判断整数，负值也是整数
        Pattern pattern = Pattern.compile(regex);
        String reDecimalString = String
                .format("^(([0-9]+\\.[0-9]%1$s})|([0-9]*\\.[0-9]%1$s})|([1-9][0-9]+)|([0-9]))$",
                        ele.getPrecision());
        Pattern patternDeciaml = Pattern.compile(regex);
        // 20190523-整型枚举可设置枚举编号为默认值；关联/udt默认值暂不支持；
        if (ele.getObjectType() != GspElementObjectType.None) {
            return sb.toString();
        }

        boolean isValidate = true;
        GspElementDataType type = ele.getMDataType();
        String value = ele.getDefaultValue();

        if (!UdtUtils.checkNull(value)) {
            switch (type) {
                case String:
                case Text:
                    break;
                case Integer:
                    Matcher matcherInt = pattern.matcher(value);
                    if (matcherInt.matches()) {
                        isValidate = true;
                    } else {
                        isValidate = false;
                    }
                    break;
                case Decimal:
                    Matcher matcherDecimal = patternDeciaml.matcher(value);
                    if (matcherDecimal.matches()) {
                        isValidate = true;
                    } else {
                        sb.append(errorMessage).append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0036"));
                        sb.append("\n");
                        return sb.toString();
                    }
                    break;
                case Date:
                case DateTime:
                    try {
                        DateFormat.getInstance().parse(value);
                        isValidate = true;
                    } catch (java.lang.Exception e) {
                        isValidate = false;
                    }
                    break;
                case Boolean:
                    if ("True".equalsIgnoreCase(value) || "False".equalsIgnoreCase(value)) {
                        isValidate = true;
                    } else {
                        isValidate = false;
                    }
                    break;
                case Binary:
                    if (value.trim().isEmpty()) {
                        isValidate = true;
                    } else {
                        isValidate = false;
                    }
                    break;
                default:
                    break;
            }
        }
        if (!isValidate) {
            sb.append(errorMessage).append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0037"));
            sb.append("\n");
        }
        return sb.toString();
    }

//C#
    ///#region 操作

    private String validateCommonDeterminations(ComplexDataTypeDef cUdt) {
        StringBuilder sb = new StringBuilder();
        CommonDtmCollection dtmsAfterCreate = cUdt.getDtmAfterCreate();
        CommonDtmCollection dtmsAfterModify = cUdt.getDtmAfterModify();
        CommonDtmCollection dtmsBeforeSave = cUdt.getDtmBeforeSave();
        java.util.ArrayList<CommonOperation> dtms = new java.util.ArrayList<>();

        dtms.addAll(dtmsAfterCreate);

        dtms.addAll(dtmsAfterModify);

        dtms.addAll(dtmsBeforeSave);
        String errorMessage = MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0039", cUdt.getName());

        //①必填项
        sb.append(validateCommonOperationBasicInfo(errorMessage, dtms));
        if (sb.length() > 0) {
            return sb.toString();
        }
        //②编号重复
        sb.append(validateCommonOperationCodeRepeat(errorMessage, dtms));

        return sb.toString();
    }

    private String validateCommonOperationBasicInfo(String errorMessage,
                                                    java.util.ArrayList<CommonOperation> ops) {
        if (ops == null || ops.isEmpty()) {
            return "";
        }

        StringBuilder sb = new StringBuilder();

        for (CommonOperation val : ops) {
            if (UdtUtils.checkNull(val.getCode())) {
                sb.append(errorMessage).append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0032", val.getName()));
                sb.append("\n");
            } else {
                // 操作Code限制非法字符
                if (!conformToNaminGspecification(val.getCode())) {
                    sb.append(errorMessage).append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0033", val.getName()));
                    sb.append("\n");
                }
            }
            if (UdtUtils.checkNull(val.getName())) {
                sb.append(errorMessage).append(MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0034", val.getCode()));
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    private String validateCommonOperationCodeRepeat(String errorMessage,
                                                     java.util.ArrayList<CommonOperation> ops) {
        StringBuilder sb = new StringBuilder();
        if (ops != null && !ops.isEmpty()) {
            for (int i = 0; i < ops.size() - 1; i++) {

                CommonOperation firstOperation = ops.get(i);
                for (int j = i + 1; j < ops.size(); j++) {

                    CommonOperation secondOperation = ops.get(j);
                    if (firstOperation.getCode().equals(secondOperation.getCode())) {
                        String message = errorMessage + MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0035", firstOperation.getName(),
                                secondOperation.getName());
                        sb.append("\n");
                        return message;
                    }
                }
            }
        }
        return sb.toString();
    }

}