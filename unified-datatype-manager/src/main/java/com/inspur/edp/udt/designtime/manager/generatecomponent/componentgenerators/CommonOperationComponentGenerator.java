package com.inspur.edp.udt.designtime.manager.generatecomponent.componentgenerators;


import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;
import com.inspur.edp.das.commonmodel.util.MetadataProjectUtil;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import com.inspur.edp.udt.designtime.api.exception.UdtModelErrorCodeEnum;
import com.inspur.edp.udt.designtime.api.exception.UdtModelException;
import com.inspur.edp.udt.designtime.api.utils.UdtUtils;
import com.inspur.edp.udt.designtime.manager.generatecomponent.ComponentGenUtil;
import com.inspur.edp.udt.designtime.manager.generatecomponent.GspMetadataExchangeUtil;
import com.inspur.edp.udt.designtime.manager.generatecomponent.JavaCompCodeNames;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

/**
 * CommonOperation构件生成器基类
 */
public abstract class CommonOperationComponentGenerator {

    /**
     * UDTDef编号
     */
    protected String udtDefCode;
    /**
     * UDTDef程序集名称
     */
    protected String assemblyName;
    protected String udtAssemblyName;
    protected String nameSpace;
    /**
     * 原始构件
     */
    protected GspComponent originalComponent;
    protected String bizObjectID;

  /**
   * 包路径前缀，默认值为com
   */
  protected String packagePrefix;

  /**
   * 生成构件元数据
   *
   * @param operation UDT操作
   * @param path 生成指定路径
   * @param udtDefCode UDT编号
   * @param assemblyName UDT程序集名称
   */
  public final void generateComponent(CommonOperation operation, String path, String udtDefCode,
      String assemblyName, String defaultNamespace, String bizObjectID, String udtAssemblyName) {
    this.udtDefCode = udtDefCode;
    this.assemblyName = assemblyName;
    this.udtAssemblyName = udtAssemblyName;
    this.nameSpace = defaultNamespace;
    this.bizObjectID = bizObjectID;
    //获取包路径前缀
    this.packagePrefix = MetadataProjectUtil.getPackagePrefix(path);

    if (operation.getComponentId() == null || "".equals(operation.getComponentId())) {
      createComponent(operation, path);
    } else {
      modifyComponent(operation, path);
    }
  }

    /**
     * 新建构件
     */
    private void createComponent(CommonOperation operation, String path) {
        //1、构建实体
        GspComponent component = buildComponent();
        //2、赋值
        evaluateComponentInfo(component, operation, null);
        //3、生成构件
        String componentMetadataName = establishComponent(component, path);
        //4、建立Action与元数据之间的关联关系
        operation.setComponentId(component.getComponentID());
        //操作的ComponentName用来记录生成的构件元数据的名称
        operation.setComponentName(componentMetadataName);
        operation.setIsGenerateComponent(true);
    }

    /**
     * 构造构件实体类
     *
     * @return 构件实体类
     * <see cref="IGspComponent"/>
     */
    protected abstract GspComponent buildComponent();

    /**
     * 为构件赋值
     *
     * @param component 构件实体信息
     * @param operation 动作信息
     */
    protected abstract void evaluateComponentInfo(GspComponent component, CommonOperation operation,
                                                  GspComponent originalComponent);

    /**
     * 生成构件实体对应的构件元数据
     *
     * @param component 构件实体
     * @param path      生成构件元数据指定路径
     * @return 生成的构件元数据名称
     * <see cref="string"/>
     */
    private String establishComponent(GspComponent component, String path) {
        return GspMetadataExchangeUtil
                .getInstance()
                .establishGSPMetdadata(component, path, this.udtDefCode, this.bizObjectID, this.nameSpace);
    }

    /**
     * 修改构件
     */
    private void modifyComponent(CommonOperation operation, String path) {
        //1、构建实体
        GspComponent component = buildComponent();
        String fullPath = "";

        MetadataService metadataService = SpringBeanUtils.getBean(MetadataService.class);
        // 得到具体类型的构件扩展名
        String cmpExtendName = evaluateCompTypeExtendNameByOpType(operation);
        // 带针对不同类型构件的扩展名的文件全名
        String metadataFileNameWithExtendName = operation.getComponentName() + cmpExtendName;
        if (!metadataService.isMetadataExist(path, metadataFileNameWithExtendName)) {
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_UDT_MODEL_0001, path, metadataFileNameWithExtendName);
        }
        fullPath = path + UdtUtils.getSeparator() + metadataFileNameWithExtendName;
        GspComponent originalComponent = getOriginalComponent(path, metadataFileNameWithExtendName);
        //2、赋值
        evaluateComponentInfo(component, operation, originalComponent);
        //4、修改更新构件元数据
        GspMetadataExchangeUtil.getInstance()
                .updateGspMetadata(component, fullPath, this.udtDefCode, this.bizObjectID, this.nameSpace);
    }

  protected final String javaModuleImportPackage(String assemblyName) {
    return ComponentGenUtil.prepareJavaPackageName(this.packagePrefix, assemblyName) + ".";
  }

    /**
     * 获得Java模版类的名称
     */
    protected final String javaModuleClassName(String classNamestr, String packageNameStr) {
        String connections = "";
        if (!UdtUtils.checkNull(classNamestr)) {
            String className = classNamestr.substring(classNamestr.lastIndexOf('.'));
            connections = String.format("%1$s%2$s", packageNameStr, className);
        }
        return connections;
    }

    /**
     * 获取修改元数据之前的元数据信息
     */
    private GspComponent getOriginalComponent(String path, String metadataFileNameWithSuffix) {
        MetadataService metadataService = SpringBeanUtils.getBean(MetadataService.class);
        GspMetadata metadata = metadataService.loadMetadata(metadataFileNameWithSuffix, path);
        GspComponent originalComponent = (GspComponent) ((metadata.getContent() instanceof GspComponent)
                ? metadata.getContent() : null);
        return originalComponent;
    }

    /**
     * 根据操作类型获取扩展名
     *
     * @param operation 操作类型
     * @return 扩展名
     */
    private String evaluateCompTypeExtendNameByOpType(CommonOperation operation) {
        if (operation instanceof CommonDetermination) {
            return JavaCompCodeNames.UDTDtmCmpExtendName;
        } else if (operation instanceof CommonValidation) {
            return JavaCompCodeNames.UDTValCmpExtendName;
        } else {
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0006, operation.getClass().getSimpleName());
        }
    }
}