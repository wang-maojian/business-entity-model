package com.inspur.edp.udt.designtime.manager.generatecomponent;

public class ComponentClassNameGenerator {

    private static final String classNameFormat = "%1$s.%2$s.%3$s%4$s";

    public static String generateUDTVldComponentClassName(String assembly, String compCode) {
        return String
                .format(classNameFormat, assembly, JavaCompCodeNames.UDTValidationNameSpaceSuffix, compCode,
                        "UDTVld");
    }

    public static String generateUDTDtmComponentClassName(String assembly, String compCode) {
        return String
                .format(classNameFormat, assembly, JavaCompCodeNames.UDTDeterminationNameSpaceSuffix,
                        compCode, "UDTDtm");
    }
}