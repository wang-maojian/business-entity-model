package com.inspur.edp.udt.designtime.manager.generatecmpcode.javacodegenerator;


import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.das.commonmodel.util.MetadataProjectUtil;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.RefCommonService;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.exception.UdtModelErrorCodeEnum;
import com.inspur.edp.udt.designtime.api.exception.UdtModelException;
import com.inspur.edp.udt.designtime.api.utils.UdtUtils;
import com.inspur.edp.udt.designtime.manager.generatecomponent.ComponentGenUtil;
import com.inspur.edp.udt.designtime.manager.generatecomponent.JavaCompCodeNames;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public abstract class JavaBaseCommonCompCodeGenerator {

    ///#region 字段
    private String entitynameSpace;
    private String apinameSpace;


    protected CommonOperation operation;
    protected String nameSpace;
    protected String entityClassName;
    protected String code;
    protected String packageName;

    private String privatePath;

  /**
   * 包路径前缀
   */
  private String packagePrefix;

  public final String getPath() {
    return privatePath;
  }

    public final void setPath(String value) {
        privatePath = value;
    }

    private RefCommonService lcmDtService;

    private RefCommonService getLcmDtService() {
        if (lcmDtService == null) {
            lcmDtService = SpringBeanUtils.getBean(RefCommonService.class);
        }
        return lcmDtService;
    }

    protected abstract String getBaseClassName();

  /**
   * 构造函数
    */
  protected JavaBaseCommonCompCodeGenerator(UnifiedDataTypeDef udtDef, CommonOperation operation,
      String nameSpace, String path) {
    this.code = udtDef.getCode();
    setPath(path);
    //获取包路径前缀
    this.packagePrefix = MetadataProjectUtil.getPackagePrefix(path);
    this.packageName = udtDef.getDotnetAssemblyName();
    this.operation = operation;
    this.nameSpace = getnameSpace();
    this.entityClassName = udtDef.getGeneratedEntityClassInfo().getClassName();
    this.entitynameSpace = udtDef.getGeneratedEntityClassInfo().getClassNamespace();
    this.apinameSpace = udtDef.getApiNamespace().getDefaultNamespace();
  }

  private String getnameSpace() {
    String nameSpace = "%1$s.%2$s";
    return String.format(nameSpace, packageName + "." + code, getNameSpaceSuffix());
  }

    protected abstract String getNameSpaceSuffix();

    /**
     * 获取当前构件名称
     */
    public final String getCompName() {
        if (UdtUtils.checkNull(operation.getComponentId())) {
            return getInitializeCompName();
        }
        GspMetadata metadata = getLcmDtService().getRefMetadata(operation.getComponentId());
        if (metadata == null) {
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_UDT_MODEL_0005);
        }

        IMetadataContent content = metadata.getContent();
        String fullClassName = "";
        if (content instanceof GspComponent) {
            GspComponent component = (GspComponent) ((metadata.getContent() instanceof GspComponent)
                    ? metadata.getContent() : null);
            if (component != null) {
                fullClassName = component.getMethod().getDotnetClassName();
            }
        } else {
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_UDT_MODEL_0006);
        }

        String[] sections = fullClassName.split("[.]", -1);
        return sections[sections.length - 1];
    }

  public final String generate() {
    StringBuilder result = new StringBuilder();

    //转换Java包名
    nameSpace = ComponentGenUtil.prepareJavaPackageName(this.packagePrefix, nameSpace);
    result.append(JavaCompCodeNames.KeywordPackage).append(" ").append(nameSpace).append(";")
        .append(getNewline());

    generateImport(result);
    result.append("\n");

    ///#region ClassStart
    result.append(JavaCompCodeNames.KeywordPublic).append(" ")
        .append(JavaCompCodeNames.KeywordClass).append(" ").append(getCompName()).append(" ")
        .append(JavaCompCodeNames.KeywordExtends).append(" ").append(getBaseClassName()).append(" ")
        .append("{").append(getNewline());

        ///#endregion
        ///#region Field

        javaGenerateField(result);
        result.append("\n");

        ///#endregion
        ///#region Constructor

        javaGenerateConstructor(result);
        result.append("\n");

        ///#endregion

        ///#region ExecuteMethod
        result.append(getIndentationStr()).append("@").append(JavaCompCodeNames.KeywordOverride)
                .append(getNewline());
        result.append(getIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ")
                .append(JavaCompCodeNames.KeywordVoid).append(" ").append("execute()").append(" ")
                .append("{").append(getNewline());

        result.append(getIndentationStr()).append("}").append(getNewline());
        ///#endregion
        ///#region ExtendMethod

        javaGenerateExtendMethod(result);

        ///#endregion
        ///#region ClassEnd
        result.append(getNewline()).append("}");

        ///#endregio
        return result.toString();
    }

    ///#region 生成方法

    /**
     * 生成Using代码
     */

    private void generateImport(StringBuilder result) {
        entitynameSpace = String.format("%1$s%2$s", entitynameSpace, ".*");
        result.append(getImportStr(entitynameSpace));
        apinameSpace = String.format("%1$s%2$s", apinameSpace, ".*");
        result.append(getImportStr(apinameSpace));
        javaGenerateExtendUsing(result);
    }

    protected abstract void javaGenerateExtendUsing(StringBuilder result);

    protected abstract void javaGenerateConstructor(StringBuilder result);

    protected void javaGenerateExtendMethod(StringBuilder result) {

    }

    protected void javaGenerateField(StringBuilder result) {

    }

    ///#endregion
    ///#region 通用方法
    protected final String getImportStr(String value) {
        return new StringBuilder(JavaCompCodeNames.KeywordImport).append(" ").append(value).append(";")
                .append(getNewline()).toString();
    }

    protected final String getNewline() {
        return "\r\n";
    }

    /**
     * 缩进
     */
    protected final String getIndentationStr() {
        return "\t";
    }

    /**
     * 双缩进
     */
    protected final String getDoubleIndentationStr() {
        return "\t\t";
    }

    ///#endregion

    protected abstract String getInitializeCompName();
}