package com.inspur.edp.udt.designtime.manager.config;

import com.inspur.edp.metadata.rtcustomization.spi.ConfigDataCollectSpi;
import com.inspur.edp.metadata.rtcustomization.spi.ConfigDataCopySpi;
import com.inspur.edp.metadata.rtcustomization.spi.ConfigDataSaveSpi;
import com.inspur.edp.udt.designtime.manager.copy.BusinessFieldConfigDataCollectImpl;
import com.inspur.edp.udt.designtime.manager.copy.BusinessFieldConfigDataCopyImpl;
import com.inspur.edp.udt.designtime.manager.copy.BusinessFieldConfigDataSaveImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BusinessFieldAutoConfig {
    @Bean
    public ConfigDataCollectSpi getBusinessFieldConfigDataCollectImpl() {
        return new BusinessFieldConfigDataCollectImpl();
    }

    @Bean
    public ConfigDataCopySpi getBusinessFieldConfigDataCopyImpl() {
        return new BusinessFieldConfigDataCopyImpl();
    }

    @Bean
    public ConfigDataSaveSpi getConfigDataSaveSpi() {
        return new BusinessFieldConfigDataSaveImpl();
    }
}
