package com.inspur.edp.udt.designtime.manager.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationCollection;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationKeyCollection;
import com.inspur.edp.cef.designtime.api.collection.GspFieldCollection;
import com.inspur.edp.cef.designtime.api.element.AssoModelInfo;
import com.inspur.edp.cef.designtime.api.element.EnumIndexType;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.element.GspAssociationKey;
import com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
import com.inspur.edp.cef.designtime.api.entity.GspCommonField;
import com.inspur.edp.cef.spi.jsonser.base.StringUtils;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.MetadataRTService;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.exception.UdtModelErrorCodeEnum;
import com.inspur.edp.udt.designtime.api.exception.UdtModelException;
import com.inspur.edp.udt.designtime.api.extension.BaseUdtExtension;
import com.inspur.edp.udt.designtime.api.extension.BaseUdtExtensionDeserializer;
import com.inspur.edp.udt.designtime.api.extension.UdtExtensionConfig;
import com.inspur.edp.udt.designtime.api.extension.UdtExtensionConfigs;
import com.inspur.edp.udt.designtime.api.nocode.BusinessField;
import com.inspur.edp.udt.designtime.api.nocode.FiledAssoInfo;
import com.inspur.edp.udt.designtime.api.nocode.IBusinessFieldService;
import com.inspur.edp.udt.designtime.api.nocode.RefField;
import com.inspur.edp.udt.designtime.manager.ContentSerializer;
import com.inspur.edp.udt.designtime.manager.repository.BusinessFieldRepository;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class BusinessFieldService implements IBusinessFieldService {
    private final BusinessFieldRepository repository;

    public BusinessFieldService(BusinessFieldRepository businessFieldRepository) {
        this.repository = businessFieldRepository;
    }

    @Override
    public List<BusinessField> getBusinessFields() {
        List<BusinessField> businessFields = this.repository.findAll();
        return businessFields;
    }

    @Override
    public List<BusinessField> getBusinessFieldsByCategoryId(String categoryId) {
        List<BusinessField> businessFields = this.repository.findByCategoryId(categoryId);
        if (businessFields != null && !businessFields.isEmpty()) {
            for (BusinessField businessField : businessFields) {
                businessField.setAssoInfo(new FiledAssoInfo());
            }
        }
        return businessFields;
    }

    @Override
    public BusinessField getBusinessField(String id) {
        BusinessField businessField = null;
        businessField = this.repository.findById(id).orElse(null);
        if (businessField != null) {
            getUdt(businessField);
            Map<String, String> map = new HashMap<>();
            businessField.setUdtExtensions(map);
        }
        return businessField;
    }

    public void saveBusinessField(BusinessField businessField) {
        BusinessField temp = null;
        if (businessField.getId() == null || businessField.getId().isEmpty()) {
            businessField.setId(UUID.randomUUID().toString());
        } else {
            temp = getBusinessField(businessField.getId());
        }
        businessField.setUnifiedDataTypeDef(getSimpleAssoUDT(businessField, "Association"));
        ContentSerializer contentSerializer = new ContentSerializer();
        businessField.setLastChangeBy(CAFContext.current.getUserId());
        Date lastChangedOn = new Date();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        String format = sf.format(lastChangedOn);
        try {
            lastChangedOn = sf.parse(format);
        } catch (ParseException e) {
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, format);
        }
        businessField.setLastChangeOn(lastChangedOn);
        //复制的时候，Content数据是有的
        if (StringUtils.isNullOrEmpty(businessField.getContent())) {
            businessField.setContent(contentSerializer.Serialize(businessField.getUnifiedDataTypeDef()).toString());
        }
        this.repository.save(businessField);
    }

    private UnifiedDataTypeDef getSimpleAssoUDT(BusinessField businessField, String type) {
        SimpleDataTypeDef udt = new SimpleDataTypeDef();
        //有可能是复制过来的
        if (businessField.getUnifiedDataTypeDef() == null && !StringUtils.isNullOrEmpty(businessField.getContent())) {
            getUdt(businessField);
        }
        handleBasicUdtInfo(udt, businessField);
        //每次都是重新搞
        //应用复制过来的，会已经复制到UDT上了
        if (businessField.getUdtExtensions() != null && !businessField.getUdtExtensions().isEmpty()) {
            for (Map.Entry<String, String> entry : businessField.getUdtExtensions().entrySet()) {
                String extendType = entry.getKey();
                UdtExtensionConfig config = UdtExtensionConfigs.getInstance().getExtensionConfig(extendType);
                try {
                    BaseUdtExtensionDeserializer deserializer = (BaseUdtExtensionDeserializer) Class.forName(config.getDeserclass()).newInstance();
                    ObjectMapper mapper = new ObjectMapper();
                    SimpleModule module = new SimpleModule();
                    module.addDeserializer(BaseUdtExtension.class, deserializer);

                    mapper.registerModule(module);
                    udt.getUdtExtensions().put(extendType, mapper.readValue(entry.getValue(), BaseUdtExtension.class));
                } catch (Exception e) {
                    throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, "BaseUdtExtension");
                }
            }
        }

        if (type.equals("Association")) {
            handleAssociationInfo(udt, businessField);
        } else if (type.equals("Enum")) {

        } else if (type.equals("Normal")) {
            handleEnumInfo(udt, businessField);
        }
        return udt;
    }

    private void getUdt(BusinessField businessField) {
        ContentSerializer contentSerializer = new ContentSerializer();
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode;
        try {
            jsonNode = objectMapper.readTree(businessField.getContent());
        } catch (JsonProcessingException e) {
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_JSON_0001, "businessField");
        }
        businessField.setUnifiedDataTypeDef((UnifiedDataTypeDef) contentSerializer.DeSerialize(jsonNode));
        businessField.setAssoInfo(new FiledAssoInfo());
    }

    /**
     * 处理基本信息
     *
     * @param udt
     * @param businessField
     */
    private void handleBasicUdtInfo(SimpleDataTypeDef udt, BusinessField businessField) {
        udt.setID(UUID.randomUUID().toString());
        udt.setCode(businessField.getCode());
        udt.setName(businessField.getName());
        udt.setCreatedDate(Date.from(CAFContext.current.getCurrentDateTime().toInstant()));
        udt.setModifiedDate(Date.from(CAFContext.current.getCurrentDateTime().toInstant()));
        udt.setMDataType(GspElementDataType.String);
        udt.setLength(36);
        udt.setEnableRtrim(true);
        udt.setEnumIndexType(EnumIndexType.Integer);
    }

    private void handleAssociationInfo(SimpleDataTypeDef udt, BusinessField businessField) {
        if (businessField.getAssoInfo() == null || StringUtils.isNullOrEmpty(businessField.getAssoInfo().getRefModelID())) {
            return;
        }
        udt.setObjectType(GspElementObjectType.Association);
        udt.setChildAssociations(new GspAssociationCollection());
        GspAssociation gspAssociation = new GspAssociation();
        GspMetadata metadata = getMetadataService().getMetadata(businessField.getAssoInfo().getRefModelID());
        GspBusinessEntity gspBusinessEntity = (GspBusinessEntity) metadata.getContent();
        IGspCommonObject refObject = gspBusinessEntity.findObjectById(businessField.getAssoInfo().getRefObjectID());

        gspAssociation.setId(UUID.randomUUID().toString());
        gspAssociation.setRefModelID(businessField.getAssoInfo().getRefModelID());
        gspAssociation.setRefModelCode(gspBusinessEntity.getCode());
        gspAssociation.setRefModelName(gspBusinessEntity.getName());
        gspAssociation.setRefObjectCode(refObject.getCode());
        gspAssociation.setRefObjectID(refObject.getID());
        gspAssociation.setRefObjectName(refObject.getName());

        GspAssociationKeyCollection keyCollection = new GspAssociationKeyCollection();
        GspAssociationKey associationKey = new GspAssociationKey();
        associationKey.setSourceElement(gspBusinessEntity.findObjectById(gspAssociation.getRefObjectID()).getIDElement().getID());
        associationKey.setSourceElementDisplay(gspBusinessEntity.findObjectById(gspAssociation.getRefObjectID()).getIDElement().getLabelID());
        associationKey.setTargetElement(businessField.getCode());
        associationKey.setTargetElementDisplay("");
        keyCollection.addAssociation(associationKey);
        gspAssociation.setKeyCollection(keyCollection);

        gspAssociation.setRefElementCollection(new GspFieldCollection());
        if (businessField.getAssoInfo().getElements() != null && !businessField.getAssoInfo().getElements().isEmpty()) {
            for (RefField field : businessField.getAssoInfo().getElements()) {
                GspCommonField commonField = new GspCommonField();
                commonField.setID(field.getId());
                IGspCommonElement commonElement = gspBusinessEntity.findElementById(field.getRefElementId());
                commonField.setLabelID(businessField.getCode() + "_" + commonElement.getLabelID());
                commonField.setCode(commonElement.getCode());
                commonField.setName(commonElement.getName());
                commonField.setMDataType(commonElement.getMDataType());
                commonField.setLength(commonElement.getLength());
                commonField.setIsRefElement(true);
                commonField.setEnableRtrim(false);
                commonField.setRefElementId(field.getRefElementId());
                commonField.setParentAssociation(gspAssociation);
                commonField.setUdtID(commonElement.getUdtID());
                commonField.setUdtName(commonElement.getUdtName());
                commonField.setIsUdt(commonElement.getIsUdt());
                if (commonField.getIsUdt()) {//带出字段设置一下
                    if (commonElement.getChildElements() != null && !commonElement.getChildElements().isEmpty()) {
                        for (IGspCommonField childField : commonElement.getChildElements()) {
                            GspBizEntityElement commonChildField = new GspBizEntityElement();
                            commonChildField.setID(childField.getID());
                            commonChildField.setLabelID(childField.getLabelID());
                            commonChildField.setIsUdt(childField.getIsUdt());
                            commonChildField.setCode(childField.getCode());
                            commonChildField.setName(childField.getName());
                            if (childField instanceof GspBizEntityElement) {
                                commonChildField.setColumnID(((GspBizEntityElement) childField).getColumnID());
                            }
                            commonChildField.setEnableRtrim(childField.isEnableRtrim());
                            commonChildField.setMDataType(childField.getMDataType());
                            commonChildField.setLength(childField.getLength());
                            commonField.getChildElements().add(commonChildField);
                        }
                    }
                }
                gspAssociation.getRefElementCollection().add(commonField);
            }
        }
        AssoModelInfo assoModelInfo = new AssoModelInfo();
        assoModelInfo.setModelConfigId(gspBusinessEntity.getGeneratedConfigID());
        assoModelInfo.setMainObjCode(gspBusinessEntity.getMainObject().getCode());
        gspAssociation.setAssoModelInfo(assoModelInfo);
        udt.getChildAssociations().add(gspAssociation);
    }

    private void handleEnumInfo(SimpleDataTypeDef udt, BusinessField businessField) {
        udt.setObjectType(GspElementObjectType.Enum);
        udt.setChildAssociations(new GspAssociationCollection());
        GspAssociation gspAssociation = new GspAssociation();
        GspMetadata metadata = getMetadataService().getMetadata(businessField.getAssoInfo().getRefModelID());
        GspBusinessEntity gspBusinessEntity = (GspBusinessEntity) metadata.getContent();
        IGspCommonObject refObject = gspBusinessEntity.findObjectById(businessField.getAssoInfo().getRefObjectID());

        gspAssociation.setId(UUID.randomUUID().toString());
        gspAssociation.setRefModelID(businessField.getAssoInfo().getRefModelID());
        gspAssociation.setRefModelCode(gspBusinessEntity.getCode());
        gspAssociation.setRefModelName(gspBusinessEntity.getName());
        gspAssociation.setRefObjectCode(refObject.getCode());
        gspAssociation.setRefObjectID(refObject.getID());
        gspAssociation.setRefObjectName(refObject.getName());

        GspAssociationKeyCollection keyCollection = new GspAssociationKeyCollection();
        GspAssociationKey associationKey = new GspAssociationKey();
        associationKey.setSourceElement(gspBusinessEntity.findObjectById(gspAssociation.getRefObjectID()).getIDElement().getID());
        associationKey.setSourceElementDisplay(gspBusinessEntity.findObjectById(gspAssociation.getRefObjectID()).getIDElement().getLabelID());
        associationKey.setTargetElement(businessField.getCode());
        associationKey.setTargetElementDisplay("");
        keyCollection.addAssociation(associationKey);
        gspAssociation.setKeyCollection(keyCollection);

        gspAssociation.setRefElementCollection(new GspFieldCollection());
        if (businessField.getAssoInfo().getElements() != null && !businessField.getAssoInfo().getElements().isEmpty()) {
            for (RefField field : businessField.getAssoInfo().getElements()) {
                GspCommonField commonField = new GspCommonField();
                commonField.setID(field.getId());
                IGspCommonElement commonElement = gspBusinessEntity.findElementById(field.getRefElementId());
                commonField.setLabelID(businessField.getCode() + "_" + commonElement.getLabelID());
                commonField.setCode(commonElement.getCode());
                commonField.setName(commonElement.getName());
                commonField.setMDataType(commonElement.getMDataType());
                commonField.setLength(commonElement.getLength());
                commonField.setIsRefElement(true);
                commonField.setEnableRtrim(false);
                commonField.setRefElementId(field.getRefElementId());
                commonField.setParentAssociation(gspAssociation);
                gspAssociation.getRefElementCollection().add(commonField);
            }
        }
        AssoModelInfo assoModelInfo = new AssoModelInfo();
        assoModelInfo.setModelConfigId(gspBusinessEntity.getGeneratedConfigID());
        assoModelInfo.setMainObjCode(gspBusinessEntity.getMainObject().getCode());
        gspAssociation.setAssoModelInfo(assoModelInfo);
        udt.getChildAssociations().add(gspAssociation);
    }

    private com.inspur.edp.lcm.metadata.api.service.MetadataRTService metadataService;

    private MetadataRTService getMetadataService() {
        if (metadataService == null)
            metadataService = SpringBeanUtils.getBean(MetadataRTService.class);
        return metadataService;
    }

    public JsonNode getNodeFromJson(String json) {

        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = null;

        try {
            node = mapper.readTree(json);
        } catch (JsonProcessingException e) {
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_JSON_0003, e);
        }
        return node;
    }
}
