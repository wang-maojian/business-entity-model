package com.inspur.gsp.commonmodel.api.typeinfos;

public final class MediateType implements Cloneable {
    public MediateType(String nameSpace, String className) {
        setClassName(className);
        setNamespace(nameSpace);
    }

    private String privateNamespace;

    public String getNamespace() {
        return privateNamespace;
    }

    public void setNamespace(String value) {
        privateNamespace = value;
    }

    private String privateClassName;

    public String getClassName() {
        return privateClassName;
    }

    public void setClassName(String value) {
        privateClassName = value;
    }

    public String getFullName() {
        return String.format("%1$s.%2$s", getNamespace(), getClassName());
    }

    public MediateType clone() {
        try {
            return (MediateType) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}