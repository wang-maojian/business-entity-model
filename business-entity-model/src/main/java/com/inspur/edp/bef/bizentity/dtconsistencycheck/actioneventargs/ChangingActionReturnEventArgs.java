package com.inspur.edp.bef.bizentity.dtconsistencycheck.actioneventargs;

public class ChangingActionReturnEventArgs extends AbstractMgrActionEventArgs {

    protected String newReturnInfo;
    protected String orignalReturnInfo;

    public ChangingActionReturnEventArgs() {

    }

    public ChangingActionReturnEventArgs(String beId, String actionId, String metadataPath,
                                         String newReturnInfo, String orignalReturnInfo) {
        super(beId, actionId, metadataPath);
        this.newReturnInfo = newReturnInfo;
        this.orignalReturnInfo = orignalReturnInfo;
    }

    public String getNewReturnInfo() {
        return newReturnInfo;
    }

    public void setNewReturnInfo(String newReturnInfo) {
        this.newReturnInfo = newReturnInfo;
    }

    public String getOrignalReturnInfo() {
        return orignalReturnInfo;
    }

    public void setOrignalReturnInfo(String orignalReturnInfo) {
        this.orignalReturnInfo = orignalReturnInfo;
    }
}
