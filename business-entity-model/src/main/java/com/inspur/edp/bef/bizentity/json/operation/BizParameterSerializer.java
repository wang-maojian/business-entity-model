package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;

public class BizParameterSerializer<T extends BizParameter> extends JsonSerializer<T> {

    protected boolean isFull = true;

    public BizParameterSerializer() {
    }

    public BizParameterSerializer(boolean full) {
        isFull = full;
    }

    @Override
    public void serialize(BizParameter value, JsonGenerator gen, SerializerProvider serializers) {

        WriteOperationInfo(gen, value);
    }

    private void WriteOperationInfo(JsonGenerator writer, BizParameter para) {
        //{
        SerializerUtils.writeStartObject(writer);
        SerializerUtils.writePropertyValue(writer, CommonModelNames.ID, para.getID());
        if (isFull || para.getParamCode() != null && !"".equals(para.getParamCode()))
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ParamCode, para.getParamCode());
        if (isFull || para.getParamName() != null && !"".equals(para.getParamName()))
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ParamName, para.getParamName());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ParameterType, para.getParameterType().getValue());
        if (isFull || para.getCollectionParameterType().getValue() != 0) {
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.CollectionParameterType, para.getCollectionParameterType().getValue());
        }
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.Assembly, para.getAssembly());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ClassName, para.getNetClassName());
//        if(isFull||para.getClassName()!=null&&!"".equals(para.getClassName()))
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.JavaClassName, para.getClassName());
        if (isFull || para.getMode().getValue() != 0) {
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.Mode, para.getMode().getValue());
        }
        if (isFull || para.getParamDescription() != null && !"".equals(para.getParamDescription())) {
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ParamDescription, para.getParamDescription());
        }
        if (isFull || para.getActualValue() != null) {
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ParamActualValue, para.getActualValue());
        }
        if (isFull || para.isVoidReturnType())
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.IsVoidReturnType, para.isVoidReturnType());

        SerializerUtils.writeEndObject(writer);
    }
}
