package com.inspur.edp.bef.bizentity.operation.collection;

import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.BizOperationCollection;
import com.inspur.edp.bef.bizentity.operation.Validation;

public class ValidationCollection extends BizOperationCollection {
    private static final long serialVersionUID = 1L;

    public final boolean add(BizOperation operation) {
        if (operation instanceof Validation) {
            operation.setOwner(getOwner());
            return super.add(operation);
        }
        throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0082, "Validation");
    }

    // /**
    // 克隆BE Validations动作集合

    // @param isGenerateId 是否生成Id
    // @return 返回动作集合
    // */
    // //C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing
    // via the 'new' keyword:
    // //ORIGINAL LINE: public new object clone(bool isGenerateId)
    // public final Object clone(boolean isGenerateId)
    // {
    // ValidationCollection col = new ValidationCollection();
    // //C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods
    // are not converted by C# to Java Converter:
    // col.addRange(this.select(item { return
    // (Validation)item.clone(isGenerateId)));
    // return col;
    // }

    // /**
    // 克隆BE Validations动作集合

    // @return 返回动作集合
    // */
    // //C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing
    // via the 'new' keyword:
    // //ORIGINAL LINE: public new object clone()
    // public final Object clone()
    // {
    // return clone(false);
    // }

    @Override
    public ValidationCollection clone() {
        return (ValidationCollection) super.clone();
    }

    @Override
    protected Validation convertOperation(BizOperation op) {
        return (Validation) super.convertOperation(op);
    }

    @Override
    protected ValidationCollection createOperationCollection() {
        return new ValidationCollection();
    }
}