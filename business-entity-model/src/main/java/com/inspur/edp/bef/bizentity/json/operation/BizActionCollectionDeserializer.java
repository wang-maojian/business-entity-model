package com.inspur.edp.bef.bizentity.json.operation;

import com.inspur.edp.bef.bizentity.common.InternalActionUtil;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.collection.BizActionCollection;

public class BizActionCollectionDeserializer extends BizOperationCollectionDeserializer<BizActionCollection> {
    @Override
    protected BizActionCollection createCollection() {
        return new BizActionCollection();
    }

    @Override
    protected BizActionDeserializer createDeserializer() {
        return new BizActionDeserializer();
    }

    @Override
    protected void addBizOp(BizActionCollection collection, BizOperation op) {
        if (!InternalActionUtil.InternalBeActionIDs.contains(op.getID())) {
            super.addBizOp(collection, op);
        }
    }
}
