package com.inspur.edp.bef.bizentity.increment.extract.determination;

import com.inspur.edp.bef.bizentity.increment.entity.determination.AddedDtmIncrement;
import com.inspur.edp.bef.bizentity.operation.Determination;

public class AddedDtmExtractor {

    public AddedDtmIncrement extract(Determination voAction) {
        AddedDtmIncrement addedEntityIncrement = this.createIncrement();
        addedEntityIncrement.setAction(voAction);
        return addedEntityIncrement;
    }

    protected AddedDtmIncrement createIncrement() {
        return new AddedDtmIncrement();
    }
}

