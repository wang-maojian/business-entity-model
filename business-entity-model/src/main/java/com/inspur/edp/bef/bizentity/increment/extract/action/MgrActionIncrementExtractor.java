package com.inspur.edp.bef.bizentity.increment.extract.action;

import com.inspur.edp.bef.bizentity.increment.entity.action.MgrActionIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.action.ModifyMgrActionIncrement;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.cef.designtime.api.increment.extractor.AbstractIncrementExtractor;
import com.inspur.edp.das.commonmodel.controlrule.CmControlRule;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmControlRuleDef;

public class MgrActionIncrementExtractor extends AbstractIncrementExtractor {

    public MgrActionIncrement extractorIncrement(BizMgrAction oldAction, BizMgrAction newAction, CmControlRule rule, CmControlRuleDef def) {

        if (oldAction == null && newAction == null) {
            return null;
        } else if (oldAction == null) {
            return this.getAddedActionExtractor().extract(newAction);
        } else if (newAction == null) {
            return null;
//            return (CommonEntityIncrement)(oldHelpConfig != null && newAction == null ? this.getDeletedEntityExtractor().extract(oldHelpConfig) : this.getModifyEntityExtractor().extract(oldHelpConfig, newAction, rule, def));
        } else {
            ModifyMgrActionIncrement increment = new ModifyMgrActionIncrement();
            increment.setActionId(newAction.getID());
            increment.setAction(newAction);
            return increment;
        }
    }

    protected AddedMgrActionExtractor getAddedActionExtractor() {
        return new AddedMgrActionExtractor();
    }

}
