package com.inspur.edp.bef.bizentity.pushchangesetargs;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;

import java.util.Map;

public class ObjectChangeDetail {
    private String bizObjectId;
    private String bizObjectCode;
    private Boolean isNecessary;
    private GspBizEntityObject bizObject;
    private Map<String, Object> changeInfo;
    private String parentObjIDElementId;

    public String getParentObjIDElementId() {
        return parentObjIDElementId;
    }

    public void setParentObjIDElementId(String parentObjIDElementId) {
        this.parentObjIDElementId = parentObjIDElementId;
    }


    public ObjectChangeDetail() {
    }

    public String getBizObjectId() {
        return bizObjectId;
    }

    public void setBizObjectId(String bizObjectId) {
        this.bizObjectId = bizObjectId;
    }

    public String getBizObjectCode() {
        return bizObjectCode;
    }

    public void setBizObjectCode(String bizObjectCode) {
        this.bizObjectCode = bizObjectCode;
    }

    public Boolean getNecessary() {
        return isNecessary;
    }

    public void setNecessary(Boolean necessary) {
        isNecessary = necessary;
    }

    public GspBizEntityObject getBizObject() {
        return bizObject;
    }

    public void setBizObject(GspBizEntityObject bizObject) {
        this.bizObject = bizObject;
    }

    public Map<String, Object> getChangeInfo() {
        return changeInfo;
    }

    public void setChangeInfo(Map<String, Object> changeInfo) {
        this.changeInfo = changeInfo;
    }
}
