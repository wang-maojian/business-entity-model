package com.inspur.edp.bef.bizentity.operation.componentbase;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BizParActualValue {
    private String value = "";

    @JsonProperty("Value")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private boolean hasValue = false;

    @JsonProperty("HasValue")
    @Deprecated
    public boolean getHasValue() {
        if (hasValue && "".equals(value))
            hasValue = false;
        return hasValue;
    }

    @Deprecated
    public void setHasValue(boolean value) {
        hasValue = value;
    }

    private boolean enable = false;

    @JsonProperty("Enable")
    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    private BizParActualValueType valueType = BizParActualValueType.Constant;

    @JsonProperty("ValueType")
    public BizParActualValueType getValueType() {
        return valueType;
    }

    public void setValueType(BizParActualValueType value) {
        this.valueType = value;
    }

}
