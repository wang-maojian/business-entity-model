package com.inspur.edp.bef.bizentity.dtconsistencycheck.fieldeventargs;


import com.inspur.edp.cef.designtime.api.dtconsistencycheck.AbstractDtEventArgs;

public abstract class AbstractBeFieldEventArgs extends AbstractDtEventArgs {

    protected String beId;
    protected String entityId;
    protected String fieldId;
    protected String metadataPath;

    public AbstractBeFieldEventArgs() {

    }

    public AbstractBeFieldEventArgs(String beId, String entityId, String fieldId) {
        this.beId = beId;
        this.entityId = entityId;
        this.fieldId = fieldId;
    }

    public void setMetadataPath(String metadataPath) {
        this.metadataPath = metadataPath;
    }

    public String getMetadataPath() {
        return metadataPath;
    }

    public String getBeId() {
        return beId;
    }

    public void setBeId(String beId) {
        this.beId = beId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }
}
