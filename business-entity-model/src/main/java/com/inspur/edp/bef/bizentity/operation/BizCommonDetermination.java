package com.inspur.edp.bef.bizentity.operation;

import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;

public class BizCommonDetermination extends CommonDetermination {
    private BizParameterCollection parameterCollection;

    public BizParameterCollection getParameterCollection() {
        return parameterCollection;
    }

    public void setParameterCollection(BizParameterCollection parameterCollection) {
        this.parameterCollection = parameterCollection;
    }

    private boolean runOnce;

    public boolean getRunOnce() {
        return runOnce;
    }

    public void setRunOnce(boolean runOnce) {
        this.runOnce = runOnce;
    }
}
