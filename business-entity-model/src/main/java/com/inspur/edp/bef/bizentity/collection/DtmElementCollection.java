package com.inspur.edp.bef.bizentity.collection;

import com.inspur.edp.cef.designtime.api.collection.BaseList;

import java.util.ArrayList;

/**
 * 联动规则过滤字段集合
 */
public class DtmElementCollection extends BaseList<String> implements Cloneable {
    private static final long serialVersionUID = 1L;

    /**
     * 是否包含指定字段ID集合中的任意字段
     *
     * @param elementLabelIDs 指定的字段LabelID集合
     */
    public final boolean ContainsAny(ArrayList<String> elementLabelIDs) {
        if (this.size() == 0) {
            return true;
        }
        if (elementLabelIDs == null || elementLabelIDs.isEmpty()) {
            return false;
        }
        for (String var : elementLabelIDs) {
            if (!this.contains(var)) {
                return false;
            }
        }
        return true;
    }
}