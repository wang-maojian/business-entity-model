package com.inspur.edp.bef.bizentity.json.operation;

import com.inspur.edp.bef.bizentity.operation.collection.DeterminationCollection;

public class DtmCollectionSerializer extends BizOperationCollectionSerializer<DeterminationCollection> {
    public DtmCollectionSerializer() {
    }

    public DtmCollectionSerializer(boolean full) {
        super(full);
        isFull = full;
    }

    @Override
    protected BizOperationSerializer getConvertor() {
        return new BizDeterminationSerializer(isFull);
    }

}
