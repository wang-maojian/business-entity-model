package com.inspur.edp.bef.bizentity.increment.entity.determination;

import com.inspur.edp.bef.bizentity.operation.Determination;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;

public class AddedDtmIncrement extends DtmIncrement {

    private Determination action;

    @Override
    public IncrementType getIncrementType() {
        return IncrementType.Added;
    }

    public Determination getAction() {
        return action;
    }

    public void setAction(Determination action) {
        this.action = action;
    }
}
