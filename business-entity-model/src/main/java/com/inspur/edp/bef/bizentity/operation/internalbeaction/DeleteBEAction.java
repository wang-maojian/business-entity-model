package com.inspur.edp.bef.bizentity.operation.internalbeaction;

import com.inspur.edp.bef.bizentity.operation.BizAction;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;

/**
 * 内置Delete操作
 */
public class DeleteBEAction extends BizAction implements IInternalBEAction {
    public static final String id = "aD73GrGaKEiA7N9jsPxIOQ";
    public static final String code = "Delete";
    public static final String name = MessageI18nUtils.getMessage("Delete");

    public DeleteBEAction() {
        setID(id);
        setCode(code);
        setName(name);
    }
}