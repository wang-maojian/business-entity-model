package com.inspur.edp.bef.bizentity.operation.bemgrcomponent;

import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;

public class BizMgrActionParamCollection extends BizParameterCollection<BizMgrActionParameter> {
    public BizMgrActionParamCollection() {
    }

}