package com.inspur.edp.bef.bizentity.i18n;


import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.cef.designtime.api.i18n.extractor.AssoResourceExtractor;
import com.inspur.edp.das.commonmodel.i18n.GspAssoRefEleResourceExtractor;

public class BizAssoRefEleResourceExtractor extends GspAssoRefEleResourceExtractor {
    public BizAssoRefEleResourceExtractor(GspBizEntityElement field, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(field, context, parentResourceInfo);
    }

    @Override
    protected final AssoResourceExtractor getAssoResourceExtractor(ICefResourceExtractContext context, CefResourcePrefixInfo fieldPrefixInfo, GspAssociation asso) {
        return new BizAssoResourceExtractor(asso, getContext(), fieldPrefixInfo);
    }
}