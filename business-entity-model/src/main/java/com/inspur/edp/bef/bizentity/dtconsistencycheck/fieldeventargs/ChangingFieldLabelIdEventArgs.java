package com.inspur.edp.bef.bizentity.dtconsistencycheck.fieldeventargs;

public class ChangingFieldLabelIdEventArgs extends AbstractBeFieldEventArgs {

    protected String newValue;
    protected String originalValue;

    public ChangingFieldLabelIdEventArgs() {

    }


    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public void setOriginalValue(String originalValue) {
        this.originalValue = originalValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public String getOriginalValue() {
        return originalValue;
    }

    public ChangingFieldLabelIdEventArgs(String beId, String entityId, String fieldId,
                                         String newValue, String originalValue) {
        super(beId, entityId, fieldId);
        this.newValue = newValue;
        this.originalValue = originalValue;
    }

    public ChangingFieldLabelIdEventArgs(String beId, String entityId, String fieldId) {
        super(beId, entityId, fieldId);
    }
}
