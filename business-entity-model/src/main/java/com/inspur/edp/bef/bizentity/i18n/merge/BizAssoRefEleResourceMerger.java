package com.inspur.edp.bef.bizentity.i18n.merge;

import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.cef.designtime.api.i18n.merger.AssoResourceMerger;
import com.inspur.edp.das.commonmodel.i18n.merge.GspAssoRefEleResourceMerger;

public class BizAssoRefEleResourceMerger extends GspAssoRefEleResourceMerger {
    public BizAssoRefEleResourceMerger(GspBizEntityElement element, ICefResourceMergeContext context) {
        super(element, context);
    }

    @Override
    protected AssoResourceMerger getAssoResourceMerger(ICefResourceMergeContext context, GspAssociation asso) {
        return new BizAssoResourceMerger(asso, getContext());
    }
}
