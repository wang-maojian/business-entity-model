package com.inspur.edp.bef.bizentity.dtconsistencycheck.actioneventargs;

public class ChangingActionCodeEventArgs extends AbstractMgrActionEventArgs {

    protected String newCode;
    protected String originalCode;

    public ChangingActionCodeEventArgs(String beId, String actionId, String metadataPath,
                                       String newCode, String originalCode) {
        super(beId, actionId, metadataPath);
        this.newCode = newCode;
        this.originalCode = originalCode;
    }

    public ChangingActionCodeEventArgs() {

    }

    public String getNewCode() {
        return newCode;
    }

    public void setNewCode(String newCode) {
        this.newCode = newCode;
    }

    public String getOriginalCode() {
        return originalCode;
    }

    public void setOriginalCode(String originalCode) {
        this.originalCode = originalCode;
    }
}
