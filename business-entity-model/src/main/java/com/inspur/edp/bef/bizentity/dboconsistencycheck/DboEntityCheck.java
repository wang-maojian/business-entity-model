package com.inspur.edp.bef.bizentity.dboconsistencycheck;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import org.springframework.util.StringUtils;

public class DboEntityCheck {
    private GspBusinessEntity bizEntity;
    private StringBuffer expMessage;

    public DboEntityCheck(GspBusinessEntity bizEntity) {
        this.bizEntity = bizEntity;
        this.expMessage = new StringBuffer();
    }

    public String checkEntityCoincidence() {
        if (this.bizEntity.getIsVirtual()) {
            return null;
        }

        checkObjectCoincidence(bizEntity.getMainObject());
        for (IGspCommonObject obj : bizEntity.getMainObject().getContainChildObjects()) {
            checkObjectCoincidence((GspBizEntityObject) obj);
        }
        return this.expMessage.toString();
    }

    private void checkObjectCoincidence(GspBizEntityObject obj) {
        DboObjectCheck checker = new DboObjectCheck(bizEntity, obj);
        String returnStr = checker.checkObjCoincidence();
        if (!StringUtils.isEmpty(returnStr)) {
            this.expMessage.append(returnStr);
        }
    }

}
