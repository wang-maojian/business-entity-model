package com.inspur.edp.bef.bizentity.beenum;

/**
 * 触发时机，供Validation、Determination共用。
 */
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//[Flags]
public enum RequestNodeTriggerType {
    /**
     * 不执行
     */
    None(0),

    /**
     * 实例创建时执行
     */
    Created(1),

    /**
     * 实例更新时执行
     */
    Updated(2),

    /**
     * 实例删除时执行
     */
    Deleted(4),

    /**
     * 校验或者自动计算时(分别用于Validation、Determination)
     */
    ValidateOrDetermine(8),

    /**
     * 数据加载时，Determination专用。
     */
    Load(16);

    private final int intValue;
    private static java.util.HashMap<Integer, RequestNodeTriggerType> mappings;

    private synchronized static java.util.HashMap<Integer, RequestNodeTriggerType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, RequestNodeTriggerType>();
        }
        return mappings;
    }

    private RequestNodeTriggerType(int value) {
        intValue = value;
        RequestNodeTriggerType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static RequestNodeTriggerType forValue(int value) {
        return getMappings().get(value);
    }
}