package com.inspur.edp.bef.bizentity.dtconsistencycheck.actioneventargs;


import com.inspur.edp.cef.designtime.api.dtconsistencycheck.AbstractDtEventArgs;

public abstract class AbstractMgrActionEventArgs extends AbstractDtEventArgs {

    public AbstractMgrActionEventArgs() {
    }

    protected String beId;
    protected String actionId;
    protected String metadataPath;

    public AbstractMgrActionEventArgs(String beId, String actionId, String metadataPath) {
        this.beId = beId;
        this.actionId = actionId;
        this.metadataPath = metadataPath;
    }

    public String getBeId() {
        return beId;
    }

    public void setBeId(String beId) {
        this.beId = beId;
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String getMetadataPath() {
        return metadataPath;
    }

    public void setMetadataPath(String metadataPath) {
        this.metadataPath = metadataPath;
    }
}
