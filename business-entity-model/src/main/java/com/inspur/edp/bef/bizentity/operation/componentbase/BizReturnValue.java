package com.inspur.edp.bef.bizentity.operation.componentbase;

import com.inspur.edp.bef.bizentity.operation.componentenum.BizParameterMode;

/**
 * 返回值
 */
public class BizReturnValue extends BizParameter {
    /**
     * 参数名
     */
    @Override
    public String getParamName() {
        return null;
    }

    @Override
    public void setParamName(String value) {
    }

    /**
     * 参数模式
     */
    @Override
    public BizParameterMode getMode() {
        return BizParameterMode.OUT;
    }

    @Override
    public void setMode(BizParameterMode value) {
    }
}