package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.BizActionBase;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.bemgrcomponent.BizMgrActionParamCollection;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

public class BizMgrActionDeserializer extends BizActionBaseDeserializer {
    @Override
    protected final boolean readExtendActionProperty(JsonParser jsonParser, BizOperation op, String propName) {
        BizMgrAction action = (BizMgrAction) op;
        boolean hasProperty = true;
        switch (propName) {
            case BizEntityJsonConst.FuncOperationID:
                action.setFuncOperationID(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.FuncOperationName:
                action.setFuncOperationName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            default:
                hasProperty = false;
                break;
        }
        return hasProperty;
    }

    @Override
    protected BizActionBase createBizActionBase() {
        return new BizMgrAction();
    }

    @Override
    protected BizParaDeserializer createPrapConvertor() {
        return new BizMgrActionParaDeserializer();
    }

    @Override
    protected BizParameterCollection createPrapCollection() {
        return new BizMgrActionParamCollection();
    }
}
