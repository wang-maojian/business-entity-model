package com.inspur.edp.bef.bizentity.increment;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.bef.bizentity.increment.entity.action.MgrActionIncrement;
import com.inspur.edp.bef.bizentity.json.increment.BizEntityIncrementDeserializer;
import com.inspur.edp.bef.bizentity.json.increment.BizEntityIncrementSerializer;
import com.inspur.edp.cef.designtime.api.entity.increment.ModifyEntityIncrement;
import com.inspur.edp.das.commonmodel.entity.increment.CommonModelIncrement;

import java.util.HashMap;

@JsonSerialize(using = BizEntityIncrementSerializer.class)
@JsonDeserialize(using = BizEntityIncrementDeserializer.class)
public class BizEntityIncrement extends CommonModelIncrement {

    private BizObjectIncrement mainEntityIncrement = new BizObjectIncrement();
    private HashMap<String, MgrActionIncrement> actions = new HashMap<>();

    public HashMap<String, MgrActionIncrement> getActions() {
        return actions;
    }

    @Override
    public ModifyEntityIncrement getMainEntityIncrement() {
        return this.mainEntityIncrement;
    }

    @Override
    public void setMainEntityIncrement(ModifyEntityIncrement mainEntityIncrement) {
        this.mainEntityIncrement = (BizObjectIncrement) mainEntityIncrement;
    }

}
