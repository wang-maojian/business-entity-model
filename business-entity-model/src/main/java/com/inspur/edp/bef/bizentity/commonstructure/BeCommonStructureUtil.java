package com.inspur.edp.bef.bizentity.commonstructure;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.das.commonmodel.entity.GspCommonElement;
import com.inspur.edp.udt.designtime.api.commonstructure.UdtCommonStructureUtil;

public class BeCommonStructureUtil extends UdtCommonStructureUtil {
    public static BeCommonStructureUtil getInstance() {
        return new BeCommonStructureUtil();
    }

    @Override
    protected String getFieldEnumTypeName(IGspCommonField field) {
        return ((GspCommonElement) field).getEnumTypeName();
    }

    @Override
    protected String getAssociationTypeName(IGspCommonField field) {
        return ((GspCommonElement) field).getAssociationTypeName();
    }
}
