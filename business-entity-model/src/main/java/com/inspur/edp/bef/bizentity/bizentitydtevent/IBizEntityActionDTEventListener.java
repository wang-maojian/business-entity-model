package com.inspur.edp.bef.bizentity.bizentitydtevent;


import com.inspur.edp.bef.bizentity.dtconsistencycheck.actioneventargs.DeletingActionEventArgs;
import io.iec.edp.caf.commons.event.IEventListener;

public interface IBizEntityActionDTEventListener extends IEventListener {

    public DeletingActionEventArgs deletingAction(DeletingActionEventArgs args);
}
