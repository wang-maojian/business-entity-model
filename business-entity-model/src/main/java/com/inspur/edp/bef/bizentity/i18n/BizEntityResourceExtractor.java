package com.inspur.edp.bef.bizentity.i18n;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.i18n.names.BeResourceDescriptionNames;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.das.commonmodel.IGspCommonModel;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.i18n.CommonModelResourceExtractor;
import com.inspur.edp.das.commonmodel.i18n.CommonObjectResourceExtractor;

public class BizEntityResourceExtractor extends CommonModelResourceExtractor {
    public BizEntityResourceExtractor(GspBusinessEntity model, ICefResourceExtractContext context) {
        super(model, context);
    }

    @Override
    protected final String getModelDescriptionName() {
        return BeResourceDescriptionNames.BizEntity;
    }

    @Override
    protected CommonObjectResourceExtractor getObjectResourceExtractor(ICefResourceExtractContext context, CefResourcePrefixInfo modelPrefixInfo, IGspCommonObject obj) {
        return new BizObjResourceExtractor((GspBizEntityObject) ((obj instanceof GspBizEntityObject) ? obj : null), context, modelPrefixInfo);
    }

    @Override
    protected void extractExtendProperties(IGspCommonModel model) {

    }
}