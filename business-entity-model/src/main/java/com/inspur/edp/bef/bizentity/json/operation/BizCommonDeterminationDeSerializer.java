package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.BizCommonDetermination;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.operation.CommonDtmDeserializer;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;

public class BizCommonDeterminationDeSerializer extends CommonDtmDeserializer {

    protected CommonOperation CreateCommonOp() {
        return new BizCommonDetermination();
    }

    @Override
    protected boolean readExtendDtmProp(CommonOperation op, String propName, JsonParser jsonParser) {
        boolean result = false;
        BizCommonDetermination dtm = (BizCommonDetermination) op;
        switch (propName) {
            case BizEntityJsonConst.Parameters:
                result = true;
                readParameters(jsonParser, (BizCommonDetermination) dtm);
                break;
            case BizEntityJsonConst.RunOnce: {
                result = true;
                dtm.setRunOnce(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            }
        }
        return result;
    }

    private void readParameters(JsonParser jsonParser, BizCommonDetermination action) {
        BizParaDeserializer paraDeserializer = new BizActionParaDeserializer();
        BizParameterCollection<BizParameter> collection = new BizParameterCollection<>();
        SerializerUtils.readArray(jsonParser, paraDeserializer, collection);
        action.setParameterCollection(collection);
    }
}
