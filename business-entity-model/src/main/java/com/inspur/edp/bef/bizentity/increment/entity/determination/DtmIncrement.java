package com.inspur.edp.bef.bizentity.increment.entity.determination;

import com.inspur.edp.cef.designtime.api.increment.AbstractIncrement;

public abstract class DtmIncrement extends AbstractIncrement {

    private String actionId;

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

}
