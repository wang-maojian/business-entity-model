package com.inspur.edp.bef.bizentity.dtconsistencycheck.actioneventargs;

public class DeletingActionEventArgs extends AbstractMgrActionEventArgs {

    public DeletingActionEventArgs() {

    }

    public DeletingActionEventArgs(String beId, String actionId, String metadataPath) {
        super(beId, actionId, metadataPath);
    }
}
