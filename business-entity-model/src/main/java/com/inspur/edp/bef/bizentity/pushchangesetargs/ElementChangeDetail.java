package com.inspur.edp.bef.bizentity.pushchangesetargs;

import com.inspur.edp.bef.bizentity.GspBizEntityElement;

import java.util.Map;

public class ElementChangeDetail {
    public ElementChangeDetail() {
    }

    private String bizObjectId;
    private String bizObjectCode;
    private String bizObjectName;
    private String bizElementId;
    private String bizElementCode;
    private String bizElementName;

    private boolean isNecessary;

    private Map<String, Object> changeInfo;
    private GspBizEntityElement bizElement;

    public String getBizObjectId() {
        return bizObjectId;
    }

    public void setBizObjectId(String bizObjectId) {
        this.bizObjectId = bizObjectId;
    }

    public String getBizObjectCode() {
        return bizObjectCode;
    }

    public void setBizObjectCode(String bizObjectCode) {
        this.bizObjectCode = bizObjectCode;
    }

    public String getBizObjectName() {
        return bizObjectName;
    }

    public void setBizObjectName(String bizObjectName) {
        this.bizObjectName = bizObjectName;
    }

    public String getBizElementId() {
        return bizElementId;
    }

    public void setBizElementId(String bizElementId) {
        this.bizElementId = bizElementId;
    }

    public String getBizElementCode() {
        return bizElementCode;
    }

    public void setBizElementCode(String bizElementCode) {
        this.bizElementCode = bizElementCode;
    }

    public String getBizElementName() {
        return bizElementName;
    }

    public void setBizElementName(String bizElementName) {
        this.bizElementName = bizElementName;
    }

    public boolean isNecessary() {
        return isNecessary;
    }

    public void setNecessary(boolean necessary) {
        isNecessary = necessary;
    }


    public GspBizEntityElement getBizElement() {
        return bizElement;
    }

    public void setBizElement(GspBizEntityElement bizElement) {
        this.bizElement = bizElement;
    }

    public Map<String, Object> getChangeInfo() {
        return changeInfo;
    }

    public void setChangeInfo(Map<String, Object> changeInfo) {
        this.changeInfo = changeInfo;
    }
}
