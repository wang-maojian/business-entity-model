package com.inspur.edp.bef.bizentity.operation.internalbeaction;

import com.inspur.edp.bef.bizentity.operation.BizAction;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;

/**
 * 内置Modify操作
 */
public class ModifyBEAction extends BizAction implements IInternalBEAction {
    public static final String id = "tf9MVINdJ0yhUrsJgfcvBA";
    public static final String code = "Modify";
    public static final String name = MessageI18nUtils.getMessage("Modify");

    public ModifyBEAction() {
        setID(id);
        setCode(code);
        setName(name);
    }
}