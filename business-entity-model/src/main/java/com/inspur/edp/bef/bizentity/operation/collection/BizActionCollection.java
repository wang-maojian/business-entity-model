package com.inspur.edp.bef.bizentity.operation.collection;

import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.operation.BizAction;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.BizOperationCollection;

public class BizActionCollection extends BizOperationCollection {
    private static final long serialVersionUID = 1L;

    public final boolean add(BizOperation operation) {
        if (operation instanceof BizAction) {
            operation.setOwner(getOwner());
            return super.add(operation);
        }
        throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0009, operation.getClass().getSimpleName());
    }

    // /**
    // 克隆BEAction动作集合

    // @param isGenerateId 是否生成Id
    // @return 返回动作集合
    // */
    // //C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing
    // via the 'new' keyword:
    // //ORIGINAL LINE: public new object clone(bool isGenerateId)
    // public final BizActionCollection clone(boolean isGenerateId)
    // {
    // BizActionCollection col = new BizActionCollection();
    // //C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods
    // are not converted by C# to Java Converter:
    // col.addRange(this.select(item { return (BizAction)item.clone(isGenerateId)));
    // return col;
    // }

    // /**
    // * 克隆BEAction动作集合
    // *
    // * @return 返回动作集合
    // */
    // // C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s
    // shadowing
    // // via the 'new' keyword:
    // // ORIGINAL LINE: public new object clone()
    // public final Object clone() {
    // return clone(false);
    // }
    @Override
    public BizActionCollection clone() {
        return (BizActionCollection) super.clone();
    }

    @Override
    protected BizAction convertOperation(BizOperation op) {
        return (BizAction) super.convertOperation(op);
    }

    @Override
    protected BizActionCollection createOperationCollection() {
        return new BizActionCollection();
    }
}