package com.inspur.edp.bef.bizentity.i18n;

import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.i18n.GspAssoRefEleResourceExtractor;
import com.inspur.edp.das.commonmodel.i18n.GspAssoResourceExtractor;

public class BizAssoResourceExtractor extends GspAssoResourceExtractor {
    public BizAssoResourceExtractor(GspAssociation asso, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(asso, context, parentResourceInfo);
    }

    @Override
    protected GspAssoRefEleResourceExtractor getAssoRefElementResourceExtractor(ICefResourceExtractContext context, CefResourcePrefixInfo assoPrefixInfo, IGspCommonElement field) {
        return new BizAssoRefEleResourceExtractor((GspBizEntityElement) ((field instanceof GspBizEntityElement) ? field : null), context, assoPrefixInfo);
    }
}