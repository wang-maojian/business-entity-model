package com.inspur.edp.bef.bizentity.pushchangesetlistener;

import com.inspur.edp.bef.bizentity.pushchangesetargs.PushChangeSetArgs;
import io.iec.edp.caf.commons.event.IEventListener;

public interface IPushChangeSetListener extends IEventListener {
    void bePushChangeSet(PushChangeSetArgs args);
}
