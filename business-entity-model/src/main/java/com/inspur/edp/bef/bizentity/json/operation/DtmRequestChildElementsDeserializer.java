package com.inspur.edp.bef.bizentity.json.operation;


import com.inspur.edp.bef.bizentity.collection.DtmElementCollection;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class DtmRequestChildElementsDeserializer extends RequestChildElementsDeserializer<DtmElementCollection> {

    @Override
    HashMap<String, DtmElementCollection> createHashMap() {
        return new LinkedHashMap<>();
    }

    @Override
    DtmElementCollection createValueType() {
        return new DtmElementCollection();
    }
}
