package com.inspur.edp.bef.bizentity.i18n;

import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.i18n.CommonElementResourceExtractor;
import com.inspur.edp.das.commonmodel.i18n.GspAssoResourceExtractor;

public class BizEleResourceExtractor extends CommonElementResourceExtractor {
    public BizEleResourceExtractor(GspBizEntityElement field, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(field, context, parentResourceInfo);
    }

    @Override
    protected void extractExtendElementProperties(IGspCommonElement commonField) {
    }

    @Override
    protected GspAssoResourceExtractor getGSPAssoResourceExtractor(ICefResourceExtractContext context, CefResourcePrefixInfo fieldPrefixInfo, GspAssociation asso) {
        return new BizAssoResourceExtractor(asso, getContext(), fieldPrefixInfo);
    }
}