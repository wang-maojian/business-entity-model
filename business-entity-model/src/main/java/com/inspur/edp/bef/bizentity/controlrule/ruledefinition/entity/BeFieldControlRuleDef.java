package com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.parser.BeFieldControlRuleDefParser;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.serializer.BeFieldControlRuleDefSerializer;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefinition;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleValue;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonFieldRuleNames;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmFieldControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmRuleNames;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;

@JsonSerialize(using = BeFieldControlRuleDefSerializer.class)
@JsonDeserialize(using = BeFieldControlRuleDefParser.class)
public class BeFieldControlRuleDef extends CmFieldControlRuleDef {
    public BeFieldControlRuleDef(ControlRuleDefinition parentRuleDefinition) {
        super(parentRuleDefinition);
        super.setRuleObjectType(BeFieldControlRuleNames.BeFieldRuleObjectType);
//        IGspCommonField field;
//        field.getMDataType()
        init();
    }

    private void init() {
        ControlRuleDefItem nameRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CmRuleNames.Name);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("name"));
                this.setDescription(MessageI18nUtils.getMessage("fieldName"));
                this.setDefaultRuleValue(ControlRuleValue.Allow);
            }
        };
        setNameControlRule(nameRule);

        ControlRuleDefItem lengthRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonFieldRuleNames.Length);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("length"));
                this.setDescription(MessageI18nUtils.getMessage("fieldLength"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setLengthControlRule(lengthRule);

        ControlRuleDefItem precisionRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonFieldRuleNames.Precision);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("precision"));
                this.setDescription(MessageI18nUtils.getMessage("fieldPrecision"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setPrecisionControlRule(precisionRule);

        ControlRuleDefItem defaultValueRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonFieldRuleNames.DefaultValue);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("defaultValue"));
                this.setDescription(MessageI18nUtils.getMessage("fieldDefaultValue"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setDefaultValueControlRule(defaultValueRule);

        ControlRuleDefItem multiLanRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonFieldRuleNames.MultiLanField);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("multiLanField"));
                this.setDescription(MessageI18nUtils.getMessage("fieldMultiLanField"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setMultiLanFieldControlRule(multiLanRule);

        ControlRuleDefItem readonlyRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonFieldRuleNames.Readonly);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("readonly"));
                this.setDescription(MessageI18nUtils.getMessage("readonlySetting"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setReadonlyControlRule(readonlyRule);

        ControlRuleDefItem requiredRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonFieldRuleNames.Required);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("required"));
                this.setDescription(MessageI18nUtils.getMessage("requiredSetting"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setRequiredControlRule(requiredRule);


    }
}
