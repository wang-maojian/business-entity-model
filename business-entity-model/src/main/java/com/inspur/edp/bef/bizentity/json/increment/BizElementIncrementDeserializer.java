package com.inspur.edp.bef.bizentity.json.increment;

import com.inspur.edp.bef.bizentity.json.element.BizElementDeserializer;
import com.inspur.edp.das.commonmodel.json.element.CmElementDeserializer;
import com.inspur.edp.das.commonmodel.json.increment.CommonElementIncrementDeserializer;

public class BizElementIncrementDeserializer extends CommonElementIncrementDeserializer {
    @Override
    protected CmElementDeserializer getCmElementDeserializer() {
        return new BizElementDeserializer();
    }
}
