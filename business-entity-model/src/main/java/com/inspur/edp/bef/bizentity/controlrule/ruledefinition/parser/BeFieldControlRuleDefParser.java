package com.inspur.edp.bef.bizentity.controlrule.ruledefinition.parser;

import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity.BeFieldControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmFieldControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.parser.CmFieldRuleDefParser;

public class BeFieldControlRuleDefParser extends CmFieldRuleDefParser<BeFieldControlRuleDef> {
    @Override
    protected final CmFieldControlRuleDef createCmFieldRuleDefinition() {
        return new BeFieldControlRuleDef(null);
    }
}
