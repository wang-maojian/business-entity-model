package com.inspur.edp.bef.bizentity.exceptions;

import lombok.Getter;

@Getter
public enum BusinessEntityErrorCodeEnum {
    /* ---TEMPLATE ERROR START ---**/
    /**
     * [{0}]
     */
    GSP_BEMODEL_TEMPLATE_ERROR(true),
    /*--- TEMPLATE ERROR END ---**/

    /*--- JSON SERIALIZE ERROR START ---**/
    /**
     * [{0}]反序列化失败
     */
    GSP_BEMODEL_JSON_0001(false),
    /**
     * [{0}]:[{1}]反序列化失败
     */
    GSP_BEMODEL_JSON_0002(false),
    /**
     * JSON解析失败,检查Json结构
     */
    GSP_BEMODEL_JSON_0003(false),
    /**
     * [{0}]序列化失败
     */
    GSP_BEMODEL_JSON_0004(false),
    /**
     * [{0}]:[{1}]序列化失败
     */
    GSP_BEMODEL_JSON_0005(false),
    /**
     * [{0}]序列化反序列化失败
     */
    GSP_BEMODEL_JSON_0013(false),

    /*-- JSON SERIALIZE ERROR END ---**/

    /*--- CLASS CLONE ERROR START ---**/
    /**
     * [{0}]克隆失败，生成的新字段类型为[{1}]，不属于GspBizEntityElement类型
     * 添加dbe新增的字段时使用
     */
    GSP_BEMODEL_CLONE_0001(false),
    /**
     * 添加DBE字段过程中出现字段克隆失败问题，生成的新字段为空
     * 添加dbe新增的字段时使用
     */
    GSP_BEMODEL_CLONE_0002(false),
    /*--- CLASS CLONE ERROR END ---**/

    /*--- ENUM NOT SUPPORT ERROR START ---**/
    /**
     * 不支持的增量类型:[{0}]
     */
    GSP_BEMODEL_ENUM_NOTSUPPORT_0004(false),
    /**
     * 未知构件类型:[{0}]
     */
    GSP_BEMODEL_ENUM_NOTSUPPORT_0006(false),
    /**
     * 不支持的操作类型:[{0}]
     */
    GSP_BEMODEL_ENUM_NOTSUPPORT_0007(false),
    /**
     * 不支持的检查时机:[{0}]
     */
    GSP_BEMODEL_ENUM_NOTSUPPORT_0008(false),
    /**
     * 不支持的动作类型:[{0}]
     */
    GSP_BEMODEL_ENUM_NOTSUPPORT_0009(false),
    /**
     * 不支持的触发时机:[{0}]
     */
    GSP_BEMODEL_ENUM_NOTSUPPORT_0010(false),
    /*--- ENUM NOT SUPPORT ERROR END ---**/

    /*--- COMMON EXCEPTION ERROR START ---**/
    /**
     * 获取方法或事件对应的代码元数据时出现异常，传入的代码元数据ID为空
     */
    GSP_BEMODEL_COMMON_0001(false),
    /**
     * 获取方法或事件对应的代码元数据时出现异常，传入的元数据路径为空
     */
    GSP_BEMODEL_COMMON_0002(false),
    /**
     * 参数[{0}]不能为空
     */
    GSP_BEMODEL_COMMON_0003(false),
    /**
     * 根据传入的文件路径[{0}]读取文件失败，请检查
     */
    GSP_BEMODEL_COMMON_0004(true),
    /**
     * 根据获取的当前Java应用的类路径[{0}]获取服务器路径失败，当前类路径无法匹配Linux和Windows的路径分隔
     */
    GSP_BEMODEL_COMMON_0005(false),
    /**
     * [{0}]:[{1}]操作不支持
     * ?是否有人在用
     */
    GSP_BEMODEL_COMMON_0006(false),
    /**
     * rpc服务调用[{0}]异常，详细错误信息请检索后台错误堆栈
     */
    GSP_BEMODEL_COMMON_0007(false),
    /**
     * 路径[{0}]对应的文件不存在，请检查
     */
    GSP_BEMODEL_COMMON_0008(false),
    /*--- COMMON EXCEPTION ERROR END ---**/

    /*--- BUSINESS ENTITY ERROR START ---**/
    /**
     * 工程目录下构件元数据[{0}]不存在，请检查
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0001(true),
    /**
     * 指定的监听者没有实现IBizEntityDTEventListener接口、IBizEntityFieldDTEventListener接口或IBizEntityActionDTEventListener接口
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0002(false),
    /**
     * 指定的监听者没有实现IEventListener接口
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0003(false),
    /**
     * 字段[{0}]类型不是业务字段和关联字段，不支持调用getNavigationPropertyElement方法
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0004(false),
    /**
     * 当前传入的业务实体ID为空，请检查。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0005(false),
    /**
     * 当前字段[{0}]类型不属于GspBizEntityElement类，实际类型为[{1}]
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0006(false),
    /**
     * 根据业务对象获取业务种类ID失败，请为业务对象关联业务种类
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0007(true),
    /**
     * 根据业务对象ID[{0}]获取业务种类失败
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0008(false),
    /**
     * 根据业务对象ID[{0}]获取SU失败
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0009(false),
    /**
     * 未获取到ID为[{0}]的UDT（业务字段）元数据
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0010(true),
    /**
     * UDT（业务字段）元数据[{0}]类型为多值UDT，暂时不支持此类型的UDT（业务字段）元数据创建业务类型字段
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0011(true),
    /**
     * 暂不支持此类型的Udt（业务字段）元数据:[{0}]
     * default
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0012(false),
    /**
     * 业务实体[{0}]生成pom文件失败
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0013(false),
    /**
     * 根据旧编码规则ID[{0}]未获取到对应的新编码规则
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0014(false),
    /**
     * 根据旧业务字段ID[{0}]未获取到对应的新业务字段
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0015(false),
    /**
     * 根据旧DBO的ID[{0}]未获取到对应的新DBO
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0016(false),
    /**
     * 当前BE元数据仅实现了对于实体节点和实体字段的被引用校验，暂不支持[{0}]类型的校验
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0017(false),
    /**
     * 部署解析BE元数据[{0}]时出错
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0018(false),
    /**
     * 增量抽取时，更新前的元数据为空
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0019(false),
    /**
     * 增量抽取时，更新后的元数据为空
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0020(false),
    /**
     * 增量合并时，扩展元数据为空
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0021(false),
    /**
     * [{0}]的关联带出字段[{1}]为UDT类型，但其UDTId为空。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0022(true),
    /**
     * [{0}]的关联带出字段[{1}]对应的udt元数据加载失败，udtId=[{2}]
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0023(true),
    /**
     * 字段[{0}]中选中的udt元数据未获取到，udt元数据ID为[{1}]
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0024(false),
    /**
     * [{0}] [{1}]无法转换为[{2}]类型。
     * 例：业务实体[]中节点[]的事件[]无法转换为[]类型
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0025(true),
    /**
     * 在使用导入CDM字段功能时使用
     * 读取的CDM文件中不存在有ID[{0}]的数据
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0026(true),
    /**
     * 不存在与cdm数据类型[{0}]对应的be字段类型，无法转换
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0027(true),
    /**
     * 子节点上只有与上级节点的关联字段允许将编号设置为parentID,请检查子节点[{0}]上的字段编号
     * 只有子节点上与父节点建立关系的字段编号可以命名为parentID，其余字段不可设置
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0028(true),
    /**
     * 不支持动作类型[{0}]生成构件
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0029(true),
    /**
     * 请先建立元数据工程
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0030(true),
    /**
     * 如果传入的isDboNew是false，则传入的databaseObjectTable对象不能为空。
     * 有人在调用吗？
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0031(false),
    /**
     * 不存在ID=[{0}]的数据库表。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0032(true),
    /**
     * 业务实体字段[{0}](编号:{1})对应的数据列在DBO(ID:{2})上不存在,请检查。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0033(true),
    /**
     * 新增的业务实体字段[{0}](编号:{1})在DBO中存在相同编号的字段，请修改新增实体字段编号或删除对应的DBO字段。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0034(true),
    /**
     * 字段[{0}](编号:{1})类型转换出错，不支持将字段类型从{2}转换为{3}
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0035(true),
    /**
     * 已存在名为[{0}]的dbo文件,请修改当前业务实体上与之重名的对象编号。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0036(true),
    /**
     * 已存在名为[{0}]的数据库表,请修改当前业务实体上与之重名的对象编号。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0037(true),
    /**
     * 字段[{0}]为多语字段，其数据类型应为[字符串]或[备注]。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0038(false),
    /**
     * 未定义的字段数据类型[{0}]，无法转换为数据库对象字段类型。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0039(false),
    /**
     * 待确认异常
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0040(false),
    /**
     * 将DBO(ID:{0})转换为业务实体对象过程中，未匹配到被选择作为对象主键的字段(ID:{1})
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0041(true),
    /**
     * 根据ConfigId[{0}]获取到多个业务对象，应为一对一的关系，请检查。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0042(true),
    /**
     * 当前传入的BE方法不属于BizActionBase及其子类，无法匹配到对应的代码生成器
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0043(false),
    /**
     * 业务实体[{0}]上的方法[{1}]存在编号重复的参数[{2}]
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0044(true),
    /**
     * 方法传入的参数[{0}]信息内集合类型异常，无法识别[{1}]的类型。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0045(false),
    /**
     * 当前参数[{0}],未获取到其对应的参数类型并且JavaClassName属性为空，请检查。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0046(false),
    /**
     * 命名空间不能为空
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0047(false),
    /**
     * 方法[{0}]的参数[{1}]编号为空，请修改
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0048(true),
    /**
     * 根据路径[{0}]及DBO的ID[{1}]获取dbo元数据失败。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0049(false),
    /**
     * 在工程路径[{0}]下未获取到构件元数据[{1}]，请检查是否未生成对应的构件元数据
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0050(true),
    /**
     * 获取的构件元数据内容无法转换为构件元数据类型，请检查。
     * 直接传入的content，存在获取构件元数据但是内容不为构件元数据的情况吗？
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0051(true),
    /**
     * 生成TCC构件内容异常，构件[{0}]的编号未以Controller结尾，获取不到对应的类名属性
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0052(false),
    /**
     * 请配置maven环境变量！具体教程可参考网上教程
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0053(true),
    /**
     * 依据元数据ID[{0}]未获取到关联对应的元数据,请检查确认元数据是否存在
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0054(true),
    /**
     * 依据元数据ID[{0}]未获取到关联对应的元数据,请检查确认元数据是否存在
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0055(true),
    /**
     * 指定的监听者没有实现[{0}]接口
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0056(false),
    /**
     * 重命名Runtime模块为精简形式失败:[{0}]
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0057(false),
    /**
     * 在工程pom文件重命名Runtime模块失败：[{0}]
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0058(false),
    /**
     * 生成TCC构件异常，生成的构件元数据内容为空
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0059(false),
    /**
     * 无法找到[{0}]的jar包路径
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0060(true),
    /**
     * 获取的业务字段元数据内容出现错误，不属于业务字段类型
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0062(false),
    /**
     * 获取元数据失败，元数据Id[{0}],元数据类型[{1}]
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0063(true),
    /**
     * [{0}]未识别的属性名：[{1}]
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0064(false),
    /**
     * 未定义的数据库对象字段类型[{0}]，无法转换为业务实体字段类型。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0065(false),
    /**
     * 没有获取到业务对象[{0}]的相关信息
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0066(true),
    /**
     * 字段[{0}]为多语字段，其数据类型应为[字符串]或[备注]。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0067(true),
    /**
     * 当前字段[{0}]类型为业务字段，元数据文件中childElements内不存在ID为[{1}]的字段，请检查
     * 字段映射关系中的id=[{0}]的[{1}]不存在
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0068(false),
    /**
     * 请先完善当前字段的[编号]。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0069(true),
    /**
     * BE元数据[{0}]上的主对象与子对象编号重复，请修改对象编号。重复编号：[{1}]
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0070(true),
    /**
     * 当前工程路径[{0}]下不存在构件[ID：{1}]
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0071(true),
    /**
     * 根据元数据ID[{0}]未加载到构件元数据，请检查工程目录内构件元数据是否存在
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0072(true),
    /**
     * 根据DBO创建业务实体子对象需要选择主键ID字段，请确认！
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0073(true),
    /**
     * 根据DBO创建业务实体子对象需要选择ParentID字段,请确认!
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0074(true),
    /**
     * 当前设置的ParentID列在dbo上不存在。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0075(false),
    /**
     * 获取当前用户ID为空
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0076(false),
    /**
     * 根据元数据ID[{0}]获取运行时业务实体元数据出错，获取元数据内容为空，请检查是否部署。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0077(true),
    /**
     * 根据元数据ID[{0}]获取运行时业务实体元数据出错，获取元数据内容为空，请检查是否部署。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0078(true),
    /**
     * 父节点ColumnGenerateID属性为空
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0079(false),
    /**
     * 单值业务字段的约束信息中无属性名[{0}]
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0080(true),
    /**
     * be变量不支持选择关联udt。
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0081(true),
    /**
     * 错误的[{0}]类型
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0082(true),
    /**
     * 生成Api模块失败
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0083(false),
    /**
     * 根据文件路径[{0}]读取文件失败
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0084(false),
    /**
     * 将文件复制到目标文件路径[{0}]时出错
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0085(false),
    /**
     * [{0}]不是正确的BE实体类型
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0086(true),
    /**
     * 获取到的关联元数据[{0}]内容无法转换为业务实体类型，请检查，元数据ID为[{1}]
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0087(true),
    /**
     * 根据业务实体生成数据库对象时检测到已经存在编号为[{0}]的同名文件（编号命名规则为应用域编号+对象编号），请修改对应的实体对象节点编号[{1}]！
     */
    GSP_BEMODEL_BUSINESS_ENTITY_0088(true);

    /*--- BUSINESS ENTITY ERROR END ---**/

    /**
     * 是否业务异常
     */
    private final boolean bizException;

    BusinessEntityErrorCodeEnum(boolean bizException) {
        this.bizException = bizException;
    }
}
