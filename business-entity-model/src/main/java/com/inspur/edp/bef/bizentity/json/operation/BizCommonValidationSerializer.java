package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.BizCommonValdation;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.operation.CommonValSerializer;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;

public class BizCommonValidationSerializer extends CommonValSerializer {
    public BizCommonValidationSerializer(boolean full) {
        super(full);
    }

    @Override
    protected void writeExtendCommonOpSelfProperty(JsonGenerator writer, CommonOperation info) {
        super.writeExtendCommonOpSelfProperty(writer, info);
        writeParameters(writer, (CommonValidation) info);
    }

    private void writeParameters(JsonGenerator writer, CommonValidation validation) {
        BizCommonValdation bizCommonValdation = (BizCommonValdation) validation;
        if (isFull || (bizCommonValdation.getParameterCollection() != null && bizCommonValdation.getParameterCollection().getCount() > 0)) {
            SerializerUtils.writePropertyName(writer, BizEntityJsonConst.Parameters);
            //[
            SerializerUtils.WriteStartArray(writer);
            if (bizCommonValdation.getParameterCollection() != null && bizCommonValdation.getParameterCollection().getCount() > 0) {
                for (Object item : bizCommonValdation.getParameterCollection()) {
                    new BizParameterSerializer(isFull).serialize((BizParameter) item, writer, null);
                }
            }
            //]
            SerializerUtils.WriteEndArray(writer);
        }
    }
}
