package com.inspur.edp.bef.bizentity.gspbusinessentitylabel.api;

import com.inspur.edp.bef.bizentity.gspbusinessentitylabel.entity.GspBeLabelInfo;

import java.util.List;

public interface IGspBeLabelInfoService {
    /**
     * 根据ID获取某条标签信息
     *
     * @param id
     * @return
     */
    GspBeLabelInfo getBeLabelInfo(String id);

    /**
     * 获取所有标签信息
     *
     * @return
     */
    List<GspBeLabelInfo> getBeLabelInfos();

    /**
     * 保存
     *
     * @param infos
     */
    void saveBeLabelInfos(List<GspBeLabelInfo> infos);
}
