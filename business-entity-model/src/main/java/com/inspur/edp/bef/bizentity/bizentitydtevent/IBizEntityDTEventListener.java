package com.inspur.edp.bef.bizentity.bizentitydtevent;

import com.inspur.edp.bef.bizentity.dtconsistencycheck.entityeventargs.RemovingEntityEventArgs;
import io.iec.edp.caf.commons.event.IEventListener;

public interface IBizEntityDTEventListener extends IEventListener {

    public RemovingEntityEventArgs removingEntity(RemovingEntityEventArgs args);
}
