package com.inspur.edp.bef.bizentity.json.operation;

import com.inspur.edp.bef.bizentity.common.InternalActionUtil;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.collection.BizMgrActionCollection;

public class BizMgrActionCollectionDeserializer extends BizOperationCollectionDeserializer<BizMgrActionCollection> {
    @Override
    protected BizMgrActionCollection createCollection() {
        return new BizMgrActionCollection();
    }

    @Override
    protected BizMgrActionDeserializer createDeserializer() {
        return new BizMgrActionDeserializer();
    }

    @Override
    protected void addBizOp(BizMgrActionCollection collection, BizOperation op) {
        if (!InternalActionUtil.InternalMgrActionIDs.contains(op.getID())) {
            super.addBizOp(collection, op);
        }
    }
}
