package com.inspur.edp.bef.bizentity.operation.componentenum;

/**
 * 参数类型
 */
public enum BizParameterType {
    /**
     * 字符型
     */
    String,
    /**
     * 布尔型
     */
    Boolean,
    /**
     * 整数
     */
    Int32,
    /**
     * 浮点数字
     */
    Decimal,
    /**
     * 双浮点
     */
    Double,
    /**
     * 时间
     */
    DateTime,
    //对象类型
    Object,
    /**
     * 自定义
     */
    Custom;

    public int getValue() {
        return this.ordinal();
    }

    public static BizParameterType forValue(int value) {
        return values()[value];
    }
}