package com.inspur.edp.bef.bizentity.increment.extract.validation;

import com.inspur.edp.bef.bizentity.increment.entity.validation.AddedValIncrement;
import com.inspur.edp.bef.bizentity.operation.Validation;

public class AddedValExtractor {

    public AddedValIncrement extract(Validation voAction) {
        AddedValIncrement addedEntityIncrement = this.createIncrement();
        addedEntityIncrement.setAction(voAction);
        return addedEntityIncrement;
    }

    protected AddedValIncrement createIncrement() {
        return new AddedValIncrement();
    }
}
