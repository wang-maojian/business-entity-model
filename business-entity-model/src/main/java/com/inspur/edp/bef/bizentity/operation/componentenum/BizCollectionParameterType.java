package com.inspur.edp.bef.bizentity.operation.componentenum;

public enum BizCollectionParameterType {
    /**
     * 未使用集合形式
     */
    None,
    /**
     * 列表
     */
    List,
    /**
     * 数组
     */
    Array;

    public int getValue() {
        return this.ordinal();
    }

    public static BizCollectionParameterType forValue(int value) {
        return values()[value];
    }
}