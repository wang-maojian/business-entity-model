package com.inspur.edp.bef.bizentity.increment.entity.action;

import com.inspur.edp.cef.designtime.api.increment.IncrementType;

public class DeletedMgrActionIncrement extends MgrActionIncrement {

    public DeletedMgrActionIncrement(String actionId) {
        setActionId(actionId);
    }

    @Override
    public IncrementType getIncrementType() {
        return IncrementType.Deleted;
    }
}
