package com.inspur.edp.bef.bizentity.i18n.merge;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.das.commonmodel.IGspCommonModel;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.i18n.merge.CommonModelResourceMerger;
import com.inspur.edp.das.commonmodel.i18n.merge.CommonobjectResourceMerger;

public class BizEntityResourceMerger extends CommonModelResourceMerger {
    public BizEntityResourceMerger(GspBusinessEntity model, ICefResourceMergeContext context) {
        super(model, context);
    }

    @Override
    protected String getModelDescriptionName() {
        return null;
    }

    @Override
    protected CommonobjectResourceMerger getObjectResourceMerger(ICefResourceMergeContext context, IGspCommonObject obj) {
        return new BizObjResourceMerger((GspBizEntityObject) ((obj instanceof GspBizEntityObject) ? obj : null), context);

    }

    @Override
    protected void extractExtendProperties(IGspCommonModel iGspCommonModel) {

    }
}
