package com.inspur.edp.bef.bizentity.json.operation;

import com.inspur.edp.bef.bizentity.operation.collection.ValidationCollection;

public class ValCollectionDeserializer extends BizOperationCollectionDeserializer<ValidationCollection> {
    @Override
    protected ValidationCollection createCollection() {
        return new ValidationCollection();
    }

    @Override
    protected BizValidationDeserializer createDeserializer() {
        return new BizValidationDeserializer();
    }
}
