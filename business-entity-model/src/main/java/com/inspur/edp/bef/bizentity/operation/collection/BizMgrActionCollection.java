package com.inspur.edp.bef.bizentity.operation.collection;

import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.BizOperationCollection;

public class BizMgrActionCollection extends BizOperationCollection {
    private static final long serialVersionUID = 1L;

    // C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing
    // via the 'new' keyword:
    // ORIGINAL LINE: public new void add(BizOperation operation)
    public final boolean add(BizOperation operation) {
        if (operation instanceof BizMgrAction) {
            return super.add(operation);
        }
        throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0009, operation.getClass().getSimpleName());
    }

    // /**
    // 克隆BEMgrAction动作集合

    // @param isGenerateId 是否生成Id
    // @return 返回动作集合
    // */
    // //C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing
    // via the 'new' keyword:
    // //ORIGINAL LINE: public new object clone(bool isGenerateId)
    // public final Object clone(boolean isGenerateId)
    // {
    // BizMgrActionCollection col = new BizMgrActionCollection();
    // //C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods
    // are not converted by C# to Java Converter:
    // col.addRange(this.select(item { return
    // (BizMgrAction)item.clone(isGenerateId)));
    // return col;
    // }

    // /**
    // * 克隆BEMgrAction动作集合
    // *
    // * @return 返回动作集合
    // */
    // // C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s
    // shadowing
    // // via the 'new' keyword:
    // // ORIGINAL LINE: public new object clone()
    // public final Object clone() {
    // return clone(false);
    // }
    @Override
    public BizMgrActionCollection clone() {
        return (BizMgrActionCollection) super.clone();
    }

    @Override
    protected BizMgrAction convertOperation(BizOperation op) {
        return (BizMgrAction) super.convertOperation(op);
    }

    @Override
    protected BizMgrActionCollection createOperationCollection() {
        return new BizMgrActionCollection();
    }
}