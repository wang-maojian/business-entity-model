package com.inspur.edp.bef.bizentity.operation;

import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;

public class BizCommonValdation extends CommonValidation {
    private BizParameterCollection parameterCollection;

    public BizParameterCollection getParameterCollection() {
        return parameterCollection;
    }

    public void setParameterCollection(BizParameterCollection parameterCollection) {
        this.parameterCollection = parameterCollection;
    }
}
