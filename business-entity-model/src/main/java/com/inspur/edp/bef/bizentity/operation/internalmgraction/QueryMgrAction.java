package com.inspur.edp.bef.bizentity.operation.internalmgraction;

import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;

/**
 * 内置Query操作
 */
public class QueryMgrAction extends BizMgrAction implements IInternalMgrAction {
    public static final String id = "m_6xIwJwoUy7qzsuZ6_S5A";
    public static final String code = "Query";
    public static final String name = MessageI18nUtils.getMessage("Query");

    public QueryMgrAction() {
        setID(id);
        setCode(code);
        setName(name);
    }

}