package com.inspur.edp.bef.bizentity.pushchangesetargs;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MetadataInfo {
    @JsonProperty("path")
    private String path;
    @JsonProperty("entityId")
    private String entityId;

    public MetadataInfo() {
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }
}
