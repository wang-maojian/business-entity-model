package com.inspur.edp.bef.bizentity.i18n.merge;

import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.i18n.merge.CommonElementResourceMerger;
import com.inspur.edp.das.commonmodel.i18n.merge.CommonobjectResourceMerger;

public class BizObjResourceMerger extends CommonobjectResourceMerger {
    public BizObjResourceMerger(GspBizEntityObject object, ICefResourceMergeContext context) {
        super(object, context);
    }

    @Override
    protected void extractExtendObjProperties(IGspCommonObject dataType) {

    }

    @Override
    protected CommonElementResourceMerger getCommonEleResourceMerger(ICefResourceMergeContext context, IGspCommonElement element) {
        return new BizEleResourceMerger((GspBizEntityElement) ((element instanceof GspBizEntityElement) ? element : null), context);

    }

    @Override
    protected CommonobjectResourceMerger getObjectResourceMerger(ICefResourceMergeContext context, IGspCommonObject obj) {
        return new BizObjResourceMerger((GspBizEntityObject) ((obj instanceof GspBizEntityObject) ? obj : null), context);

    }
}
