package com.inspur.edp.bef.bizentity.dboconsistencycheck;

public class DboCheckMessageUtil {
    static String DBOInfo = "数据对象ID为[%s];";
    static String BEInfo = "实体名称为:[%s],实体编号为:[%s];";
    static String ObjectInfo = "实体对象名称[%s],对象编号为:[%s];";
    static String FieldInfo = "实体字段名称[%s],字段编号为:[%s];";
    static String DBONotFound = "当前对象找不到对应的数据库对象,";
    static String ColumnNotFound = "BE字段找不到对应数据库对象的列，";
    static String FieldMDataTypeNotMatch = "字段数据类型不一致,BE字段数据类型为[%s],DBO列数据类型为[%s],";
    static String AssoRefMetadataNotFound = "关联字段找不到依赖的业务实体,实体ID为[%s],";
    static String AssoRefObjectNotFound = "关联字段找不到依赖的实体对象,实体ID为[%s],实体对象ID为[%s],";
    static String AssoRefFieldNotFound = "关联带出字段[%s]找不到依赖的实体字段，元数据ID为[%s],实体对象ID为[%s]，实体字段ID为[%s]";
    static String NewLine = System.lineSeparator();
    static String exceptionCode = "GenerateBeCheck";
    static String FieldLengthExp = "BE字段长度大于对应DBO长度，BE字段长度为[%s],DBO列长度为[%s],";
    static String FieldPrecisionExp = "BE字段精度大于对应DBO精度，BE字段精度为[%s],DBO列精度为[%s],";
}
