package com.inspur.edp.bef.bizentity.operation;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.bef.bizentity.beenum.BEOperationType;
import com.inspur.edp.bef.bizentity.json.operation.BizMgrActionDeserializer;
import com.inspur.edp.bef.bizentity.json.operation.BizMgrActionSerializer;
import com.inspur.edp.bef.bizentity.operation.bemgrcomponent.BizMgrActionParamCollection;

/**
 * BEMgr操作
 */
@JsonSerialize(using = BizMgrActionSerializer.class)
@JsonDeserialize(using = BizMgrActionDeserializer.class)
public class BizMgrAction extends BizActionBase implements Cloneable {
    /**
     * 业务操作ID
     */
    private String privateFuncOperationID;

    public final String getFuncOperationID() {
        return privateFuncOperationID;
    }

    public final void setFuncOperationID(String value) {
        privateFuncOperationID = value;
    }

    /**
     * 业务操作名称, 显示用
     */
    private String privateFuncOperationName;

    public final String getFuncOperationName() {
        return privateFuncOperationName;
    }

    public final void setFuncOperationName(String value) {
        privateFuncOperationName = value;
    }

    private BizMgrActionParamCollection beMgrParams;

    /**
     * 操作类型
     */
    @Override
    public BEOperationType getOpType() {
        return BEOperationType.BizMgrAction;
    }

    public BizMgrAction() {
        super();
        beMgrParams = new BizMgrActionParamCollection();
    }

    @Override
    protected BizMgrActionParamCollection getActionParameters() {
        return beMgrParams;
    }
}