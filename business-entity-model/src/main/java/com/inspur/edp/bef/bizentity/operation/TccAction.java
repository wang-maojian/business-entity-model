package com.inspur.edp.bef.bizentity.operation;

import com.inspur.edp.bef.bizentity.beenum.BEOperationType;

public class TccAction extends BizOperation implements Cloneable {
    private TccReturnValue tccReturnValue;

    public TccAction() {
        super();
        tccReturnValue = new TccReturnValue();
    }

    /**
     * 操作类型
     */
    @Override
    public BEOperationType getOpType() {
        return BEOperationType.TccAction;
    }

    /**
     * 返回值
     */
    public TccReturnValue getTccReturnValue() {
        return tccReturnValue;
    }

    /**
     *
     */
    public void setTccReturnValue(TccReturnValue value) {
        this.tccReturnValue = value;
    }
}
