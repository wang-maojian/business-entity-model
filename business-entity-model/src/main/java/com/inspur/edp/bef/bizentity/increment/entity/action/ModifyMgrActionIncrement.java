package com.inspur.edp.bef.bizentity.increment.entity.action;

import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;

public class ModifyMgrActionIncrement extends MgrActionIncrement {

    private BizMgrAction action;

    @Override
    public IncrementType getIncrementType() {
        return IncrementType.Modify;
    }

    public BizMgrAction getAction() {
        return action;
    }

    public void setAction(BizMgrAction action) {
        this.action = action;
    }
}
