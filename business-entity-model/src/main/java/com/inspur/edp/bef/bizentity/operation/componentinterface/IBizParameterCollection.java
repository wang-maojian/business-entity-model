package com.inspur.edp.bef.bizentity.operation.componentinterface;

public interface IBizParameterCollection<T extends IBizParameter> extends Iterable<T> {
    IBizParameter getItem(int index);

    boolean add(IBizParameter item);

    void insert(int index, IBizParameter item);

    void removeAt(int index);

    boolean remove(IBizParameter item);

    boolean contains(IBizParameter item);

    void clear();

    int indexOf(IBizParameter item);

    int getCount();
}