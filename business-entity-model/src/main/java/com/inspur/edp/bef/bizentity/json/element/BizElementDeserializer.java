package com.inspur.edp.bef.bizentity.json.element;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.bef.bizentity.beenum.RequiredCheckOccasion;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.json.object.BizObjectDeserializer;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.entity.GspCommonElement;
import com.inspur.edp.das.commonmodel.json.element.CmElementDeserializer;

public class BizElementDeserializer extends CmElementDeserializer {
    @Override
    protected final GspBizEntityElement createElement() {
        return new GspBizEntityElement();
    }


    @Override
    protected void beforeCMElementDeserializer(GspCommonElement item) {
        GspBizEntityElement field = (GspBizEntityElement) item;
        field.setRtElementConfigId("");
    }

    @Override
    protected boolean readExtendElementProperty(GspCommonElement item, String propName, JsonParser jsonParser) {
        boolean result = true;
        GspBizEntityElement field = (GspBizEntityElement) item;
        switch (propName) {
            case BizEntityJsonConst.CalculationExpress:
                field.setCalculationExpress(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.ValidationExpress:
                field.setValidationExpress(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.RequiredCheckOccasion:
                field.setRequiredCheckOccasion(SerializerUtils.readPropertyValue_Enum(jsonParser, RequiredCheckOccasion.class, RequiredCheckOccasion.values()));
                break;
            case BizEntityJsonConst.IsDefaultNull:
                field.setIsDefaultNull(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case BizEntityJsonConst.UnifiedDataType:
                //todo:json-文件中无Udt信息,为null
                SerializerUtils.readPropertyValue_String(jsonParser);
                break;
            case BizEntityJsonConst.RtElementConfigId:
                field.setRtElementConfigId(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.ValueChangedDtm:
                field.setValueChangedDtms((new BizObjectDeserializer()).readCommonDtmCollection(jsonParser));
                break;
            case BizEntityJsonConst.ComputationDtm:
                field.setComputationDtms((new BizObjectDeserializer()).readCommonDtmCollection(jsonParser));
                break;
            case BizEntityJsonConst.ValueChangedVal:
                field.setValueChangedVals((new BizObjectDeserializer()).readCommonValidationCollection(jsonParser));
                break;
            default:
                result = false;
                break;
        }
        return result;
    }
}
