package com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.parser.BeEntityControlRuleDefParser;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.serializer.BeEntityControlRuleDefSerializer;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleValue;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeRuleNames;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmEntityControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmEntityRuleNames;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmRuleNames;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;

@JsonSerialize(using = BeEntityControlRuleDefSerializer.class)
@JsonDeserialize(using = BeEntityControlRuleDefParser.class)
public class BeEntityControlRuleDef extends CmEntityControlRuleDef {
    public BeEntityControlRuleDef(BeControlRuleDef parentRuleDefinition) {
        super(parentRuleDefinition);
        super.setRuleObjectType(BeEntityRuleDefNames.BeEntityRuleObjectType);
        init();
    }


    private void init() {

        ControlRuleDefItem nameRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CmRuleNames.Name);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("name"));
                this.setDescription(MessageI18nUtils.getMessage("nodeName"));
                this.setDefaultRuleValue(ControlRuleValue.Allow);
            }
        };
        setNameControlRule(nameRule);

        ControlRuleDefItem addUqRule = new ControlRuleDefItem() {
            {
                this.setRuleName(BeEntityRuleDefNames.AddUQConstraint);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("addUQConstraint"));
                this.setDescription(MessageI18nUtils.getMessage("addUQConstraint"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setAddUQConstraintControlRule(addUqRule);

        ControlRuleDefItem modifyUqRule = new ControlRuleDefItem() {
            {
                this.setRuleName(BeEntityRuleDefNames.ModifyUQConstraint);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("modifyUQConstraint"));
                this.setDescription(MessageI18nUtils.getMessage("modifyUQConstraint"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setModifyUQConstraintControlRule(modifyUqRule);

        ControlRuleDefItem addDtmRule = new ControlRuleDefItem() {
            {
                this.setRuleName(BeEntityRuleDefNames.AddDtermination);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("addDtermination"));
                this.setDescription(MessageI18nUtils.getMessage("addDtermination"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setAddDterminationControlRule(addDtmRule);

        ControlRuleDefItem modifyDtmRule = new ControlRuleDefItem() {
            {
                this.setRuleName(BeEntityRuleDefNames.ModifyDetermination);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("modifyDetermination"));
                this.setDescription(MessageI18nUtils.getMessage("modifyDetermination"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setModifyDterminationControlRule(modifyDtmRule);

        ControlRuleDefItem addValRule = new ControlRuleDefItem() {
            {
                this.setRuleName(BeEntityRuleDefNames.AddValidation);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("addValidation"));
                this.setDescription(MessageI18nUtils.getMessage("addValidation"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setAddValidationControlRule(addValRule);

        ControlRuleDefItem modifyValRule = new ControlRuleDefItem() {
            {
                this.setRuleName(BeEntityRuleDefNames.ModifyValidation);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("modifyValidation"));
                this.setDescription(MessageI18nUtils.getMessage("modifyValidation"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setModifyValidationControlRule(modifyValRule);

        ControlRuleDefItem addChildEntityRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CmEntityRuleNames.AddChildEntity);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("addChildEntity"));
                this.setDescription(MessageI18nUtils.getMessage("addChildEntity"));
                this.setDefaultRuleValue(ControlRuleValue.Allow);
            }
        };
        setAddChildEntityControlRule(addChildEntityRule);

        ControlRuleDefItem modifyChildEntityRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CmEntityRuleNames.ModifyChildEntities);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("modifyChildEntities"));
                this.setDescription(MessageI18nUtils.getMessage("modifyChildEntities"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setModifyChildEntitiesControlRule(modifyChildEntityRule);

        ControlRuleDefItem addElementRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonDataTypeRuleNames.AddField);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("addField"));
                this.setDescription(MessageI18nUtils.getMessage("addField"));
                this.setDefaultRuleValue(ControlRuleValue.Allow);
            }
        };
        setAddFieldControlRule(addElementRule);

        ControlRuleDefItem modifyElementRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonDataTypeRuleNames.ModifyFields);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("modifyField"));
                this.setDescription(MessageI18nUtils.getMessage("modifyField"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setModifyFieldsControlRule(modifyElementRule);

//        BeEntityControlRuleDef objectRuleDef = new BeEntityControlRuleDef(this);
//        getChildControlRules().put(CommonModelNames.MainObject, objectRuleDef);
        BeFieldControlRuleDef fieldRuleDef = new BeFieldControlRuleDef(this);
        getChildControlRules().put(CommonModelNames.Element, fieldRuleDef);
    }

    public ControlRuleDefItem getAddUQConstraintControlRule() {
        return super.getControlRuleItem(BeEntityRuleDefNames.AddUQConstraint);
    }

    public void setAddUQConstraintControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(BeEntityRuleDefNames.AddUQConstraint, ruleItem);
    }

    public ControlRuleDefItem getModifyUQConstraintControlRule() {
        return super.getControlRuleItem(BeEntityRuleDefNames.ModifyUQConstraint);
    }

    public void setModifyUQConstraintControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(BeEntityRuleDefNames.ModifyUQConstraint, ruleItem);
    }

    public ControlRuleDefItem getAddDeterminationControlRule() {
        return super.getControlRuleItem(BeEntityRuleDefNames.AddDtermination);
    }

    public void setAddDterminationControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(BeEntityRuleDefNames.AddDtermination, ruleItem);
    }

    public ControlRuleDefItem getModifyDterminationControlRule() {
        return super.getControlRuleItem(BeEntityRuleDefNames.ModifyDetermination);
    }

    public void setModifyDterminationControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(BeEntityRuleDefNames.ModifyDetermination, ruleItem);
    }

    public ControlRuleDefItem getAddValidationControlRule() {
        return super.getControlRuleItem(BeEntityRuleDefNames.AddValidation);
    }

    public void setAddValidationControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(BeEntityRuleDefNames.AddValidation, ruleItem);
    }

    public ControlRuleDefItem getModifyValidationControlRule() {
        return super.getControlRuleItem(BeEntityRuleDefNames.ModifyValidation);
    }

    public void setModifyValidationControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(BeEntityRuleDefNames.ModifyValidation, ruleItem);
    }
}
