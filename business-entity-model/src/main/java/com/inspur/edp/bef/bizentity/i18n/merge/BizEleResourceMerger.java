package com.inspur.edp.bef.bizentity.i18n.merge;

import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.i18n.merge.CommonElementResourceMerger;
import com.inspur.edp.das.commonmodel.i18n.merge.GspAssoResourceMerger;

public class BizEleResourceMerger extends CommonElementResourceMerger {
    public BizEleResourceMerger(GspBizEntityElement field, ICefResourceMergeContext context) {
        super(field, context);
    }

    @Override
    protected void extractExtendElementProperties(IGspCommonElement iGspCommonElement) {

    }

    @Override
    protected GspAssoResourceMerger getGSPAssoResourceMerger(ICefResourceMergeContext context, GspAssociation asso) {
        return new BizAssoResourceMerger(asso, getContext());

    }
}
