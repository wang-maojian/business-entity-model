package com.inspur.edp.bef.bizentity.pushchangesetargs;

public class PushChangeSetArgs extends AbstractPushChangeSetArgs {
    public PushChangeSetArgs() {
        super();
    }

    private PushChangeSet pushChangeSet;

    public PushChangeSetArgs(PushChangeSet pushChangeSet) {
        super();
        this.pushChangeSet = pushChangeSet;
    }

    public PushChangeSet getPushChangeSet() {
        return pushChangeSet;
    }

    public void setPushChangeSet(PushChangeSet pushChangeSet) {
        this.pushChangeSet = pushChangeSet;
    }
}
