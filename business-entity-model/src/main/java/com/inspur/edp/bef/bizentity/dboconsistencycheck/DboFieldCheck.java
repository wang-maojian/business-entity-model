package com.inspur.edp.bef.bizentity.dboconsistencycheck;

import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.util.DboTransferUtil;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.collection.GspFieldCollection;
import com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import com.inspur.edp.cef.designtime.api.util.MetadataUtil;
import com.inspur.edp.das.commonmodel.entity.element.GspCommonAssociation;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectColumn;
import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectTable;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

public class DboFieldCheck extends DboObjectCheck {

    private GspBizEntityElement bizElement;
    private DatabaseObjectColumn databaseColumn;

    public DboFieldCheck(DatabaseObjectTable databaseObject, GspBusinessEntity bizEntity, GspBizEntityObject bizEntityObject,
                         GspBizEntityElement bizElement) {
        super(bizEntity, bizEntityObject);
        this.bizElement = bizElement;
        this.databaseObject = databaseObject;
    }

    public String checkFieldCoincidence() {
        if (!checkFieldExist()) {
            return this.expMessage.toString();
        }
        checkFieldProperties();
        String msg = expMessage.toString();
        if (StringUtils.isEmpty(msg)) {
            return null;
        }
        return msg + getFieldInfo() + DboCheckMessageUtil.NewLine;
    }

    /**
     * 字段信息
     *
     * @return
     */
    protected String getFieldInfo() {
        return getObjectInfo() + String.format(DboCheckMessageUtil.FieldInfo, bizElement.getName(), bizElement.getCode()) + DboCheckMessageUtil.NewLine;
    }

    /**
     * 字段检查
     *
     * @return
     */
    private String getColumnNotFoundMsg() {
        return DboCheckMessageUtil.ColumnNotFound + getFieldInfo() + DboCheckMessageUtil.NewLine;
    }

    private boolean checkFieldExist() {
        List<DatabaseObjectColumn> columns = (this.databaseObject).getColumns().stream().filter(item -> item.getId().equals(bizElement.getColumnID())).collect(
                Collectors.toList());
        if (columns.isEmpty()) {
            this.expMessage.append(getColumnNotFoundMsg());
            return false;
        }
        this.databaseColumn = columns.get(0);
        return true;
    }

    /**
     * 检查字段属性
     */
    private void checkFieldProperties() {
        checkMDataType();
        checkAssociation();
        checkLengthAndPrecision();
    }

    /**
     * 检查字段数据类型
     */
    private void checkMDataType() {
        GspElementDataType mDataType = this.bizElement.getMDataType();
        List<GspElementDataType> dataTypes = DboTransferUtil.checkMDataTypeByDataType(this.databaseColumn.getDataType(), DboCheckMessageUtil.exceptionCode);
        if (dataTypes.contains(mDataType)) {
            return;
        }
        this.expMessage.append(mDataTypeNotMatchedMsg());
    }

    private String mDataTypeNotMatchedMsg() {
        return String.format(DboCheckMessageUtil.FieldMDataTypeNotMatch, this.bizElement.getMDataType(), this.databaseColumn.getDataType()) + DboCheckMessageUtil.NewLine;
    }

    /**
     * 检查字段对象类型
     */
    private void checkAssociation() {
        if (this.bizElement.getHasAssociation()) {
            this.bizElement.getChildAssociations().forEach(child -> {
                findAssoRefMetadata((GspCommonAssociation) child);
            });
        }
    }

    private void findAssoRefMetadata(GspCommonAssociation asso) {
        String refModelID = asso.getRefModelID();
        String refObjectId = asso.getRefObjectID();
        GspFieldCollection refFields = asso.getRefElementCollection();

        GspMetadata refMetadata = MetadataUtil.getCustomRTMetadata(refModelID);
        if (refMetadata == null) {
            this.expMessage.append(assoRefMetadataNotFound(refModelID));
            return;
        }

        List<GspBizEntityObject> objects = ((GspBusinessEntity) refMetadata.getContent()).getAllNodes().stream().filter(obj ->
                obj.getID().equals(asso.getRefObjectID())).collect(Collectors.toList());
        if (objects.isEmpty()) {
            this.expMessage.append(assRefObjectNotFound(refModelID, refObjectId));
            return;
        }

        refFields.forEach(field -> {
            List<IGspCommonField> gotRefField = objects.get(0).getContainElements().stream().filter(ele -> ele.getID().equals(field.getRefElementId())).collect(Collectors.toList());
            if (gotRefField.isEmpty()) {
                this.expMessage.append(AssRefFieldNotFound(field.getName(), refModelID, refObjectId, field.getRefElementId()));
            }
        });
    }


    private String assoRefMetadataNotFound(String metadataId) {
        return String.format(DboCheckMessageUtil.AssoRefMetadataNotFound, metadataId) + DboCheckMessageUtil.NewLine;
    }

    private String assRefObjectNotFound(String metadataId, String objectId) {
        return String.format(DboCheckMessageUtil.AssoRefObjectNotFound, metadataId, objectId);
    }

    private String AssRefFieldNotFound(String assBringName, String metadataId, String objectId, String fieldId) {
        return String.format(DboCheckMessageUtil.AssoRefFieldNotFound, assBringName, metadataId, objectId, fieldId);
    }

    /**
     * 长度精度校验
     */
    private void checkLengthAndPrecision() {
        if (!needCheckLengthAndPrec()) {
            return;
        }
        // 浮点类型的长度精度单独校验
        if (this.bizElement.getMDataType().equals(GspElementDataType.Decimal)) {
            checkDecimalLengthAndPre();
            return;
        }
        if (this.bizElement.getLength() > this.databaseColumn.getLength()) {
            this.expMessage.append(getFieldLengthExp(this.bizElement.getLength(), this.databaseColumn.getLength()));
        }
        if (this.bizElement.getPrecision() > this.databaseColumn.getPrecision()) {
            this.expMessage.append(getFieldPrecisionExp(this.bizElement.getPrecision(), this.databaseColumn.getPrecision()));
        }
    }

    private String getFieldLengthExp(int elementLen, int columnLen) {
        return String.format(DboCheckMessageUtil.FieldLengthExp, elementLen, columnLen) + DboCheckMessageUtil.NewLine;
    }

    private String getFieldPrecisionExp(int elementPre, int columnPre) {
        return String.format(DboCheckMessageUtil.FieldPrecisionExp, elementPre, columnPre) + DboCheckMessageUtil.NewLine;
    }

    private void checkDecimalLengthAndPre() {
        if (this.bizElement.getLength() > this.databaseColumn.getPrecision()) {
            this.expMessage.append(getFieldLengthExp(this.bizElement.getLength(), this.databaseColumn.getPrecision()));
        }
        if (this.bizElement.getPrecision() > this.databaseColumn.getScale()) {
            this.expMessage.append(getFieldPrecisionExp(this.bizElement.getPrecision(), this.databaseColumn.getScale()));
        }
    }

    // 需要判断的DBO类型是 VarChar NChar NVarChar(String) Decimal(Decimal)
    private boolean needCheckLengthAndPrec() {
        GspElementDataType dataType = this.bizElement.getMDataType();
        return dataType.equals(GspElementDataType.String) | dataType.equals(GspElementDataType.Decimal);
    }

}
