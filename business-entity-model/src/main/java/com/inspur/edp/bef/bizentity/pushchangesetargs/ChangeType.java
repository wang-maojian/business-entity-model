package com.inspur.edp.bef.bizentity.pushchangesetargs;

public enum ChangeType {
    /**
     * 新增
     */
    Add(1),
    /**
     * 修改
     */
    Modify(2),
    /**
     * 删除
     */
    Delete(0);

    private int intValue;
    private static java.util.HashMap<Integer, ChangeType> mappings;

    private synchronized static java.util.HashMap<Integer, ChangeType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, ChangeType>();
        }
        return mappings;
    }

    private ChangeType(int value) {
        intValue = value;
        ChangeType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static ChangeType forValue(int value) {
        return getMappings().get(value);
    }
}
