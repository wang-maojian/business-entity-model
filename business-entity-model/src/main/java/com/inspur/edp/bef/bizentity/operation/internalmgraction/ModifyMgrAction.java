package com.inspur.edp.bef.bizentity.operation.internalmgraction;

import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;

/**
 * 内置Modify操作
 */
public class ModifyMgrAction extends BizMgrAction implements IInternalMgrAction {
    public static final String id = "8Wna6KaBQEOHpRJfmGMBYQ";
    public static final String code = "Modify";
    public static final String name = MessageI18nUtils.getMessage("Modify");

    public ModifyMgrAction() {
        setID(id);
        setCode(code);
        setName(name);
    }

}