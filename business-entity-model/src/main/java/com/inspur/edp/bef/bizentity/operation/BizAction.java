package com.inspur.edp.bef.bizentity.operation;

import com.inspur.edp.bef.bizentity.beenum.BEOperationType;
import com.inspur.edp.bef.bizentity.operation.beomponent.BizActionParamCollection;
// import IBizParameterCollection;

/**
 * BE基本操作
 */
public class BizAction extends BizActionBase implements Cloneable {
    private final BizActionParamCollection beParams;

    public BizAction() {
        super();
        beParams = new BizActionParamCollection();
    }

    /**
     * 操作类型
     */
    @Override
    public BEOperationType getOpType() {
        return BEOperationType.BizAction;
    }

    @Override
    protected BizActionParamCollection getActionParameters() {
        return beParams;
    }
}