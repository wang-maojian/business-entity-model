package com.inspur.edp.bef.bizentity.increment.extract;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.increment.BizEntityIncrement;
import com.inspur.edp.bef.bizentity.increment.extract.action.MgrActionIncrementExtractor;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.collection.BizMgrActionCollection;
import com.inspur.edp.cef.designtime.api.increment.extractor.ExtractUtils;
import com.inspur.edp.das.commonmodel.controlrule.CmControlRule;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmControlRuleDef;
import com.inspur.edp.das.commonmodel.entity.GspCommonModel;
import com.inspur.edp.das.commonmodel.entity.increment.CommonModelIncrement;
import com.inspur.edp.das.commonmodel.entity.increment.extract.CommonModelExtractor;
import com.inspur.edp.das.commonmodel.entity.object.increment.extract.CommonObjectIncrementExtractor;
import lombok.var;

public class BizEntityExtractor extends CommonModelExtractor {

    public BizEntityExtractor() {
        super();
    }

    public BizEntityExtractor(boolean includeAll) {
        super(includeAll);
    }

    protected void extractExtendInfo(CommonModelIncrement increment, GspCommonModel oldModel, GspCommonModel newModel, CmControlRule rule, CmControlRuleDef def) {
        extractBizEntityInfo(increment, (GspBusinessEntity) oldModel, (GspBusinessEntity) newModel, rule, def);

    }

    private void extractBizEntityInfo(CommonModelIncrement increment, GspBusinessEntity oldBe, GspBusinessEntity newBe, CmControlRule rule, CmControlRuleDef def) {
        extractBizActions((BizEntityIncrement) increment, oldBe, newBe, rule, def);
        if (includeAll)
            extractBizEntityAllInfo(increment, oldBe, newBe);
    }

    private void extractBizEntityAllInfo(CommonModelIncrement increment, GspBusinessEntity oldBe, GspBusinessEntity newBe) {

        var changes = increment.getChangeProperties();
        ExtractUtils.extractValue(changes, oldBe.getDependentEntityId(), newBe.getDependentEntityId(), BizEntityJsonConst.DependentEntityId, null, null);
        ExtractUtils.extractValue(changes, oldBe.getDependentEntityName(), newBe.getDependentEntityName(), BizEntityJsonConst.DependentEntityName, null, null);
        ExtractUtils.extractValue(changes, oldBe.getDependentEntityPackageName(), newBe.getDependentEntityPackageName(), BizEntityJsonConst.DependentEntityPackageName, null, null);

        ExtractUtils.extractValue(changes, oldBe.getCacheConfiguration(), newBe.getCacheConfiguration(), BizEntityJsonConst.CacheConfiguration, null, null);
        ExtractUtils.extractValue(changes, oldBe.getEnableCaching(), newBe.getEnableCaching(), BizEntityJsonConst.EnableCaching, null, null);
        ExtractUtils.extractValue(changes, oldBe.getEnableTreeDtm(), newBe.getEnableTreeDtm(), BizEntityJsonConst.EnableTreeDtm, null, null);
        ExtractUtils.extractValue(changes, oldBe.getComponentAssemblyName(), newBe.getComponentAssemblyName(), BizEntityJsonConst.ComponentAssemblyName, null, null);
        ExtractUtils.extractValue(changes, oldBe.getCategory(), newBe.getCategory(), BizEntityJsonConst.Category, null, null);
        ExtractUtils.extractValue(changes, oldBe.getExtendType(), newBe.getExtendType(), BizEntityJsonConst.ExtendType, null, null);
        ExtractUtils.extractValue(changes, oldBe.getIsUsingTimeStamp(), newBe.getIsUsingTimeStamp(), BizEntityJsonConst.IsUsingTimeStamp, null, null);
        ExtractUtils.extractValue(changes, oldBe.getEnableApproval(), newBe.getEnableApproval(), BizEntityJsonConst.EnableApproval, null, null);
        ExtractUtils.extractValue(changes, oldBe.isTccSupported(), newBe.isTccSupported(), BizEntityJsonConst.TccSupported, null, null);
        ExtractUtils.extractValue(changes, oldBe.isAutoTccLock(), newBe.isAutoTccLock(), BizEntityJsonConst.AutoTccLock, null, null);
        ExtractUtils.extractValue(changes, oldBe.isAutoComplete(), newBe.isAutoComplete(), BizEntityJsonConst.AutoComplete, null, null);
        ExtractUtils.extractValue(changes, oldBe.isAutoCancel(), newBe.isAutoCancel(), BizEntityJsonConst.AutoCancel, null, null);
    }

    private void extractBizActions(BizEntityIncrement increment, GspBusinessEntity oldBe, GspBusinessEntity newBe, CmControlRule rule, CmControlRuleDef def) {

        BizMgrActionCollection oldActions = oldBe.getBizMgrActions();
        BizMgrActionCollection newActions = newBe.getBizMgrActions();
        if (oldActions.isEmpty() && newActions.isEmpty())
            return;

        if (newActions.isEmpty()) {
            //TODO 暂不支持删除动作
//            extractAllDeleteIncrement(increment, oldObj, extractor, rule, def);
            return;
        }
        var extractor = new MgrActionIncrementExtractor();
        for (BizOperation action : newActions) {
            BizMgrAction oldAction = (BizMgrAction) oldActions.getItem(action.getID());

            var actionIncrement = extractor.extractorIncrement(oldAction, (BizMgrAction) action, rule, def);
            if (actionIncrement == null)
                continue;
            increment.getActions().put(action.getID(), actionIncrement);
        }
    }

    @Override
    protected CommonObjectIncrementExtractor getCommonObjectIncrementExtractor() {
        return new BizObjectIncrementExtractor(this.includeAll);
    }

    protected CommonModelIncrement createCommonModelIncrement() {
        return new BizEntityIncrement();
    }
}
