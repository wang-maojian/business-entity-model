package com.inspur.edp.bef.bizentity.operation.componentbase;

import com.inspur.edp.bef.bizentity.operation.componentenum.BizParameterType;

/**
 * void类型的返回值
 */
public class BizVoidReturnType extends BizReturnValue {
    public static final String assembly = "mscorlib.dll";
    public static final String className = "Void";
    public static final String javaClassName = "void";

    /**
     * 参数类对应程序集类
     */
    @Override
    public String getAssembly() {
        return assembly;
    }

    @Override
    public void setAssembly(String value) {
    }

    /**
     * 参数类名
     */
    @Override
    public String getClassName() {
        return javaClassName;
    }

    @Override
    public void setClassName(String value) {
    }


    public String getNetClassName() {
        return className;
    }

    public void setNetClassName(String value) {

    }

    /**
     * 参数类型，与Assembly和ClassName关联
     */
    @Override
    public BizParameterType getParameterType() {
        return BizParameterType.Custom;
    }

    @Override
    public void setParameterType(BizParameterType value) {
    }

    /**
     * 新版判断返回值为空
     *
     * @return
     */
    public boolean isVoidReturnType() {
        return true;
    }

    public void setVoidReturnType(boolean voidReturnType) {

    }
}