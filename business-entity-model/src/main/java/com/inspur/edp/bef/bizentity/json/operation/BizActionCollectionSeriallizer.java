package com.inspur.edp.bef.bizentity.json.operation;

import com.inspur.edp.bef.bizentity.operation.collection.BizActionCollection;

public class BizActionCollectionSeriallizer extends BizOperationCollectionSerializer<BizActionCollection> {

    public BizActionCollectionSeriallizer() {
    }

    public BizActionCollectionSeriallizer(boolean full) {
        super(full);
        isFull = full;
    }

    @Override
    protected BizOperationSerializer getConvertor() {
        return new BizActionSerializer(isFull);
    }
}
