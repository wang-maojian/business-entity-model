package com.inspur.edp.bef.bizentity.util;

public class StringUtil {
    public static boolean checkNull(Object propValue) {
        if (propValue == null) {
            return true;
        }
        if (propValue.getClass().isAssignableFrom(String.class)) {
            String stringValue = (String) propValue;
            if (stringValue == null || stringValue.isEmpty()) {
                return true;
            }
        }
        return false;
    }

}
