package com.inspur.edp.bef.bizentity.controlrule.ruledefinition.parser;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity.BeControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.parser.CmRuleDefParser;

public class BeControlRuleDefParser extends CmRuleDefParser<BeControlRuleDef> {

    @Override
    protected final CmControlRuleDef createCmRuleDef() {
        return new BeControlRuleDef();
    }

//    protected  Class getChildTypes(String childTypeName) {
//        return BeEntityControlRuleDef.class;
//    }

    protected JsonDeserializer getChildDeserializer(String childTypeName) {
        return new BeEntityControlRuleDefParser();
    }
}
