package com.inspur.edp.bef.bizentity.util;

import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import io.iec.edp.caf.databaseobject.api.entity.DataType;

import java.util.Arrays;
import java.util.List;

public class DboTransferUtil {
    public static GspElementDataType getMDataTypeByDataType(DataType dataType, String exceptionCode) {
        // 列数据类型转换为字段数据类型
        switch (dataType) {
            case Int:
            case SmallInt:
                return GspElementDataType.Integer;
            case Decimal:
                return GspElementDataType.Decimal;
            case NVarchar:
            case NChar:
            case Varchar:
            case Char:
                return GspElementDataType.String;
            case Clob:
            case NClob:
                return GspElementDataType.Text;
            case DateTime:
                return GspElementDataType.Date;
            case TimeStamp:
                return GspElementDataType.DateTime;
            case Blob:
                return GspElementDataType.Binary;
            case Boolean:
                return GspElementDataType.Boolean;
            default:
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0065, dataType.toString());
        }
    }

    public static List<GspElementDataType> checkMDataTypeByDataType(DataType dataType, String exceptionCode) {
        // 列数据类型转换为字段数据类型
        switch (dataType) {
            case Int:
            case SmallInt:
                return Arrays.asList(GspElementDataType.Integer);
            case Decimal:
                return Arrays.asList(GspElementDataType.Decimal);
            case NVarchar:
            case NChar:
            case Varchar:
                return Arrays.asList(GspElementDataType.String);
            case Char:
                return Arrays.asList(GspElementDataType.Boolean, GspElementDataType.String);
            case Clob:
            case NClob:
                return Arrays.asList(GspElementDataType.Text);
            case DateTime:
            case TimeStamp:
                return Arrays.asList(GspElementDataType.Date, GspElementDataType.DateTime);
            case Blob:
                return Arrays.asList(GspElementDataType.Binary);
            case Boolean:
                return Arrays.asList(GspElementDataType.Boolean);
            default:
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0065, dataType.toString());

        }
    }
}
