package com.inspur.edp.bef.bizentity.pushchangesetargs;

import java.util.List;

public class PushChangeSet {
    private MetadataInfo metadataInfo;
    private List<RelatedMetadata> relatedMetadatas;


    private List<ActionChangeSet> actionChangeSets;
    private List<ObjectChangeSet> objectChangeSets;
    private List<ElementChangeSet> elementChangeSets;

    public PushChangeSet() {
    }

    public List<ActionChangeSet> getActionChangeSets() {
        return actionChangeSets;
    }

    public void setActionChangeSets(List<ActionChangeSet> actionChangeSets) {
        this.actionChangeSets = actionChangeSets;
    }

    public List<ObjectChangeSet> getObjectChangeSets() {
        return objectChangeSets;
    }

    public void setObjectChangeSets(List<ObjectChangeSet> objectChangeSets) {
        this.objectChangeSets = objectChangeSets;
    }


    public MetadataInfo getMetadataInfo() {
        return metadataInfo;
    }

    public void setMetadataInfo(MetadataInfo metadataInfo) {
        this.metadataInfo = metadataInfo;
    }

    public void setRelatedMetadatas(List<RelatedMetadata> relatedMetadatas) {
        this.relatedMetadatas = relatedMetadatas;
    }

    public List<RelatedMetadata> getRelatedMetadatas() {
        return relatedMetadatas;
    }


    public List<ElementChangeSet> getElementChangeSets() {
        return elementChangeSets;
    }

    public void setElementChangeSets(List<ElementChangeSet> elementChangeSets) {
        this.elementChangeSets = elementChangeSets;
    }

}
