package com.inspur.edp.bef.bizentity.increment.extract.determination;

import com.inspur.edp.bef.bizentity.increment.entity.determination.DtmIncrement;
import com.inspur.edp.bef.bizentity.operation.Determination;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonDataTypeControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeControlRuleDef;
import com.inspur.edp.cef.designtime.api.increment.extractor.AbstractIncrementExtractor;

public class DtmIncrementExtractor extends AbstractIncrementExtractor {

    public DtmIncrement extractorIncrement(Determination oldAction, Determination newAction, CommonDataTypeControlRule rule, CommonDataTypeControlRuleDef def) {

        if (oldAction == null && newAction == null) {
            return null;
        } else if (oldAction == null) {
            return new AddedDtmExtractor().extract(newAction);
        } else if (newAction == null) {
            return null;
//            return (CommonEntityIncrement)(oldHelpConfig != null && newAction == null ? this.getDeletedEntityExtractor().extract(oldHelpConfig) : this.getModifyEntityExtractor().extract(oldHelpConfig, newAction, rule, def));
        } else {
//            ModifyMgrActionIncrement increment = new ModifyMgrActionIncrement();
//            increment.setActionId(newAction.getID());
//            increment.setAction(newAction);
//            return increment;
            return null;
        }
    }
}
