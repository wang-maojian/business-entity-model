package com.inspur.edp.bef.bizentity.controlrule.rule;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.bef.bizentity.controlrule.rule.parser.BeObjectControlRuleParser;
import com.inspur.edp.bef.bizentity.controlrule.rule.serializer.BeObjectControlRuleSerializer;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity.BeEntityRuleDefNames;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.das.commonmodel.controlrule.CmEntityControlRule;

@JsonSerialize(using = BeObjectControlRuleSerializer.class)
@JsonDeserialize(using = BeObjectControlRuleParser.class)
public class BeObjControlRule extends CmEntityControlRule {

    public ControlRuleItem getAddUQConstraintControlRule() {
        return super.getControlRule(BeEntityRuleDefNames.AddUQConstraint);
    }

    public void setAddUQConstraintControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(BeEntityRuleDefNames.AddUQConstraint, ruleItem);
    }

    public ControlRuleItem getModifyUQConstraintControlRule() {
        return super.getControlRule(BeEntityRuleDefNames.ModifyUQConstraint);
    }

    public void setModifyUQConstraintControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(BeEntityRuleDefNames.ModifyUQConstraint, ruleItem);
    }

    public ControlRuleItem getAddDeterminationControlRule() {
        return super.getControlRule(BeEntityRuleDefNames.AddDtermination);
    }

    public void setAddDterminationControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(BeEntityRuleDefNames.AddDtermination, ruleItem);
    }

    public ControlRuleItem getModifyDterminationControlRule() {
        return super.getControlRule(BeEntityRuleDefNames.ModifyDetermination);
    }

    public void setModifyDterminationControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(BeEntityRuleDefNames.ModifyDetermination, ruleItem);
    }

    public ControlRuleItem getAddValidationControlRule() {
        return super.getControlRule(BeEntityRuleDefNames.AddValidation);
    }

    public void setAddValidationControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(BeEntityRuleDefNames.AddValidation, ruleItem);
    }

    public ControlRuleItem getModifyValidationControlRule() {
        return super.getControlRule(BeEntityRuleDefNames.ModifyValidation);
    }

    public void setModifyValidationControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(BeEntityRuleDefNames.ModifyValidation, ruleItem);
    }
}
