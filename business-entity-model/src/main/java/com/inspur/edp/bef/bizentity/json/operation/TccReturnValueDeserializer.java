package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.inspur.edp.bef.bizentity.operation.TccReturnValue;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;

public class TccReturnValueDeserializer extends BizParaDeserializer {

    @Override
    public final TccReturnValue deserialize(JsonParser jsonParser,
                                            DeserializationContext deserializationContext) {
        TccReturnValue rez = (TccReturnValue) super.deserialize(jsonParser, null);
        TccReturnValue newRez = new TccReturnValue();
        newRez.setID(rez.getID());
        newRez.setMode(rez.getMode());
        newRez.setAssembly(rez.getAssembly());
        newRez.setClassName(rez.getClassName());
        newRez.setCollectionParameterType(rez.getCollectionParameterType());
        return newRez;
    }

    @Override
    protected BizParameter createBizPara() {
        return new TccReturnValue();
    }
}
