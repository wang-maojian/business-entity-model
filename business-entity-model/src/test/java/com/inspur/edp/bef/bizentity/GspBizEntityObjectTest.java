package com.inspur.edp.bef.bizentity;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.json.object.BizObjectSerializer;
import io.iec.edp.caf.commons.core.SerializerFactory;
import io.iec.edp.caf.commons.core.api.DataSerializer;
import io.iec.edp.caf.commons.core.enums.SerializeType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @author zhangweiqing
 * @since 2024/5/29 15:13
 */
class GspBizEntityObjectTest {

    @Test
    void testSecurityElementID_null() throws IOException {
        //密级字段ID为null测试序列化、反序列化
        GspBizEntityObject gspBizEntityObject = new GspBizEntityObject();
        String json = toJson(gspBizEntityObject, false);
        //为null时，json中不存在密级字段ID
        Assertions.assertFalse(json.contains(BizEntityJsonConst.SecurityElementID));
        //为null时，反序列化中的密级字段也为null
        GspBizEntityObject newBeObj = fromJson(json, GspBizEntityObject.class);
        Assertions.assertNull(newBeObj.getSecurityElementID());
    }

    @Test
    void testSecurityElementID_null_full() throws IOException {
        //密级字段ID为null测试序列化、反序列化
        GspBizEntityObject gspBizEntityObject = new GspBizEntityObject();
        String json = toJson(gspBizEntityObject, true);
        //完整模式下，为null时，json中不存在密级字段ID
        Assertions.assertTrue(json.contains(BizEntityJsonConst.SecurityElementID));
        //为null时，反序列化中的密级字段也为null
        GspBizEntityObject newBeObj = fromJson(json, GspBizEntityObject.class);
        Assertions.assertNull(newBeObj.getSecurityElementID());
    }

    @Test
    void testSecurityElementID_empty() throws IOException {
        //密级字段ID为空字符串测试序列化、反序列化
        GspBizEntityObject gspBizEntityObject = new GspBizEntityObject();
        gspBizEntityObject.setSecurityElementID("");
        String json = toJson(gspBizEntityObject, false);
        //为空字符串时，json中不存在密级字段ID
        Assertions.assertFalse(json.contains(BizEntityJsonConst.SecurityElementID));
        //为空字符串时，反序列化中的密级字段为null
        GspBizEntityObject newBeObj = fromJson(json, GspBizEntityObject.class);
        Assertions.assertNull(newBeObj.getSecurityElementID());
    }

    @Test
    void testSecurityElementID_empty_full() throws IOException {
        //密级字段ID为空字符串测试序列化、反序列化
        GspBizEntityObject gspBizEntityObject = new GspBizEntityObject();
        gspBizEntityObject.setSecurityElementID("");
        String json = toJson(gspBizEntityObject, true);
        //为空字符串时，json中不存在密级字段ID
        Assertions.assertTrue(json.contains(BizEntityJsonConst.SecurityElementID));
        //为空字符串时，反序列化中的密级字段为null
        GspBizEntityObject newBeObj = fromJson(json, GspBizEntityObject.class);
        Assertions.assertEquals("", newBeObj.getSecurityElementID());
    }

    @Test
    void testSecurityElementID_not_empty() throws IOException {
        //密级字段ID为不为空字符串，测试序列化、反序列化
        GspBizEntityObject gspBizEntityObject = new GspBizEntityObject();
        gspBizEntityObject.setSecurityElementID("988de351-8b03-4dd0-9aa2-8fc1e65f8a35");
        String json = toJson(gspBizEntityObject, false);
        //为空字符串时，反序列化中的密级字段能正确赋值
        GspBizEntityObject newBeObj = fromJson(json, GspBizEntityObject.class);
        Assertions.assertEquals("988de351-8b03-4dd0-9aa2-8fc1e65f8a35", newBeObj.getSecurityElementID());
    }

    private static String toJson(GspBizEntityObject obj, boolean isFull) throws IOException {
        DataSerializer serializer = SerializerFactory.getSerializer(SerializeType.Json);
        ObjectMapper objectMapper = (ObjectMapper) serializer.getObjectMapper();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        JsonGenerator generator = objectMapper.createGenerator(outputStream);
        new BizObjectSerializer(isFull).serialize(obj, generator, null);
        generator.close();
        return outputStream.toString();
    }

    private static <T> T fromJson(String json, Class<T> clazz) {
        DataSerializer serializer = SerializerFactory.getSerializer(SerializeType.Json);
        return serializer.deserialize(json, clazz);
    }
}