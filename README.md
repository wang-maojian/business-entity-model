# 业务实体模型

### 介绍

采用DDD领域驱动设计方法，本仓库属于UBML低代码建模体系中领域层模型结构描述仓库。  
在DDD领域驱动设计中，领域层作为核心层，承载了领域核心的业务逻辑和数据结构。UBML采用领域模型驱动设计方法，引用业务实体（BusinessEntity，简称BE）描述领域模型，业务实体支持“充血模型”设计，包含了实体数据结构和丰富的业务逻辑建模，可以支撑业务逻辑开发中各种场景的建模。业务实体通过提供细粒度的业务逻辑建模支撑，通过业务实体将业务逻辑打散，进行细粒度拆分，然后在不同的时机将这些业务逻辑进行编排，最终实现业务逻辑的沉淀复用。

![alt UBML结构示意](figures/ubml-be-metadata-structure.png)

### 业务实体结构说明

#### 数据结构

在实体数据结构方面，UBML提供实体定义来描述DDD（领域驱动设计）中的实体，在一个业务实体模型中，包含一个到多个实体，业务实体是这些实体的聚合，在这些实体中有且仅有一个根实体（聚合根）。

在实体中，可以定义多个实体属性，实体属性可以是基本类型也可以是复杂类型。

支持设置实体之前的关联定义：主子实体之间的关联以及在实体属性上设置与其他业务实体的关联。

#### 业务逻辑

在业务逻辑方面，UBML提供自定义动作定义支撑领域服务建模，自定义动作是提供给外部调用的操作（其他业务实体调用、业务应用层调用等），在业务实体框架中内置了增删改查等基本操作，同时支持添加业务相关的自定义动作。

提供实体操作支撑业务实体内部不同实体中的业务操作建模，实体操作围绕一个实体实例，属于最细粒度的业务操作。实体操作只能在业务实体内部使用，允许自定义动作以及其他实体操作进行调用。

相对于自定义动作与实体动作这一类主动调用触发的业务操作，浪潮iGIX低代码平台提供了根据数据状态变化触发的业务逻辑—联动计算规则和数据校验规则，它们是围绕实体实例的最细粒度的业务规则，并且只要相应的字段数据产生变化就能够触发，从而保证业务规则的稳定。

联动计算规则是计算规则，允许在规则中进行数据的修改，数据校验规则是验证规则，只允许进行数据验证，不能够进行数据修改。
联动计算规则和数据校验规则，支持设置规则触发的数据状态（新增数据、修改数据和删除数据）以及规则触发的字段变化，保证规则只有在必要的时候执行，避免了冗余执行造成的额外性能损耗。

### 目录结构

/business-entity-model  
├── common-entity-model #通用实体模型，包含实体、值对象等公共结构；  
├── common-entity-manager #通用实体相关的管理类和工具类，比如代码模板生成等；  
├── common-model #通用模型，包括公共的实体聚合关系等；  
├── unified-datatype-model #统一数据类型模型结构  
├── unified-datatype-manager #统一数据类型模型管理相关内容，包括增删改查、校验等  
├── unified-datatype-webapi #统一数据类型模型提供的webapi接口  
├── business-entity-model #业务实体模型结构  
├── business-entity-manager #业务实体模型管理相关内容，包括增删改查、校验等

### 约束

**开发语言：**  
Java，开发框架SpringBoot  
**依赖仓库：**  
底层开发框架相关：
[caf-framework](https://gitee.com/ubml/caf-framework)、
[caf-boot](https://gitee.com/ubml/caf-boot)  
元数据基础框架：
[metadata-common](https://gitee.com/ubml/metadata-common)、
[metadata-service](https://gitee.com/ubml/metadata-service)、
[metadata-service-dev](https://gitee.com/ubml/metadata-service-dev)  
数据库对象相关：
[database-object-model](https://gitee.com/ubml/database-object-model)、
[database-object-service](https://gitee.com/ubml/database-object-service)  
**相关仓库：**
[business-entity-framework](https://gitee.com/ubml/business-entity-framework)、
[business-entity-generator](https://gitee.com/ubml/business-entity-generator)、
[business-entity-engine](https://gitee.com/ubml/business-entity-engine)

### 使用说明

待完善

### 参与贡献

参与贡献，请参照[如何贡献](https://gitee.com/ubml/community/blob/master/CONTRIBUTING.md)获取帮助。



