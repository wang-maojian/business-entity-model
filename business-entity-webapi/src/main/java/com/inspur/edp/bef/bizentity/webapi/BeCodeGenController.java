package com.inspur.edp.bef.bizentity.webapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.bef.bemanager.befdtconsistencycheckevent.BefDtConsistencyCheckEventBroker;
import com.inspur.edp.bef.bemanager.dependency.ManageProjectDependencyUtil;
import com.inspur.edp.bef.bemanager.dependency.MavenDependency;
import com.inspur.edp.bef.bemanager.expression.BefExpressionAdapter;
import com.inspur.edp.bef.bemanager.expression.entity.ExpressionSchema;
import com.inspur.edp.bef.bemanager.util.BefFileUtil;
import com.inspur.edp.bef.bemanager.validate.model.BizEntityChecker;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.actioneventargs.DeletingActionEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.entityeventargs.RemovingEntityEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.fieldeventargs.RemovingFieldEventArgs;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.gspbusinessentity.api.IGspBeExtendInfoService;
import com.inspur.edp.bef.bizentity.gspbusinessentity.entity.GspBeExtendInfo;
import com.inspur.edp.bef.bizentity.gspbusinessentitylabel.api.IGspBeLabelInfoService;
import com.inspur.edp.bef.bizentity.gspbusinessentitylabel.entity.GspBeLabelInfo;
import com.inspur.edp.cef.designtime.api.dtconsistencycheck.ConsistencyCheckEventMessage;
import com.inspur.edp.cef.designtime.api.util.MetadataUtil;
import com.inspur.edp.cef.spi.jsonser.base.StringUtils;
import com.inspur.edp.das.commonmodel.entity.object.GspCommonObjectType;
import com.inspur.edp.das.commonmodel.util.HandleAssemblyNameUtil;
import com.inspur.edp.das.commonmodel.util.MetadataProjectUtil;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.MetadataRTService;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import com.inspur.lcm.metadata.logging.LoggerDisruptorQueue;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectDtService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Path("")
@Produces(MediaType.APPLICATION_JSON)
public class BeCodeGenController {

    private final IDatabaseObjectDtService databaseObjectDtService;

    public BeCodeGenController(IDatabaseObjectDtService databaseObjectDtService){
        this.databaseObjectDtService = databaseObjectDtService;
    }

    @Autowired
    private IGspBeExtendInfoService beExtendInfoService;
    @Autowired
    private IGspBeLabelInfoService beLabelInfoService;

    /**
     * 根据id获取一条标签信息
     *
     * @return
     */
    @Path("beLabelInfo")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public GspBeLabelInfo getBeLabelInfo(@QueryParam("id") String id) {
        return this.beLabelInfoService.getBeLabelInfo(id);
    }

    /**
     * 获取所有标签信息
     *
     * @return
     */
    @Path("beLabelInfos")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<GspBeLabelInfo> getBeLabelInfos() {
        return this.beLabelInfoService.getBeLabelInfos();
    }

    /**
     * 保存标签配置列表
     *
     * @return
     */
    @Path("beLabelInfos")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public void saveBeLabelInfos(List<GspBeLabelInfo> infos) {
        this.beLabelInfoService.saveBeLabelInfos(infos);
    }

    /**
     * 获取所有扩展配置
     *
     * @return
     */
    @Path("beExtendInfo")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public GspBeExtendInfo getExtendConfigById(@QueryParam("id") String id) {
        return this.beExtendInfoService.getBeExtendInfo(id);
    }

    /**
     * 获取所有扩展配置
     *
     * @return
     */
    @Path("beExtendInfo/configId")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public GspBeExtendInfo getExtendConfigByConfigId(@QueryParam("configId") String configId) {
        return this.beExtendInfoService.getBeExtendInfoByConfigId(configId);
    }

    /**
     * 获取所有扩展配置
     *
     * @return
     */
    @Path("beExtendInfos")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<GspBeExtendInfo> getExtendConfigs() {
        return this.beExtendInfoService.getBeExtendInfos();
    }

    /**
     * 保存扩展配置列表
     *
     * @return
     */
    @Path("beExtendInfos")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public void saveExtendConfigs(List<GspBeExtendInfo> infos) {
        this.beExtendInfoService.saveGspBeExtendInfos(infos);
    }


    /**
     * 删除节点
     */
    @Path("befDtConsistencyCheck/removingEntity")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public RemovingEntityEventArgs removingEntity(RemovingEntityEventArgs args) {
        BefDtConsistencyCheckEventBroker beEventBroker = SpringBeanUtils
                .getBean(BefDtConsistencyCheckEventBroker.class);
        beEventBroker.fireRemovingEntity(args);

        if (!args.eventMessages.isEmpty()) {
            BefConsistencyCheckMessage(args.eventMessages, "请先解除该BE节点的依赖和关联信息");
        }
        return args;
    }


    /**
     * 删除字段
     */
    @Path("befDtConsistencyCheck/removingField")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public RemovingFieldEventArgs removingField(RemovingFieldEventArgs args) {
        BefDtConsistencyCheckEventBroker beEventBroker = SpringBeanUtils
                .getBean(BefDtConsistencyCheckEventBroker.class);
        beEventBroker.fireRemovingField(args);
        if (!args.eventMessages.isEmpty()) {
            BefConsistencyCheckMessage(args.eventMessages, "请先解除该BE字段的依赖和关联信息");
        }
        return args;
    }

    /**
     * 删除动作
     */
    @Path("befDtConsistencyCheck/deletingAction")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public DeletingActionEventArgs deletingAction(DeletingActionEventArgs args) {
        BefDtConsistencyCheckEventBroker beEventBroker = SpringBeanUtils
                .getBean(BefDtConsistencyCheckEventBroker.class);
        beEventBroker.fireDeletingAction(args);


        if (!args.eventMessages.isEmpty()) {
            BefConsistencyCheckMessage(args.eventMessages, "请先解除该动作的依赖信息");
        }
        return args;
    }



    /**
     * 添加默认maven引用
     *
     * @param metaProjectPath
     */
    @Path("handleProjectDependency")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public void handleProjectDependency(@QueryParam("path") String metaProjectPath) {
        ManageProjectDependencyUtil util = ManageProjectDependencyUtil.getInstance();
        util.addDefaultProjectDependency(BefFileUtil.getJavaProjPath(metaProjectPath));
    }

    /**
     * 添加默认maven引用
     *
     * @param javaProjectPath
     * @param jarPath         jar包所在路径
     * @param dependencyJson  dependency列表json
     */
    @Path("installCustomProjectDependency")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public void installCustomProjectDependency(@QueryParam("javaProjectPath") String javaProjectPath,
                                               @QueryParam("jarPath") String jarPath, String dependencyJson) {
        ManageProjectDependencyUtil util = ManageProjectDependencyUtil.getInstance();
        if (dependencyJson == null) {
            return;
        }
        ArrayList<MavenDependency> dependencies = new ArrayList<MavenDependency>();
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode node = mapper.readTree(dependencyJson);
            for (JsonNode item : node) {
                dependencies.add(mapper.readValue(item.toString(), MavenDependency.class));
            }
        } catch (IOException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, "MavenDependency");
        }
        util.addProjectDependency(javaProjectPath, jarPath, dependencies);
    }

    @Path("loadMetadata")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String loadMetadata(@QueryParam("relativePath") String relativePath,
                               @QueryParam("fileName") String fileName) throws JsonProcessingException {
        MetadataService service = SpringBeanUtils.getBean(MetadataService.class);
        GspMetadata metadata = service.loadMetadata(fileName, relativePath);
        return new ObjectMapper().writeValueAsString(metadata.getHeader());
    }

    @Path("handledBeMeta")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String loadMetadata(@QueryParam("beId") String beId) throws JsonProcessingException {
        GspBusinessEntity sourceBe = loadBe(beId);
        GspBusinessEntity beAfterToCamel = convertToCamel(sourceBe);
        GspMetadata newMeta = new GspMetadata();
        newMeta.setContent(beAfterToCamel);
        return new ObjectMapper().writeValueAsString(newMeta);
    }

    private GspBusinessEntity loadBe(String beId) {
        if (beId == null || beId.isEmpty()) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_COMMON_0003, "beId");
        }
        MetadataRTService service = SpringBeanUtils
                .getBean(MetadataRTService.class);
        GspMetadata meta = service.getMetadata(beId);
        if (meta == null) {
            meta = MetadataUtil.getCustomMetadata(beId);
        }
        if (meta == null) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0077, beId);
        }
        GspBusinessEntity be = ((GspBusinessEntity) meta.getContent()).clone();
        if (be == null) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0078, beId);
        }
        return be;
    }

    private GspBusinessEntity convertToCamel(GspBusinessEntity sourceBe) {
        GspBusinessEntity commonModel = (GspBusinessEntity) sourceBe.convertToCamelCaseProperty();
        // 所有节点编号camel
        commonModel.getAllObjectList().forEach(object -> {
            if (object.getObjectType() == GspCommonObjectType.ChildObject) {
                object.setCode(StringUtils.toCamelCase(object.getCode()));
            }
        });
        return commonModel;
    }

    @Path("expression")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getExpression(@QueryParam("beId") String beId) throws JsonProcessingException {
        GspBusinessEntity sourceBe = loadBe(beId);
        GspBusinessEntity beAfterToCamel = convertToCamel(sourceBe);
        // 调用Exception方法
        BefExpressionAdapter expressionAdapter = new BefExpressionAdapter();
        ExpressionSchema schema = expressionAdapter.convertCommonModel(beAfterToCamel);
        return new ObjectMapper().writeValueAsString(schema);
    }

    @Deprecated//迁移到runtime-customize-webapi
    @Path("generatedConfigId")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getGeneratedConfigId(@QueryParam("generatingAssembly") String generatingAssembly,
                                       @QueryParam("relativePath") String relativePath)
            throws JsonProcessingException {
        if (generatingAssembly == null || generatingAssembly.isEmpty()) {
            return "";
        }
        String packagePrefix = "";
        //传递了工程路径时，增加包前缀的相关处理
        if (!StringUtils.isNullOrEmpty(relativePath)) {
            packagePrefix = MetadataProjectUtil.getPackagePrefix(relativePath);
        }
        return new ObjectMapper()
                .writeValueAsString(HandleAssemblyNameUtil.convertToJavaPackageName(packagePrefix, generatingAssembly));
    }

    @Path("checkbe")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public void checkKeywords(String jsonObject) {
        if (jsonObject == null || jsonObject.isEmpty()) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0003);
        }
        ObjectMapper mapper = new ObjectMapper();
        GspBusinessEntity businessEntity;
        try {
            businessEntity = mapper.readValue(jsonObject, GspBusinessEntity.class);
        } catch (JsonProcessingException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, "GspBusinessEntity");
        }
        BizEntityChecker.getInstance().check(businessEntity);
    }

    /**
     * 控制台输出BEF Warnings
     *
     * @param messages
     */
    protected void BefConsistencyCheckMessage(List<ConsistencyCheckEventMessage> messages, String
            suffix) {
        StringBuilder befWarnings = new StringBuilder();
        if (!messages.isEmpty())
            messages.forEach(message -> befWarnings.append(message.message));
        befWarnings.append(suffix);
        String currentUerId = CAFContext.current.getUserId();
        if (currentUerId == null || currentUerId.isEmpty()) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0076);
        }
        LoggerDisruptorQueue.publishEvent("[BEF Warning]:" + befWarnings, currentUerId);
    }

    /**
     * 前端BE设计器检查新增字段编号是否为DBO关键字
     */
    @Path("checkCodeIsDboKeyWord")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public boolean checkCodeIsDboKeyWord(@QueryParam("codeValue") String codeValue) {
        return databaseObjectDtService.isCodeReservedWordsOrNot(codeValue);
    }
}
