package com.inspur.edp.bef.bizentity.webapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.TreeTraversingParser;
import com.inspur.edp.bef.bemanager.codegenerator.JavaCodeFileGenerator;
import com.inspur.edp.bef.bemanager.compcodebutton.CompPathButton;
import com.inspur.edp.bef.bemanager.compcodebutton.GenerateCompCodeButton;
import com.inspur.edp.bef.bemanager.generatecomponent.ComponentGenUtil;
import com.inspur.edp.bef.bemanager.generatecomponent.ComponentGenerator;
import com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators.TccActionComponentGenerator;
import com.inspur.edp.bef.bemanager.generatedbo.GenerateFromDbo;
import com.inspur.edp.bef.bemanager.pushchangesetevent.PushChangeSetEventBroker;
import com.inspur.edp.bef.bemanager.service.BeManagerService;
import com.inspur.edp.bef.bemanager.service.BusinessFieldService;
import com.inspur.edp.bef.bemanager.service.ImportCdmService;
import com.inspur.edp.bef.bemanager.service.UpdateElementCollectionService;
import com.inspur.edp.bef.bemanager.util.*;
import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.beenum.BEOperationType;
import com.inspur.edp.bef.bizentity.common.OperationConvertUtils;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.json.element.BizElementDeserializer;
import com.inspur.edp.bef.bizentity.json.element.BizElementSerializer;
import com.inspur.edp.bef.bizentity.json.model.BizEntityDeserializer;
import com.inspur.edp.bef.bizentity.json.model.BizEntitySerializer;
import com.inspur.edp.bef.bizentity.json.object.BizObjectDeserializer;
import com.inspur.edp.bef.bizentity.json.object.BizObjectSerializer;
import com.inspur.edp.bef.bizentity.json.operation.*;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.Determination;
import com.inspur.edp.bef.bizentity.operation.Validation;
import com.inspur.edp.bef.bizentity.operation.internalmgraction.TccSettingElement;
import com.inspur.edp.bef.bizentity.pushchangesetargs.PushChangeSet;
import com.inspur.edp.bef.bizentity.pushchangesetargs.PushChangeSetArgs;
import com.inspur.edp.bef.bizentity.pushchangesetargs.RelatedMetadata;
import com.inspur.edp.bef.bizentity.util.ReferenceService;
import com.inspur.edp.bef.bizentity.util.StringUtil;
import com.inspur.edp.bef.bizentity.util.changeset.ChangeSetInfoUtil;
import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import com.inspur.edp.cef.designtime.api.json.Variable.CommonVariableDeserializer;
import com.inspur.edp.cef.designtime.api.json.Variable.CommonVariableSerializer;
import com.inspur.edp.cef.designtime.api.json.operation.CommonDtmDeserializer;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;
import com.inspur.edp.cef.designtime.api.variable.CommonVariable;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.IGspCommonModel;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.collection.GspElementCollection;
import com.inspur.edp.das.commonmodel.entity.object.GspCommonObjectType;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataHeader;
import com.inspur.edp.lcm.metadata.api.entity.ProcessMode;
import com.inspur.edp.lcm.metadata.api.service.FileService;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import io.iec.edp.caf.common.JSONSerializer;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.rpc.api.service.RpcClient;
import lombok.Data;
import lombok.SneakyThrows;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

@Path("")
@Produces(MediaType.APPLICATION_JSON)
public class BeController {

    /**
     * BE保存并同步，同步dbo,生成构件元数据，构件代码模板，保存元数据
     *
     * @param metadataInfo
     */
    @Path("sysnbe")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public boolean saveAndSyn(String metadataInfo) {
        JsonNode node = getNodeFromWebJson(metadataInfo);

        isMetadataCodeExist(node);

        String path = node.get("path").textValue();
        String metadataName = node.get("name").textValue();
        boolean enableApproval = false;
        GspMetadata metadata = SpringBeanUtils.getBean(MetadataService.class).loadMetadata(metadataName, path);
        //校验元数据主子节点编号
        GspBusinessEntity businessEntity = (GspBusinessEntity) metadata.getContent();
        if (businessEntity.getMainObject().getContainChildObjects() != null && !businessEntity.getMainObject().getContainChildObjects().isEmpty()) {
            for (IGspCommonObject object : businessEntity.getMainObject().getContainChildObjects()) {
                if (object.getCode() != null && object.getCode().equals(businessEntity.getMainObject().getCode())) {
                    throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0070, metadata.getHeader().getName(), object.getCode());
                }
            }
        }
        //预置基础入口单据的RPC接口
        enableApproval = BeGuideUtil.getInstance().precastFlowFormPayload(metadata);
        // 主节点添加UDT字段
        // SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //System.out.println("----BEFLog----开始同步DBO，" + sdf.format(new Date()));
        BeManagerService.generateDbo(metadata, path, true);
        //System.out.println("----BEFLog----同步DBO完成，" + sdf.format(new Date()));
        //System.out.println("----BEFLog----开始同步构件元数据，" + sdf.format(new Date()));
        ArrayList<String> actionList = BeManagerService.generateComponent(metadata, path, false);

        if (actionList != null && !actionList.isEmpty() && BeManagerService.getProjectProcessMode(path) == ProcessMode.interpretation) {
            //System.out.println("----BEFLog----开始生成pom文件，" + sdf.format(new Date()));
            BeManagerService.generatePom(path, businessEntity.getName());
            //System.out.println("----BEFLog----生成pom文件完成，" + sdf.format(new Date()));
        }

        //System.out.println("----BEFLog----同步构件元数据完成。" + sdf.format(new Date()));
        //System.out.println("----BEFLog----开始同步构件代码，" + sdf.format(new Date()));
        BeManagerService.generateComponentCode(metadata, path, actionList);
        //System.out.println("----BEFLog----同步构件代码完成。" + sdf.format(new Date()));
        //System.out.println("----BEFLog----开始保存元数据，" + sdf.format(new Date()));
        BeManagerService.saveMetadata(metadata, path);
        //System.out.println("----BEFLog----保存元数据完成，" + sdf.format(new Date()));
        //System.out.println("----BEFLog----开始同步关联信息，" + sdf.format(new Date()));
        ReferenceService.getInstance().registerReference(metadata);
        //System.out.println("----BEFLog----同步关联信息完成。" + sdf.format(new Date()));
        return enableApproval;
    }

    @Path("checkBizTypeState")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public boolean checkBizTypeState(String boId) {
        return BeGuideUtil.getInstance().bizObjectContainBizType(boId);
    }

    public JsonNode getNodeFromWebJson(String json) {

        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = null;

        try {
            node = mapper.readTree(json);
        } catch (JsonProcessingException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0003, e);
        }
        return node;
    }

    /**
     * 选择业务字段
     *
     * @param info
     * @return
     */
    @Path("chooseUdt")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String updateUdtElementWhenChooseUdt(String info) {
        JsonNode node = getNodeFromWebJson(info);
        String refUdtId = node.get("refUdtId").textValue();
        String path = node.get("path").textValue();
        String beElementJson = node.get("udtElementJson").textValue();

        return UpdateElementCollectionService.getInstance().updateElementWithRefUdt(refUdtId, path, beElementJson, true);
    }

    @Path("updateUdt")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String updateUdtElementWhenLoading(String info) {
        JsonNode node = getNodeFromWebJson(info);
        String refUdtId = node.get("refUdtId").textValue();
        String path = node.get("path").textValue();
        String beElementJson = node.get("udtElementJson").textValue();

        return UpdateElementCollectionService.getInstance().updateElementWithRefUdt(refUdtId, path, beElementJson, false);
    }

    /**
     * 关联字段在修改对象类型页面，添加关联字段时触发调用
     */
    @Path("handleRefElement")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String hanldeRefElement(String info) {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(IGspCommonField.class, new BizElementSerializer());
        module.addDeserializer(IGspCommonField.class, new BizElementDeserializer());
        mapper.registerModule(module);
        JsonNode node = getNodeFromWebJson(info);
        String beElementJson = node.get("udtElementJson").textValue();
        GspBizEntityElement entityElement;
        String eleJson;
        try {
            entityElement = mapper.readValue(beElementJson, GspBizEntityElement.class);
            UpdateElementCollectionService.getInstance().updateFieldWithAsso(null, entityElement);
            eleJson = mapper.writeValueAsString(entityElement);
        } catch (JsonProcessingException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0013, e, "GspBizEntityElement");
        }

        return eleJson;
    }

    /**
     * 同步业务字段
     *
     * @param info
     * @return
     */
    @Path("handleElements")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public boolean handleElements(String info) {
        JsonNode node = getNodeFromWebJson(info);
        String fileName = node.get("fileName").textValue();
        String path = node.get("path").textValue();
        String nodeCode = node.get("nodeCode") != null ? node.get("nodeCode").textValue() : null;
        GspMetadata metadata = SpringBeanUtils.getBean(MetadataService.class).loadMetadata(fileName, path);

        GspBusinessEntity beEntity = (GspBusinessEntity) metadata.getContent();
        ArrayList<IGspCommonElement> elements = nodeCode == null ? beEntity.getAllElementList(false)
                : beEntity.findObjectByCode(nodeCode).getAllElementList(false);
        //先后有区别：先更新udt,后更新关联
        UpdateElementCollectionService.getInstance().handleUdtElements(path, elements);
        UpdateElementCollectionService.getInstance().handleRefElements(path, elements);
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(GspBusinessEntity.class, new BizEntitySerializer());
        module.addDeserializer(IGspCommonModel.class, new BizEntityDeserializer());
        mapper.registerModule(module);
        String eleJson;
        GspBusinessEntity businessEntity;
        try {
            eleJson = mapper.writeValueAsString((GspBusinessEntity) metadata.getContent());
            businessEntity = mapper.readValue(eleJson, GspBusinessEntity.class);
            metadata.setContent(businessEntity);
            BeManagerService.saveMetadata(metadata, path);
        } catch (JsonProcessingException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0013, e, "GspBusinessEntity");
        }

        return true;
    }

    /**
     * 根据业务字段生成关联字段信息
     *
     * @param info
     * @return
     */
    @Path("handleBusinessField")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String hanldeBusinessField(String info) {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(IGspCommonField.class, new BizElementSerializer());
        module.addDeserializer(IGspCommonField.class, new BizElementDeserializer());
        mapper.registerModule(module);

        BusinessFieldService businessFieldService = new BusinessFieldService();
        GspBizEntityElement bizEntityElement = businessFieldService.handelElements(info);
        String json;
        try {
            json = mapper.writeValueAsString(bizEntityElement);
        } catch (JsonProcessingException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0004, e, "GspBizEntityElement");
        }
        return json;
    }

    /**
     * 变量选择业务字段
     *
     * @param info
     * @return
     */
    @Path("chooseVarUdt")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String updateVarUdtElementWhenChooseUdt(String info) {
        JsonNode node = getNodeFromWebJson(info);
        String refUdtId = node.get("refUdtId").textValue();
        String path = node.get("path").textValue();
        String beElementJson = node.get("udtElementJson").textValue();

        return UpdateElementCollectionService.getInstance().updateVariableWithRefUdt(refUdtId, path, beElementJson, true);
    }

    @Path("updateVarUdt")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String updateVarUdtElementWhenLoading(String info) {
        JsonNode node = getNodeFromWebJson(info);
        String refUdtId = node.get("refUdtId").textValue();
        String path = node.get("path").textValue();
        String beElementJson = node.get("udtElementJson").textValue();

        return UpdateElementCollectionService.getInstance().updateVariableWithRefUdt(refUdtId, path, beElementJson, false);
    }

    @Path("handleRefVariable")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String hanldeRefVariable(String info) {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(IGspCommonField.class, new CommonVariableSerializer());
        module.addDeserializer(IGspCommonField.class, new CommonVariableDeserializer());
        mapper.registerModule(module);
        JsonNode node = getNodeFromWebJson(info);
        String beElementJson = node.get("udtElementJson").textValue();
        CommonVariable commonVariable;
        String eleJson;
        try {
            commonVariable = mapper.readValue(beElementJson, CommonVariable.class);
            UpdateElementCollectionService.getInstance().updateFieldWithAsso(null, commonVariable);
            eleJson = mapper.writeValueAsString(commonVariable);
        } catch (JsonProcessingException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0013, e, "CommonVariable");
        }

        return eleJson;
    }

    /**
     * BE设计器内选择导入CDM字段功能后，在选择完CDM文件生成对应的CDM字段时调用
     *
     * @param info
     * @return
     */
    @Path("uploadCdm")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String uploadCdm(String info) {
        JsonNode node = getNodeFromWebJson(info);
        String xml = node.get("xml").textValue();
        return ImportCdmService.getInstance().convertToJson(xml);
    }

    @Path("convertCdmTableToBizElements")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String convertCdmTableToBizElements(String info) {
        JsonNode node = getNodeFromWebJson(info);
        String columnsJson = node.get("columnsJson").textValue();
        return ImportCdmService.getInstance().convertCdmTableToBizElements(columnsJson);
    }

    /**
     * 创建BE元数据，根据DBO创建
     *
     * @param info
     * @return
     */
    @Path("convertDboToBizEntity")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String convertDboToBizEntity(String info) {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(IGspCommonModel.class, new BizEntitySerializer());
        module.addDeserializer(IGspCommonModel.class, new BizEntityDeserializer());
        mapper.registerModule(module);
        try {
            JsonNode node = mapper.readTree(info);
            String path = node.get("path").textValue();
            String dboId = node.get("dboId").textValue();
            String primaryKey = node.get("primaryKey").textValue();
            String beJson = node.get("content").textValue();
            DataValidatorUtil.CheckForEmptyString(path, "path");
            DataValidatorUtil.CheckForEmptyString(dboId, "dboId");
            DataValidatorUtil.CheckForEmptyString(primaryKey, "primaryKey");
            DataValidatorUtil.CheckForEmptyString(beJson, "当前BE");
            GspBusinessEntity newBe = mapper.readValue(beJson, GspBusinessEntity.class);

            GspBizEntityObject mainObj = GenerateFromDbo.getInstance().convertDboToBizObject(path, dboId, primaryKey);
            mainObj.setCode(newBe.getCode());
            mainObj.setName(newBe.getName());
            mainObj.setObjectType(GspCommonObjectType.MainObject);
            newBe.setMainObject(mainObj);
            // 处理版本字段
            newBe.getVersionContronInfo().setVersionControlElementId(null);
            if (mainObj.getElementByLabelId("Version") instanceof IGspCommonElement) {
                if (mainObj.getElementByLabelId("Version").getMDataType() == GspElementDataType.DateTime) {
                    newBe.getVersionContronInfo().setVersionControlElementId(mainObj.getElementByLabelId("Version").getID());
                }
            }
            return mapper.writeValueAsString(newBe);
        } catch (JsonProcessingException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0013, e, "GspBusinessEntity");
        }

    }

    /**
     * 根据dbo子对象创建BE子对象
     *
     * @param info
     * @return
     */
    @Path("addDboToParentObject")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String addDboToParentObject(String info) {

        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(IGspCommonDataType.class, new BizObjectSerializer());
        module.addDeserializer(IGspCommonDataType.class, new BizObjectDeserializer());
        mapper.registerModule(module);
        try {
            JsonNode node = mapper.readTree(info);
            String path = node.get("path").textValue();
            String dboId = node.get("dboId").textValue();
            String columnIdsJson = node.get("columnIdsJson").textValue();
            String primaryKey = node.get("primaryKey").textValue();
            String parentId = node.get("parentId").textValue();
            String parentObjectIdElementId = node.get("parentObjectIdElementId").textValue();
            String parentObjectIdElementName = node.get("parentObjectIdElementName").textValue();
            String newChildObjCode = node.get("newChildObjCode").textValue();
            String newChildObjName = node.get("newChildObjName").textValue();

            GspBizEntityObject childObject = convertDboToChildObj(path, dboId, primaryKey, parentId, parentObjectIdElementId, parentObjectIdElementName);
            handleCheckedColumns(childObject, columnIdsJson, primaryKey, parentId);
            handleChildObjectCodeName(childObject, newChildObjCode, newChildObjName);

            String objJson = mapper.writeValueAsString(childObject);
            return objJson;
        } catch (JsonProcessingException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0004, e, "GspBusinessEntity");
        }

    }


    private void handleChildObjectCodeName(GspBizEntityObject childObject, String newChildObjCode, String newChildObjName) {
        if (!CheckInfoUtil.checkNull(newChildObjCode)) {
            childObject.setCode(newChildObjCode);
        }
        if (!CheckInfoUtil.checkNull(newChildObjName)) {
            childObject.setName(newChildObjName);
        }
    }

    private GspBizEntityObject convertDboToChildObj(String path, String dboId, String primaryKey, String parentId, String parentObjectIdElementId, String parentObjectIdElementName) {
        GspBizEntityObject childObject = GenerateFromDbo.getInstance().convertDboToBizObject(path, dboId, primaryKey);
        childObject.setObjectType(GspCommonObjectType.ChildObject);
        GspBizEntityElement parentElement = (GspBizEntityElement) childObject.getContainElements().stream().filter(item -> ((IGspCommonElement) item).getColumnID().equals(parentId)).findFirst().orElse(null);
        if (parentElement == null) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0075);
        }
        childObject.convertToChildObject(parentElement.getID(), parentObjectIdElementId, parentObjectIdElementName);
        return childObject;
    }

    private void handleCheckedColumns(GspBizEntityObject childObject, String columnIdsJson, String primaryKey, String parentId) {
        List<String> ids = readIdList(columnIdsJson);
        if (ids == null || ids.isEmpty()) {
            return;
        }
        if (!ids.isEmpty()) {
            if (!ids.contains(primaryKey)) {
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0073);
            }
            if (!ids.contains(parentId)) {
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0074);
            }
            GspElementCollection elements = childObject.getContainElements().clone(childObject, null);
            childObject.getContainElements().clear();
            childObject.getContainElements().addAll(elements.stream().filter(item -> ids.contains(((IGspCommonElement) item).getColumnID())).collect(Collectors.toList()));
        }
    }

    private List<String> readIdList(String beEleIds) {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(String.class, new StringDeserializer());
        mapper.registerModule(module);
        JavaType type = mapper.getTypeFactory().
                constructCollectionType(List.class, String.class);
        List<String> ids = null;
        try {
            ids = mapper.readValue(beEleIds, type);
        } catch (JsonProcessingException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, "IdList");
        }
        return ids;
    }

    /**
     * @param info
     * @return
     */
    @Path("addDboColumns")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String addDboColumns(String info) {

        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(IGspCommonField.class, new BizElementSerializer());
        module.addDeserializer(IGspCommonField.class, new BizElementDeserializer());
        mapper.registerModule(module);
        try {
            JsonNode node = mapper.readTree(info);
            String path = node.get("path").textValue();
            String dboId = node.get("dboId").textValue();
            String columnIdsJson = node.get("columnIdsJson").textValue();
            List<String> columnIds = readIdList(columnIdsJson);

            GspElementCollection elements = GenerateFromDbo.getInstance().ConvertDboColumnsToElements(path, dboId, columnIds, null);

            return mapper.writeValueAsString(elements);
        } catch (JsonProcessingException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0004, e, "GspBusinessEntity");
        }
    }

    /**
     * 获取BE依赖的元数据（当前BO）
     *
     * @param info
     * @return
     */
    @Path("getRefMetadataList")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String getRefMetadataList(String info) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = null;
        try {
            node = mapper.readTree(info);
            String metadataPath = node.get("metadataPath").textValue();
            String BizEntityId = node.get("BizEntityId").textValue();
            MetadataService metadataService = SpringBeanUtils.getBean(com.inspur.edp.lcm.metadata.api.service.MetadataService.class);
            List<GspMetadata> gspMetadataList = metadataService.getMetadataListByRefedMetadataId(metadataPath, BizEntityId);
            ArrayList<RelatedMetadata> infos = new ArrayList<>();
            gspMetadataList.
                    forEach(item -> {
                        MetadataHeader header = item.getHeader();
                        if (item.getHeader().getType().equals("GSPViewModel")) {
                            infos.add(new RelatedMetadata(header.getNameSpace(), header.getCode(), header.getName(), "GSPViewModel"));
                        }
                        ;
                    });
            return mapper.writeValueAsString(infos);
        } catch (JsonProcessingException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0004, e, "GspMetadata");
        }
    }

    /**
     * 推送BE 变更集
     *
     * @param info 变更内容
     */
    @Path("pushChangeSet")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public void pushChangeSet(JsonNode info) {
        PushChangeSet changeSet = ChangeSetInfoUtil.getChangeSetInfo(info);
        if (changeSet != null) {
            PushChangeSetEventBroker pushEventBroker = SpringBeanUtils.getBean(PushChangeSetEventBroker.class);
            pushEventBroker.firePushChangeSet(new PushChangeSetArgs(changeSet));
        }
    }


    /**
     * 推送数据集
     */
    @Path("pushDataSet")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String pushDataSet(JsonNode info) {
        RpcClient client = SpringBeanUtils.getBean(RpcClient.class);
        String dataSetName = info.get("dataSetName").textValue();
        String metadataName = info.get("fileName").textValue();
        String relativePath = info.get("relativePath").textValue();
        GspMetadata metadata = SpringBeanUtils.getBean(MetadataService.class).loadMetadata(metadataName, relativePath);

        LinkedHashMap<String, Object> paramMap = new LinkedHashMap<String, Object>();
        paramMap.put("gspMetadata", metadata);
        String bizTypeId = BeGuideUtil.getInstance().getBizTypeId(metadata.getHeader().getBizobjectID());
        paramMap.put("productId", bizTypeId);
        paramMap.put("name", dataSetName);
        try {
            return client.invoke(String.class
                    , "com.inspur.gs.gsp.qdp.qdpdatasetinfo.service.DatasetInfoInnerService.generateDataSetByBe"
                    , "bcc", paramMap, null);
        } catch (Throwable e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_COMMON_0007, e, "com.inspur.gs.gsp.qdp.qdpdatasetinfo.service.DatasetInfoInnerService.generateDataSetByBe");
        }
    }

    @Path("getDataSetInfoByBeId")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String getDataSetInfoByBeId(String beId) {
        RpcClient client = SpringBeanUtils.getBean(RpcClient.class);
        LinkedHashMap<String, Object> paramMap = new LinkedHashMap<String, Object>();
        paramMap.put("beId", beId);
        try {
            return client.invoke(String.class
                    , "com.inspur.gs.gsp.qdp.qdpdatasetinfo.service.DatasetInfoInnerService.getDataSetInfoByBeId"
                    , "bcc", paramMap, null);
        } catch (CAFRuntimeException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_COMMON_0007, e, "com.inspur.gs.gsp.qdp.qdpdatasetinfo.service.DatasetInfoInnerService.getDataSetInfoByBeId");
        }
    }


    /**
     * 获取BE动作构件代码
     *
     * @param info
     * @return
     */
    @Path("bizOperationComponentCode")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String getBizOperationComponentCode(JsonNode info) {
        String id = info.get("metadataId").textValue();
        String projectPath = info.get("metadataPath").textValue();
        String type = info.get("actionType").textValue();
        String action = info.get("actionCode").textValue();
        ArrayList<String> actionList = new ArrayList<>();
        actionList.add(action);
        GspMetadata metadata = SpringBeanUtils.getBean(MetadataService.class).loadMetadataByMetadataId(id, projectPath);
        GspBusinessEntity be = (GspBusinessEntity) metadata.getContent();
        String relativePath = metadata.getRelativePath();
        String compAssemblyName = ComponentGenUtil.GetComponentNamespace(relativePath);

        GenerateCompCodeButton button = new GenerateCompCodeButton();
        String code = button.getCompCodeButton(be, type, relativePath, actionList, compAssemblyName);

        return code;
    }

    /**
     * 获取BE动作构件代码路径
     *
     * @param info
     * @return
     */
    @Path("bizOperationComponentCodePath")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String getBizOperationComponentCodePath(JsonNode info) {
        String id = info.get("metadataId").textValue();
        String projectPath = info.get("metadataPath").textValue();
        String type = info.get("actionType").textValue();
        String action = info.get("actionCode").textValue();
        ArrayList<String> actionList = new ArrayList<>();
        actionList.add(action);
        GspMetadata metadata = SpringBeanUtils.getBean(MetadataService.class).loadMetadataByMetadataId(id, projectPath);
        GspBusinessEntity be = (GspBusinessEntity) metadata.getContent();
        String relativePath = metadata.getRelativePath();
        String compAssemblyName = ComponentGenUtil.GetComponentNamespace(relativePath);

        CompPathButton button = new CompPathButton();
        String filePath = button.getCompPath(be, type, relativePath, actionList, compAssemblyName);
        filePath.replaceAll("\\\\", Matcher.quoteReplacement(File.separator));
        filePath.replaceAll("/", Matcher.quoteReplacement(File.separator));
        return filePath;
    }

    /**
     * 新版BE设计器在事件页面编辑代码，以及在代码页面打开构件代码文件时调用
     */
    @Path("eventComponentCodePath")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String getEventComponentCodePath(JsonNode info) {
        String compMetaDataId = info.get("compMetaDataId").textValue();
        if (compMetaDataId == null || compMetaDataId.isEmpty()) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_COMMON_0001);
        }
        String projectPath = info.get("metadataPath").textValue();
        if (projectPath == null || projectPath.isEmpty()) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_COMMON_0002);
        }
        GspMetadata compMetaData = SpringBeanUtils.getBean(MetadataService.class).getRefMetadata(projectPath, compMetaDataId);
        if (compMetaData == null) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0072, compMetaDataId);
        }
        return BeManagerService.getCodePath(projectPath, compMetaData);
    }

    /**
     * 判断当前元数据是否在当前BO内
     *
     * @param info
     * @return
     */
    @Path("isMetadataExist")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public boolean isMetadataExist(JsonNode info) {
        if (StringUtil.checkNull(info)) {
            return false;
        }
        String path = info.get("path").textValue();
        String metadataFileName = info.get("metadataFileName").textValue();
        MetadataService service = SpringBeanUtils.getBean(com.inspur.edp.lcm.metadata.api.service.MetadataService.class);
        return service.isMetadataExist(path, metadataFileName);
    }


    private void isMetadataCodeExist(JsonNode info) {
        if (StringUtil.checkNull(info)) {
            return;
        }
        String metadataName = info.get("name").textValue();
        String projectPath = info.get("path").textValue();

        GspMetadata metadata = SpringBeanUtils.getBean(MetadataService.class).loadMetadata(metadataName, projectPath);
        GspBusinessEntity be = (GspBusinessEntity) metadata.getContent();
        List<CheckComUtil> checkInfo = new ArrayList<>();
        getNewActions(be, checkInfo);
        CompPathButton button = new CompPathButton();
        String relativePath = metadata.getRelativePath();
        if (checkInfo.isEmpty()) {
            return;
        }
        for (CheckComUtil checkComUtil : checkInfo) {
            ArrayList<String> actionList = new ArrayList<>();
            actionList.add(checkComUtil.getAction());
            String filePath = button.getFilePath(be, checkComUtil.getType(), actionList, relativePath);
            FileService fsService = SpringBeanUtils.getBean(FileService.class);
            filePath.replaceAll("\\\\", Matcher.quoteReplacement(File.separator));
            filePath.replaceAll("/", Matcher.quoteReplacement(File.separator));
            if (fsService.isFileExist(filePath)) {
                button.JavaReplace(filePath);
//                throw new RuntimeException("BE元数据:"+be.getName()+"上新增的 "+checkComUtil.getChangeInfo()+checkComUtil.getAction()+" 已存在构件代码。请修改其编号或按路径删除旧java代码文件后，再执行保存。"+"\r\n代码路径："+filePath);
            }
        }
    }

    private void getNewActions(GspBusinessEntity be, List<CheckComUtil> checkInfo) {
        for (BizOperation bizMgrAction : be.getCustomMgrActions()) {
            if (bizMgrAction.getComponentId() == null || "".equals(bizMgrAction.getComponentId())) {
                CheckComUtil tem = new CheckComUtil();
                tem.setType("BizActions");
                tem.setAction(bizMgrAction.getCode());
                tem.setChangeInfo("对外接口方法");
                checkInfo.add(tem);
            }
        }
        for (GspBizEntityObject object : be.getAllNodes()) {
            for (BizOperation bizAction : object.getCustomBEActions()) {
                if (bizAction.getComponentId() == null || "".equals(bizAction.getComponentId())) {
                    CheckComUtil tem = new CheckComUtil();
                    tem.setType("EntityActions");
                    tem.setAction(bizAction.getCode());
                    tem.setChangeInfo("节点" + object.getName() + "的内部方法");
                    checkInfo.add(tem);
                }
            }
        }
        for (GspBizEntityObject object : be.getAllNodes()) {
            for (BizOperation determination : object.getDeterminations()) {
                if (determination.getComponentId() == null || "".equals(determination.getComponentId())) {
                    CheckComUtil tem = new CheckComUtil();
                    tem.setType("Determinations");
                    tem.setAction(determination.getCode());
                    tem.setChangeInfo("节点" + object.getName() + "的事件");
                    checkInfo.add(tem);
                }
            }
        }
        for (GspBizEntityObject object : be.getAllNodes()) {
            for (BizOperation validation : object.getValidations()) {
                if (validation.getComponentId() == null || "".equals(validation.getComponentId())) {
                    CheckComUtil tem = new CheckComUtil();
                    tem.setType("Validations");
                    tem.setAction(validation.getCode());
                    tem.setChangeInfo("对象" + object.getName() + "的校验规则");
                    checkInfo.add(tem);
                }
            }
        }
        for (GspBizEntityObject object : be.getAllNodes()) {
            for (TccSettingElement ele : object.getTccSettings()) {
                if (ele.getTccAction().getComponentId() == null || "".equals(ele.getTccAction().getComponentId())) {
                    CheckComUtil tem = new CheckComUtil();
                    tem.setType("TccActions");
                    tem.setAction(ele.getTccAction().getCode());
                    tem.setChangeInfo("节点" + object.getName() + "的TCC配置");
                    checkInfo.add(tem);
                }
            }
        }
        for (CommonDetermination dtm : be.getVariables().getDtmAfterModify()) {
            if (dtm.getComponentId() == null || dtm.getComponentId().isEmpty()) {
                CheckComUtil tem = new CheckComUtil();
                tem.setType("VarDeterminations");
                tem.setAction(dtm.getCode());
                tem.setChangeInfo("自定义变量联动计算");
                checkInfo.add(tem);
            }
        }
    }

    @SneakyThrows
    @Path("deletecompcode")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteCompAndCode(JsonNode node) {
        ArrayNode compIdsNode = (ArrayNode) Objects.requireNonNull(node.get("compIds"), "构件id列表不能为空");
        String path = Objects.requireNonNull(node.get("path"), "路径不能为空").textValue();
        List<String> compIds = new ArrayList<>(compIdsNode.size());
        compIdsNode.iterator().forEachRemaining(item -> compIds.add(Objects.requireNonNull(item.textValue(), "构件id不能为空")));

        MetadataProjectService projService = SpringBeanUtils.getBean(MetadataProjectService.class);
        MetadataService metadataService = SpringBeanUtils.getBean(MetadataService.class);
        FileService fService = SpringBeanUtils.getBean(FileService.class);
        for (String compId : compIds) {
            if (!metadataService.isMetadataExistInProject(path, compId)) {
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0071, path, compId);
            }
        }
        String compPath = projService.getJavaCompProjectPath(path);
        for (String compId : compIds) {
            GspMetadata compMetaData = metadataService.loadMetadataByMetadataId(compId, path);
            String javaClass = ((GspComponent) compMetaData.getContent()).getMethod().getClassName();
            javaClass = javaClass.replace(".", File.separator).concat(".java");
            String codePath = CheckInfoUtil.getCombinePath(compPath, javaClass);
            metadataService.deleteMetadata(compMetaData.getRelativePath(), compMetaData.getHeader().getFileName());
            if (fService.isFileExist(codePath)) {
                fService.fileDelete(codePath);
            }
        }
    }

    private FutureTask generatePomAsync(String path) {
        if (BeManagerService.getProjectProcessMode(path) != ProcessMode.interpretation) {
            return null;
        }
        Callable mc = () -> {
            BeManagerService.generatePom(path, path);
            return null;
        };

        FutureTask ft = new FutureTask(mc);

        Thread t1 = new Thread(ft);
        t1.start();
        return ft;
    }

    @SneakyThrows
    @Path("buildvarcompcode")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public BuildCompCodeResult createVariableCompAndCode(JsonNode node) {
        JsonNode beContent = node.get("beContent");
        String path = node.get("path").textValue();
        String bizObjectId = node.get("bizObjectId").textValue();
        JsonNode opContent = node.get("opContent");
        FutureTask pomFt = generatePomAsync(path);
        GspBusinessEntity be = JSONSerializer.getObjectMapper().readValue(
                new TreeTraversingParser(beContent), GspBusinessEntity.class);
        TreeTraversingParser opParser = new TreeTraversingParser(opContent);
        opParser.nextToken();
        CommonDetermination dtm = (CommonDetermination) new CommonDtmDeserializer()
                .deserialize(opParser, null);

        GspMetadata compMeta = ComponentGenerator.getInstance().createCompByVarDtm(
                dtm, be, path, bizObjectId);
        dtm.setComponentId(compMeta.getHeader().getId());
        dtm.setComponentName(compMeta.getHeader().getCode());
        new JavaCodeFileGenerator(path, be).createVarDtmJavaCodeFile(dtm);
        String codePath = BeManagerService.getCodePath(path, compMeta);
        if (pomFt != null)
            pomFt.get();
        return new BuildCompCodeResult(compMeta.getHeader(), codePath);
    }

    @SneakyThrows
    @Path("buildtcccompcode")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public BuildCompCodeResult createTccCompAndCode(JsonNode node) {
        JsonNode beContent = node.get("beContent");
        String path = node.get("path").textValue();
        String bizObjectId = node.get("bizObjectId").textValue();
        String nodeCode = node.get("nodeCode").textValue();
        boolean isChild = node.get("isChild").booleanValue();
        JsonNode opContent = node.get("opContent");
        FutureTask pomFt = generatePomAsync(path);

        GspBusinessEntity be = JSONSerializer.getObjectMapper().readValue(
                new TreeTraversingParser(beContent), GspBusinessEntity.class);
        TreeTraversingParser opParser = new TreeTraversingParser(opContent);
        opParser.nextToken();
        TccSettingElement operation = new TccSettingElementDeserializer().deserialize(
                opParser, null);
        operation.setOwner(isChild ? (GspBizEntityObject) be.findObjectByCode(nodeCode) : be.getMainObject());
        operation.getTccAction().setOwner(operation.getOwner());

        GspMetadata compMeta = TccActionComponentGenerator.getInstance().createComponent(
                operation, path, bizObjectId, nodeCode);
        operation.getTccAction().setComponentId(compMeta.getHeader().getId());
        operation.getTccAction().setComponentName(compMeta.getHeader().getCode());
        new JavaCodeFileGenerator(path, be).createJavaCodeFile(operation.getTccAction(), isChild, nodeCode);
        String codePath = BeManagerService.getCodePath(path, compMeta);
        if (pomFt != null)
            pomFt.get();
        return new BuildCompCodeResult(compMeta.getHeader(), codePath);
    }

    @SneakyThrows
    @Path("builddtmcompcode")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public BuildCompCodeResult createDtmCompAndCode(JsonNode node) {
        JsonNode beContent = node.get("beContent");
        String path = node.get("path").textValue();
        String bizObjectId = node.get("bizObjectId").textValue();
        String nodeCode = node.get("nodeCode").textValue();
        boolean isChild = node.get("isChild").booleanValue();
        String triggerTimePointType = node.get("triggerTimePointType").textValue();
        JsonNode opContent = node.get("opContent");
        FutureTask pomFt = generatePomAsync(path);

        GspBusinessEntity be = JSONSerializer.getObjectMapper().readValue(
                new TreeTraversingParser(beContent), GspBusinessEntity.class);
        TreeTraversingParser jsonParser = new TreeTraversingParser(opContent);
        jsonParser.nextToken();
        CommonDetermination commonDtm = (CommonDetermination) new BizCommonDeterminationDeSerializer()
                .deserialize(jsonParser, null);

        Determination operation = OperationConvertUtils.convertToDtm(commonDtm, triggerTimePointType,
                isChild ? (GspBizEntityObject) be.findObjectByCode(nodeCode) : be.getMainObject());
        GspMetadata metadata = BeManagerService.createCompCode(path, bizObjectId, nodeCode, isChild, be, operation);
        String codePath = BeManagerService.getCodePath(path, metadata);
        if (pomFt != null)
            pomFt.get();
        return new BuildCompCodeResult(metadata.getHeader(), codePath);
    }

    @SneakyThrows
    @Path("buildvalcompcode")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public BuildCompCodeResult createValCompAndCode(JsonNode node) {
        JsonNode beContent = node.get("beContent");
        String path = node.get("path").textValue();
        String bizObjectId = node.get("bizObjectId").textValue();
        String nodeCode = node.get("nodeCode").textValue();
        boolean isChild = node.get("isChild").booleanValue();
        String triggerTimePointType = node.get("triggerTimePointType").textValue();
        JsonNode opContent = node.get("opContent");
        FutureTask pomFt = generatePomAsync(path);

        GspBusinessEntity be = JSONSerializer.getObjectMapper().readValue(
                new TreeTraversingParser(beContent), GspBusinessEntity.class);
        TreeTraversingParser jsonParser = new TreeTraversingParser(opContent);
        jsonParser.nextToken();
        CommonValidation commonOperation = (CommonValidation) new BizCommonValdationDeSerializer()
                .deserialize(jsonParser, null);
        Validation operation = OperationConvertUtils.convertToVal(commonOperation, triggerTimePointType,
                isChild ? (GspBizEntityObject) be.findObjectByCode(nodeCode) : be.getMainObject());
        GspMetadata metadata = BeManagerService.createCompCode(path, bizObjectId, nodeCode, isChild, be, operation);
        String codePath = BeManagerService.getCodePath(path, metadata);
        if (pomFt != null)
            pomFt.get();
        return new BuildCompCodeResult(metadata.getHeader(), codePath);
    }

    /**
     *
     */
    @SneakyThrows
    @Path("buildcompcode")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public BuildCompCodeResult createCompAndCode(JsonNode node) {
        JsonNode beContent = node.get("beContent");
        String path = node.get("path").textValue();
        String bizObjectId = node.get("bizObjectId").textValue();
        String nodeCode = node.get("nodeCode").textValue();
        boolean isChild = node.get("isChild").booleanValue();
        BEOperationType opType = Enum.valueOf(BEOperationType.class, node.get("opType").textValue());
        JsonNode opContent = node.get("opContent");
        FutureTask pomFt = generatePomAsync(path);

        GspBusinessEntity be = JSONSerializer.getObjectMapper().readValue(
                new TreeTraversingParser(beContent), GspBusinessEntity.class);
        BizOperation operation = deserializeOperation(opType, opContent);
        operation.setOwner(isChild ? (GspBizEntityObject) be.findObjectByCode(nodeCode) : be.getMainObject());

        //生成构件元数据
        GspMetadata metadata = BeManagerService.createCompCode(path, bizObjectId, nodeCode, isChild, be, operation);
        String codePath = BeManagerService.getCodePath(path, metadata);
        if (pomFt != null)
            pomFt.get();
        return new BuildCompCodeResult(metadata.getHeader(), codePath);
    }

    @SneakyThrows
    private static BizOperation deserializeOperation(BEOperationType opType, JsonNode op) {
        JsonDeserializer deserializer;
        switch (opType) {
            case Determination:
                deserializer = new BizDeterminationDeserializer();
                break;
            case Validation:
                deserializer = new BizValidationDeserializer();
                break;
            case BizMgrAction:
                deserializer = new BizMgrActionDeserializer();
                break;
            case BizAction:
                deserializer = new BizActionDeserializer();
                break;
            default:
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0007, opType.toString());
        }
        TreeTraversingParser jsonParser = new TreeTraversingParser(op);
        jsonParser.nextToken();
        return (BizOperation) deserializer.deserialize(jsonParser, null);
    }

    @Data
    private class BuildCompCodeResult {
        public BuildCompCodeResult(MetadataHeader comp, String codePath) {
            this.comp = comp;
            this.codePath = codePath;
        }

        private MetadataHeader comp;
        private String codePath;
    }

    @SneakyThrows
    @Path("deleteObjectDbo")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteObjectDboFiles(JsonNode node) {
        ArrayNode dboIDSNode = (ArrayNode) Objects.requireNonNull(node.get("dboIDs"), "DBOID列表不能为空");
        String path = Objects.requireNonNull(node.get("path"), "路径不能为空").textValue();
        List<String> dboIDs = new ArrayList<>(dboIDSNode.size());
        dboIDSNode.iterator().forEachRemaining(item -> dboIDs.add(Objects.requireNonNull(item.textValue(), "构件id不能为空")));

        DboUtil.dealDboFiles(dboIDs, path);
    }
}
