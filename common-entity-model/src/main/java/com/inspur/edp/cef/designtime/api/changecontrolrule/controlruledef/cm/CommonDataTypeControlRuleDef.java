package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefinition;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.RangeRuleDefinition;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.parser.CommonDataTypeRuleDefParser;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.serializer.CommonDataTypeRuleDefSerializer;

/**
 * The Definition Of CommonDataTypeControlRuleDef
 *
 * @ClassName: CommonDataTypeControlRuleDef
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonSerialize(using = CommonDataTypeRuleDefSerializer.class)
@JsonDeserialize(using = CommonDataTypeRuleDefParser.class)
public class CommonDataTypeControlRuleDef extends RangeRuleDefinition {
    public CommonDataTypeControlRuleDef(ControlRuleDefinition parentRuleDefinition, String ruleObjectType) {
        super(parentRuleDefinition, ruleObjectType);
    }

    //region 名称规则
    public ControlRuleDefItem getNameControlRule() {
        return super.getControlRuleItem(CommonDataTypeRuleNames.Name);
    }

    public void setNameControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CommonDataTypeRuleNames.Name, ruleItem);
    }
    //endregion

    //region 子表新增规则
    public ControlRuleDefItem getAddChildEntityControlRule() {
        return super.getControlRuleItem(CommonDataTypeRuleNames.AddChildEntity);
    }

    public void setAddChildEntityControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CommonDataTypeRuleNames.AddChildEntity, ruleItem);
    }
    //endregion

    //region 子表修改规则
    public ControlRuleDefItem getModifyChildEntitiesControlRule() {
        return super.getControlRuleItem(CommonDataTypeRuleNames.ModifyChildEntities);
    }

    public void setModifyChildEntitiesControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CommonDataTypeRuleNames.ModifyChildEntities, ruleItem);
    }
    //endregion

    //region 字段新增规则
    public ControlRuleDefItem getAddFieldControlRule() {
        return super.getControlRuleItem(CommonDataTypeRuleNames.AddField);
    }

    public void setAddFieldControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CommonDataTypeRuleNames.AddField, ruleItem);
    }
    //endregion

    //region 字段修改规则
    public ControlRuleDefItem getModifyFieldsControlRule() {
        return super.getControlRuleItem(CommonDataTypeRuleNames.ModifyFields);
    }

    public void setModifyFieldsControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CommonDataTypeRuleNames.ModifyFields, ruleItem);
    }
    //endregion

}
