package com.inspur.edp.cef.designtime.api.element;

/**
 * The Definition Of ElementDefaultValue
 *
 * @ClassName: ElementDefaultVauleType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum ElementDefaultVauleType {
    /**
     * 值类型
     */
    Vaule(0),
    /**
     * 表达式类型
     */
    Expression(1);


    private final int intValue;
    private static java.util.HashMap<Integer, ElementDefaultVauleType> mappings;

    private synchronized static java.util.HashMap<Integer, ElementDefaultVauleType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, ElementDefaultVauleType>();
        }
        return mappings;
    }

    private ElementDefaultVauleType(int value) {
        intValue = value;
        ElementDefaultVauleType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static ElementDefaultVauleType forValue(int value) {
        return getMappings().get(value);
    }
}
