package com.inspur.edp.cef.designtime.api.i18n.extractor;

import com.inspur.edp.cef.designtime.api.i18n.context.CefResourceInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;

/**
 * 国际化抽取器基类
 */
public abstract class AbstractResourceExtractor {
    /**
     * 国际化抽取器基类
     *
     * @param context            上下文
     * @param parentResourceInfo 父抽取器的前缀信息
     */
    protected AbstractResourceExtractor(ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        this.context = context;
        this.parentResourcePrefixInfo = parentResourceInfo;
    }

    protected AbstractResourceExtractor(ICefResourceExtractContext context) {
        this(context, null);
    }

    //#region 属性

    /**
     * 国际化抽取上下文
     */
    private final ICefResourceExtractContext context;

    protected final ICefResourceExtractContext getContext() {
        return context;
    }

    /**
     * 父抽取器的前缀信息
     */
    private final CefResourcePrefixInfo parentResourcePrefixInfo;

    protected final CefResourcePrefixInfo getParentResourcePrefixInfo() {
        return parentResourcePrefixInfo;
    }

    /**
     * 当前抽取器的前缀信息
     */
    private CefResourcePrefixInfo currentResourcePrefixInfo;

    protected final CefResourcePrefixInfo getCurrentResourcePrefixInfo() {
        return currentResourcePrefixInfo;
    }

    private void setCurrentResourcePrefixInfo(CefResourcePrefixInfo value) {
        currentResourcePrefixInfo = value;
    }

    //#endregion

    /**
     * 抽取国际化项入口方法
     */
    public final void extract() {
        setCurrentResourcePrefixInfo(buildCurrentPrefix());
        setPrefixInfo();
        extractItems();
    }

    /**
     * 抽取
     */
    protected abstract void extractItems();

    /**
     * 构造前缀
     *
     * @return
     */
    protected abstract CefResourcePrefixInfo buildCurrentPrefix();

    /**
     * 赋值前缀
     */
    protected abstract void setPrefixInfo();

    /**
     * 新增抽取资源项
     *
     * @param propName            属性名
     * @param value               值
     * @param descriptionPropName 描述中属性名
     */
    protected final void addResourceInfo(String propName, String value, String descriptionPropName) {
        CefResourceInfo info = new CefResourceInfo(getCurrentResourcePrefixInfo(), propName, descriptionPropName, value);
        getContext().setResourceItem(info);
    }

}