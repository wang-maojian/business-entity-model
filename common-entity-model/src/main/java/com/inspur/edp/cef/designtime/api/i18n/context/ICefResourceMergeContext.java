package com.inspur.edp.cef.designtime.api.i18n.context;

import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;

public interface ICefResourceMergeContext {

    /**
     * 获取前缀
     *
     * @return
     */
    String getKeyPrefix();

    void setKeyPrefix(String keyPrefix);

    /**
     * 获取资源项
     *
     * @return
     */
    I18nResourceItemCollection getResourceItems();
}
