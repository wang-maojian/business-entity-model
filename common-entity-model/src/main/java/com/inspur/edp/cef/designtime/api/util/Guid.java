package com.inspur.edp.cef.designtime.api.util;

import java.util.UUID;

/**
 * The Utils For Guid
 *
 * @ClassName: Guid
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class Guid {
    public static UUID newGuid() {
        return UUID.randomUUID();
    }
}