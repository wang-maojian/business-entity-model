package com.inspur.edp.cef.designtime.api.variable;

/**
 * The Definition Of Variable Source Type
 *
 * @ClassName: VariableSourceType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum VariableSourceType {
    BE,
    VO,
    Context;

    public int getValue() {
        return this.ordinal();
    }

    public static VariableSourceType forValue(int value) {
        return values()[value];
    }
}