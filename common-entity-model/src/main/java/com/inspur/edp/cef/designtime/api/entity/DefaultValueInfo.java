package com.inspur.edp.cef.designtime.api.entity;

import com.inspur.edp.cef.designtime.api.element.ElementDefaultVauleType;

public class DefaultValueInfo {

    private String defaultValue;
    private String displayDefaultValue;
    private ElementDefaultVauleType defaultVauleType = ElementDefaultVauleType.Vaule;

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDisplayDefaultValue() {
        return displayDefaultValue;
    }

    public void setDisplayDefaultValue(String displayDefaultValue) {
        this.displayDefaultValue = displayDefaultValue;
    }

    public ElementDefaultVauleType getDefaultVauleType() {
        return defaultVauleType;
    }

    public void setDefaultVauleType(ElementDefaultVauleType defaultVauleType) {
        this.defaultVauleType = defaultVauleType;
    }

}
