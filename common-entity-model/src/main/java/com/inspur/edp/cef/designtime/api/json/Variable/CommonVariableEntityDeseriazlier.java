package com.inspur.edp.cef.designtime.api.json.Variable;

import com.inspur.edp.cef.designtime.api.collection.GspFieldCollection;
import com.inspur.edp.cef.designtime.api.entity.GspCommonDataType;
import com.inspur.edp.cef.designtime.api.json.element.CefFieldDeserializer;
import com.inspur.edp.cef.designtime.api.json.object.GspCommonDataTypeDeserializer;
import com.inspur.edp.cef.designtime.api.variable.CommonVariableCollection;
import com.inspur.edp.cef.designtime.api.variable.CommonVariableEntity;

/**
 * The Json Parser Of Common Variable Entity
 *
 * @ClassName: CommonVariableEntityDeseriazlier
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonVariableEntityDeseriazlier extends GspCommonDataTypeDeserializer {
    @Override
    protected void beforeCefObjectDeserializer(GspCommonDataType commonDataType) {

    }

    @Override
    protected GspCommonDataType createCommonDataType() {
        return new CommonVariableEntity();
    }

    @Override
    protected CefFieldDeserializer createFieldDeserializer() {
        return new CommonVariableDeserializer();
    }

    @Override
    protected GspFieldCollection createFieldCollection() {
        return new CommonVariableCollection((CommonVariableEntity) getGspCommonDataType());
    }
}
