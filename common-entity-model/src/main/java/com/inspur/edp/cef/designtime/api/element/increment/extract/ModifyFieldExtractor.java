package com.inspur.edp.cef.designtime.api.element.increment.extract;

import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonFieldControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonFieldContrulRuleDef;
import com.inspur.edp.cef.designtime.api.element.increment.ModifyFieldIncrement;
import com.inspur.edp.cef.designtime.api.entity.GspCommonField;
import com.inspur.edp.cef.designtime.api.increment.extractor.ExtractUtils;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import lombok.var;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ModifyFieldExtractor {

    protected boolean includeAll = false;

    public ModifyFieldExtractor() {

    }

    public ModifyFieldExtractor(boolean includeAll) {

        this.includeAll = includeAll;
    }

    public ModifyFieldIncrement extract(GspCommonField oldField, GspCommonField newField, CommonFieldControlRule rule, CommonFieldContrulRuleDef def) {

        ModifyFieldIncrement increment = createModifyFieldIncrement();
        increment.setId(oldField.getId());
        //ChangeProperties()
        extractSelfInfo(increment, oldField, newField, rule, def);
        extractExtendInfo(increment, oldField, newField, rule, def);

        if (increment.getChangeProperties().isEmpty())
            return null;
        return increment;
    }

    //region BaseInfo
    private void extractSelfInfo(ModifyFieldIncrement increment, GspCommonField oldField, GspCommonField newField, CommonFieldControlRule rule, CommonFieldContrulRuleDef def) {

        if (includeAll) {
            extractSelfAllInfo(increment, oldField, newField);
            return;
        }

//        ExtractUtils.extractValue(increment.getChangeProperties(), oldField.getCode(), newField.getCode(), CefNames.Code, rule);
        ExtractUtils.extractValue(increment.getChangeProperties(), oldField.getName(), newField.getName(), CefNames.Name, rule == null ? null : rule.getNameControlRule(), def.getNameControlRule());
        ExtractUtils.extractValue(increment.getChangeProperties(), oldField.getDefaultValue(), newField.getDefaultValue(), CefNames.DefaultValue, rule == null ? null : rule.getDefaultValueControlRule(), def.getDefaultValueControlRule());
        ExtractUtils.extractValue(increment.getChangeProperties(), oldField.getDefaultValueType(), newField.getDefaultValueType(), CefNames.DefaultValueType, rule == null ? null : rule.getDefaultValueControlRule(), def.getDefaultValueControlRule());
        ExtractUtils.extracLengthtValue(increment.getChangeProperties(), oldField.getLength(), newField.getLength(), CefNames.Length, null, null);
        ExtractUtils.extractValueAllowShort(increment.getChangeProperties(), oldField.getPrecision(), newField.getPrecision(), CefNames.Precision, rule == null ? null : rule.getPrecisionControlRule(), def.getPrecisionControlRule());
        ExtractUtils.extractValue(increment.getChangeProperties(), oldField.getContainEnumValues(), newField.getContainEnumValues(), CefNames.ContainEnumValues, null, null);

    }

    private void extractSelfAllInfo(ModifyFieldIncrement increment, GspCommonField oldField, GspCommonField newField) {

        var changes = increment.getChangeProperties();

        ExtractUtils.extractValue(changes, oldField.getName(), newField.getName(), CefNames.Name, null, null);
        ExtractUtils.extractValue(changes, oldField.getDefaultValue(), newField.getDefaultValue(), CefNames.DefaultValue, null, null);
        ExtractUtils.extracLengthtValue(changes, oldField.getLength(), newField.getLength(), CefNames.Length, null, null);
        ExtractUtils.extractValue(changes, oldField.getPrecision(), newField.getPrecision(), CefNames.Precision, null, null);

        ExtractUtils.extractValue(changes, oldField.getLabelID(), newField.getLabelID(), CefNames.LabelID, null, null);
        ExtractUtils.extractValue(changes, oldField.getIsRef(), newField.getIsRef(), CefNames.IsRef, null, null);
        ExtractUtils.extractValue(changes, oldField.getI18nResourceInfoPrefix(), newField.getI18nResourceInfoPrefix(), CefNames.I18nResourceInfoPrefix, null, null);

        ExtractUtils.extractValue(changes, oldField.getCode(), newField.getCode(), CefNames.Code, null, null);
        ExtractUtils.extractValue(changes, oldField.getIsRequire(), newField.getIsRequire(), CefNames.IsRequire, null, null);
        ExtractUtils.extractValue(changes, oldField.getIsMultiLanguage(), newField.getIsMultiLanguage(), CefNames.IsMultiLanguage, null, null);
        ExtractUtils.extractValue(changes, oldField.getIsRefElement(), newField.getIsRefElement(), CefNames.IsRefElement, null, null);
        ExtractUtils.extractValue(changes, oldField.getRefElementId(), newField.getRefElementId(), CefNames.RefElementID, null, null);
        ExtractUtils.extractValue(changes, oldField.getIsUdt(), newField.getIsUdt(), CefNames.IsUdt, null, null);
        ExtractUtils.extractValue(changes, oldField.getUdtPkgName(), newField.getUdtPkgName(), CefNames.UdtPkgName, null, null);
        ExtractUtils.extractValue(changes, oldField.getUdtID(), newField.getUdtID(), CefNames.UdtID, null, null);
        ExtractUtils.extractValue(changes, oldField.getUdtName(), newField.getUdtName(), CefNames.UdtName, null, null);
        ExtractUtils.extractValue(changes, oldField.getIsVirtual(), newField.getIsVirtual(), CefNames.IsVirtual, null, null);
        ExtractUtils.extractValue(changes, oldField.getRefBusinessFieldId(), newField.getRefBusinessFieldId(), CefNames.RefBusinessFieldId, null, null);
        ExtractUtils.extractValue(changes, oldField.getRefBusinessFieldName(), newField.getRefBusinessFieldName(), CefNames.RefBusinessFieldName, null, null);
        ExtractUtils.extractValue(changes, oldField.getBeLabel(), newField.getBeLabel(), CefNames.BeLabel, null, null);
        ExtractUtils.extractValue(changes, oldField.getIsFromAssoUdt(), newField.getIsFromAssoUdt(), CefNames.IsFromAssoUdt, null, null);
        ExtractUtils.extractValue(changes, oldField.getEnumIndexType(), newField.getEnumIndexType(), CefNames.EnumIndexType, null, null);

        ExtractUtils.extractValue(changes, oldField.getCollectionType(), newField.getCollectionType(), CefNames.CollectionType, null, null);
        ExtractUtils.extractValue(changes, oldField.getObjectType(), newField.getObjectType(), CefNames.ObjectType, null, null);
        ExtractUtils.extractValue(changes, oldField.getMDataType(), newField.getMDataType(), CefNames.MDataType, null, null);
        ExtractUtils.extractValue(changes, oldField.getDefaultValueType(), newField.getDefaultValueType(), CefNames.DefaultValueType, null, null);


        ExtractUtils.extractValue(changes, oldField.getMappingRelation(), newField.getMappingRelation(), CefNames.MappingRelation, null, null);
        ExtractUtils.extractValue(changes, oldField.getContainEnumValues(), newField.getContainEnumValues(), CefNames.ContainEnumValues, null, null);
        ExtractUtils.extractValue(changes, oldField.getChildElements(), newField.getChildElements(), CefNames.ChildElements, null, null);
        ExtractUtils.extractValue(changes, oldField.getChildAssociations(), newField.getChildAssociations(), CefNames.ChildAssociations, null, null);

    }
    //endregion

    protected void extractExtendInfo(ModifyFieldIncrement increment, GspCommonField oldField, GspCommonField newField, CommonFieldControlRule rule, CommonFieldContrulRuleDef def) {

    }

    protected ModifyFieldIncrement createModifyFieldIncrement() {
        return new ModifyFieldIncrement();
    }

}
