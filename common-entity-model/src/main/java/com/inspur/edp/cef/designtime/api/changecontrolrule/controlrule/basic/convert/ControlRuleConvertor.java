package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.convert;

import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.AbstractControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefinition;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleValue;

import java.util.Map;

/**
 * The Convertor Of ControlRule
 *
 * @ClassName: ControlRuleConvertor
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ControlRuleConvertor {

    public static void convert2ControlRule(ControlRuleDefinition ruleDef, AbstractControlRule rule, IGspCommonDataType dataType) {
        convert2ControlRule(ruleDef, rule);
        rule.setRuleId(dataType.getID());
    }

    public static void convert2ControlRule(ControlRuleDefinition ruleDef, AbstractControlRule rule, IGspCommonField field) {
        convert2ControlRule(ruleDef, rule);
        rule.setRuleId(field.getID());
    }

    public static void convert2ControlRule(ControlRuleDefinition ruleDef, AbstractControlRule rule) {
        rule.setRuleDefId(ruleDef.getRuleObjectType());

        Map<String, ControlRuleDefItem> defSelfRules = ruleDef.getSelfControlRules();
        if (defSelfRules == null || defSelfRules.isEmpty())
            return;
        for (Map.Entry<String, ControlRuleDefItem> defItem :
                defSelfRules.entrySet()) {
            ControlRuleItem item = convert2ControlRuleItem(defItem.getValue());
            rule.getSelfControlRules().put(defItem.getKey(), item);
        }
    }

    public static ControlRuleItem convert2ControlRuleItem(ControlRuleDefItem ruleDefItem) {

        ControlRuleItem ruleItem = new ControlRuleItem();
        ruleItem.setRuleName(ruleDefItem.getRuleName());
        ruleItem.setControlRuleValue(ControlRuleValue.Default);
        return ruleItem;
    }
}
