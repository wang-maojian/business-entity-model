package com.inspur.edp.cef.designtime.api.json.operation;

import com.inspur.edp.cef.designtime.api.collection.ValElementCollection;

import java.util.HashMap;

public class ValRequestChildElementsDeserializer extends RequestChildElementsDeserializer<ValElementCollection> {

    @Override
    HashMap<String, ValElementCollection> createHashMap() {
        return new HashMap<String, ValElementCollection>();
    }

    @Override
    ValElementCollection createValueType() {
        return new ValElementCollection();
    }
}