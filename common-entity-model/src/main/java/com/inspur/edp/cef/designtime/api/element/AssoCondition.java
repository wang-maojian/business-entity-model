package com.inspur.edp.cef.designtime.api.element;

public class AssoCondition {
    private String leftNodeCode;
    //前期设计器没有出来，暂时不赋值
    private String leftElemenetId;
    private String leftField;

    public String getLeftNodeCode() {
        return leftNodeCode;
    }

    public void setLeftNodeCode(String leftNodeCode) {
        this.leftNodeCode = leftNodeCode;
    }

    public String getLeftField() {
        return leftField;
    }

    public void setLeftField(String leftField) {
        this.leftField = leftField;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getRightNodeCode() {
        return rightNodeCode;
    }

    public void setRightNodeCode(String rightNodeCode) {
        this.rightNodeCode = rightNodeCode;
    }

    public String getRightField() {
        return rightField;
    }

    public void setRightField(String rightField) {
        this.rightField = rightField;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLeftElemenetId() {
        return leftElemenetId;
    }

    public void setLeftElemenetId(String leftElemenetId) {
        this.leftElemenetId = leftElemenetId;
    }

    public String getRightElementId() {
        return rightElementId;
    }

    public void setRightElementId(String rightElementId) {
        this.rightElementId = rightElementId;
    }

    private String operator;
    private String rightNodeCode;
    private String rightElementId;
    private String rightField;
    private String value;

    public AssoConditionType getAssoConditionType() {
        return assoConditionType;
    }

    public void setAssoConditionType(AssoConditionType assoConditionType) {
        this.assoConditionType = assoConditionType;
    }

    private AssoConditionType assoConditionType;
}
