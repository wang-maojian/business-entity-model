package com.inspur.edp.cef.designtime.api.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.designtime.api.collection.DtmElementCollection;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.operation.ChildDtmTriggerInfo;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.cef.designtime.api.operation.CommonTriggerPointType;
import com.inspur.edp.cef.designtime.api.operation.ExecutingDataStatus;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * The Json Serializer Of CommonDetermination
 *
 * @ClassName: CommonDtmSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonDtmSerializer extends CommonOpSerializer {
    @Override
    protected void writeExtendCommonOpBaseProperty(JsonGenerator writer, CommonOperation info) {

    }

    public CommonDtmSerializer(boolean full) {
        super(full);
        isFull = full;
    }

    @Override
    protected void writeExtendCommonOpSelfProperty(JsonGenerator writer, CommonOperation info) {
        CommonDetermination dtm = (CommonDetermination) info;
        if (isFull || (!dtm.getGetExecutingDataStatus().isEmpty() && !dtm.getGetExecutingDataStatus().iterator().next().equals(ExecutingDataStatus.None))) {
            SerializerUtils.writePropertyName(writer, CefNames.GetExecutingDataStatus);
            writeGetExecutingDataStatus(writer, dtm.getGetExecutingDataStatus());
        }
        if (isFull || (dtm.getTriggerPointType() != CommonTriggerPointType.None)) {
            SerializerUtils.writePropertyValue(writer, CefNames.TriggerPointType, dtm.getTriggerPointType().getValue());
        }
        if (isFull || (dtm.getRequestElements() != null && !dtm.getRequestElements().isEmpty()))
            writeRequestElements(writer, dtm.getRequestElements());
        if ((isFull && dtm.getChildTriggerInfo() != null) || isSerialChildTriggerInfo(dtm.getChildTriggerInfo()))
            writeChildTriggerInfo(writer, dtm.getChildTriggerInfo());
        if (isFull || (dtm.getRequestChildElements() != null && !dtm.getRequestChildElements().isEmpty()))
            writeRequestChildElements(writer, dtm.getRequestChildElements());
    }

    private boolean isSerialChildTriggerInfo(HashMap<String, ChildDtmTriggerInfo> hashMap) {
        return hashMap != null && !hashMap.isEmpty();
    }

    protected void writeRequestElements(JsonGenerator writer, DtmElementCollection childElementsIds) {
        SerializerUtils.writePropertyName(writer, CefNames.RequestElements);

        SerializerUtils.WriteStartArray(writer);

        for (String childElementsId : childElementsIds)
            if (isFull || (childElementsId != null && !"".equals(childElementsId)))
                SerializerUtils.writePropertyValue_String(writer, childElementsId);

        SerializerUtils.WriteEndArray(writer);
    }

    protected void writeChildTriggerInfo(JsonGenerator writer, HashMap<String, ChildDtmTriggerInfo> hashMap) {
        SerializerUtils.writePropertyName(writer, CefNames.ChildTriggerInfo);
        SerializerUtils.WriteStartArray(writer);
        if (hashMap != null && !hashMap.isEmpty()) {
            for (Map.Entry<String, ChildDtmTriggerInfo> item : hashMap.entrySet()) {
                SerializerUtils.writeStartObject(writer);
                SerializerUtils.writePropertyValue(writer, CefNames.RequestChildCode, item.getKey());
                SerializerUtils.writePropertyName(writer, CefNames.RequestDtmChildElementValue);
                ChildDtmTriggerInfoSerializer dtmTriggerInfoSerializer = new ChildDtmTriggerInfoSerializer(isFull);
                try {
                    dtmTriggerInfoSerializer.serialize(item.getValue(), writer, null);
                } catch (IOException e) {
                    throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_JSON_0004, e, "ChildTriggerInfo");
                }
                SerializerUtils.writeEndObject(writer);
            }
        }
        SerializerUtils.WriteEndArray(writer);
    }

    private void writeDtmElementCollection(JsonGenerator writer, DtmElementCollection childElementsIds) {
        SerializerUtils.WriteStartArray(writer);
        if (!childElementsIds.isEmpty()) {
            for (String item : childElementsIds) {
                SerializerUtils.writePropertyValue_String(writer, item);
            }
        }
        SerializerUtils.WriteEndArray(writer);
    }

    protected void writeRequestChildElements(JsonGenerator writer, HashMap<String, DtmElementCollection> dic) {
        if (dic != null && !dic.isEmpty()) {
            SerializerUtils.writePropertyName(writer, CefNames.RequestChildElements);
            SerializerUtils.WriteStartArray(writer);
            for (Map.Entry<String, DtmElementCollection> item : dic.entrySet()) {
                SerializerUtils.writeStartObject(writer);
                SerializerUtils.writePropertyValue(writer, CefNames.RequestChildElementKey, item.getKey());
                SerializerUtils.writePropertyName(writer, CefNames.RequestChildElementValue);
                writeDtmElementCollection(writer, item.getValue());
                SerializerUtils.writeEndObject(writer);
            }
            SerializerUtils.WriteEndArray(writer);
        }
    }

}
