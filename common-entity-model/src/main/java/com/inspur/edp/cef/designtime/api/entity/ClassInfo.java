package com.inspur.edp.cef.designtime.api.entity;

/**
 * The Definition Of ClassInfo
 *
 * @ClassName: ClassInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ClassInfo {
    public ClassInfo(DataTypeAssemblyInfo assemblyInfo, String className, String classNamespace) {
        this.className = className;
        this.classNamespace = classNamespace;
        this.assemblyInfo = assemblyInfo;
    }

    private final DataTypeAssemblyInfo assemblyInfo;
    private final String className;
    private final String classNamespace;

    public final DataTypeAssemblyInfo getAssemblyInfo() {
        return assemblyInfo;
    }

    public final String getClassName() {
        return className;
    }

    public final String getClassNamespace() {
        return classNamespace;
    }

    public final String getClassFullName() {
        return String.format("%1$s.%2$s", getClassNamespace(), getClassName());
    }
}