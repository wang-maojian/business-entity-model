package com.inspur.edp.cef.designtime.api.increment;

import java.util.HashMap;

public enum IncrementType {
    Added(0),
    Modify(1),
    Deleted(2);

    private final int intValue;
    private static HashMap<Integer, IncrementType> mappings;

    private synchronized static HashMap<Integer, IncrementType> getMappings() {
        if (mappings == null) {
            mappings = new HashMap<>();
        }
        return mappings;
    }

    private IncrementType(int value) {
        intValue = value;
        IncrementType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static IncrementType forValue(int value) {
        return getMappings().get(value);
    }

}
