package com.inspur.edp.cef.designtime.api.entity.increment.extractor;

import com.inspur.edp.cef.designtime.api.entity.GspCommonDataType;
import com.inspur.edp.cef.designtime.api.entity.increment.DeletedEntityIncrement;

public class DeletedEntityExtractor {

    public DeletedEntityIncrement extract(GspCommonDataType oldDataType) {
        DeletedEntityIncrement increment = createDeletedEntityIncrement(oldDataType.getID());
        return increment;
    }

    protected DeletedEntityIncrement createDeletedEntityIncrement(String deleteId) {
        return new DeletedEntityIncrement(deleteId);
    }
}

