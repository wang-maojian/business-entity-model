package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefinition;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.parser.CommonFieldRuleDefParser;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.serializer.CommonFieldRuleDefSerializer;

/**
 * The Definition Of CommonFieldContrulRuleDef
 *
 * @ClassName: CommonFieldContrulRuleDef
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonSerialize(using = CommonFieldRuleDefSerializer.class)
@JsonDeserialize(using = CommonFieldRuleDefParser.class)
public class CommonFieldContrulRuleDef extends ControlRuleDefinition {
    public CommonFieldContrulRuleDef(ControlRuleDefinition parentRuleDefinition, String ruleObjectType) {
        super(parentRuleDefinition, ruleObjectType);
    }

    //region 名称规则
    public ControlRuleDefItem getNameControlRule() {
        return super.getControlRuleItem(CommonFieldRuleNames.Name);
    }

    public void setNameControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CommonFieldRuleNames.Name, ruleItem);
    }
    //endregion

    //region 长度规则
    public ControlRuleDefItem getLengthControlRule() {
        return super.getControlRuleItem(CommonFieldRuleNames.Length);
    }

    public void setLengthControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CommonFieldRuleNames.Length, ruleItem);
    }
    //endregion

    //region 精度规则
    public ControlRuleDefItem getPrecisionControlRule() {
        return super.getControlRuleItem(CommonFieldRuleNames.Precision);
    }

    public void setPrecisionControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CommonFieldRuleNames.Precision, ruleItem);
    }
    //endregion

    //region 默认值规则
    public ControlRuleDefItem getDefaultValueControlRule() {
        return super.getControlRuleItem(CommonFieldRuleNames.DefaultValue);
    }

    public void setDefaultValueControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CommonFieldRuleNames.DefaultValue, ruleItem);
    }
    //endregion

    //region 对象类型规则
    public ControlRuleDefItem getObjectTypeControlRule() {
        return super.getControlRuleItem(CommonFieldRuleNames.ObjectType);
    }

    public void setObjectTypeControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CommonFieldRuleNames.ObjectType, ruleItem);
    }
    //endregion

    //region 多语规则
    public ControlRuleDefItem getMultiLanFieldControlRule() {
        return super.getControlRuleItem(CommonFieldRuleNames.MultiLanField);
    }

    public void setMultiLanFieldControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CommonFieldRuleNames.MultiLanField, ruleItem);
    }
    //endregion

    //region 只读性规则
    public ControlRuleDefItem geReadonlyControlRule() {
        return super.getControlRuleItem(CommonFieldRuleNames.Readonly);
    }

    public void setReadonlyControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CommonFieldRuleNames.Readonly, ruleItem);
    }
    //endregion

    //region 唯一性规则
    public ControlRuleDefItem geRequiredControlRule() {
        return super.getControlRuleItem(CommonFieldRuleNames.Required);
    }

    public void setRequiredControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CommonFieldRuleNames.Required, ruleItem);
    }
    //endregion

}
