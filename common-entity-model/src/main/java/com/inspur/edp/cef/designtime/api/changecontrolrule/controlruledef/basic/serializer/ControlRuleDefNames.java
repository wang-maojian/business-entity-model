package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.serializer;

/**
 * The Serializer Names Of ControlRuleDef
 *
 * @ClassName: ControlRuleDefNames
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ControlRuleDefNames {
    public static final String RuleObjectType = "RuleObjectType";
    public static final String SelfControlRules = "SelfControlRules";
    public static final String RangeControlRules = "RangeControlRules";
    public static final String Description = "Description";
    public static final String RuleDisplayName = "RuleDisplayName";
    public static final String RuleName = "RuleName";
    public static final String DefaultRuleValue = "DefaultRuleValue";
    public static final String ChildControlRules = "ChildControlRules";
}
