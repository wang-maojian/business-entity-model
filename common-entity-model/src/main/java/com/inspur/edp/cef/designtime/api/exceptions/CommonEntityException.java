package com.inspur.edp.cef.designtime.api.exceptions;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

public class CommonEntityException extends CAFRuntimeException {
    private static final String SERVICE_UNIT_CODE = "pfcommon";
    private static final String RESOURCE_FILE = "business_entity_model_exception.properties";

    public CommonEntityException(CommonEntityErrorCodeEnum exceptionCode, String[] messageParams, Throwable innerException, ExceptionLevel level) {
        super(SERVICE_UNIT_CODE, RESOURCE_FILE, exceptionCode.name(), messageParams, innerException, level, exceptionCode.isBizException());
    }

    public static CommonEntityException createException(CommonEntityErrorCodeEnum exceptionCode) {
        return new CommonEntityException(exceptionCode, null, null, ExceptionLevel.Error);
    }

    public static CommonEntityException createException(CommonEntityErrorCodeEnum exceptionCode, String... messageParams) {
        return new CommonEntityException(exceptionCode, messageParams, null, ExceptionLevel.Error);
    }

    public static CommonEntityException createException(CommonEntityErrorCodeEnum exceptionCode, Throwable innerException) {
        return new CommonEntityException(exceptionCode, null, innerException, ExceptionLevel.Error);
    }

    public static CommonEntityException createException(CommonEntityErrorCodeEnum exceptionCode, Throwable innerException, String... messageParams) {
        return new CommonEntityException(exceptionCode, messageParams, innerException, ExceptionLevel.Error);
    }

    public static CommonEntityException createException(CommonEntityErrorCodeEnum exceptionCode, Throwable innerException, ExceptionLevel level, String... messageParams) {
        return new CommonEntityException(exceptionCode, messageParams, innerException, level);
    }
}
