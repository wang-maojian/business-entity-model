package com.inspur.edp.cef.designtime.api.json.element;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.entity.MappingInfo;
import com.inspur.edp.cef.designtime.api.entity.MappingRelation;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.io.IOException;
import java.util.ArrayList;

public class MappingRelationDeserializer extends JsonDeserializer<MappingRelation> {
    @Override
    public MappingRelation deserialize(JsonParser jsonParser,
                                       DeserializationContext ctxt) throws IOException, JsonProcessingException {
        MappingRelation mappingInfos = new MappingRelation();
        ArrayList<MappingInfo> list = new ArrayList<MappingInfo>();
        JsonDeserializer<MappingInfo> deserializer = new MappingInfoDeserializer();
        SerializerUtils.readArray(jsonParser, deserializer, list);
        for (MappingInfo info : list) {
            mappingInfos.add(info);
        }
        return mappingInfos;
    }
}
