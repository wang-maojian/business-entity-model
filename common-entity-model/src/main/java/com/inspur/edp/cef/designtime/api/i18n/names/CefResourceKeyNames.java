package com.inspur.edp.cef.designtime.api.i18n.names;

public final class CefResourceKeyNames {

    public static final String Name = "Name";
    // 字段枚举
    public static final String DisplayValue = "DisplayValue";
}