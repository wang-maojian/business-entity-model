package com.inspur.edp.cef.designtime.api.element.increment;

import com.inspur.edp.cef.designtime.api.increment.DeletedIncrement;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DeletedFieldIncrement extends GspCommonFieldIncrement implements DeletedIncrement {

    private final String deleteId;

    public DeletedFieldIncrement(String deleteId) {
        this.deleteId = deleteId;
        setId(deleteId);
    }

    public String getDeleteId() {
        return deleteId;
    }

    @Override
    public final IncrementType getIncrementType() {
        return IncrementType.Deleted;
    }
}
