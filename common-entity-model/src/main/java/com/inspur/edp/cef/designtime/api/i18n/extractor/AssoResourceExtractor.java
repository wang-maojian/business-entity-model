package com.inspur.edp.cef.designtime.api.i18n.extractor;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;

public abstract class AssoResourceExtractor extends AbstractResourceExtractor {
    private final GspAssociation asso;

    protected AssoResourceExtractor(GspAssociation asso, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(context, parentResourceInfo);
        this.asso = asso;
    }

    protected void extractItems() {
        if (asso.getRefElementCollection() != null && !asso.getRefElementCollection().isEmpty()) {
            for (IGspCommonField item : asso.getRefElementCollection()) {
                getAssoRefFieldResourceExtractor(getContext(), getCurrentResourcePrefixInfo(), item).extract();
            }
        }
    }

    protected CefResourcePrefixInfo buildCurrentPrefix() {
        if (getParentResourcePrefixInfo() == null) {
            throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_COMMON_ENTITY_0014, asso.getBelongElement().getName());
        }
        return getParentResourcePrefixInfo();
    }

    /**
     * 赋值节点的国际化项前缀
     */
    protected final void setPrefixInfo() {
        asso.setI18nResourceInfoPrefix(getCurrentResourcePrefixInfo().getResourceKeyPrefix());
    }

    protected abstract AssoRefFieldResourceExtractor getAssoRefFieldResourceExtractor(ICefResourceExtractContext context, CefResourcePrefixInfo assoPrefixInfo, IGspCommonField field);

}