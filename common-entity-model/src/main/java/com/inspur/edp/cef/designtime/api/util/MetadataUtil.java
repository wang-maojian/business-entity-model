package com.inspur.edp.cef.designtime.api.util;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationService;
import com.inspur.edp.metadata.rtcustomization.api.entity.MetadataQueryParam;
import com.inspur.edp.metadata.rtcustomization.api.entity.MetadataScopeEnum;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public class MetadataUtil {

    private static final CustomizationService customizationService = SpringBeanUtils.getBean(CustomizationService.class);

    public static GspMetadata getCustomMetadata(String id) {
        MetadataQueryParam param = new MetadataQueryParam();
        param.setMetadataId(id);
        param.setMetadataScopeEnum(MetadataScopeEnum.CUSTOMIZING);
        return customizationService.getGspMetadata(param);
    }

    public static GspMetadata getCustomMetadataByI18n(String id, boolean i18n) {
        MetadataQueryParam param = new MetadataQueryParam();
        param.setMetadataId(id);
        param.setIsI18n(i18n);
        param.setMetadataScopeEnum(MetadataScopeEnum.CUSTOMIZING);
        return customizationService.getGspMetadata(param);
    }

    public static GspMetadata getCustomRTMetadata(String id) {
        MetadataQueryParam param = new MetadataQueryParam();
        param.setMetadataId(id);
        param.setMetadataScopeEnum(MetadataScopeEnum.RUNTIME);
        return customizationService.getGspMetadata(param);
    }

    public static GspMetadata getCustomRTMetadataByI18n(String id, boolean i18n) {
        MetadataQueryParam param = new MetadataQueryParam();
        param.setMetadataId(id);
        param.setIsI18n(i18n);
        param.setMetadataScopeEnum(MetadataScopeEnum.RUNTIME);
        return customizationService.getGspMetadata(param);
    }

}
