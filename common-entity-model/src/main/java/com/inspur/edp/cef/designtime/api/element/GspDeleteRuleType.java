package com.inspur.edp.cef.designtime.api.element;

/**
 * The Definition Of DeleteRuleType
 *
 * @ClassName: DeleteRuleType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum GspDeleteRuleType {
    /**
     * 拒绝删除
     */
    Refuse(0),
    /**
     * 允许删除
     */
    Allow(1);
    private final int intValue;
    private static java.util.HashMap<Integer, GspDeleteRuleType> mappings;

    private synchronized static java.util.HashMap<Integer, GspDeleteRuleType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, GspDeleteRuleType>();
        }
        return mappings;
    }

    private GspDeleteRuleType(int value) {
        intValue = value;
        GspDeleteRuleType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static GspDeleteRuleType forValue(int value) {
        return getMappings().get(value);
    }
}