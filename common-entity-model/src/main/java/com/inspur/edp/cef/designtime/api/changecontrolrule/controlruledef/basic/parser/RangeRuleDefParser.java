package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.RangeRuleDefinition;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.serializer.ControlRuleDefNames;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.fasterxml.jackson.core.JsonToken.END_ARRAY;

/**
 * The Json Parser Of RangeRuleDefParser
 *
 * @ClassName: RangeRuleDefParser
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class RangeRuleDefParser<T extends RangeRuleDefinition> extends ControlRuleDefParser<T> {
    @Override
    protected T createRuleDefinition() {
        return (T) new RangeRuleDefinition();
    }

    @Override
    protected final boolean readExtendControlRuleProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        if (super.readExtendControlRuleProperty(ruleDefinition, propName, jsonParser, deserializationContext))
            return true;
        if (propName.equals(ControlRuleDefNames.RangeControlRules)) {
            SerializerUtils.readStartObject(jsonParser);
            innerReadRangeRules(ruleDefinition, jsonParser, deserializationContext);
            SerializerUtils.readEndObject(jsonParser);
            return true;
        }
        return readRangeExtendControlRuleProperty(ruleDefinition, propName, jsonParser, deserializationContext);
    }

    protected boolean readRangeExtendControlRuleProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        return false;
    }

    private void innerReadRangeRules(T ruleDefinition, JsonParser jsonParser, DeserializationContext deserializationContext) {
        while (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
            String properyName = SerializerUtils.readPropertyName(jsonParser);
            Map<String, ControlRuleDefItem> items = readRangeRuleItems(jsonParser, deserializationContext);
            ruleDefinition.getRangeRules().put(properyName, items);
        }
    }

    private Map<String, ControlRuleDefItem> readRangeRuleItems(JsonParser jsonParser, DeserializationContext deserializationContext) {
        Map<String, ControlRuleDefItem> map = new HashMap<>();
        SerializerUtils.readStartArray(jsonParser);
        JsonToken tokentype = jsonParser.getCurrentToken();
        if (tokentype != END_ARRAY) {
            while (jsonParser.getCurrentToken() == tokentype) {
                try {
                    ControlRuleDefItem item = SerializerUtils.readPropertyValue_Object(ControlRuleDefItem.class, jsonParser);
                    map.put(item.getRuleName(), item);
                    jsonParser.nextToken();
                } catch (IOException e) {
                    throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, "ControlRuleDefItem");
                }
            }
        }
        SerializerUtils.readEndArray(jsonParser);
        return map;
    }
}
