package com.inspur.edp.cef.designtime.api.entity.increment.extractor;

import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonDataTypeControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeControlRuleDef;
import com.inspur.edp.cef.designtime.api.entity.GspCommonDataType;
import com.inspur.edp.cef.designtime.api.entity.increment.CommonEntityIncrement;
import com.inspur.edp.cef.designtime.api.increment.extractor.AbstractIncrementExtractor;

public class DataTypeIncrementExtractor extends AbstractIncrementExtractor {

    protected boolean includeAll;

    public DataTypeIncrementExtractor() {

    }

    public DataTypeIncrementExtractor(boolean includeAll) {

        this.includeAll = includeAll;
    }

    public CommonEntityIncrement extractorIncrement(
            GspCommonDataType oldDataType,
            GspCommonDataType newDataType,
            CommonDataTypeControlRule rule,
            CommonDataTypeControlRuleDef def) {
//        CommonEntityIncrement increment = createCommonEntityIncrement();

        if (oldDataType == null && newDataType == null)
            return null;
        if (oldDataType == null && newDataType != null)
            return getAddedEntityExtractor().extract(newDataType);
        if (oldDataType != null && newDataType == null)
            return getDeletedEntityExtractor().extract(oldDataType);
        return getModifyEntityExtractor().extract(oldDataType, newDataType, rule, def);

    }

    protected AddedEntityExtractor getAddedEntityExtractor() {
        return new AddedEntityExtractor();
    }

    protected DeletedEntityExtractor getDeletedEntityExtractor() {
        return new DeletedEntityExtractor();
    }

    protected ModifyEntityExtractor getModifyEntityExtractor() {
        return new ModifyEntityExtractor(includeAll);
    }
}
