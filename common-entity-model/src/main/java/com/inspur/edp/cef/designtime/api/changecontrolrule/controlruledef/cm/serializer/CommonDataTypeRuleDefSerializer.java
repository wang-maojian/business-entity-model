package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.serializer.RangeRuleDefSerializer;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeControlRuleDef;

/**
 * The Json Serializer Of CommonDataTypeRuleDef
 *
 * @ClassName: CommonDataTypeRuleDefSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonDataTypeRuleDefSerializer<T extends CommonDataTypeControlRuleDef> extends RangeRuleDefSerializer<T> {
    @Override
    protected final void writeRangeExtendInfos(T controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        super.writeRangeExtendInfos(controlRuleDefinition, jsonGenerator, serializerProvider);
        writeCommonDataTypeRuleExtendInfos(controlRuleDefinition, jsonGenerator, serializerProvider);
    }

    protected void writeCommonDataTypeRuleExtendInfos(T controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {

    }
}
