package com.inspur.edp.cef.designtime.api.element;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Definition Of EnumValue
 *
 * @ClassName: EnumValue
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspEnumValue implements Cloneable {

    private String val;

    private String name;

    private int index;

    private String stringIndex;

    private boolean isDefault;

    private String privateI18nResourceInfoPrefix;

    private boolean enumItemDisabled;

    /**
     * 值
     */
    @JsonProperty("Value")
    public final String getValue() {
        return val;
    }

    public final void setValue(String value) {
        val = value;
    }

    /**
     * 名称
     */
    @JsonProperty("Name")
    public final String getName() {
        return name;
    }

    public final void setName(String value) {
        name = value;
    }

    /**
     * 枚举的索引值
     */
    @JsonProperty("Index")
    public final int getIndex() {
        return index;
    }

    public final void setIndex(int value) {
        index = value;
    }

    /**
     * 枚举的索引值-String类型
     */
    @JsonProperty("StringIndex")
    public final String getStringIndex() {
        return stringIndex;
    }

    public void setStringIndex(String value) {
        stringIndex = value;
    }

    /**
     * 该枚举值是否为默认值
     */
    @JsonProperty("IsDefaultEnum")
    public final boolean getIsDefaultEnum() {
        return isDefault;
    }

    public final void setIsDefaultEnum(boolean value) {
        isDefault = value;
    }

    /**
     * 国际化项前缀
     */
    @JsonProperty("I18nResourceInfoPrefix")
    public final String getI18nResourceInfoPrefix() {
        return privateI18nResourceInfoPrefix;
    }

    public final void setI18nResourceInfoPrefix(String value) {
        privateI18nResourceInfoPrefix = value;
    }

    /**
     * 枚举启用
     */
    @JsonProperty("EnumItemDisabled")
    public final boolean getEnumItemDisabled() {
        return this.enumItemDisabled;
    }

    public final void setEnumItemDisabled(boolean value) {
        this.enumItemDisabled = value;
    }

    /**
     * 构造方法
     */
    public GspEnumValue() {
        setIsDefaultEnum(false);
    }

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    //#region ICloneable Members

    /**
     * 克隆
     *
     * @return
     */
    public final GspEnumValue clone() {
        GspEnumValue newEnumValue = new GspEnumValue();
        newEnumValue.val = this.val;
        newEnumValue.name = this.name;
        newEnumValue.index = this.index;
        newEnumValue.stringIndex = this.stringIndex;
        newEnumValue.setIsDefaultEnum(this.isDefault);
        newEnumValue.setI18nResourceInfoPrefix(this.getI18nResourceInfoPrefix());
        newEnumValue.setEnumItemDisabled(this.getEnumItemDisabled());
        return newEnumValue;
    }

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    //#endregion
}
