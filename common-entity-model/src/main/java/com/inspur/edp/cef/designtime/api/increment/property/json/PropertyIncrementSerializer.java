package com.inspur.edp.cef.designtime.api.increment.property.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;
import com.inspur.edp.cef.designtime.api.increment.property.IncrementPropType;
import com.inspur.edp.cef.designtime.api.increment.property.NativePropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.ObjectPropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.io.IOException;
import java.math.BigDecimal;

public class PropertyIncrementSerializer extends JsonSerializer<PropertyIncrement> {
    @Override
    public void serialize(PropertyIncrement value, JsonGenerator gen, SerializerProvider serializers) {
        SerializerUtils.writeStartObject(gen);
        writeBaseInfo(gen, value);

        SerializerUtils.writeEndObject(gen);
    }

    private void writeBaseInfo(JsonGenerator writer, PropertyIncrement value) {
        SerializerUtils.writePropertyValue(writer, CefNames.HasIncrement, value.hasIncrement());
        writeNativeInfo(writer, value);
    }

    private void writeNativeInfo(JsonGenerator writer, PropertyIncrement value) {
        if (!(value instanceof NativePropertyIncrement)) {
            return;
        }
        NativePropertyIncrement increment = (NativePropertyIncrement) value;
        IncrementPropType type = increment.getPropertyType();
        SerializerUtils.writePropertyValue(writer, CefNames.PropertyType, type);

        Object propertyValue = increment.getPropertyValue();
        SerializerUtils.writePropertyName(writer, CefNames.PropertyValue);
        if (propertyValue == null) {
            SerializerUtils.writePropertyValue_Null(writer);
            return;
        }

        switch (type) {
            case String:
                SerializerUtils.writePropertyValue_String(writer, propertyValue.toString());
                break;
            case Int:
                SerializerUtils.writePropertyValue_Integer(writer, (int) propertyValue);
                break;
            case Date:
                SerializerUtils.writePropertyValue_String(writer, propertyValue.toString());
            case Boolean:
                SerializerUtils.writePropertyValue_boolean(writer, (boolean) propertyValue);
                break;
            case Decimal:
                SerializerUtils.writePropertyValue_Number(writer, (BigDecimal) propertyValue);
                break;
            case Object:
                writeObjectValue(writer, (ObjectPropertyIncrement) increment);
                break;

        }
    }

    private void writeObjectValue(JsonGenerator writer, ObjectPropertyIncrement increment) {
        JsonSerializer serializer = increment.getJsonSerializer();

        if (serializer == null) {
            SerializerUtils.writePropertyValue_Object(writer, increment.getPropertyValue());
        } else {
            try {
                serializer.serialize(increment.getPropertyValue(), writer, null);
            } catch (IOException e) {
                throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_JSON_0004, e, "ObjectPropertyIncrement");
            }
        }

        SerializerUtils.writePropertyValue(writer, CefNames.ObjectIncrementType, increment.getClass().getTypeName());

    }

}
