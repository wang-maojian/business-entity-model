package com.inspur.edp.cef.designtime.api.validate.common;

import java.util.Arrays;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class Keywords {
    private static String[] keys;

    private static String[] getJavaKeywords() {
        String[] words = ("abstract,assert,boolean,break,byte,case,catch,char,class,const,continue,default,do,double,else,enum,extends,final,finally,float,for,goto,if,implements,import,instanceof,int,interface,long,native,new,package,private,protected,public,return,strictfp,short,static,super,switch,synchronized,this,throw,throws,transient,try,void,volatile,while,true,false,null,Class").split("[,]", -1);
        Arrays.sort(words);
        if (keys == null || keys.length <= 0) {
            keys = words;
        }
        return keys;
    }

    public static String[] javaKeyworks = getJavaKeywords();
}
