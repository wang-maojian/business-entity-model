package com.inspur.edp.cef.designtime.api.i18n.context;

import com.inspur.edp.cef.designtime.api.util.DataValidator;

public class CefResourceInfo {

    public CefResourceInfo(CefResourcePrefixInfo prefixInfo, String resourceKeyPropName, String descriptionPropName, String resourceValue) {
        DataValidator.checkForNullReference(prefixInfo, "PrefixInfo");
        DataValidator.checkForEmptyString(prefixInfo.getDescriptionPrefix(), "DescriptionPrefix");
        DataValidator.checkForEmptyString(prefixInfo.getResourceKeyPrefix(), "ResourceKeyPrefix");

        DataValidator.checkForEmptyString(resourceKeyPropName, "ResourceKeyPropName");
        DataValidator.checkForEmptyString(descriptionPropName, "DescriptionPropName");
        DataValidator.checkForEmptyString(resourceValue, "ResourceValue");

        this.prefixInfo = prefixInfo;
        this.resourceKeyPropName = resourceKeyPropName;
        this.descriptionPropName = descriptionPropName;
        this.resourceValue = resourceValue;
    }

    private final CefResourcePrefixInfo prefixInfo;

    public final CefResourcePrefixInfo getPrefixInfo() {
        return prefixInfo;
    }

    /**
     * 属性名
     * e.g. Name
     */
    private final String resourceKeyPropName;

    public final String getResourceKeyPropName() {
        return resourceKeyPropName;
    }

    /**
     * 描述中属性名
     * e.g. 名称
     */
    private final String descriptionPropName;

    public final String getDescriptionPropName() {
        return descriptionPropName;
    }

    /**
     * 默认语言的值
     */
    private final String resourceValue;

    public final String getResourceValue() {
        return resourceValue;
    }

    public String getResourceKey() {
        return getPrefixInfo().getResourceKeyPrefix() + "." + getResourceKeyPropName();
    }

    public String getDescription() {
        return getPrefixInfo().getDescriptionPrefix() + "的" + getDescriptionPropName();
    }
}