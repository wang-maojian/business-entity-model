package com.inspur.edp.cef.designtime.api.json.operation;

import com.inspur.edp.cef.designtime.api.collection.DtmElementCollection;

import java.util.HashMap;

public class DtmRequestChildElementsDeserializer extends RequestChildElementsDeserializer<DtmElementCollection> {

    @Override
    HashMap<String, DtmElementCollection> createHashMap() {
        return new HashMap<String, DtmElementCollection>();
    }

    @Override
    DtmElementCollection createValueType() {
        return new DtmElementCollection();
    }
}