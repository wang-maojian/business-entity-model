package com.inspur.edp.cef.designtime.api.element;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationKeyCollection;
import com.inspur.edp.cef.designtime.api.collection.GspFieldCollection;
import com.inspur.edp.cef.designtime.api.entity.GspCommonField;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;
import com.inspur.edp.cef.designtime.api.json.element.GspAssociationSerializer;

import java.util.ArrayList;

/**
 * The Definition Of Association
 *
 * @ClassName: Association
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonSerialize(using = GspAssociationSerializer.class)
//@JsonDeserialize(using = GspAssociationDeserializer.class)
public class GspAssociation implements Cloneable {
    private String id;
    private String refModelId = "";
    private String refModelPkgName = "";
    private String refModelCode = "";
    private String refModelName = "";
    private String refObjectId = "";
    private String refObjectCode = "";
    private String refObjectName = "";
    private GspAssociationKeyCollection associationKeyCollection = null;
    private ArrayList<AssoCondition> assoConditions;
    private ArrayList<AssoVariable> assoVariables;
    private IGspCommonField belongElement = null;
    private boolean keepAssoPropertyForExpression = false;
    /**
     * 字段集合
     */
    protected GspFieldCollection refElementCollection = null;
    private String where = "";
    private String assSendMessage = "";
    private ForeignKeyConstraintType foreignKeyConstraintType = ForeignKeyConstraintType.Permit;
    private GspDeleteRuleType deleteRuleType = GspDeleteRuleType.Refuse;

    /**
     * 关联表别名
     */
    private String refTableAlias = "";

    /**
     * 创建关联关系
     */
    public GspAssociation() {
        setForeignKeyConstraintType(ForeignKeyConstraintType.Permit);
        setDeleteRuleType(GspDeleteRuleType.Refuse);
        setAssoModelInfo(new AssoModelInfo());
    }

    /**
     * 标识。
     */
    public final String getId() {
        return this.id;
    }

    public final void setId(String value) {
        this.id = value;
    }

    /**
     * 引用的数据模型ID
     */
    public final String getRefModelID() {
        return refModelId;
    }

    public final void setRefModelID(String value) {
        refModelId = value;
    }

    /**
     * 引用的数据模型包ID
     */
    public final String getRefModelPkgName() {
        return refModelPkgName;
    }

    public final void setRefModelPkgName(String value) {
        refModelPkgName = value;
    }

    /**
     * 引用的数据模型编号
     */
    public final String getRefModelCode() {
        return refModelCode;
    }

    public final void setRefModelCode(String value) {
        refModelCode = value;
    }

    /**
     * 引用的数据模型名称
     */
    public final String getRefModelName() {
        return refModelName;
    }

    public final void setRefModelName(String value) {
        refModelName = value;
    }

    /**
     * 引用的节点ID
     */
    public final String getRefObjectID() {
        return refObjectId;
    }

    public final void setRefObjectID(String value) {
        refObjectId = value;
    }

    /**
     * 引用的节点名称
     */
    public final String getRefObjectName() {
        return refObjectName;
    }

    public final void setRefObjectName(String value) {
        refObjectName = value;
    }

    /**
     * 引用的节点编号
     */
    public final String getRefObjectCode() {
        return refObjectCode;
    }

    public final void setRefObjectCode(String value) {
        refObjectCode = value;
    }

    /**
     * 隶属于的元素
     */
    public final IGspCommonField getBelongElement() {
        if (belongElement == null) {
            belongElement = new GspCommonField();
        }
        return belongElement;
    }

    public final void setBelongElement(IGspCommonField value) {
        if (belongElement != value) {
            belongElement = value;
            this.getRefElementCollection().setParentObject(belongElement.getBelongObject());
        }
    }

    /**
     * KeyCollection
     */
    public final void setKeyCollection(GspAssociationKeyCollection value) {
        associationKeyCollection = value;
    }

    public final GspAssociationKeyCollection getKeyCollection() {
        if (associationKeyCollection == null) {
            associationKeyCollection = createKeyCollection();
        }
        return associationKeyCollection;
    }

    /**
     * 创建关联外键集合
     *
     * @return
     */
    protected GspAssociationKeyCollection createKeyCollection() {
        return new GspAssociationKeyCollection();
    }

    /**
     * 创建字段集合
     *
     * @return
     */
    protected GspFieldCollection createElementCollection() {
        return new GspFieldCollection(null);
    }

    /**
     * 引用的元素集合
     */
    public final void setRefElementCollection(GspFieldCollection value) {
        refElementCollection = value;
    }

    public final GspFieldCollection getRefElementCollection() {
        if (refElementCollection == null) {
            refElementCollection = new GspFieldCollection(getBelongElement().getBelongObject());
        }
        return refElementCollection;
    }

    /**
     * 过滤条件
     */
    public final String getWhere() {
        return this.where;
    }

    public final void setWhere(String value) {
        this.where = value;
    }

    /**
     * 提示错误信息
     */
    public final String getAssSendMessage() {
        return this.assSendMessage;
    }

    public final void setAssSendMessage(String value) {
        this.assSendMessage = value;
    }

    /**
     * 外键约束类型
     */
    public final ForeignKeyConstraintType getForeignKeyConstraintType() {
        return this.foreignKeyConstraintType;
    }

    public final void setForeignKeyConstraintType(ForeignKeyConstraintType value) {
        this.foreignKeyConstraintType = value;
    }

    /**
     * 删除规则类型
     */
    public final GspDeleteRuleType getDeleteRuleType() {
        return this.deleteRuleType;
    }

    public final void setDeleteRuleType(GspDeleteRuleType value) {
        this.deleteRuleType = value;
    }

    public String getRefTableAlias() {
        return refTableAlias;
    }

    public void setRefTableAlias(String refTableAlias) {
        this.refTableAlias = refTableAlias;
    }

    /**
     * 联接方式
     */
    public final JoinMode getJoinMode() {
        JoinMode joinMode = JoinMode.InnerJoin;
        if (this.belongElement != null) {
            if (this.belongElement.getIsRequire()) {
                joinMode = JoinMode.InnerJoin;
            } else {
                joinMode = JoinMode.OuterJoin;
            }
        }
        return joinMode;
    }

    /**
     * 国际化项前缀
     */
    private String privateI18nResourceInfoPrefix;

    public final String getI18nResourceInfoPrefix() {
        return privateI18nResourceInfoPrefix;
    }

    public final void setI18nResourceInfoPrefix(String value) {
        privateI18nResourceInfoPrefix = value;
    }

    /**
     * 关联模型信息
     */
    private AssoModelInfo privateAssoModelInfo;

    public final AssoModelInfo getAssoModelInfo() {
        return privateAssoModelInfo;
    }

    public final void setAssoModelInfo(AssoModelInfo value) {
        privateAssoModelInfo = value;
    }

    public ArrayList<AssoCondition> getAssoConditions() {
        return assoConditions;
    }

    public void setAssoConditions(ArrayList<AssoCondition> assoConditions) {
        this.assoConditions = assoConditions;
    }

    public ArrayList<AssoVariable> getAssoVariables() {
        return assoVariables;
    }

    public void setAssoVariables(ArrayList<AssoVariable> assoVariables) {
        this.assoVariables = assoVariables;
    }

    /**
     * 克隆
     *
     * @return
     */
    public final GspAssociation clone() {
        Object tempVar = null;
        try {
            tempVar = super.clone();
        } catch (CloneNotSupportedException e) {
            throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_CLONE_0001, e, "GspAssociation");
        }
        GspAssociation newAssociation = (GspAssociation) tempVar;
        newAssociation.id = this.id;
        newAssociation.refModelId = this.refModelId;
        newAssociation.refModelCode = this.refModelCode;
        newAssociation.refModelName = this.refModelName;
        newAssociation.belongElement = this.belongElement;
        newAssociation.where = this.where;
        newAssociation.assSendMessage = this.assSendMessage;
        newAssociation.foreignKeyConstraintType = this.foreignKeyConstraintType;
        newAssociation.deleteRuleType = this.deleteRuleType;
        Object tempVar2 = associationKeyCollection.clone();
        newAssociation.associationKeyCollection = (GspAssociationKeyCollection) ((tempVar2 instanceof GspAssociationKeyCollection)
                ? tempVar2
                : null);
        GspFieldCollection tempVar3 = refElementCollection.clone(null, newAssociation);
        newAssociation.refElementCollection = (GspFieldCollection) ((tempVar3 instanceof GspFieldCollection) ? tempVar3
                : null);
        return newAssociation;
    }

    /**
     * 克隆
     *
     * @param element
     * @return
     */
    public final GspAssociation clone(IGspCommonField element) {
        GspAssociation tempVar = clone();
        tempVar.belongElement = element;
        if (refElementCollection != null) {
            tempVar.refElementCollection = refElementCollection.clone(element.getBelongObject(), tempVar);
        }
        return tempVar;
    }

    public boolean isKeepAssoPropertyForExpression() {
        return keepAssoPropertyForExpression;
    }

    public void setKeepAssoPropertyForExpression(boolean keepAssoPropertyForExpression) {
        this.keepAssoPropertyForExpression = keepAssoPropertyForExpression;
    }
}
