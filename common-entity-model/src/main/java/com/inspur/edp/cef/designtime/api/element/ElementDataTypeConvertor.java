package com.inspur.edp.cef.designtime.api.element;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;
import com.inspur.edp.cef.entity.entity.FieldType;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.ObjectType;

public class ElementDataTypeConvertor {
    public static FieldType convertFieldTypeFromDevType(IGspCommonField field) {
        if (field.getIsUdt())
            return FieldType.Udt;

        switch (field.getMDataType()) {
            case String:
                return FieldType.String;
            case Text:
                return FieldType.Text;
            case Integer:
                return FieldType.Integer;
            case Decimal:
                return FieldType.Decimal;
            case Boolean:
                return FieldType.Boolean;
            case Date:
                return FieldType.Date;
            case DateTime:
                return FieldType.DateTime;
            case Binary:
                return FieldType.Binary;
            default:
                throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0002, field.getMDataType().toString());
        }
    }

    public static ObjectType convertObjectTypeFormDevTime(IGspCommonField field) {
        if (field.getIsUdt())
            return ObjectType.UDT;
        switch (field.getObjectType()) {
            case None:
                return ObjectType.Normal;
            case Association:
                return ObjectType.Association;
            case Enum:
                return ObjectType.Enum;
            case DynamicProp:
                return ObjectType.DynamicProp;
            default:
                throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0001, field.getObjectType().toString());
        }
    }
}
