package com.inspur.edp.cef.designtime.api.i18n.extractor;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationCollection;
import com.inspur.edp.cef.designtime.api.collection.GspEnumValueCollection;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.element.GspEnumValue;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.cef.designtime.api.i18n.names.CefResourceDescriptionNames;
import com.inspur.edp.cef.designtime.api.i18n.names.CefResourceKeyNames;

public abstract class CefFieldResourceExtractor extends AbstractResourceExtractor {
    private final IGspCommonField commonField;

    protected CefFieldResourceExtractor(IGspCommonField field, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(context, parentResourceInfo);
        commonField = field;
    }

    protected final void extractItems() {
        // Name
        addResourceInfo(CefResourceKeyNames.Name, commonField.getName(), CefResourceDescriptionNames.Name);

        //关联枚举
        switch (commonField.getObjectType()) {
            case Association:
                extractAssoInfo(commonField.getChildAssociations());
                break;
            case Enum:
                extractEnumValue(commonField.getContainEnumValues());
                break;
            case None:
                break;
        }

        //扩展
        extractExtendProperties(commonField);
    }

    /**
     * 赋值字段的国际化项前缀
     */
    protected final void setPrefixInfo() {
        commonField.setI18nResourceInfoPrefix(getCurrentResourcePrefixInfo().getResourceKeyPrefix());
    }

    protected CefResourcePrefixInfo buildCurrentPrefix() {
        if (getParentResourcePrefixInfo() == null) {
            throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_COMMON_ENTITY_0015, commonField.getBelongObject().getName(), commonField.getName());
        }
        CefResourcePrefixInfo fieldResourceInfo = new CefResourcePrefixInfo();
        fieldResourceInfo.setResourceKeyPrefix(getParentResourcePrefixInfo().getResourceKeyPrefix() + "." + commonField.getLabelID());
        fieldResourceInfo.setDescriptionPrefix(getParentResourcePrefixInfo().getDescriptionPrefix() + "中'" + commonField.getName() + "'属性");
        return fieldResourceInfo;
    }


    //#region 私有方法
    private void extractAssoInfo(GspAssociationCollection assos) {
        if (assos != null && !assos.isEmpty()) {
            for (GspAssociation item : assos) {
                getAssoResourceExtractor(getContext(), getCurrentResourcePrefixInfo(), item).extract();
            }
        }
    }

    private void extractEnumValue(GspEnumValueCollection enumValues) {
        if (enumValues != null && !enumValues.isEmpty()) {
            for (GspEnumValue item : enumValues) {
                String keyPropName = item.getValue() + "." + CefResourceKeyNames.DisplayValue;
                String value = item.getName();
                String descriptionPropName = "枚举信息'" + item.getName() + "'的" + CefResourceDescriptionNames.DisplayValue;
                addResourceInfo(keyPropName, value, descriptionPropName);

                String enumKey = getCurrentResourcePrefixInfo().getResourceKeyPrefix() + "." + item.getValue();
                item.setI18nResourceInfoPrefix(enumKey);
            }
        }
    }

    //#endregion

    protected void extractExtendProperties(IGspCommonField commonField) {
    }

    protected abstract AssoResourceExtractor getAssoResourceExtractor(ICefResourceExtractContext context, CefResourcePrefixInfo fieldPrefixInfo, GspAssociation asso);

}