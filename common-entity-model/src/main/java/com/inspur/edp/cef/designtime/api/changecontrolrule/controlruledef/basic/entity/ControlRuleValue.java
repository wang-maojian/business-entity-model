package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity;

/**
 * The Definition Of ControlRuleValue
 *
 * @ClassName: ControlRuleValue
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum ControlRuleValue {
    Allow,
    Forbiddon,
    Default
}
