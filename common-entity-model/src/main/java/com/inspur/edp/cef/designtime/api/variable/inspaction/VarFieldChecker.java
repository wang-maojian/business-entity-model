package com.inspur.edp.cef.designtime.api.variable.inspaction;

import com.inspur.edp.cef.designtime.api.validate.element.FieldChecker;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VarFieldChecker extends FieldChecker {
    private static VarFieldChecker varFieldInspection;

    public static VarFieldChecker getInstance() {
        if (varFieldInspection == null) {
            varFieldInspection = new VarFieldChecker();
        }
        return varFieldInspection;
    }
}
