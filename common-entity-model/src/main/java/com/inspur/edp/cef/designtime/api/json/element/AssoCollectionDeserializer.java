package com.inspur.edp.cef.designtime.api.json.element;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.io.IOException;

public class AssoCollectionDeserializer extends JsonDeserializer<GspAssociationCollection> {
    public CefFieldDeserializer getCefFieldDeserializer() {
        return cefFieldDeserializer;
    }

    public void setCefFieldDeserializer(CefFieldDeserializer cefFieldDeserializer) {
        this.cefFieldDeserializer = cefFieldDeserializer;
    }

    private CefFieldDeserializer cefFieldDeserializer;

    @Override
    public GspAssociationCollection deserialize(JsonParser jsonParser,
                                                DeserializationContext ctxt) throws IOException, JsonProcessingException {
        GspAssociationCollection collection = new GspAssociationCollection();
        GspAssociationDeserializer deserializer = new GspAssociationDeserializer(this.cefFieldDeserializer);
        SerializerUtils.readArray(jsonParser, deserializer, collection);
        return collection;
    }
}
