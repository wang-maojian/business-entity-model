package com.inspur.edp.cef.designtime.api.increment.property;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationCollection;
import com.inspur.edp.cef.designtime.api.json.element.AssoCollectionDeserializer;

public class AssoColPropIncrement extends ObjectPropertyIncrement {
    public AssoColPropIncrement(boolean hasIncrement) {
        super(hasIncrement);
    }

    public Class getPropertyIncrementClass() {
        return GspAssociationCollection.class;
    }

    public JsonDeserializer getJsonDeserializer() {
        return new AssoCollectionDeserializer();
    }
}
