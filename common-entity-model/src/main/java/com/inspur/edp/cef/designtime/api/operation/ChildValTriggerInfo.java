package com.inspur.edp.cef.designtime.api.operation;

import com.inspur.edp.cef.designtime.api.collection.ValElementCollection;

import java.util.EnumSet;

public class ChildValTriggerInfo {
    ValElementCollection requestChildElements;
    private EnumSet<ExecutingDataStatus> status = EnumSet.of(ExecutingDataStatus.forValue(0));

    public ValElementCollection getRequestChildElements() {
        return requestChildElements;
    }

    public void setRequestChildElements(ValElementCollection requestChildElements) {
        this.requestChildElements = requestChildElements;
    }

    public final EnumSet<ExecutingDataStatus> getGetExecutingDataStatus() {
        return status;
    }

    public final void setGetExecutingDataStatus(EnumSet<ExecutingDataStatus> value) {
        status = value;
    }
}

