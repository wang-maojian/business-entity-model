package com.inspur.edp.cef.designtime.api.json.element;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.entity.MappingRelation;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.io.IOException;

public class MappingRelationSerializer extends JsonSerializer<MappingRelation> {
    @Override
    public void serialize(MappingRelation mapping, JsonGenerator writer, SerializerProvider serializers) throws IOException {
        if (mapping == null || mapping.getCount() <= 0)
            return;
        SerializerUtils.WriteStartArray(writer);
        for (String key : mapping.getKeys()) {
            SerializerUtils.writeStartObject(writer);
            SerializerUtils.writePropertyValue(writer, key, mapping.getMappingInfo(key));
            SerializerUtils.writeEndObject(writer);
        }
        SerializerUtils.WriteEndArray(writer);
    }
}
