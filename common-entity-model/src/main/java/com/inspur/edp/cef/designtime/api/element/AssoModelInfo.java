package com.inspur.edp.cef.designtime.api.element;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inspur.edp.cef.designtime.api.json.CefNames;

/**
 * The Definition Of AssoModelInfo
 *
 * @ClassName: ElementDefaultVauleType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class AssoModelInfo {
    /**
     * 模型ConfigID
     */
    private String privateModelConfigId;

    @JsonProperty(CefNames.ModelConfigId)
    public final String getModelConfigId() {
        return privateModelConfigId;
    }

    public final void setModelConfigId(String value) {
        privateModelConfigId = value;
    }

    /**
     * 主表编号
     */
    private String privateMainObjCode;

    @JsonProperty(CefNames.MainObjCode)
    public final String getMainObjCode() {
        return privateMainObjCode;
    }

    public final void setMainObjCode(String value) {
        privateMainObjCode = value;
    }

}