package com.inspur.edp.cef.designtime.api.i18n.context;

public class CefResourcePrefixInfo {
    private String resourceKeyPrefix;
    private String descriptionPrefix;


    public String getResourceKeyPrefix() {
        return resourceKeyPrefix;
    }

    public void setResourceKeyPrefix(String resourceKeyPrefix) {
        this.resourceKeyPrefix = resourceKeyPrefix;
    }

    public String getDescriptionPrefix() {
        return descriptionPrefix;
    }

    public void setDescriptionPrefix(String descriptionPrefix) {
        this.descriptionPrefix = descriptionPrefix;
    }
}

