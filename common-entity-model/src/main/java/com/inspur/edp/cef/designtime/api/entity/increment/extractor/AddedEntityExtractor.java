package com.inspur.edp.cef.designtime.api.entity.increment.extractor;

import com.inspur.edp.cef.designtime.api.entity.GspCommonDataType;
import com.inspur.edp.cef.designtime.api.entity.increment.AddedEntityIncrement;

public class AddedEntityExtractor {

    public AddedEntityIncrement extract(GspCommonDataType newDataType) {
        AddedEntityIncrement addedEntityIncrement = createIncrement();
        addedEntityIncrement.setAddedDataType(newDataType);
        return addedEntityIncrement;
    }

    protected AddedEntityIncrement createIncrement() {
        return new AddedEntityIncrement();
    }
}
