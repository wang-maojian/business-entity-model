package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm;

/**
 * The Serlializer Names Of CommonFieldRule
 *
 * @ClassName: CommonFieldRuleNames
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonFieldRuleNames {
    public static String Code = "Code";
    public static String Name = "Name";
    public static String Length = "Length";
    public static String Precision = "Precision";
    public static String DefaultValue = "DefaultValue";
    public static String ObjectType = "ObjectType";
    public static String MultiLanField = "MultiLanField";
    public static String Readonly = "Readonly";
    public static String Required = "Required";
    public static String RuleObjectType = "CommonField";
}
