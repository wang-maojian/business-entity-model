package com.inspur.edp.cef.designtime.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inspur.edp.cef.designtime.api.json.CefNames;

/**
 * The Definition Of CustomizationInfo
 *
 * @ClassName: CustomizationInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CustomizationInfo {

    private boolean isCustomized = false;

    @JsonProperty(CefNames.IsCustomized)
    public boolean isCustomized() {
        return isCustomized;
    }

    public void setCustomized(boolean customized) {
        isCustomized = customized;
    }

    private DimensionInfo dimensionInfo = new DimensionInfo();

    @JsonProperty(CefNames.DimensionInfo)
    public DimensionInfo getDimensionInfo() {
        return dimensionInfo;
    }

    public void setDimensionInfo(DimensionInfo dimensionInfo) {
        this.dimensionInfo = dimensionInfo;
    }
}
