package com.inspur.edp.cef.designtime.api.element.increment.extract;

import com.inspur.edp.cef.designtime.api.element.increment.AddedFieldIncrement;
import com.inspur.edp.cef.designtime.api.entity.GspCommonField;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class AddedFieldExtractor {

    public AddedFieldIncrement extract(GspCommonField field) {
        AddedFieldIncrement addedEntityIncrement = createIncrement();
        addedEntityIncrement.setField(field);
        return addedEntityIncrement;
    }

    protected AddedFieldIncrement createIncrement() {
        return new AddedFieldIncrement();
    }
}
