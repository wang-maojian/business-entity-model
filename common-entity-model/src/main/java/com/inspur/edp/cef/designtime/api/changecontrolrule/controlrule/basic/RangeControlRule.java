package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic;

import java.util.HashMap;
import java.util.Map;

/**
 * The Definition Of RangeControlRule
 *
 * @ClassName: RangeControlRule
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class RangeControlRule extends AbstractControlRule {

    private final Map<String, Map<String, ControlRuleItem>> rangeControlRules = new HashMap<>();

    public Map<String, Map<String, ControlRuleItem>> getRangeControlRules() {
        return rangeControlRules;
    }

    public ControlRuleItem getRangeControlRuleItem(String ruleObjectType, String ruleName) {
        if (!getRangeControlRules().containsKey(ruleObjectType))
            return null;
        Map<String, ControlRuleItem> objectRules = getRangeControlRules().get(ruleObjectType);
        if (objectRules == null || !objectRules.containsKey(ruleName))
            return null;
        return objectRules.get(ruleName);
    }

    public void setRangeControlRules(String ruleObjectType, String ruleName, ControlRuleItem ruleItem) {
        if (!getRangeControlRules().containsKey(ruleObjectType)) {
            Map<String, ControlRuleItem> map = new HashMap<>();
            getRangeControlRules().put(ruleObjectType, map);
        }
        Map<String, ControlRuleItem> objectRules = getRangeControlRules().get(ruleObjectType);
        objectRules.put(ruleName, ruleItem);
    }
}
