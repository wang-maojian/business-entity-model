package com.inspur.edp.cef.designtime.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inspur.edp.caf.cef.schema.datatype.StructuredType;
import com.inspur.edp.caf.cef.schema.element.Property;
import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.collection.CommonDtmCollection;
import com.inspur.edp.cef.designtime.api.collection.CommonValCollection;
import com.inspur.edp.cef.designtime.api.collection.GspFieldCollection;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;
import com.inspur.edp.cef.designtime.api.util.Guid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class GspCommonDataType implements IGspCommonDataType, StructuredType {
    // region 私有属性
    private String id = Guid.newGuid().toString();
    ;
    private String code = "";
    private String name = "";
    private final CommonDtmCollection dtmBeforeSave = new CommonDtmCollection();
    private final CommonDtmCollection dtmAfterSave = new CommonDtmCollection();
    private final CommonDtmCollection dtmAfterCreate = new CommonDtmCollection();
    private final CommonDtmCollection dtmAfterModify = new CommonDtmCollection();
    private final CommonValCollection valBeforeSave = new CommonValCollection();
    private final CommonValCollection valAfterModify = new CommonValCollection();
    private GspFieldCollection containElements = new GspFieldCollection();
    private CustomizationInfo customizationInfo = new CustomizationInfo();
    private List<String> beLabel;
    private List<String> bizTagIds;
    private boolean bigNumber;
    private Map<String, String> extendInfo = new HashMap<>();
    // endregion

    // region 构造函数
    protected GspCommonDataType() {
        setContainElements(new GspFieldCollection());
        // DynamicPropSerializerComps = new List<MdRefInfo>();

        setIsRef(false);
    }
    // endregion

    // region 公共属性

    /**
     * 唯一标识
     */
    @JsonIgnore
    public String getID() {
        return id;
    }

    public void setID(String value) {
        id = value;
    }

    /**
     * 编码
     */
    public String getCode() {
        return code;
    }

    public void setCode(String value) {
        code = value;
    }

    /**
     * 名称
     */
    public String getName() {
        return name;
    }


    public void setName(String value) {
        name = value;
    }

    /**
     * 标签
     *
     * @return
     */
    public List<String> getBeLabel() {
        if (beLabel == null) {
            beLabel = new ArrayList<String>();
            return beLabel;
        }
        return beLabel;
    }

    public void setBeLabel(List<String> value) {
        beLabel = value;
    }

    ;

    public List<String> getBizTagIds() {
        if (bizTagIds == null) {
            bizTagIds = new ArrayList<String>();
            return bizTagIds;
        }
        return bizTagIds;
    }

    public void setBizTagIds(List<String> value) {
        bizTagIds = value;
    }

    ;
    /**
     * 是否引用对象
     */
    private boolean privateIsRef;

    public final boolean getIsRef() {
        return privateIsRef;
    }

    public final void setIsRef(boolean value) {
        privateIsRef = value;
    }

    /**
     * bigNumber
     */
    @Deprecated
    public boolean isBigNumber() {
        return this.bigNumber;
    }

    @Deprecated
    public void setIsBigNumber(boolean value) {
        this.bigNumber = value;
    }

    /**
     * 当前节点字段集合
     *
     */
    // private IFieldCollection IGspCommonDataType.ContainElements=>

    // getContainElements();

    /**
     * 当前节点字段集合
     */
    public GspFieldCollection getContainElements() {
        return containElements;
    }

    public void setContainElements(GspFieldCollection value) {
        this.containElements = value;
    }

    /**
     * 保存前联动计算
     */
    public CommonDtmCollection getDtmBeforeSave() {
        return dtmBeforeSave;
    }

    public CommonDtmCollection getAllDtmBeforeSave() {
        return new CommonDtmCollection();
    }

    public void setDtmBeforeSave(CommonDtmCollection value) {
        setDtms(dtmBeforeSave, value);
    }

    public void setDtmAfterSave(CommonDtmCollection value) {
        setDtms(dtmAfterSave, value);
    }

    public CommonDtmCollection getDtmAfterSave() {
        return dtmAfterSave;
    }

    /**
     * 更新后联动计算
     */
    public CommonDtmCollection getDtmAfterModify() {
        return dtmAfterModify;
    }

    public CommonDtmCollection getAllDtmAfterModify() {
        return new CommonDtmCollection();
    }

    public void setDtmAfterModify(CommonDtmCollection value) {
        setDtms(dtmAfterModify, value);
    }

    /**
     * 新建后联动计算
     */
    public CommonDtmCollection getDtmAfterCreate() {
        return dtmAfterCreate;
    }

    public CommonDtmCollection getAllDtmAfterCreate() {
        return new CommonDtmCollection();
    }

    public void setDtmAfterCreate(CommonDtmCollection value) {
        setDtms(dtmAfterCreate, value);
    }

    /**
     * 保存前校验规则
     */
    public CommonValCollection getValBeforeSave() {
        return valBeforeSave;
    }

    public CommonValCollection getallValBeforeSave() {
        return new CommonValCollection();
    }

    public void setValBeforeSave(CommonValCollection value) {
        setVals(valBeforeSave, value);
    }

    /**
     * 更新后校验规则
     */
    public CommonValCollection getValAfterModify() {
        return valAfterModify;
    }

    public CommonValCollection getAllValAfterModify() {
        return new CommonValCollection();
    }

    public void setValAfterModify(CommonValCollection value) {
        setVals(valAfterModify, value);
    }

    public void setDtms(CommonDtmCollection dtms, CommonDtmCollection value) {
        if (value == null || value.isEmpty())
            return;
        if (dtms == null) {
            dtms = value;
            return;
        }
        for (CommonDetermination commonDetermination : value) {
            CommonDetermination exists = dtms.stream().filter(item -> item.getID().equals(commonDetermination.getID())).findFirst().orElse(null);
            if (exists == null) {
                dtms.add(commonDetermination);
            }
        }
    }

    public void setVals(CommonValCollection vals, CommonValCollection value) {
        if (value == null || value.isEmpty())
            return;
        if (vals == null) {
            return;
        }
        for (CommonValidation commonValidation : value) {
            CommonValidation exists = vals.stream().filter(item -> item.getID().equals(commonValidation.getID())).findFirst().orElse(null);
            if (exists == null) {
                vals.add(commonValidation);
            }
        }
    }

    /**
     * 国际化项前缀
     */
    private String privateI18nResourceInfoPrefix;

    public final String getI18nResourceInfoPrefix() {
        return privateI18nResourceInfoPrefix;
    }

    public final void setI18nResourceInfoPrefix(String value) {
        privateI18nResourceInfoPrefix = value;
    }

    /**
     * 运行时定制信息
     */
    public CustomizationInfo getCustomizationInfo() {
        return customizationInfo;
    }

    public void setCustomizationInfo(CustomizationInfo customizationInfo) {
        this.customizationInfo = customizationInfo;
    }
    // endregion

    // region 方法
    public abstract IGspCommonField findElement(String id);

    public abstract ClassInfo getGeneratedEntityClassInfo();

    // endregion

    /**
     * 根据标签获取字段列表
     *
     * @return
     */
    public List<IGspCommonField> getFieldsByBeLabel() {
        List<IGspCommonField> list = new ArrayList<>();
        for (IGspCommonField item : getContainElements()) {
            if (!item.getBeLabel().isEmpty()) {

                list.add(item);

            }
        }
        return list;
    }

    public Map<String, String> getExtendInfo() {
        return extendInfo;
    }

    public void setExtendInfo(Map<String, String> extendInfo) {
        this.extendInfo = extendInfo;
    }


    // region StructureType
    @Override
    public final String getId() {
        return getID();
    }

    @Override
    public final String getDescription() {
        return "";
    }

    @Override
    public List<Property> getProperties() {
        List<Property> list = new ArrayList<>();
        for (IGspCommonField item : getContainElements()) {
            list.add((GspCommonField) item);
        }
        return list;
    }

    @Override
    public final String getUri() {
        return "";
    }

    @Override
    public final String getKind() {
        return getStructuredTypeKind();
    }

    @Override
    public final List<String> getExtensionKeys() {
        return null;
    }

    @Override
    public final String getExtensionValue(String s) {
        return null;
    }

    protected String getStructuredTypeKind() {
        // 子类实现
        return "";
    }
    // endregion

}
