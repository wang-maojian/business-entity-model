package com.inspur.edp.cef.designtime.api.exceptions;

import lombok.Getter;

public enum CommonEntityErrorCodeEnum {
    /*--- TEMPLATE ERROR START ---*/
    /**
     * [{0}]
     */
    GSP_BEMODEL_TEMPLATE_ERROR(true),
    /* --- TEMPLATE ERROR END ---*/

    /* --- JSON SERIALIZER ERROR START ---*/
    /**
     * [{0}]反序列化失败
     */
    GSP_BEMODEL_JSON_0001(false),
    /**
     * [{0}]:[{1}]反序列化失败
     */
    GSP_BEMODEL_JSON_0002(false),
    /**
     * JSON解析失败,检查Json结构
     */
    GSP_BEMODEL_JSON_0003(false),
    /**
     * [{0}]序列化失败
     */
    GSP_BEMODEL_JSON_0004(false),
    /**
     * [{0}]:[{1}]序列化失败
     */
    GSP_BEMODEL_JSON_0005(false),
    /**
     * [{0}]反序列化失败，类名为[{1}]
     */
    GSP_BEMODEL_JSON_0006(false),
    /**
     * [{0}]序列化失败，类名为[{1}]
     */
    GSP_BEMODEL_JSON_0007(false),
    /**
     * 反序列化失败，未识别数据类型:[{0}]
     */
    GSP_BEMODEL_JSON_0008(false),
    /**
     * 反序列化失败
     */
    GSP_BEMODEL_JSON_0009(false),
    /**
     * 解析文本失败
     */
    GSP_BEMODEL_JSON_0010(false),
    /**
     * 解析数组失败
     */
    GSP_BEMODEL_JSON_0011(false),
    /**
     * 当前JsonToken应为[{0}],实际为[{1}]
     */
    GSP_BEMODEL_JSON_0012(false),
    /* --- JSON SERIALIZER ERROR END ---*/

    /* --- COMMON EXCEPTION ERROR START ---*/
    /**
     * [{0}]实例化失败
     */
    GSP_BEMODEL_COMMON_0010(false),
    /* --- COMMON EXCEPTION ERROR END ---*/

    /* --- CLONE ERROR START ---*/
    /**
     * [{0}]克隆失败
     */
    GSP_BEMODEL_CLONE_0001(false),
    /**
     * 克隆字段失败
     */
    GSP_BEMODEL_CLONE_0003(false),
    /* --- CLONE ERROR END ---*/

    /* --- ENUM NOT SUPPORT ERROR START --*/
    /**
     * 不支持的对象类型:[{0}]
     */
    GSP_BEMODEL_ENUM_NOTSUPPORT_0001(true),
    /**
     * 不支持的数据类型:[{0}]
     */
    GSP_BEMODEL_ENUM_NOTSUPPORT_0002(true),
    /**
     * 不支持的JAVA限定词或修饰符:[{0}]
     */
    GSP_BEMODEL_ENUM_NOTSUPPORT_0003(true),
    /**
     * 不支持的增量类型:[{0}]
     */
    GSP_BEMODEL_ENUM_NOTSUPPORT_0004(true),
    /* --- ENUM NOT SUPPORT ERROR END --*/

    /* --- COMMON ENTITY ERROR START ---*/
    /**
     * 根据UDT（业务资源）元数据ID[{1}]找不到字段[{0}]对应的UDT元数据
     */
    GSP_BEMODEL_COMMON_ENTITY_0001(true),
    /**
     * 字段[{0}]类型不属于关联业务字段，不可调用方法[{1}]
     */
    GSP_BEMODEL_COMMON_ENTITY_0002(true),
    /**
     * 字段[{0}]类型为关联，但是关联信息内容为空，请检查。
     */
    GSP_BEMODEL_COMMON_ENTITY_0003(true),
    /**
     * 字段[{0}]类型为关联，但是不存在关联带出字段，请检查。
     */
    GSP_BEMODEL_COMMON_ENTITY_0004(true),
    /**
     * 生成文件失败
     */
    GSP_BEMODEL_COMMON_ENTITY_0005(false),
    /**
     * [{0}]未识别的属性名：[{1}]
     */
    GSP_BEMODEL_COMMON_ENTITY_0006(false),
    /**
     * 根据规则名称[{0}]获取规则失败
     */
    GSP_BEMODEL_COMMON_ENTITY_0007(false),
    /**
     * 根据规则名称[{0}]获取到的规则内容为空，请了解。
     */
    GSP_BEMODEL_COMMON_ENTITY_0008(false),
    /**
     * 对象[{0}]中没有字段信息，请检查
     */
    GSP_BEMODEL_COMMON_ENTITY_0009(true),
    /**
     * 当前对象[{0}]不允许添加字段
     */
    GSP_BEMODEL_COMMON_ENTITY_0010(true),
    /**
     * 对象[{0}]上没有找到字段[{1}]
     */
    GSP_BEMODEL_COMMON_ENTITY_0011(true),
    /**
     * [多语项抽取]非关联带出字段，不应使用AssoRefFieldResourceExtractor进行抽取。
     */
    GSP_BEMODEL_COMMON_ENTITY_0012(false),
    /**
     * 多语项抽取_字段关联信息[{0}]无对应的多语前缀信息。
     */
    GSP_BEMODEL_COMMON_ENTITY_0013(false),
    /**
     * 多语项抽取_字段[{0}]无对应的多语前缀信息。
     */
    GSP_BEMODEL_COMMON_ENTITY_0014(false),
    /**
     * 多语项抽取_节点[{0}]中字段[{1}]，无节点对应的多语前缀信息。
     */
    GSP_BEMODEL_COMMON_ENTITY_0015(false),
    /**
     * 暂不支持[{0}]类型字段的多语合并
     */
    GSP_BEMODEL_COMMON_ENTITY_0016(false),
    /**
     * 字段[{1}](编号:{0})的数据类型为空，请检查。
     */
    GSP_BEMODEL_COMMON_ENTITY_0017(true),
    /**
     * 多语项抽取_节点[{0}]所属模型，无对应的多语前缀信息。
     */
    GSP_BEMODEL_COMMON_ENTITY_0018(false),
    /**
     * 没有找到对应的控制方式:[{0}]
     */
    GSP_BEMODEL_COMMON_ENTITY_0019(false),
    /**
     * 属性[{0}]的值不允许增加
     */
    GSP_BEMODEL_COMMON_ENTITY_0020(true),
    /**
     * 属性[{0}]的值不允许设置为否
     */
    GSP_BEMODEL_COMMON_ENTITY_0021(true),

    /* --- COMMON ENTITY ERROR END ---*/

    /**
     * ID为[{0}]的实体[{1}]上未找到编号为[{2}]的字段
     */
    REF_BE_STRUCTURE_NOT_FOUND(false);

    /**
     * 是否业务异常
     */
    @Getter
    private final boolean bizException;

    CommonEntityErrorCodeEnum(boolean bizException) {
        this.bizException = bizException;
    }
}
