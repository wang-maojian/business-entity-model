package com.inspur.edp.cef.designtime.api.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.cef.designtime.api.collection.ValElementCollection;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.operation.ChildValTriggerInfo;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.cef.designtime.api.operation.CommonTriggerPointType;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * The Json Parser Of Common Validation
 *
 * @ClassName: CommonValDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonValDeserializer extends CommonOpDeserializer {
    @Override
    protected void beforeDeserializeCommonOp(CommonOperation op) {
        CommonValidation val = (CommonValidation) op;
        val.setRequestElements(new ValElementCollection());
    }

    @Override
    protected boolean readExtendOpProperty(CommonOperation op, String propName, JsonParser jsonParser) {
        boolean result = true;
        CommonValidation val = (CommonValidation) op;
        switch (propName) {
            case CefNames.GetExecutingDataStatus:
                val.setGetExecutingDataStatus(readGetExecutingDataStatus(jsonParser));
                break;
            case CefNames.RequestElements:
                val.setRequestElements(readRequestElements(jsonParser));
                break;
            case CefNames.RequestChildElements:
                readRequestChildElements(val, jsonParser);
                break;
            case CefNames.ChildTriggerInfo:
                val.setChildTriggerInfo(readChildTriggerInfo(jsonParser));
                break;
            case CefNames.TriggerPointType:
                val.setTriggerPointType(SerializerUtils
                        .readPropertyValue_Enum(jsonParser, CommonTriggerPointType.class,
                                CommonTriggerPointType.values()));
                break;
            default:
                result = readExtendValProp(op, propName, jsonParser);
                break;
        }
        return result;
    }

    protected boolean readExtendValProp(CommonOperation op, String propName, JsonParser jsonParser) {
        return false;
    }

    private ValElementCollection readRequestElements(JsonParser jsonParser) {
        ArrayList<String> list = SerializerUtils.readStringArray(jsonParser);
        ValElementCollection collection = new ValElementCollection();
        collection.addAll(list);
        return collection;
    }

    private void readRequestChildElements(CommonValidation val, JsonParser jsonParser) {
        RequestChildElementsDeserializer deserializer = new ValRequestChildElementsDeserializer();
        HashMap<String, ValElementCollection> childRequestELements = deserializer.deserialize(jsonParser, null);
        val.setRequestChildElements(childRequestELements);
    }

    private HashMap<String, ChildValTriggerInfo> readChildTriggerInfo(JsonParser jsonParser) {
        CommonChildValTriggerInfoDeserializer deserializer = new CommonChildValTriggerInfoDeserializer();
        return deserializer.deserialize(jsonParser, null);
    }

    @Override
    protected CommonOperation CreateCommonOp() {
        return new CommonValidation();
    }
}
