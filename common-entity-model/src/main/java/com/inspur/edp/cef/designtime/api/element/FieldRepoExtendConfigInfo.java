package com.inspur.edp.cef.designtime.api.element;

/**
 * 字段持久化扩展信息
 */
public class FieldRepoExtendConfigInfo {
    private String Id;
    private String configId;
    private String configCode;
    private String configName;
    private String configClassImpl;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getConfigCode() {
        return configCode;
    }

    public void setConfigCode(String configCode) {
        this.configCode = configCode;
    }

    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    public String getConfigClassImpl() {
        return configClassImpl;
    }

    public void setConfigClassImpl(String configClassImpl) {
        this.configClassImpl = configClassImpl;
    }
}
