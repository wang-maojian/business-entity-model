package com.inspur.edp.cef.designtime.api.json.element;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.entity.MappingInfo;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.io.IOException;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

/**
 * The Json Parser Of MappingInfo
 *
 * @ClassName: MappingInfoDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappingInfoDeserializer extends JsonDeserializer<MappingInfo> {
    @Override
    public MappingInfo deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        MappingInfo mappingInfo = new MappingInfo();
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            String propValue = SerializerUtils.readPropertyValue_String(jsonParser);
            mappingInfo.setKeyInfo(propName);
            mappingInfo.setValueInfo(propValue);
        }
        SerializerUtils.readEndObject(jsonParser);
        return mappingInfo;
    }
}
