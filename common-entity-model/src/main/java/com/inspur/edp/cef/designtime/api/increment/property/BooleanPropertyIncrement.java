package com.inspur.edp.cef.designtime.api.increment.property;

public class BooleanPropertyIncrement extends NativePropertyIncrement {

    public BooleanPropertyIncrement(boolean hasIncrement) {
        super(hasIncrement);
    }

    @Override
    public IncrementPropType getPropertyType() {
        return IncrementPropType.Boolean;
    }
}
