package com.inspur.edp.cef.designtime.api.entity;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappingInfo {
    private String privateKeyInfo;

    public final String getKeyInfo() {
        return privateKeyInfo;
    }

    public final void setKeyInfo(String value) {
        privateKeyInfo = value;
    }

    private String privateValueInfo;

    public final String getValueInfo() {
        return privateValueInfo;
    }

    public final void setValueInfo(String value) {
        privateValueInfo = value;
    }
}