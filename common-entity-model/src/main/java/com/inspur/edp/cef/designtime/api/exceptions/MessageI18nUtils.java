/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.exceptions;

import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.i18n.api.ResourceLocalizer;
import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;

/**
 * 国际化资源文件信息构造工具类
 */
public class MessageI18nUtils {
    // 资源文件名称
    private static final String RESOURCE_FILE = "business_entity_model_exception.properties";
    // 资源文件所属SU
    private static final String SU = "pfcommon";
    // 资源文件本地化对象
    private static ResourceLocalizer resourceLocalizer;

    /**
     * 获取国际化资源信息
     *
     * @param msgCode       消息编码
     * @param messageParams 消息参数
     * @return 国际化消息
     */
    public static String getMessage(String msgCode, Object... messageParams) {
        // 获取资源文件中对应消息编码的文本内容
        String message = getMessage(msgCode);
        // 如果消息参数为空，则直接返回消息编码
        if (messageParams == null || messageParams.length < 1) {
            return message;
        }
        // 根据消息参数构造消息
        return MessageFormat.format(message, messageParams);
    }

    /**
     * 获取国际化资源信息
     *
     * @param msgCode 消息编码
     * @return 国际化消息
     */
    public static String getMessage(String msgCode) {
        // 获取资源文件本地化对象
        if (resourceLocalizer == null) {
            resourceLocalizer = SpringBeanUtils.getBean(ResourceLocalizer.class);
        }
        // 如果资源文件本地化对象为空，则返回消息编码
        if (resourceLocalizer == null) {
            return msgCode;
        }
        // 从国际化资源文件中获取对应编码的消息
        String messageFormat = resourceLocalizer.getString(msgCode, RESOURCE_FILE, SU, CAFContext.current.getLanguage());
        if (StringUtils.isEmpty(messageFormat)) {
            messageFormat = msgCode;
        }
        return messageFormat;
    }
}
