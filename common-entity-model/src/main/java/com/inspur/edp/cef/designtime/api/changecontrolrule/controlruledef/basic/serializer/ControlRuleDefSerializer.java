package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefinition;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.util.Map;

/**
 * The Json Serializer Of ControlRuleDef
 *
 * @ClassName: ControlRuleDefSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ControlRuleDefSerializer<T extends ControlRuleDefinition> extends JsonSerializer<T> {
    @Override
    public void serialize(T controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        SerializerUtils.writeStartObject(jsonGenerator);
        writeSelfInfos(controlRuleDefinition, jsonGenerator, serializerProvider);
        writeExtendInfos(controlRuleDefinition, jsonGenerator, serializerProvider);
        SerializerUtils.writeEndObject(jsonGenerator);
    }

    private void writeSelfInfos(ControlRuleDefinition controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        writeRuleObjectType(controlRuleDefinition, jsonGenerator, serializerProvider);
        writeSelfControlRules(controlRuleDefinition, jsonGenerator, serializerProvider);
        writeChildControlRules(controlRuleDefinition, jsonGenerator, serializerProvider);
    }

    private void writeRuleObjectType(ControlRuleDefinition controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        SerializerUtils.writePropertyValue(jsonGenerator, ControlRuleDefNames.RuleObjectType, controlRuleDefinition.getRuleObjectType());
    }

    private void writeSelfControlRules(ControlRuleDefinition controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        SerializerUtils.writePropertyName(jsonGenerator, ControlRuleDefNames.SelfControlRules);
        SerializerUtils.WriteStartArray(jsonGenerator);
        innerWriteSelfControlRules(controlRuleDefinition, jsonGenerator, serializerProvider);
        SerializerUtils.WriteEndArray(jsonGenerator);
    }

    private void innerWriteSelfControlRules(ControlRuleDefinition controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        if (controlRuleDefinition.getSelfControlRules() == null || controlRuleDefinition.getSelfControlRules().isEmpty())
            return;
        for (Map.Entry<String, ControlRuleDefItem> item : controlRuleDefinition.getSelfControlRules().entrySet()) {
            SerializerUtils.writePropertyValue_Object(jsonGenerator, item.getValue());
        }
    }

    private void writeChildControlRules(ControlRuleDefinition controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        SerializerUtils.writePropertyName(jsonGenerator, ControlRuleDefNames.ChildControlRules);
        SerializerUtils.writeStartObject(jsonGenerator);
        innerWriteChildRules(controlRuleDefinition, jsonGenerator, serializerProvider);
        SerializerUtils.writeEndObject(jsonGenerator);
    }

    private void innerWriteChildRules(ControlRuleDefinition controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        if (controlRuleDefinition.getChildControlRules() == null || controlRuleDefinition.getChildControlRules().isEmpty())
            return;
        for (Map.Entry<String, ControlRuleDefinition> item : controlRuleDefinition.getChildControlRules().entrySet()) {
            SerializerUtils.writePropertyName(jsonGenerator, item.getKey());
            SerializerUtils.writePropertyValue_Object(jsonGenerator, item.getValue());
        }
    }

    protected void writeExtendInfos(T controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
    }

}
