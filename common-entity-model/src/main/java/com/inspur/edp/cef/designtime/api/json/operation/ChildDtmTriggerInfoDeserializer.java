package com.inspur.edp.cef.designtime.api.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.operation.ChildDtmTriggerInfo;

import java.util.HashMap;

import static com.fasterxml.jackson.core.JsonToken.END_ARRAY;
import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

public class ChildDtmTriggerInfoDeserializer extends JsonDeserializer<HashMap<String, ChildDtmTriggerInfo>> {
    private String curChildCode = "";

    @Override
    public HashMap<String, ChildDtmTriggerInfo> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        HashMap<String, ChildDtmTriggerInfo> hashMap = new HashMap<>();
        if (SerializerUtils.readNullObject(jsonParser)) {
            return null;
        }
        SerializerUtils.readStartArray(jsonParser);
        JsonToken tokentype = jsonParser.getCurrentToken();
        if (tokentype != END_ARRAY) {
            while (jsonParser.getCurrentToken() == tokentype) {
                readHashMap(hashMap, jsonParser);
            }
        }
        SerializerUtils.readEndArray(jsonParser);
        return hashMap;
    }

    private void readHashMap(HashMap<String, ChildDtmTriggerInfo> hashMap, JsonParser parser) {
        SerializerUtils.readStartObject(parser);
        while (parser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(parser);
            readPropertyValue(hashMap, propName, parser);
        }
        SerializerUtils.readEndObject(parser);
    }

    private void readPropertyValue(HashMap<String, ChildDtmTriggerInfo> hashMap, String propName, JsonParser jsonParser) {
        switch (propName) {
            case CefNames.RequestChildCode:
                curChildCode = SerializerUtils.readPropertyValue_String(jsonParser);
                break;
            case CefNames.RequestDtmChildElementValue: {
                ChildTriggerInfoDeserializer childTriggerInfoDeserializer = new ChildTriggerInfoDeserializer();
                ChildDtmTriggerInfo childDtmTriggerInfo = childTriggerInfoDeserializer.deserialize(jsonParser, null);
                hashMap.put(curChildCode, childDtmTriggerInfo);
            }
            break;
        }
    }
}