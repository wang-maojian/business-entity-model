package com.inspur.edp.cef.designtime.api.element;

/**
 * The Definition Of ForeignKeyConstraintType
 *
 * @ClassName: ForeignKeyConstraintType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum ForeignKeyConstraintType {
    /**
     * 禁用
     */
    Forbid(0),
    /**
     * 启用
     */
    Permit(1);

    private final int intValue;
    private static java.util.HashMap<Integer, ForeignKeyConstraintType> mappings;

    private synchronized static java.util.HashMap<Integer, ForeignKeyConstraintType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, ForeignKeyConstraintType>();
        }
        return mappings;
    }

    private ForeignKeyConstraintType(int value) {
        intValue = value;
        ForeignKeyConstraintType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static ForeignKeyConstraintType forValue(int value) {
        return getMappings().get(value);
    }
}