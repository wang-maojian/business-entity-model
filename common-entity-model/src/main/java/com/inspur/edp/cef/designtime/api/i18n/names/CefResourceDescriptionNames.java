package com.inspur.edp.cef.designtime.api.i18n.names;

public final class CefResourceDescriptionNames {
    public static final String Name = "名称";
    // 字段枚举
    public static final String DisplayValue = "枚举显示值";
}