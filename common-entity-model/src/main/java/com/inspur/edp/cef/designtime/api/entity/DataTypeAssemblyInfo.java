package com.inspur.edp.cef.designtime.api.entity;

/**
 * The Definition Of DataTypeAssemblyInfo
 *
 * @ClassName: DataTypeAssemblyInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DataTypeAssemblyInfo {
    private final String assemblyName;

    private final String defaultNamespace;

    public DataTypeAssemblyInfo(String assemblyName, String defaultNamespace) {
        this.assemblyName = assemblyName;
        this.defaultNamespace = defaultNamespace;
    }

    public String getAssemblyName() {
        return assemblyName;
    }

    public String getDefaultNamespace() {
        return defaultNamespace;
    }
}