package com.inspur.edp.cef.designtime.api.element;

/**
 * The Definition Of JoinMode
 *
 * @ClassName: JoinMode
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum JoinMode {
    /**
     * 内联接
     */
    InnerJoin(0),

    /**
     * 内联接
     */
    OuterJoin(1);

    private final int intValue;
    private static java.util.HashMap<Integer, JoinMode> mappings;

    private synchronized static java.util.HashMap<Integer, JoinMode> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, JoinMode>();
        }
        return mappings;
    }

    private JoinMode(int value) {
        intValue = value;
        JoinMode.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static JoinMode forValue(int value) {
        return getMappings().get(value);
    }
}