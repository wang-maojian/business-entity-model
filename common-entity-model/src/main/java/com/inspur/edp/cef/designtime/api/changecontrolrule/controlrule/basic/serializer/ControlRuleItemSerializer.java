package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.io.IOException;

/**
 * The Json Serializer Of ControlRuleItem
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ControlRuleItemSerializer extends JsonSerializer<ControlRuleItem> {
    @Override
    public void serialize(ControlRuleItem value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        SerializerUtils.writeStartObject(gen);
        SerializerUtils.writePropertyValue(gen, ControlRuleNames.RuleValue, value.getControlRuleValue());
        SerializerUtils.writePropertyValue(gen, ControlRuleNames.RuleName, value.getRuleName());
        SerializerUtils.writeEndObject(gen);
    }
}
