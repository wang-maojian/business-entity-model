package com.inspur.edp.cef.designtime.api.util;

import com.inspur.edp.metadata.rtcustomization.api.CustomizationRtService;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CefDtBeanUtil {

    private static CustomizationService customizationService;

    public static CustomizationService getCustomizationService() {
        if (customizationService == null) {
            customizationService = SpringBeanUtils.getBean(CustomizationService.class);
        }
        return customizationService;
    }

    private static CustomizationRtService customizationRtService;

    public static CustomizationRtService getCustomizationRtService() {
        if (customizationRtService == null) {
            customizationRtService = SpringBeanUtils.getBean(CustomizationRtService.class);
        }
        return customizationRtService;
    }
}
