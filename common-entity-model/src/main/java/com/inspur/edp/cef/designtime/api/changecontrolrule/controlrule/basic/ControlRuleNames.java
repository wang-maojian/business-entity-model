package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic;

/**
 * The Property Names  Of ControlRule
 *
 * @ClassName: ControlRuleNames
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ControlRuleNames {
    public static final String RuleId = "RuleId";
    public static final String RuleDefId = "RuleDefId";
    public static final String SelfControlRules = "SelfControlRules";
    public static final String ChildControlRules = "ChildControlRules";
    public static final String RoleType = "RuleType";
    public static final String RoleValues = "RuleValues";
    public static final String RuleValue = "RuleValue";
    public static final String RuleName = "RuleName";
    public static final String RangeControlRules = "RangeControlRules";
}
