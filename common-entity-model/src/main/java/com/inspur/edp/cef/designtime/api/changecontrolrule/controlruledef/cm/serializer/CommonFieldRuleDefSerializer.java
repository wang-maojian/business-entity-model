package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.serializer.ControlRuleDefSerializer;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonFieldContrulRuleDef;

/**
 * The Json Serializer Of CommonFieldRuleDef
 *
 * @ClassName: CommonFieldRuleDefSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonFieldRuleDefSerializer<T extends CommonFieldContrulRuleDef> extends ControlRuleDefSerializer<T> {
    @Override
    protected final void writeExtendInfos(T controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        super.writeExtendInfos(controlRuleDefinition, jsonGenerator, serializerProvider);
        writeCommonFieldRuleExtendInfos(controlRuleDefinition, jsonGenerator, serializerProvider);
    }

    protected void writeCommonFieldRuleExtendInfos(T controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {

    }
}
