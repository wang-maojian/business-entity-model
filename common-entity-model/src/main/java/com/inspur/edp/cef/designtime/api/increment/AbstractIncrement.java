package com.inspur.edp.cef.designtime.api.increment;

import com.inspur.edp.metadata.rtcustomization.api.AbstractCustomizedContent;

public abstract class AbstractIncrement extends AbstractCustomizedContent implements Increment {

    public abstract IncrementType getIncrementType();
}
