package com.inspur.edp.cef.designtime.api.element;

/**
 * The Definition Of EnumIndexType
 *
 * @ClassName: EnumIndexType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum EnumIndexType {

    /**
     * 整型
     */
    Integer(0),
    /**
     * 字符串
     */
    String(1);
    private final int intValue;
    private static java.util.HashMap<Integer, EnumIndexType> mappings;

    private synchronized static java.util.HashMap<Integer, EnumIndexType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, EnumIndexType>();
        }
        return mappings;
    }

    private EnumIndexType(int value) {
        intValue = value;
        EnumIndexType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static EnumIndexType forValue(int value) {
        return getMappings().get(value);
    }


}
