package com.inspur.edp.cef.designtime.api.dtconsistencycheck;

import io.iec.edp.caf.commons.event.CAFEventArgs;

import java.util.ArrayList;
import java.util.List;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class AbstractDtEventArgs extends CAFEventArgs {

    public List<ConsistencyCheckEventMessage> eventMessages;

    public AbstractDtEventArgs() {
        eventMessages = new ArrayList<>();
    }


    public void addEventMessage(ConsistencyCheckEventMessage eventMessage) {
        this.eventMessages.add(eventMessage);
    }

    public List<ConsistencyCheckEventMessage> getEventMessages() {
        return eventMessages;
    }

    public void setEventMessages(
            List<ConsistencyCheckEventMessage> eventMessages) {
        this.eventMessages = eventMessages;
    }

}
