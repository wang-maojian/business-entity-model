package com.inspur.edp.cef.designtime.api.collection;

import com.inspur.edp.cef.designtime.api.operation.CommonValidation;
import com.inspur.edp.cef.designtime.api.util.Guid;

/**
 * The Collection Of Validation
 *
 * @ClassName: CommonValCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonValCollection extends BaseList<CommonValidation> implements Cloneable {
    private static final long serialVersionUID = 1L;

    public CommonValCollection clone(boolean isGenerateId) {
        CommonValCollection col = new CommonValCollection();
        for (CommonValidation var : this) {
            CommonValidation dtm = (CommonValidation) var.clone();
            if (isGenerateId) {
                dtm.setID(Guid.newGuid().toString());
            }
            col.add(dtm);
        }
        return col;
    }

    public CommonValCollection clone() {
        return clone(false);
    }
}