package com.inspur.edp.cef.designtime.api.increment.property;

import java.util.HashMap;

public enum IncrementPropType {
    String(0),
    Int(1),
    Decimal(2),
    Boolean(3),
    Date(4),
    Object(5);


    private final int intValue;
    private static HashMap<Integer, IncrementPropType> mappings;

    private synchronized static HashMap<Integer, IncrementPropType> getMappings() {
        if (mappings == null) {
            mappings = new HashMap<>();
        }
        return mappings;
    }

    private IncrementPropType(int value) {
        intValue = value;
        IncrementPropType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static IncrementPropType forValue(int value) {
        return getMappings().get(value);
    }

}
