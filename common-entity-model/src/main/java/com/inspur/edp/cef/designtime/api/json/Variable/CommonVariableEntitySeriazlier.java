package com.inspur.edp.cef.designtime.api.json.Variable;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.json.element.CefFieldSerializer;
import com.inspur.edp.cef.designtime.api.json.object.GspCommonDataTypeSerializer;

/**
 * The Json Seriazlier Of Common Variable Entity
 *
 * @ClassName: CommonVariableEntitySeriazlier
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonVariableEntitySeriazlier extends GspCommonDataTypeSerializer {

    public CommonVariableEntitySeriazlier() {
    }

    public CommonVariableEntitySeriazlier(boolean full) {
        super(full);
        isFull = full;
    }

    @Override
    protected CefFieldSerializer getFieldSerializer() {
        return new CommonVariableSerializer(isFull);
    }

    @Override
    protected void writeExtendCdtSelfProperty(IGspCommonDataType info, JsonGenerator jsonGenerator) {

    }
}
