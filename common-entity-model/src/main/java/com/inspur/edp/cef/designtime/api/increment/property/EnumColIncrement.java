package com.inspur.edp.cef.designtime.api.increment.property;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.collection.GspEnumValueCollection;
import com.inspur.edp.cef.designtime.api.json.element.EnumValuesDeserializer;

public class EnumColIncrement extends ObjectPropertyIncrement {
    public EnumColIncrement(boolean hasIncrement) {
        super(hasIncrement);
    }

    public Class getPropertyIncrementClass() {
        return GspEnumValueCollection.class;
    }

    public JsonDeserializer getJsonDeserializer() {
        return new EnumValuesDeserializer();
    }

}
