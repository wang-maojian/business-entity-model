package com.inspur.edp.cef.designtime.api.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.inspur.edp.cef.designtime.api.collection.DtmElementCollection;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.operation.ChildDtmTriggerInfo;
import com.inspur.edp.cef.designtime.api.operation.ExecutingDataStatus;

import java.util.EnumSet;

public class ChildTriggerInfoDeserializer extends JsonDeserializer<ChildDtmTriggerInfo> {
    @Override
    public ChildDtmTriggerInfo deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        SerializerUtils.readStartObject(jsonParser);
        ChildDtmTriggerInfo childTriggerInfo = new ChildDtmTriggerInfo();
        while (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
            String propertyName = SerializerUtils.readPropertyName(jsonParser);
            switch (propertyName) {
                case CefNames.GetExecutingDataStatus:
                    childTriggerInfo.setGetExecutingDataStatus(readGetExecutingDataStatus(jsonParser));
                    break;
                case CefNames.RequestChildElements:
                    childTriggerInfo.setRequestChildElements(readRequestChildElements(jsonParser));
                    break;
                default:
                    throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_COMMON_ENTITY_0006, "ChildTriggerInfoDeserializer", propertyName);
            }
        }
        SerializerUtils.readEndObject(jsonParser);
        return childTriggerInfo;
    }

    protected EnumSet<ExecutingDataStatus> readGetExecutingDataStatus(JsonParser jsonParser) {
        EnumSet<ExecutingDataStatus> result = EnumSet.noneOf(ExecutingDataStatus.class);
        int intValueSum = SerializerUtils.readPropertyValue_Integer(jsonParser);
        ExecutingDataStatus[] values = ExecutingDataStatus.values();
        for (int i = values.length - 1; i >= 0; i--) {
            ExecutingDataStatus value = values[i];
            if (intValueSum > 0 && intValueSum >= value.getValue()) {
                result.add(value);
                intValueSum -= value.getValue();
            }
        }
        return result;
    }

    private DtmElementCollection readRequestChildElements(JsonParser jsonParser) {
        DtmElementCollection dtmElementCollection = new DtmElementCollection();
        SerializerUtils.readArray(jsonParser, new StringDeserializer(), dtmElementCollection, true);
        return dtmElementCollection;
    }
}