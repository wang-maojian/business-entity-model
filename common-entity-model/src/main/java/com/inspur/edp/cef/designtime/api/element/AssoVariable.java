package com.inspur.edp.cef.designtime.api.element;

public class AssoVariable {
    /**
     * 变量编号
     */
    private String varCode;
    /**
     * 变量值
     */
    private String varValue;

    public String getVarCode() {
        return varCode;
    }

    public void setVarCode(String varCode) {
        this.varCode = varCode;
    }

    public String getVarValue() {
        return varValue;
    }

    public void setVarValue(String varValue) {
        this.varValue = varValue;
    }
}
