package com.inspur.edp.cef.designtime.api.element.increment.extract;

import com.inspur.edp.cef.designtime.api.element.increment.DeletedFieldIncrement;
import com.inspur.edp.cef.designtime.api.entity.GspCommonField;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DeletedFieldExtractor {
    public DeletedFieldIncrement extract(GspCommonField field) {
        DeletedFieldIncrement increment = createDeletedFieldIncrement(field.getID());
        increment.setId(field.getId());
        return increment;
    }

    protected DeletedFieldIncrement createDeletedFieldIncrement(String deleteId) {
        return new DeletedFieldIncrement(deleteId);
    }
}
