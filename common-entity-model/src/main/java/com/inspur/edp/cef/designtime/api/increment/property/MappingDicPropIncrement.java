package com.inspur.edp.cef.designtime.api.increment.property;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.inspur.edp.cef.designtime.api.json.element.MappingRelationDeserializer;
import com.inspur.edp.cef.designtime.api.json.element.MappingRelationSerializer;

public class MappingDicPropIncrement extends ObjectPropertyIncrement {
    public MappingDicPropIncrement(boolean hasIncrement) {
        super(hasIncrement);
    }

    public JsonSerializer getJsonSerializer() {
        return new MappingRelationSerializer();
    }

    public JsonDeserializer getJsonDeserializer() {
        return new MappingRelationDeserializer();
    }
}
