package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm;

import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.AbstractControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonFieldRuleNames;

/**
 * The Definition Of CommonFieldControlRule
 *
 * @ClassName: CommonFieldControlRule
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonFieldControlRule extends AbstractControlRule {

    //region 名称规则
    public ControlRuleItem getNameControlRule() {
        return super.getControlRule(CommonFieldRuleNames.Name);
    }

    public void setNameControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CommonFieldRuleNames.Name, ruleItem);
    }
    //endregion

    //region 长度规则
    public ControlRuleItem getLengthControlRule() {
        return super.getControlRule(CommonFieldRuleNames.Length);
    }

    public void setLengthControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CommonFieldRuleNames.Length, ruleItem);
    }
    //endregion

    //region 精度规则
    public ControlRuleItem getPrecisionControlRule() {
        return super.getControlRule(CommonFieldRuleNames.Precision);
    }

    public void setPrecisionControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CommonFieldRuleNames.Precision, ruleItem);
    }
    //endregion

    //region 默认值规则
    public ControlRuleItem getDefaultValueControlRule() {
        return super.getControlRule(CommonFieldRuleNames.DefaultValue);
    }

    public void setDefaultValueControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CommonFieldRuleNames.DefaultValue, ruleItem);
    }
    //endregion

    //region 对象类型规则
    public ControlRuleItem getObjectTypeControlRule() {
        return super.getControlRule(CommonFieldRuleNames.ObjectType);
    }

    public void setObjectTypeControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CommonFieldRuleNames.ObjectType, ruleItem);
    }
    //endregion

    //region 多语规则
    public ControlRuleItem getMultiLanFieldControlRule() {
        return super.getControlRule(CommonFieldRuleNames.MultiLanField);
    }

    public void setMultiLanFieldControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CommonFieldRuleNames.MultiLanField, ruleItem);
    }
    //endregion

    //region 只读规则
    public ControlRuleItem geReadonlyControlRule() {
        return super.getControlRule(CommonFieldRuleNames.Readonly);
    }

    public void setReadonlyControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CommonFieldRuleNames.Readonly, ruleItem);
    }
    //endregion

    //region 唯一性规则
    public ControlRuleItem geRequiredControlRule() {
        return super.getControlRule(CommonFieldRuleNames.Required);
    }

    public void setRequiredControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CommonFieldRuleNames.Required, ruleItem);
    }
    //endregion

}
