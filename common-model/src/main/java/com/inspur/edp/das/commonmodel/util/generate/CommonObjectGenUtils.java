package com.inspur.edp.das.commonmodel.util.generate;

import com.inspur.edp.das.commonmodel.IGspCommonObject;

public class CommonObjectGenUtils {
    public static String getGeneratedInterfaceName(IGspCommonObject node) {
        return GeneratingUtils.InterfacePrefix + node.getCode();
    }
}