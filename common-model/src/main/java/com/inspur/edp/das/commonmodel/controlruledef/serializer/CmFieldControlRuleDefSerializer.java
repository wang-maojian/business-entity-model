package com.inspur.edp.das.commonmodel.controlruledef.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.serializer.CommonFieldRuleDefSerializer;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmFieldControlRuleDef;

public class CmFieldControlRuleDefSerializer<T extends CmFieldControlRuleDef> extends CommonFieldRuleDefSerializer<T> {

    @Override
    protected final void writeCommonFieldRuleExtendInfos(T controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        super.writeCommonFieldRuleExtendInfos(controlRuleDefinition, jsonGenerator, serializerProvider);
        writeCmFieldRuleExtendInfos(controlRuleDefinition, jsonGenerator, serializerProvider);
    }

    protected void writeCmFieldRuleExtendInfos(T controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {


    }
}
