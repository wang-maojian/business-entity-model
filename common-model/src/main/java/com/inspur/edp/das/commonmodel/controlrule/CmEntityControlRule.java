package com.inspur.edp.das.commonmodel.controlrule;

import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonDataTypeControlRule;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmEntityRuleNames;

public class CmEntityControlRule extends CommonDataTypeControlRule {

    public ControlRuleItem getAddChildEntityControlRule() {
        return super.getControlRule(CmEntityRuleNames.AddChildEntity);
    }

    public void setAddChildEntityControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CmEntityRuleNames.AddChildEntity, ruleItem);
    }

    public ControlRuleItem getModifyChildEntitiesControlRule() {
        return super.getControlRule(CmEntityRuleNames.ModifyChildEntities);
    }

    public void setModifyChildEntitiesControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CmEntityRuleNames.ModifyChildEntities, ruleItem);
    }

}
