package com.inspur.edp.das.commonmodel.json.increment;

import com.inspur.edp.cef.designtime.api.element.increment.json.GspFieldIncrementSerializer;
import com.inspur.edp.cef.designtime.api.entity.increment.json.GspDataTypeIncrementSerializer;
import com.inspur.edp.cef.designtime.api.json.object.GspCommonDataTypeSerializer;
import com.inspur.edp.das.commonmodel.json.object.CmObjectSerializer;

public abstract class CommonObjectIncrementSerializer extends GspDataTypeIncrementSerializer {

    @Override
    protected final GspCommonDataTypeSerializer getDataTypeSerializer() {
        return getCommonObjectSerializer();
    }

    protected abstract CmObjectSerializer getCommonObjectSerializer();

    @Override
    protected GspFieldIncrementSerializer getFieldSerializer() {
        return getCmElementIncrementSerizlizer();
    }

    protected abstract CommonElementIncrementSerializer getCmElementIncrementSerizlizer();

    @Override
    protected final GspDataTypeIncrementSerializer getGspDataTypeIncrementSerializer() {
        return getCmObjectIncrementSerializer();
    }

    protected abstract CommonObjectIncrementSerializer getCmObjectIncrementSerializer();
}
