package com.inspur.edp.das.commonmodel.collection;

import com.inspur.edp.cef.designtime.api.collection.BaseList;
import com.inspur.edp.cef.designtime.api.util.DataValidator;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.IObjectCollection;
import com.inspur.edp.das.commonmodel.entity.GspCommonObject;

/**
 * The Collection Of Common Object
 *
 * @ClassName: GspObjectCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspObjectCollection extends BaseList<IGspCommonObject> implements IObjectCollection, Cloneable {
    private static final long serialVersionUID = 1L;
    private final IGspCommonObject parentObject;

    /**
     * 创建节点集合
     *
     * @param parentObject 父结点
     */
    public GspObjectCollection(IGspCommonObject parentObject) {
        this.parentObject = parentObject;
    }

    // region ICloneable Members

    /**
     * 克隆
     *
     * @param parentObj 父节点
     * @return
     */
    public final GspObjectCollection clone(IGspCommonObject parentObj) {
        GspObjectCollection newObj = new GspObjectCollection(parentObj);
        for (IGspCommonObject item : this) {
            newObj.add(((GspCommonObject) item).clone(parentObj));
        }
        return newObj;
    }

    // endregion

    /**
     * 集合所属父节点
     */
    @Override
    public IGspCommonObject getParentObject() {
        return parentObject;
    }

    @Override
    public boolean add(IGspCommonObject obj) {
        DataValidator.checkForNullReference(obj, "obj");
        boolean result = super.add(obj);
        obj.setParentObject(getParentObject());
        if (getParentObject() != null) {
            obj.setBelongModel(getParentObject().getBelongModel());
        }
        return result;
    }

    private IGspCommonObject getByID(String id) {
        for (IGspCommonObject item : this) {
            if (id.equals(item.getID())) {
                return item;
            }
        }
        return null;
    }

    public void remove(String id) {
        IGspCommonObject obj = getByID(id);
        if (obj != null)
            super.remove(obj);
    }
}