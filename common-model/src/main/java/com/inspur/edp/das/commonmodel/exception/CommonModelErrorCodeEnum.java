package com.inspur.edp.das.commonmodel.exception;

import lombok.Getter;

public enum CommonModelErrorCodeEnum {
    /*--- JSON SERIALIZE ERROR START ---**/
    /**
     * [{0}]反序列化失败
     */
    GSP_BEMODEL_JSON_0001(false),
    /**
     * [{0}]:[{1}]反序列化失败
     */
    GSP_BEMODEL_JSON_0002(false),
    /**
     * JSON解析失败,检查Json结构
     */
    GSP_BEMODEL_JSON_0003(false),
    /**
     * [{0}]序列化失败
     */
    GSP_BEMODEL_JSON_0004(false),
    /**
     * [{0}]:[{1}]序列化失败
     */
    GSP_BEMODEL_JSON_0005(false),
    /*-- JSON SERIALIZE ERROR END ---**/

    /*--- CLASS CLONE ERROR START ---**/
    /**
     * [{0}]克隆失败
     */
    GSP_BEMODEL_CLONE_0001(false),
    /*--- CLASS CLONE ERROR END ---**/


    /*--- COMMON EXCEPTION ERROR START ---**/
    /**
     * [{0}]的[{1}]方法未被继承实现
     */
    GSP_BEMODEL_COMMON_0009(false),
    /*--- COMMON EXCEPTION ERROR END ---**/

    /*--- COMMON MODEL ERROR START ---**/
    /**
     * [{0}]未识别的属性名：[{1}]。请将系统补丁升级至可更新范围内最新。
     * default
     */
    GSP_BEMODEL_COMMON_MODEL_0001(true),
    /**
     * 获取扩展对象节点为空，请联系管理员
     */
    GSP_BEMODEL_COMMON_MODEL_0002(false),
    /**
     * 不存在控制规则方式：[{0}]
     * default
     */
    GSP_BEMODEL_COMMON_MODEL_0003(false),
    /**
     * 节点对象[{0}]上没有找到子对象节点[{1}]
     */
    GSP_BEMODEL_COMMON_MODEL_0004(true),
    /**
     * 新增子表时，未获取到新增子表上的parentID字段，无法建立主子表的对应关系，其字段ID为[{0}]。
     */
    GSP_BEMODEL_COMMON_MODEL_0005(false),
    /**
     * [{0}]
     */
    GSP_BEMODEL_TEMPLATE_ERROR(false);

    /*--- COMMON MODEL ERROR END ---**/

    /**
     * 是否业务异常
     */
    @Getter
    private final boolean bizException;

    CommonModelErrorCodeEnum(boolean bizException) {
        this.bizException = bizException;
    }
}
