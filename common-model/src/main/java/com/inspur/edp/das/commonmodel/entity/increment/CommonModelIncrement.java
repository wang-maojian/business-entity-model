package com.inspur.edp.das.commonmodel.entity.increment;

import com.inspur.edp.cef.designtime.api.entity.increment.ModifyEntityIncrement;
import com.inspur.edp.cef.designtime.api.increment.AbstractIncrement;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;
import com.inspur.edp.cef.designtime.api.increment.ModifyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;

import java.util.HashMap;

public class CommonModelIncrement extends AbstractIncrement implements ModifyIncrement {
    private HashMap<String, PropertyIncrement> changeProperties;
    private ModifyEntityIncrement mainEntityIncrement = new ModifyEntityIncrement();


    public IncrementType getIncrementType() {
        return IncrementType.Modify;
    }

    public HashMap<String, PropertyIncrement> getChangeProperties() {
        if (changeProperties == null)
            changeProperties = new HashMap<>();
        return changeProperties;
    }


    public ModifyEntityIncrement getMainEntityIncrement() {
        return mainEntityIncrement;
    }

    public void setMainEntityIncrement(ModifyEntityIncrement mainEntityIncrement) {
        this.mainEntityIncrement = mainEntityIncrement;
    }

}
