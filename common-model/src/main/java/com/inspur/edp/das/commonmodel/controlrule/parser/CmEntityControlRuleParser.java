package com.inspur.edp.das.commonmodel.controlrule.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonDataTypeControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.parser.CommonDataTypeRuleParser;
import com.inspur.edp.das.commonmodel.controlrule.CmEntityControlRule;

public class CmEntityControlRuleParser<T extends CmEntityControlRule> extends CommonDataTypeRuleParser<T> {

    @Override
    protected final boolean readCommonDataTypeRuleExtendProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        if (super.readCommonDataTypeRuleExtendProperty(ruleDefinition, propName, jsonParser, deserializationContext))
            return true;
        return readCmEntityRuleExtendProperty(ruleDefinition, propName, jsonParser, deserializationContext);
    }

    protected boolean readCmEntityRuleExtendProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        return false;
    }

    @Override
    protected final CommonDataTypeControlRule createCommonDataTypeRule() {
        return createCmEntityRule();
    }

    protected CmEntityControlRule createCmEntityRule() {
        return new CmEntityControlRule();
    }
}
