package com.inspur.edp.das.commonmodel.collection;

// import Inspur.Ecp.Caf.Common.*;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.collection.GspFieldCollection;
import com.inspur.edp.cef.designtime.api.util.DataValidator;
import com.inspur.edp.das.commonmodel.IElementCollection;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.entity.GspCommonElement;
import com.inspur.edp.das.commonmodel.entity.element.GspCommonAssociation;

/**
 * The Collection Of ELement
 *
 * @ClassName: GspElementCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspElementCollection extends GspFieldCollection implements IElementCollection, Cloneable {
    private static final long serialVersionUID = 1L;
    private IGspCommonObject parentObject;

    /**
     * 创建字段集合
     *
     * @param parentObj
     */
    public GspElementCollection(IGspCommonObject parentObj) {
        super(parentObj);
        parentObject = parentObj;
    }

    /**
     * 移动字段
     *
     * @param toIndex 目的索引为止
     * @param element 需要移动的字段
     */
    public final void move(int toIndex, IGspCommonElement element) {
        if (contains(element)) {
            super.remove(element);
            super.insert(toIndex, element);
        }
    }


    /**
     * 克隆
     *
     * @param absObject
     * @param parentAssociation
     * @return
     */
    public GspElementCollection clone(IGspCommonObject absObject, GspCommonAssociation parentAssociation) {
        GspElementCollection newCollection = new GspElementCollection(absObject);
        for (IGspCommonField item : this) {
            newCollection.add(item.clone(absObject, parentAssociation));
        }
        return newCollection;
    }

    /**
     * Index[string]
     */
    public IGspCommonElement getItem(String id) {
        Object tempVar = super.getByID(id);
        return (IGspCommonElement) ((tempVar instanceof IGspCommonElement) ? tempVar : null);
    }

    public final IGspCommonElement getItem(int index) {
        Object tempVar = super.getByIndex(index);
        return (IGspCommonElement) ((tempVar instanceof IGspCommonElement) ? tempVar : null);
    }

    /**
     * 所属结点
     */
    // [Newtonsoft.Json.jsonIgnore()]
    @Override
    public IGspCommonObject getParentObject() {
        return parentObject;
    }

    public final void setParentObject(IGspCommonObject value) {
        if (parentObject != value) {
            parentObject = value;
            for (IGspCommonField item : this) {
                GspCommonElement ele = (GspCommonElement) item;
                ele.setBelongObject(value);
            }
        }
    }

    /**
     * 添加一个新字段
     *
     * @param element
     */
    public final void addField(IGspCommonElement element) {
        DataValidator.checkForNullReference(element, "element");

        super.add(element);
        if (getParentObject() != null) {
            element.setBelongObject(getParentObject());
        }
    }
}