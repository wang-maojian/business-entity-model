package com.inspur.edp.das.commonmodel;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.das.commonmodel.entity.element.ElementCodeRuleConfig;
import com.inspur.edp.das.commonmodel.entity.element.GspCommonAssociation;

/**
 * The Interface  Of Common Element
 *
 * @ClassName: IGspCommonElement
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface IGspCommonElement extends IGspCommonField {

    /**
     * @return The Bill Code Config Of The  Element
     */
    ElementCodeRuleConfig getBillCodeConfig();


    /**
     * @return The Column Id Of The Element
     */
    String getColumnID();

    /**
     * @param value The Column Id Of The Element
     */
    void setColumnID(String value);

    /**
     * @return The Element Is Custom Item
     */
    boolean getIsCustomItem();

    /**
     * @param value Set Wether The Element Is Custom Item
     */
    void setIsCustomItem(boolean value);


    /**
     * @return Belong Model Id Of The Element
     */
    String getBelongModelID();

    /**
     * @param value Belong Model Id To Set Of The Element
     */
    void setBelongModelID(String value);


    /**
     * @return Belong Object Of The Element
     */
    IGspCommonObject getBelongObject();

    /**
     * @param value Belong Object To Set Of The Element
     */
    void setBelongObject(IGspCommonObject value);


    /**
     * @return The Element Is Readonly
     */
    boolean getReadonly();

    /**
     * @param value The Readonly Value To Set Of The Element
     */
    void setReadonly(boolean value);


    /**
     * @param absObject         The Parent Object Of The New Element,It`s Used When The Element Is  Not An Association  Element
     * @param parentAssociation The Parent Association  Of  The Element When The Element Is An Association Element
     * @return
     */
    IGspCommonElement clone(IGspCommonObject absObject, GspCommonAssociation parentAssociation);


    /**
     * @return The Association Type Name Of The Element
     */
    String getAssociationTypeName();


    /**
     * @return The Enum Type Name Of The Element
     */
    String getEnumTypeName();
    // endregion

    /**
     * 字段是否参与持久化处理
     *
     * @return
     */
    boolean getIsPersistent();

    /**
     * 字段是否参与持久化处理
     *
     * @param value
     */
    void setIsPersistent(boolean value);

    /**
     * 字段加密处理
     *
     * @return
     */
    boolean getIsEnableEncryption();

    void setIsEnableEncryption(boolean value);

    String getCustomizeRefObjectID();

    void setCustomizeRefObjectID(String value);

    String getCustomizeRefElementID();

    void setCustomizeRefElementID(String value);

}
