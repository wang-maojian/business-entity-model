package com.inspur.edp.das.commonmodel.entity.element;

/**
 * The Definition Of Element Code Rule Info
 *
 * @ClassName: ElementCodeRuleConfig
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ElementCodeRuleConfig implements Cloneable {
    private boolean canBillCode = false;
    private String billCodeId = "";
    private String billCodeName;
    private GspBillCodeGenerateType codeGenerateType = GspBillCodeGenerateType.none;
    private GspBillCodeGenerateOccasion codeGenerateOccasion = GspBillCodeGenerateOccasion.SystemProcess;

    public ElementCodeRuleConfig() {
        setCanBillCode(false);
    }

    /**
     * 是否单据编号
     */
    public final boolean getCanBillCode() {
        return canBillCode;
    }

    public final void setCanBillCode(boolean value) {
        canBillCode = value;
    }

    /**
     * 单据编号Id
     */
    public final String getBillCodeID() {
        return billCodeId;
    }

    public final void setBillCodeID(String value) {
        billCodeId = value;
    }

    /**
     * 单据编号名称
     */
    public final String getBillCodeName() {
        return billCodeName;
    }

    public final void setBillCodeName(String value) {
        billCodeName = value;
    }

    /**
     * 代码生成类型
     */
    public final GspBillCodeGenerateType getCodeGenerateType() {
        return codeGenerateType;
    }

    public final void setCodeGenerateType(GspBillCodeGenerateType value) {
        codeGenerateType = value;
    }

    /**
     * 编码生成时机
     */
    public final GspBillCodeGenerateOccasion getCodeGenerateOccasion() {
        return codeGenerateOccasion;
    }

    public final void setCodeGenerateOccasion(GspBillCodeGenerateOccasion value) {
        codeGenerateOccasion = value;
    }

    @Override
    public ElementCodeRuleConfig clone() {
        try {
            return (ElementCodeRuleConfig) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}