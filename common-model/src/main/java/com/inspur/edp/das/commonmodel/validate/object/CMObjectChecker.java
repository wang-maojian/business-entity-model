package com.inspur.edp.das.commonmodel.validate.object;

import com.inspur.edp.cef.designtime.api.validate.object.CommonDataTypeChecker;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.entity.GspCommonObject;

public abstract class CMObjectChecker extends CommonDataTypeChecker {

    public final void checkObj(GspCommonObject gspCommonObject) {
        checkAllObj(this, gspCommonObject);
    }

    //遍历Child
    private void checkAllObj(CMObjectChecker cmInspaction, IGspCommonObject commonObject) {
        super.checkCDT(commonObject);
        checkAction(commonObject);
        checkExtension(commonObject);
        for (IGspCommonObject childNode : commonObject.getContainChildObjects()) {
            if (childNode == null)
                continue;
            checkAllObj(this, childNode);
        }
    }

    protected void checkAction(IGspCommonObject commonObject) {

    }

    protected void checkExtension(IGspCommonObject commonObject) {

    }
}
