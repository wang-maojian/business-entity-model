package com.inspur.edp.das.commonmodel.entity.element;

import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.das.commonmodel.IGspCommonModel;

/**
 * The Definition Of Common Association
 *
 * @ClassName: GspCommonAssociation
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspCommonAssociation extends GspAssociation implements Cloneable {
    private IGspCommonModel refModel;

    /**
     * 创建关联关系
     */
    public GspCommonAssociation() {
    }

    /**
     * 引用的数据模型
     */
    public final IGspCommonModel getRefModel() {
        return refModel;
    }

    public final void setRefModel(IGspCommonModel value) {
        refModel = value;
    }

    // region ICloneable Members

    // endregion
}