package com.inspur.edp.das.commonmodel.util;

import io.iec.edp.caf.commons.utils.StringUtils;

import java.util.Locale;

public class HandleAssemblyNameUtil {
	@Deprecated
	public static String convertToJavaPackageName(String assemblyName) {
		String[] list = assemblyName.split("\\.");
		String result = "";

		for (int i = 0; i < list.length; i++) {
			String lowerCase = list[i].toLowerCase(Locale.ROOT);
			if (i == 0) {
				if (lowerCase.equals("inspur")) {
					result = result.concat("com.inspur");
					continue;
				} else {
					result = result.concat(lowerCase);
					continue;
				}
			}
			result = result.concat(".");
			result = result.concat(lowerCase);
		}
		return result;
	}

	/**
	 * 根据
	 * @param packagePrefix
	 * @param assemblyName
	 * @return
	 */
	public static String convertToJavaPackageName(String packagePrefix, String assemblyName) {
		String namespace = assemblyName;
		if(!StringUtils.isEmpty(packagePrefix))
			namespace = packagePrefix + "." + assemblyName;

		String[] list = namespace.split("\\.");
		String result = "";

        for (int i = 0; i < list.length; i++) {
            String lowerCase = list[i].toLowerCase(Locale.ROOT);
            if (i == 0) {
                if (lowerCase.equals("inspur")) {
                    result = result.concat("com.inspur");
                    continue;
                } else {
                    result = result.concat(lowerCase);
                    continue;
                }
            }
            result = result.concat(".");
            result = result.concat(lowerCase);
        }
        return result;
    }
}
