package com.inspur.edp.das.commonmodel.util;

import com.inspur.edp.caf.db.dbaccess.DataValidator;
import com.inspur.edp.das.commonmodel.exception.CommonModelErrorCodeEnum;
import com.inspur.edp.das.commonmodel.exception.CommonModelException;
import com.inspur.edp.lcm.metadata.api.entity.GspProject;
import com.inspur.edp.lcm.metadata.api.service.GspProjectService;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.commons.utils.StringUtils;

import java.io.IOException;

/**
 * 元数据工程工具类，提供工程namespace、包路径前缀等获取方法
 */
public class MetadataProjectUtil {

    /**
     * 根据工程relativePath获取Java工程路径下java文件夹路径
     * 举例：D:\projects\Scm\OrderMrg\SaleOrder\bo-saleorder-back\java\code\comp\src\main\java
     * @param relativePath
     * @return
     */
    public static String getJavaCompProjectPath(String relativePath) {
        if(StringUtils.isEmpty(relativePath))
            return null;

        MetadataProjectService service = SpringBeanUtils.getBean(MetadataProjectService.class);
        return service.getJavaCompProjectPath(relativePath);
    }

    /**
     * 根据工程路径，获取包路径前缀，默认为com
     * @param relativePath
     * @return
     */
    public static String getPackagePrefix(String relativePath) {
        DataValidator.checkForEmptyString(relativePath,"relativePath");

        GspProjectService gspProjectService = SpringBeanUtils.getBean(GspProjectService.class);
        try {
            GspProject gspProject = gspProjectService.getGspProjectInfo(relativePath);
            if (gspProject == null) {
                throw new NullPointerException("GspProject is null");
            }
            return gspProject.getPackagePrefix();
        } catch (IOException e) {
            throw CommonModelException.createException(CommonModelErrorCodeEnum.GSP_BEMODEL_TEMPLATE_ERROR,e);
        }
    }
}
