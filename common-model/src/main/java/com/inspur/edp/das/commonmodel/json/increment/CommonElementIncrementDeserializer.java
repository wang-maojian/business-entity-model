package com.inspur.edp.das.commonmodel.json.increment;

import com.inspur.edp.cef.designtime.api.element.increment.json.GspFieldIncrementDeserializer;
import com.inspur.edp.cef.designtime.api.json.element.CefFieldDeserializer;
import com.inspur.edp.das.commonmodel.json.element.CmElementDeserializer;

public abstract class CommonElementIncrementDeserializer extends GspFieldIncrementDeserializer {
    @Override
    protected final CefFieldDeserializer getFieldDeserializer() {
        return getCmElementDeserializer();
    }

    protected abstract CmElementDeserializer getCmElementDeserializer();
}
