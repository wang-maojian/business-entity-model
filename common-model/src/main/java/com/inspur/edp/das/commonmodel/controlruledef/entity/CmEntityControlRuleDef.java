package com.inspur.edp.das.commonmodel.controlruledef.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefinition;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.parser.CmEntityControlRuleDefParser;
import com.inspur.edp.das.commonmodel.controlruledef.serializer.CmEntityControlRuleDefSerializer;

@JsonSerialize(using = CmEntityControlRuleDefSerializer.class)
@JsonDeserialize(using = CmEntityControlRuleDefParser.class)
public class CmEntityControlRuleDef extends CommonDataTypeControlRuleDef {
    public CmEntityControlRuleDef(ControlRuleDefinition parentRuleDefinition) {
        super(parentRuleDefinition, CmEntityRuleNames.CmEntityRuleObjectType);
    }

    public ControlRuleDefItem getAddChildEntityControlRule() {
        return super.getControlRuleItem(CmEntityRuleNames.AddChildEntity);
    }

    public void setAddChildEntityControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CmEntityRuleNames.AddChildEntity, ruleItem);
    }

    public ControlRuleDefItem getModifyChildEntitiesControlRule() {
        return super.getControlRuleItem(CmEntityRuleNames.ModifyChildEntities);
    }

    public void setModifyChildEntitiesControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CmEntityRuleNames.ModifyChildEntities, ruleItem);
    }
}
