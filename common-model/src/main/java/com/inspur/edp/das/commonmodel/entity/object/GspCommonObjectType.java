package com.inspur.edp.das.commonmodel.entity.object;

/**
 * The Definition Of Common Object Type ,It Has Two Values:MainObject And ChildObject
 *
 * @ClassName: ElementCodeRuleConfig
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum GspCommonObjectType {
    /**
     * 主节点
     */
    MainObject,

    /**
     * 非主节点
     */
    ChildObject;

    public int getValue() {
        return this.ordinal();
    }

    public static GspCommonObjectType forValue(int value) {
        return values()[value];
    }
}