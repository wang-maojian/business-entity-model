package com.inspur.edp.das.commonmodel.i18n;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.cef.designtime.api.i18n.extractor.AssoResourceExtractor;
import com.inspur.edp.cef.designtime.api.i18n.extractor.CefFieldResourceExtractor;
import com.inspur.edp.das.commonmodel.IGspCommonElement;

public abstract class CommonElementResourceExtractor extends CefFieldResourceExtractor {
    protected CommonElementResourceExtractor(IGspCommonElement field, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(field, context, parentResourceInfo);
    }

    @Override
    protected final void extractExtendProperties(IGspCommonField commonField) {
        extractExtendElementProperties((IGspCommonElement) ((commonField instanceof IGspCommonElement) ? commonField : null));
    }

    @Override
    protected final AssoResourceExtractor getAssoResourceExtractor(ICefResourceExtractContext context, CefResourcePrefixInfo fieldPrefixInfo, GspAssociation asso) {
        return getGSPAssoResourceExtractor(context, fieldPrefixInfo, (asso instanceof GspAssociation) ? asso : null);
    }

    protected abstract void extractExtendElementProperties(IGspCommonElement commonField);

    protected abstract GspAssoResourceExtractor getGSPAssoResourceExtractor(ICefResourceExtractContext context, CefResourcePrefixInfo fieldPrefixInfo, GspAssociation asso);

}