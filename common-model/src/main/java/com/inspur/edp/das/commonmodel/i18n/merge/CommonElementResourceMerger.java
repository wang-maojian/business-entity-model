package com.inspur.edp.das.commonmodel.i18n.merge;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.cef.designtime.api.i18n.merger.AssoResourceMerger;
import com.inspur.edp.cef.designtime.api.i18n.merger.CefFieldResourceMerger;
import com.inspur.edp.das.commonmodel.IGspCommonElement;

public abstract class CommonElementResourceMerger extends CefFieldResourceMerger {
    protected CommonElementResourceMerger(IGspCommonField field, ICefResourceMergeContext context) {
        super(field, context);
    }

    @Override
    protected AssoResourceMerger getAssoResourceMerger(ICefResourceMergeContext context, GspAssociation asso) {
        return getGSPAssoResourceMerger(context, (asso instanceof GspAssociation) ? asso : null);

    }

    @Override
    protected final void extractExtendProperties(IGspCommonField commonField) {
        extractExtendElementProperties((IGspCommonElement) ((commonField instanceof IGspCommonElement) ? commonField : null));
    }

    protected abstract void extractExtendElementProperties(IGspCommonElement commonField);

    protected abstract GspAssoResourceMerger getGSPAssoResourceMerger(ICefResourceMergeContext context, GspAssociation asso);

}
