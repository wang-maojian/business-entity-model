package com.inspur.edp.das.commonmodel.controlrule.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.serializer.CommonDataTypeRuleSerializer;
import com.inspur.edp.das.commonmodel.controlrule.CmEntityControlRule;

public class CmEntityControlRuleSerializer<T extends CmEntityControlRule> extends CommonDataTypeRuleSerializer<T> {
    @Override
    protected final void writeCommonDataTypeRuleExtendInfos(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        super.writeCommonDataTypeRuleExtendInfos(controlRule, jsonGenerator, serializerProvider);
        writeCmEntityRuleExtendInfos(controlRule, jsonGenerator, serializerProvider);
    }

    protected void writeCmEntityRuleExtendInfos(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {

    }
}
