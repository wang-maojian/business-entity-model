package com.inspur.edp.das.commonmodel.entity.object.increment.extract;

import com.inspur.edp.cef.designtime.api.entity.increment.extractor.DataTypeIncrementExtractor;
import com.inspur.edp.cef.designtime.api.entity.increment.extractor.ModifyEntityExtractor;

public class CommonObjectIncrementExtractor extends DataTypeIncrementExtractor {

    public CommonObjectIncrementExtractor() {
        super();
    }

    public CommonObjectIncrementExtractor(boolean includeAll) {
        super(includeAll);
    }

    protected final ModifyEntityExtractor getModifyEntityExtractor() {
        return getModifyCommonObjectExtractor();
    }

    protected ModifyCommonObjectExtractor getModifyCommonObjectExtractor() {
        return new ModifyCommonObjectExtractor(includeAll);
    }

}
