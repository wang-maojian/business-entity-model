package com.inspur.edp.das.commonmodel.entity.element.increment.extract;

import com.inspur.edp.cef.designtime.api.element.increment.extract.CommonFieldIncrementExtractor;
import com.inspur.edp.cef.designtime.api.element.increment.extract.ModifyFieldExtractor;

public class CommonElementExtractor extends CommonFieldIncrementExtractor {

    public CommonElementExtractor() {
        super();
    }

    public CommonElementExtractor(boolean includeAll) {
        super(includeAll);
    }

    @Override
    protected final ModifyFieldExtractor getModifyFieldExtractor() {
        return getModifyElementExtractor();
    }

    protected ModifyElementExtractor getModifyElementExtractor() {
        return new ModifyElementExtractor(includeAll);
    }

}
