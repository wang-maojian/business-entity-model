package com.inspur.edp.das.commonmodel.controlruledef.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.parser.RangeRuleDefParser;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmRuleNames;

public class CmRuleDefParser<T extends CmControlRuleDef> extends RangeRuleDefParser<T> {
    @Override
    protected final boolean readRangeExtendControlRuleProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        if (super.readRangeExtendControlRuleProperty(ruleDefinition, propName, jsonParser, deserializationContext))
            return true;
        return readCmRuleExtendProperty(ruleDefinition, propName, jsonParser, deserializationContext);
    }

    protected boolean readCmRuleExtendProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        return false;
    }

    @Override
    protected T createRuleDefinition() {
        return (T) createCmRuleDef();
    }

    protected CmControlRuleDef createCmRuleDef() {
        return new CmControlRuleDef(null, CmRuleNames.CmRuleObjectType);
    }
}
