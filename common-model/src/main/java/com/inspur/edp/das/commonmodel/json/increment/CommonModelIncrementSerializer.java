package com.inspur.edp.das.commonmodel.json.increment;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.json.PropertyIncrementSerializer;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.entity.increment.CommonModelIncrement;
import com.inspur.edp.das.commonmodel.exception.CommonModelErrorCodeEnum;
import com.inspur.edp.das.commonmodel.exception.CommonModelException;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import lombok.var;

import java.io.IOException;
import java.util.HashMap;

public abstract class CommonModelIncrementSerializer extends JsonSerializer<CommonModelIncrement> {

    @Override
    public void serialize(CommonModelIncrement value, JsonGenerator gen, SerializerProvider serializers) {
        SerializerUtils.writeStartObject(gen);
        writeBaseInfo(value, gen);
        writeExtendInfo(value, gen);
        SerializerUtils.writeEndObject(gen);
    }

    private void writeBaseInfo(CommonModelIncrement value, JsonGenerator gen) {
        //①MainObject
        if (value.getMainEntityIncrement() != null) {
            SerializerUtils.writePropertyName(gen, CommonModelNames.MainObject);
            CommonObjectIncrementSerializer objSerializer = getObjectIncrementSerializer();

            try {
                objSerializer.serialize(value.getMainEntityIncrement(), gen, null);
            } catch (IOException e) {
                throw CommonModelException.createException(CommonModelErrorCodeEnum.GSP_BEMODEL_JSON_0004, e, "ModifyEntityIncrement");
            }
        }

        //②prop
        HashMap<String, PropertyIncrement> properties = value.getChangeProperties();
        if (properties != null && !properties.isEmpty())
            writeProperties(properties, gen);
    }

    protected abstract CommonObjectIncrementSerializer getObjectIncrementSerializer();

    private void writeProperties(HashMap<String, PropertyIncrement> properties, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, CefNames.PropertyIncrements);
        SerializerUtils.WriteStartArray(gen);
        for (var item : properties.entrySet()) {
            SerializerUtils.writeStartObject(gen);
            SerializerUtils.writePropertyValue(gen, CefNames.Id, item.getKey());

            SerializerUtils.writePropertyName(gen, CefNames.Value);
            PropertyIncrementSerializer serializer = getPropertyIncrementSerializer();
            serializer.serialize(item.getValue(), gen, null);
            SerializerUtils.writeEndObject(gen);

        }
        SerializerUtils.WriteEndArray(gen);
    }

    private PropertyIncrementSerializer getPropertyIncrementSerializer() {
        return new PropertyIncrementSerializer();
    }

    protected void writeExtendInfo(CommonModelIncrement value, JsonGenerator gen) {

    }
}
