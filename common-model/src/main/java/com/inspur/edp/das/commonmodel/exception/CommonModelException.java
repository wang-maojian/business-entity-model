package com.inspur.edp.das.commonmodel.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

public class CommonModelException extends CAFRuntimeException {
    private static final String SERVICE_UNIT_CODE = "pfcommon";
    private static final String RESOURCE_FILE = "business_entity_model_exception.properties";

    public CommonModelException(CommonModelErrorCodeEnum exceptionCode, String[] messageParams, Throwable innerException, ExceptionLevel level) {
        super(SERVICE_UNIT_CODE, RESOURCE_FILE, exceptionCode.name(), messageParams, innerException, level, exceptionCode.isBizException());
    }

    public static CommonModelException createException(CommonModelErrorCodeEnum exceptionCode) {
        return new CommonModelException(exceptionCode, null, null, ExceptionLevel.Error);
    }

    public static CommonModelException createException(CommonModelErrorCodeEnum exceptionCode, String... messageParams) {
        return new CommonModelException(exceptionCode, messageParams, null, ExceptionLevel.Error);
    }

    public static CommonModelException createException(CommonModelErrorCodeEnum exceptionCode, Throwable innerException) {
        return new CommonModelException(exceptionCode, null, innerException, ExceptionLevel.Error);
    }

    public static CommonModelException createException(CommonModelErrorCodeEnum exceptionCode, Throwable innerException, String... messageParams) {
        return new CommonModelException(exceptionCode, messageParams, innerException, ExceptionLevel.Error);
    }

    public static CommonModelException createException(CommonModelErrorCodeEnum exceptionCode, Throwable innerException, ExceptionLevel level, String... messageParams) {
        return new CommonModelException(exceptionCode, messageParams, innerException, level);
    }

}
