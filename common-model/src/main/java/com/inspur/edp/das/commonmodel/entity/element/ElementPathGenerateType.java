package com.inspur.edp.das.commonmodel.entity.element;

/**
 * The Definition Of Path Generate Time
 *
 * @ClassName: ElementPathGenerateType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum ElementPathGenerateType {
    /**
     * 创建时
     */
    CreateNode(0),
    /**
     * 保存时
     */
    SaveNode(1);

    private final int intValue;
    private static java.util.HashMap<Integer, ElementPathGenerateType> mappings;

    private synchronized static java.util.HashMap<Integer, ElementPathGenerateType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, ElementPathGenerateType>();
        }
        return mappings;
    }

    private ElementPathGenerateType(int value) {
        intValue = value;
        ElementPathGenerateType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static ElementPathGenerateType forValue(int value) {
        return getMappings().get(value);
    }
}