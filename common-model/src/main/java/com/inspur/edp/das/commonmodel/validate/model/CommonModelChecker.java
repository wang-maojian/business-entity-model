package com.inspur.edp.das.commonmodel.validate.model;

import com.inspur.edp.cef.designtime.api.validate.common.CheckUtil;
import com.inspur.edp.cef.designtime.api.variable.CommonVariableEntity;
import com.inspur.edp.cef.designtime.api.variable.inspaction.VariableChecker;
import com.inspur.edp.das.commonmodel.entity.GspCommonModel;
import com.inspur.edp.das.commonmodel.entity.GspCommonObject;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;
import com.inspur.edp.das.commonmodel.validate.object.CMObjectChecker;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectDtService;
import org.springframework.util.StringUtils;

public abstract class CommonModelChecker {

    protected void checkCM(GspCommonModel commonModel) {
        //节点编号
        checkModelCode(commonModel);
        //变量
        checkVar(commonModel.getVariables());
        //节点动作
        checkExtension(commonModel);
        //实体动作，字段。以及子集
        checkObjExtension(commonModel.getMainObject());
    }

    //节点编号
    private void checkModelCode(GspCommonModel gspCommonModel) {
        if (StringUtils.isEmpty(gspCommonModel.getCode())) {
            CheckUtil.exception(MessageI18nUtils.getMessage("GSP_NULL_ERROR_0003"));
        }
        if (StringUtils.isEmpty(gspCommonModel.getName())) {
            CheckUtil.exception(MessageI18nUtils.getMessage("GSP_NULL_ERROR_0004"));
        }
        if (StringUtils.isEmpty(gspCommonModel.getGeneratedConfigID())) {
            CheckUtil.exception(MessageI18nUtils.getMessage("GSP_NULL_ERROR_0005"));
        }
        if (!CheckUtil.isLegality(gspCommonModel.getCode())) {
            CheckUtil.exception(MessageI18nUtils.getMessage("GSP_VALIDATE_ERROR_0014", gspCommonModel.getCode()));
        }
    }

    private void checkVar(CommonVariableEntity variableEntity) {
        VariableChecker.getInstance().checkVariable(variableEntity);
    }

    protected abstract void checkExtension(GspCommonModel gspCommonModel);

    private void checkObjExtension(GspCommonObject object) {
        getCMObjectChecker().checkObj(object);
    }

    protected abstract CMObjectChecker getCMObjectChecker();
}
