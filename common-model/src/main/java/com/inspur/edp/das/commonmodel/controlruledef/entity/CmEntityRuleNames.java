package com.inspur.edp.das.commonmodel.controlruledef.entity;

public class CmEntityRuleNames {
    public static String CmEntityRuleObjectType = "GspCommonModelEntity";
    public static String AddChildEntity = "AddChildEntity";
    public static String ModifyChildEntities = "ModifyChildEntities";

}
