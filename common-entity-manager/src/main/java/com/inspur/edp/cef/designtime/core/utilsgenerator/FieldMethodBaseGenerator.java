package com.inspur.edp.cef.designtime.core.utilsgenerator;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
import com.inspur.edp.cef.designtime.api.entity.ClassInfo;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;
import com.inspur.edp.cef.entity.entity.AssoInfoBase;
import com.inspur.edp.cef.entity.entity.dynamicProp.IDynamicPropSet;
import com.inspur.edp.lcm.metadata.api.service.RefCommonService;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * DataUtils工具类，字段get、set方法生成器基类
 */
abstract class FieldMethodBaseGenerator {
    protected IGspCommonField field;
    protected JavaClassInfo classInfo;
    protected String varData = "data";
    protected MethodInfo methodInfo = new MethodInfo();

    FieldMethodBaseGenerator(IGspCommonField field, JavaClassInfo classInfo) {
        this.field = field;
        this.classInfo = classInfo;
    }

    public static TypeRefInfo getTypeInfo(IGspCommonField field) {
        if (field.getObjectType() == GspElementObjectType.DynamicProp)
            return new TypeRefInfo(IDynamicPropSet.class);
        else if (field.getIsUdt()) {
            UnifiedDataTypeDef udtDef = (UnifiedDataTypeDef) SpringBeanUtils
                    .getBean(RefCommonService.class).getRefMetadata(field.getUdtID()).getContent();
            if (udtDef == null) {
                throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_COMMON_ENTITY_0001, field.getName(), field.getUdtID());
            }

            ClassInfo udtClassInfo = udtDef.getGeneratedEntityClassInfo();
            TypeRefInfo typeInfo = new TypeRefInfo(udtClassInfo.getClassName(), udtClassInfo.getClassNamespace());
            return typeInfo;
        } else {
            return getNormalTypeInfo(field);
        }
    }

    private static TypeRefInfo getNormalTypeInfo(IGspCommonField field) {
        switch (field.getObjectType()) {
            case None:
                return getNativeType(field);
            case Enum:
                return new TypeRefInfo(String.class);
            case Association:
                return new TypeRefInfo(AssoInfoBase.class);
            case DynamicProp:
            default:
                throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0001, field.getObjectType().toString());
        }
    }

    private static TypeRefInfo getNativeType(IGspCommonField field) {
        switch (field.getMDataType()) {
            case String:
            case Text:
                return new TypeRefInfo(String.class);
            case Boolean:
                return new TypeRefInfo(Boolean.class);
            case Integer:
                return new TypeRefInfo(Integer.class);
            case Decimal:
                TypeRefInfo typeInfo = new TypeRefInfo(BigDecimal.class);
                return typeInfo;
            case Date:
            case DateTime:
                TypeRefInfo dateTypeInfo = new TypeRefInfo(Date.class);
                return dateTypeInfo;
            case Binary:
                TypeRefInfo binaryTypeInfo = new TypeRefInfo(byte[].class);
                return binaryTypeInfo;
            default:
                throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0002, field.getMDataType().toString());
        }
    }

    /**
     * 获取方法名
     *
     * @return
     */
    protected abstract String getMethodName();

    /**
     * 获取方法返回类型
     *
     * @return
     */
    protected abstract TypeRefInfo getReturnType();

    /**
     * 判断当前字段方法是否需要注释
     * 1.非UDT字段需要生成，不需要注释
     * 2.内置UDT字段，需要生成，不需要注释
     * 3.当前文件中，已存在的Udt类型，不需要注释(通过import判断)
     *
     * @return
     */
    protected boolean isNeedComment() {
        List<String> imports = this.classInfo.getImportClassInfo();
        if (!field.getIsUdt()) {
            return false;
        }
        TypeRefInfo typeRefInfo = getTypeInfo(field);
        //如果是内置UDT
        if (typeRefInfo.getFullName().startsWith("com.inspur.edp.common.commonudt")) {
            return false;
        }
        if (imports.contains(typeRefInfo.getFullName())) {
            return false;
        }
        return true;
    }

    public void generate() {
        methodInfo.setNeedCommented(isNeedComment());
        methodInfo.setMethodName(getMethodName());
        methodInfo.setReturnType(getReturnType());
        methodInfo.getAccessModifiers().add(JavaAccessModifier.Public);
        methodInfo.getAccessModifiers().add(JavaAccessModifier.Static);
        generateMethodParameters();
        generateMethodBodies();
        classInfo.addMethodInfo(methodInfo);
    }

    /**
     * 生成方法参数
     */
    protected abstract void generateMethodParameters();

    /**
     * 生成方法体
     */
    protected abstract void generateMethodBodies();
}
