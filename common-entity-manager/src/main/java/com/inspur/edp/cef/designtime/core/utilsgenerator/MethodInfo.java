package com.inspur.edp.cef.designtime.core.utilsgenerator;

import java.util.ArrayList;
import java.util.List;

public final class MethodInfo {
    public MethodInfo() {
    }

    private String methodName;
    private TypeRefInfo returnType;

    public boolean isNeedCommented() {
        return isNeedCommented;
    }

    public void setNeedCommented(boolean needCommented) {
        isNeedCommented = needCommented;
    }

    /**
     * 是否需要注释此方法
     */
    private boolean isNeedCommented = false;
    private final List<ParameterInfo> parameters = new ArrayList<>();
    private final List<JavaAccessModifier> accessModifiers = new ArrayList<>();
    private final List<String> methodBodies = new ArrayList<>();
    private static final String methodPrefix = "    ";
    private static final String methodBodyPrefix = "        ";

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public TypeRefInfo getReturnType() {
        return returnType;
    }

    public void setReturnType(TypeRefInfo returnType) {
        this.returnType = returnType;
    }

    public List<ParameterInfo> getParameters() {
        return parameters;
    }

    public List<JavaAccessModifier> getAccessModifiers() {
        return accessModifiers;
    }

    public List<String> getMethodBodies() {
        return methodBodies;
    }

    public void write(StringBuilder stringBuilder) {

        String prefix = isNeedCommented ? "// " : "";
        stringBuilder.append(prefix).append(methodPrefix);
        for (JavaAccessModifier modifier : getAccessModifiers())
            stringBuilder.append(modifier.toString()).append(" ");
        getReturnType().write(stringBuilder);
        stringBuilder.append(" ");
        stringBuilder.append(getMethodName()).append("(");
        int paramsCount = getParameters().size();
        for (int i = 0; i < paramsCount; i++) {
            ParameterInfo item = getParameters().get(i);
            item.write(stringBuilder);
            if (i < paramsCount - 1)
                stringBuilder.append(",");
        }
        stringBuilder.append("){\n");

        for (String methodBody : getMethodBodies()) {
            stringBuilder.append(prefix).append(methodBodyPrefix).append(methodBody).append("\n");
        }

        stringBuilder.append(prefix).append(methodPrefix).append("}");
    }
}
