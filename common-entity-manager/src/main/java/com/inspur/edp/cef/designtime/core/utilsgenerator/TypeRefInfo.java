package com.inspur.edp.cef.designtime.core.utilsgenerator;

public class TypeRefInfo {
    private String typePackageName;
    private String typeName;

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTypePackageName() {
        return typePackageName;
    }

    public void setTypePackageName(String typePackageName) {
        this.typePackageName = typePackageName;
    }

    public TypeRefInfo(String typeName, String typePackageName) {
        this.typeName = typeName;
        this.typePackageName = typePackageName;
    }

    public TypeRefInfo(Class typeClass) {
        typeName = typeClass.getSimpleName();
        if (typeClass.getPackage() != null)
            typePackageName = typeClass.getPackage().getName();
    }

    public void write(StringBuilder stringBuilder) {
        stringBuilder.append(getTypeName());
    }

    public String getFullName() {
        return getTypePackageName() + "." + getTypeName();
    }
}
