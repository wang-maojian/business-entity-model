package com.inspur.edp.cef.designtime.core.utilsgenerator;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.entity.entity.IEntityData;

class FieldSetMethodGenerator extends FieldMethodBaseGenerator {

    FieldSetMethodGenerator(IGspCommonField field, JavaClassInfo classInfo) {
        super(field, classInfo);
    }

    private static final String varPropValue = "propertyValue";


    //    @Override
    protected String getMethodName() {
        return "set" + field.getLabelID();
    }

    //
//    @Override
    protected TypeRefInfo getReturnType() {
        return new TypeRefInfo(void.class);
    }

    @Override
    protected void generateMethodParameters() {
        methodInfo.getParameters().add(new com.inspur.edp.cef.designtime.core.utilsgenerator.ParameterInfo(varData, new TypeRefInfo(IEntityData.class)));
        methodInfo.getParameters().add(new com.inspur.edp.cef.designtime.core.utilsgenerator.ParameterInfo(varPropValue, getTypeInfo(field)));
    }

    @Override
    protected void generateMethodBodies() {
        classInfo.getImportInfos().addImportPackage("com.inspur.edp.cef.entity.entity.EntityDataUtils");
        methodInfo.getMethodBodies().add("EntityDataUtils.setValue(data,\"" + field.getLabelID() + "\",propertyValue);");
    }
}
