package com.inspur.edp.cef.designtime.core.utilsgenerator;

import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;

public abstract class DataTypeUtilsGenerator {
    private final IGspCommonDataType commonDataType;

    private final JavaClassInfo classInfo = new JavaClassInfo();


    public DataTypeUtilsGenerator(IGspCommonDataType commonDataType, String basePath) {
        this.commonDataType = commonDataType;
        classInfo.setFilePath(basePath);
    }

    protected abstract String getClassName();

    protected abstract String getClassPackage();

    public final void generate() {
        classInfo.setPackageName(getClassPackage());
        classInfo.setClassName(getClassName());
        classInfo.getAccessModifiers().add(JavaAccessModifier.Public);
        classInfo.getAccessModifiers().add(JavaAccessModifier.Final);

        generateFieldsMethods();
        generateExtendInfos();
        classInfo.write2File();
    }

    protected void generateExtendInfos() {
    }

    private void generateFieldsMethods() {
        for (IGspCommonField field : commonDataType.getContainElements()) {
            generateFieldMethods(field);
        }
    }

    private void generateFieldMethods(IGspCommonField field) {
        generateFieldGetMethod(field);
        generateFieldSetMethod(field);
    }

    private void generateFieldGetMethod(IGspCommonField field) {
        new FieldGetMethodGenerator(field, classInfo).generate();
    }

    private void generateFieldSetMethod(IGspCommonField field) {
        new FieldSetMethodGenerator(field, classInfo).generate();
    }

    protected JavaClassInfo getClassInfo() {
        return classInfo;
    }
}
