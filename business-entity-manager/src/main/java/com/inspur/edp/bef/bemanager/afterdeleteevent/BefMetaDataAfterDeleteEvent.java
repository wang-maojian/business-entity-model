package com.inspur.edp.bef.bemanager.afterdeleteevent;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.serverapi.CustomizationRtServerService;
import com.inspur.edp.metadata.rtcustomization.spi.MetadataRtSpi;
import com.inspur.edp.metadata.rtcustomization.spi.args.MetadataDeletedArgs;
import com.inspur.edp.metadata.rtcustomization.spi.args.MetadataDeletingArgs;
import com.inspur.edp.metadata.rtcustomization.spi.args.MetadataSavedArgs;
import com.inspur.edp.svc.reference.check.api.IReferenceCfgManager;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public class BefMetaDataAfterDeleteEvent implements MetadataRtSpi {

    @Override
    public String getMetadataType() {
        return "GSPBusinessEntity";
    }

    /**
     * 元数据删除前事件
     * 删除BE元数据，打包，发布或者升级补丁，将BE上的关联信息从gsprefconfiginfo表中去掉，
     * 此处针对的是删除整个bE元数据以后清空关联信息
     *
     * @param metadataDeletedArgs
     */
    @Override
    public void metadataDeleting(MetadataDeletingArgs metadataDeletedArgs) {
        GspMetadata metadata = metadataDeletedArgs.getMetadata();
        if (metadata == null || metadata.getHeader() == null)
            return;
        if (!"GSPBusinessEntity".equals(metadata.getHeader().getType()))
            return;
        String delMetaId = metadata.getHeader().getId();
        CustomizationRtServerService SERVICE = SpringBeanUtils.getBean(CustomizationRtServerService.class);
        GspMetadata gspMetadata = SERVICE.getMetadataWithoutI18n(delMetaId);
        if (gspMetadata == null)
            return;
        GspBusinessEntity be = (GspBusinessEntity) gspMetadata.getContent();
        String referer = be.getDotnetGeneratedConfigID();
        IReferenceCfgManager MANAGER = SpringBeanUtils.getBean(IReferenceCfgManager.class);
        MANAGER.deleteByReferrer(referer);
    }

    /**
     * 元数据删除后事件，暂无逻辑处理
     *
     * @param metadataDeletedArgs
     */
    @Override
    public void metadataDeleted(MetadataDeletedArgs metadataDeletedArgs) {

    }

    /**
     * 元数据保存后事件，暂无逻辑处理
     *
     * @param metadataSavedArgs
     */
    @Override
    public void metadataSaved(MetadataSavedArgs metadataSavedArgs) {

    }
}
