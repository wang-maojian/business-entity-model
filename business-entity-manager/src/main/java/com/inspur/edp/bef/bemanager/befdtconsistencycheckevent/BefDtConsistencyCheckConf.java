package com.inspur.edp.bef.bemanager.befdtconsistencycheckevent;

import io.iec.edp.caf.commons.event.config.EventListenerSettings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public
class BefDtConsistencyCheckConf {
    @Bean(name = "BefDtConsistencyCheckEventBroker")
    public BefDtConsistencyCheckEventBroker setEventBroker(EventListenerSettings settings, BefDtConsistencyCheckEventManager eventManager) {
        return new BefDtConsistencyCheckEventBroker(settings, eventManager);
    }

    @Bean(name = "BefDtConsistencyCheckEventManager")
    public BefDtConsistencyCheckEventManager setEventManager() {
        return new BefDtConsistencyCheckEventManager();
    }
}
