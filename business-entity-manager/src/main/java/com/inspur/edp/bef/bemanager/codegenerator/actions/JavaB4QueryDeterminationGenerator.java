package com.inspur.edp.bef.bemanager.codegenerator.actions;


import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.BizOperation;

public class JavaB4QueryDeterminationGenerator extends JavaDeterminationGenerator {
    @Override
    protected String getBaseClassName() {
        return "AbstractQueryDetermination";
    }

    public JavaB4QueryDeterminationGenerator(GspBusinessEntity be, BizOperation operation, String nameSpace, String path) {
        super(be, operation, nameSpace, path);

    }

    @Override
    protected void JavaGenerateExtendUsing(StringBuilder result) {
        result.append(GetImportStr(JavaCompCodeNames.ActionApiNameSpace));
        result.append(GetImportStr(JavaCompCodeNames.DaterminationNameSpace));
        result.append(GetImportStr(JavaCompCodeNames.ChangesetNameSpace));
        result.append(GetImportStr(JavaCompCodeNames.AbstractDeterminationNameSpace));
        result.append(GetImportStr(JavaCompCodeNames.IDeterminationContextNameSpace));
    }

    @Override
    protected void JavaGenerateConstructor(StringBuilder result) {
        result.append(GetDoubleIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ").append(GetCompName()).append("(IQueryDeterminationContext context)").append(" ").append("{").append(getNewline());
        result.append(GetDoubleIndentationStr()).append(GetIndentationStr()).append("super(context)").append(";").append(getNewline());
        result.append(GetDoubleIndentationStr()).append("}");
    }

    @Override
    protected void JavaGenerateExtendMethod(StringBuilder result) {
    }

}