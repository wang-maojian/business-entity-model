package com.inspur.edp.bef.bemanager.validate;

import com.inspur.edp.bcc.biztag.api.BizTagService;
import com.inspur.edp.bcc.biztag.entity.BizTag;
import com.inspur.edp.bef.bemanager.generatedbo.GenerateFromDbo;
import com.inspur.edp.bef.bemanager.service.BeManagerService;
import com.inspur.edp.bef.bemanager.util.CheckInfoUtil;
import com.inspur.edp.bef.bemanager.util.ExpressionVariableCheckUtil;
import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.LogicDeleteControlInfo;
import com.inspur.edp.bef.bizentity.beenum.BETriggerTimePointType;
import com.inspur.edp.bef.bizentity.beenum.RequestNodeTriggerType;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.operation.BizActionBase;
import com.inspur.edp.bef.bizentity.operation.BizCommonDetermination;
import com.inspur.edp.bef.bizentity.operation.BizCommonValdation;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.Determination;
import com.inspur.edp.bef.bizentity.operation.Validation;
import com.inspur.edp.bef.bizentity.operation.collection.TccSettingCollection;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizReturnValue;
import com.inspur.edp.bef.bizentity.operation.componentenum.BizParameterType;
import com.inspur.edp.bef.bizentity.operation.componentinterface.IBizParameter;
import com.inspur.edp.bef.bizentity.operation.internalbeaction.IInternalBEAction;
import com.inspur.edp.bef.bizentity.operation.internalmgraction.IInternalMgrAction;
import com.inspur.edp.bef.bizentity.operation.internalmgraction.TccSettingElement;
import com.inspur.edp.bef.component.ICompParameter;
import com.inspur.edp.bef.component.ICompParameterCollection;
import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.collection.CommonDtmCollection;
import com.inspur.edp.cef.designtime.api.collection.CommonValCollection;
import com.inspur.edp.cef.designtime.api.collection.DtmElementCollection;
import com.inspur.edp.cef.designtime.api.collection.ValElementCollection;
import com.inspur.edp.cef.designtime.api.element.ElementDefaultVauleType;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.element.GspAssociationKey;
import com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
import com.inspur.edp.cef.designtime.api.element.GspEnumValue;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;
import com.inspur.edp.cef.designtime.api.validate.common.CheckUtil;
import com.inspur.edp.cef.designtime.api.variable.CommonVariableCollection;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.IObjectCollection;
import com.inspur.edp.das.commonmodel.entity.GspCommonModel;
import com.inspur.edp.das.commonmodel.entity.GspCommonObject;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.api.service.RefCommonService;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.databaseobject.api.context.DatabaseReservedWords;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class BizEntityValidater {
    private static final String FIELD = "Field";
    private static final String VARIABLE = "Variable";
    private static final List<String> farrisKeyWords = new ArrayList<>();

    static {
        farrisKeyWords.add("properties");
    }

    RefCommonService service;

    private RefCommonService getMetadataService() {
        if (service == null) {
            service = SpringBeanUtils.getBean(RefCommonService.class);
        }
        return service;
    }

    private String metaPath;

    /**
     * 业务实体保存前校验
     *
     * @param model 业务实体
     * @param path  当前路径
     */
    public final void validate(GspBusinessEntity model, String path) {
        metaPath = path;
        String validateResult = validateModel(model, path);
        if (!validateResult.isEmpty()) {
            //保存前校验不通过，抛异常
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_TEMPLATE_ERROR, validateResult);
        }
    }

    /**
     * 业务实体保存前校验
     *
     * @param model
     * @return 返回报错内容
     */
    private String validateModel(GspBusinessEntity model, String path) {
        ///#region Model
        // 模型基本信息
        String message = validateModelBasicInfo(model, path);
        if (!message.isEmpty()) {
            return message;
        }
        ///#endregion Model

        ///#region Variables
        message = validateVariableEntity(model);
        if (!message.isEmpty()) {
            return message;
        }

        ///#endregion


        ///#region Object

        // 所有对象基本信息
        message = validateObjectBasicInfo(model, path); //1. 编号不能重复
        if (!message.isEmpty()) {
            return message;
        }

        message = validateBizTags(model);
        if (!message.isEmpty()) {
            return message;
        }

        // 检查主对象ID字段
        message = validateIDElement(model);
        if (!message.isEmpty()) {
            return message;
        }

        // 检查所有子对象的关联关系
        message = validateChildRelation(model);
        if (!message.isEmpty()) {
            return message;
        }

        // 检查所有子对象的关联关系
        message = validateLogicDeleteElement(model);
        if (!message.isEmpty()) {
            return message;
        }


        ///#endregion Object


        ///#region Field

        // 检查元素的基本信息
        message = validateElementBasicInfo(model);
        if (!message.isEmpty()) {
            return message;
        }

        if (!model.getIsVirtual()) {
            //检查元素的数据类型是否和数据对象的数据类型对应
            message = this.validateElementDataType(model);
            if (!message.isEmpty()) {
                return message;
            }
        }

        //检查元素的默认值
        message = this.validateElementDefaultValue(model);
        if (!message.isEmpty()) {
            return message;
        }

        // 同一对象上的元素的LabelID不允许重复
        message = validateElementLabelID(model);
        if (!message.isEmpty()) {
            return message;
        }

        // 同一对象上元素的Name不允许重复
        message = validateElementName(model);
        if (!message.isEmpty()) {
            return message;
        }


        // 同一对象上的元素的Code不允许重复
        message = validateElementCode(model);
        if (!message.isEmpty()) {
            return message;
        }

        //字段引用[业务字段]，但是没有配置
        message = validateElementUdt(model);
        if (!message.isEmpty()) {
            return message;
        }


        ///#endregion Field


        ///#region Operation

        //MgrAction
        message = validateMgrActions(model);
        if (!message.isEmpty()) {
            return message;
        }

        //obj操作（beAction,validation,determinations）
        message = validateBeActions(model);
        if (!message.isEmpty()) {
            return message;
        }

        message = validateDeterminations(model);
        if (!message.isEmpty()) {
            return message;
        }

        message = validateValidations(model);
        if (!message.isEmpty()) {
            return message;
        }


        message = validateTccSettingCollection(model);
        if (!message.isEmpty()) {
            return message;
        }

        if (!SpringBeanUtils.getBean(MetadataProjectService.class).isInterpretation(path)) {
            // 关联字段校验。只做生成型
            // 430628 已去掉该校验
            // message = validateAssociateEnum(model);
            if (!message.isEmpty()) {
                return message;
            }
        }
        ///#endregion Operation

        return "";
    }

    private String validateElementName(GspBusinessEntity model) {
        java.util.ArrayList<IGspCommonElement> allElementList = model.getAllElementList(true);

        if (allElementList != null && !allElementList.isEmpty()) {
            for (int i = 0; i < allElementList.size() - 1; i++) {
                IGspCommonElement firstElement = allElementList.get(i);
                if (firstElement.getIsRefElement()) {
                    continue;
                }
                for (int j = i + 1; j < allElementList.size(); j++) {
                    IGspCommonElement secondElement = allElementList.get(j);
                    if (secondElement.getIsRefElement()) {
                        continue;
                    }
                    if (firstElement.getName().equals(secondElement.getName()) && firstElement.getBelongObject().getID().equals(secondElement.getBelongObject().getID())) {
                        String message = MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0012", model.getName(), firstElement.getBelongObject().getName(), firstElement.getLabelID(), secondElement.getLabelID());
                        return message;
                    }
                }
            }
        }
        return "";
    }

    /**
     * 校验be关联枚举字段，不允许有相同编号
     *
     * @param model 业务实体be
     * @return 错误提示
     */
    private String validateAssociateEnum(GspBusinessEntity model) {
        StringBuilder sb = new StringBuilder();
        List<IGspCommonObject> allObjectList = model.getAllObjectList();
        for (IGspCommonObject iGspCommonObject : allObjectList) {
            List<IGspCommonElement> elementList = iGspCommonObject.getAllElementList(false);
            // 获取be内枚举字段的编号及名称对应关系
            Map<String, String> elementCodeName = getElementCodeName(elementList);
            if (elementCodeName.isEmpty()) {
                // 无枚举、关联字段，跳过检查
                continue;
            }
            for (IGspCommonElement iGspCommonElement : elementList) {
                if (iGspCommonElement.getObjectType() == GspElementObjectType.Association) {
                    List<GspAssociation> elementAssociation = iGspCommonElement.getChildAssociations();
                    validateEnum(elementCodeName, elementAssociation, iGspCommonElement.getName(), sb);
                }
            }
        }
        return sb.toString();
    }

    /**
     * 获取枚举、关联字段中编号对应的名称
     *
     * @param elementList be业务实体
     * @return 编号对应名称
     */
    private Map<String, String> getElementCodeName(List<IGspCommonElement> elementList) {
        Map<String, String> elementCodeName = new HashMap<>();
        for (IGspCommonElement iGspCommonElement : elementList) {
            if (iGspCommonElement.getObjectType() == GspElementObjectType.Enum || iGspCommonElement.getObjectType() == GspElementObjectType.Association) {
                elementCodeName.put(iGspCommonElement.getCode(), iGspCommonElement.getName());
            }
        }
        return elementCodeName;
    }

    /**
     * 判断关联关系中枚举、关联字段和be中是否存在相同编号
     *
     * @param elementCodeName    编号对应名称
     * @param elementAssociation 关联关系
     * @param eleName            关联字段名称
     * @param sb                 错误提示
     */
    private void validateEnum(Map<String, String> elementCodeName, List<GspAssociation> elementAssociation, String eleName, StringBuilder sb) {
        for (GspAssociation gspAssociation : elementAssociation) {
            List<IGspCommonField> refElementList = gspAssociation.getRefElementCollection();
            for (IGspCommonField refElement : refElementList) {
                if (refElement.getObjectType() == GspElementObjectType.Enum || refElement.getObjectType() == GspElementObjectType.Association) {
                    if (elementCodeName.containsKey(refElement.getCode())) {
                        // 存在枚举类型和关联枚举编号相同
                        String elementName = elementCodeName.get(refElement.getCode());
                        sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0011", eleName, refElement.getName(), elementName, elementName));
                        sb.append("\n");
                    }
                }
            }
        }
    }

    ///#region Model
    // 检查数据模型的基本信息
    private String validateModelBasicInfo(GspBusinessEntity model, String path) {
        StringBuilder sb = new StringBuilder();
        if (CheckInfoUtil.checkNull(model.getCode())) {
            sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0013", model.getName()));
        } else {
            // 模型Code限制非法字符
            if (!ConformToNaminGspecification(model.getCode())) {
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0014", model.getName()));
                sb.append("\n");
            }
        }
        if (CheckInfoUtil.checkNull(model.getName())) {
            sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0015", model.getCode()));
        }

        //todo:[启用时间戳]方案成熟前，先隐藏
        //if (!model.IsUsingTimeStamp)
        //{
        //	// 若传入路径不为空，则检查旧be的值
        //	if (!string.IsNullOrEmpty(path))
        //	{
        //		//旧be中的启用时间戳为true，且已经生成dbo,则不允许设置为false
        //		var service = ServiceManager.GetService<IMetadataService>();
        //		var oldBeMetadata = service.GetRefMetadata(path, model.ID, null);
        //		var oldBe = oldBeMetadata.Content as GspBusinessEntity;
        //		if (oldBe != null && oldBe.IsUsingTimeStamp && !string.IsNullOrEmpty(oldBe.MainObject.RefObjectName))
        //		{
        //			sb.append(String.format("已生成dbo的实体，不允许将[启用时间戳]属性由true改为false。");
        //		}
        //	}
        //}
        if (model.getEnableCaching() && CheckInfoUtil.checkNull(model.getCacheConfiguration())) {
            sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0017", model.getName()));
        }
        if (model.getCode().equals(model.getGeneratingAssembly())) {
            sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0018", model.getName()));
        }
        if (!CheckInfoUtil.checkNull(model.getVersionContronInfo().getVersionControlElementId())) {

            IGspCommonField ele = model.getMainObject().findElement(model.getVersionContronInfo().getVersionControlElementId());
            if (ele == null) {
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0019", model.getName()));
            } else if (ele.getMDataType() != GspElementDataType.DateTime) {
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0020", model.getName(), ele.getName(), ele.getMDataType().toString()));
            }
        }
        return sb.toString();
    }

    private String validateVariableEntity(GspCommonModel model) {
        StringBuilder sb = new StringBuilder();
        sb.append(validateVariables(model));
        return sb.toString();
    }

    private String validateVariables(GspCommonModel model) {

        CommonVariableCollection vars = model.getVariables().getContainElements();
        StringBuilder sb = new StringBuilder();
        if (vars != null && !vars.isEmpty()) {

            for (IGspCommonField variable : vars) {
                // 检查元素的基本信息
                validateElementBasicInfo(variable, sb, model.getName(), MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0046"),
                        MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0046"), model.getIsVirtual(), VARIABLE);
            }
            // 编号不可重复
            for (int i = 0; i < vars.size() - 1; i++) {
                IGspCommonField firstElement = vars.getItem(i);
                for (int j = i + 1; j < vars.size(); j++) {
                    IGspCommonField secondElement = vars.getItem(j);
                    if (firstElement.getCode().equals(secondElement.getCode()) && firstElement.getBelongObject().getID().equals(secondElement.getBelongObject().getID())) {
                        String message = MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0047", model.getName(), firstElement.getName(), secondElement.getName());
                        return message;
                    }
                }
            }
        }

        //联动计算
        CommonDtmCollection dtms = new CommonDtmCollection();
        dtms.addAll(model.getVariables().getDtmAfterCreate());
        dtms.addAll(model.getVariables().getDtmAfterModify());
        dtms.addAll(model.getVariables().getDtmBeforeSave());
        sb.append(validateVarDtms(dtms, model.getName()));

        return sb.toString();
    }

    private String validateVarDtms(CommonDtmCollection collection, String modelName) {
        //联动计算
        String errorMessage = MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0005", modelName);

        List<CommonOperation> dtms = collection.stream().collect(Collectors.toList());
        //①必填项
        StringBuilder sb = new StringBuilder();
        sb.append(validateCommonOperationBasicInfo(errorMessage, (ArrayList<CommonOperation>) dtms));
        if (sb.length() > 0) {
            return sb.toString();
        }
        //②编号重复
        sb.append(String.format(validateCommonOperationCodeRepeat(errorMessage, (ArrayList<CommonOperation>) dtms)));
        return sb.toString();
    }


    ///#endregion


    ///#region Object

    private void validateObjectBasicInfo(IGspCommonDataType aObject, StringBuilder sb, String modelName, String path) {
        if (CheckInfoUtil.checkNull(aObject.getCode())) {
            sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0090", modelName, aObject.getName()));
        } else {
            // 对象Code限制非法字符
            if (!ConformToNaminGspecification(aObject.getCode())) {
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0091", modelName, aObject.getName()));
                sb.append("\n");
            }
            // 对象Code限制数据库保留字
            if (!CheckUtil.isLegality(aObject.getCode())) {
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0092",
                        modelName, aObject.getName(), aObject.getCode()));
                sb.append("\n");
            }
        }
        if (CheckInfoUtil.checkNull(aObject.getName())) {
            sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0093", modelName, aObject.getCode()));
        }
        GenerateFromDbo generateFromDbo = GenerateFromDbo.getInstance();
        String appCode = generateFromDbo.GetAppCode(path);
        String dboCode = appCode + aObject.getCode();
        if (dboCode.length() > 30) {
            sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0094", modelName, aObject.getName(), dboCode));
        }
    }

    // 检查数据模型中对象的基本信息
    private String validateObjectBasicInfo(GspBusinessEntity model, String path) {
        StringBuilder sb = new StringBuilder();
        java.util.ArrayList<IGspCommonObject> aObjectList = model.getAllObjectList();
        for (IGspCommonObject aObject : aObjectList) {
            validateObjectBasicInfo(aObject, sb, model.getName(), path);
        }

        for (IGspCommonObject aObject : aObjectList) {
            for (IGspCommonObject bObject : aObjectList) {
                // 排除本身
                if (aObject.getID().compareTo(bObject.getID()) == 0) {
                    continue;
                }

                if (aObject.getID().compareTo(bObject.getCode()) == 0) {
                    sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0095", model.getName(), aObject.getName(), bObject.getName()));
                    return sb.toString();
                }
            }
        }

        return sb.toString();
    }

    // 检查主对象ID字段
    private String validateIDElement(GspBusinessEntity model) {
        GspBizEntityObject mObject = model.getMainObject();
        //验证ID字段和Code字段不能有引用和枚举。并且必须必填和插入, 更新必须为false
        GspBizEntityElement idElement = (GspBizEntityElement) mObject.getIDElement();

        StringBuilder sb = new StringBuilder();
        if (idElement.getHasAssociation()) {
            sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0048", model.getName(), mObject.getName(), idElement.getName()));
            sb.append("\n");
        } else if (idElement.getObjectType() == GspElementObjectType.Enum) {
            sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0049", model.getName(), mObject.getName(), idElement.getName()));
            sb.append("\n");
        }
        if (!idElement.getIsRequire()) {
            sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0050", model.getName(), mObject.getName(), idElement.getName()));
            sb.append("\n");
        }
        //ID字段长度及字段数据类型校验
        if (!validateIDElementTypeAndLength(idElement)) {
            sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0051", model.getName(), mObject.getName(), idElement.getName()));
            sb.append("\n");
        }
        return sb.toString();
    }

    ///#endregion


    ///#region Element

    private String validateElementDefaultValue(GspBusinessEntity model) {
        StringBuilder sb = new StringBuilder();
        String regexInt = "^-?\\d+$"; //判断整数，负值也是整数

        Pattern patternInt = Pattern.compile(regexInt);

        java.util.ArrayList<IGspCommonObject> aObjectList = model.getAllObjectList();
        for (IGspCommonObject aObject : aObjectList) {
            java.util.ArrayList<IGspCommonElement> elementList = aObject.getAllElementList(false);
            for (IGspCommonElement ele : elementList) {
                boolean isvalidate = true;
                if (ele.getDefaultValueType() == ElementDefaultVauleType.Expression) {
                    validateElementExpression(ele, sb, model.getName(), aObject.getName());
                    continue;
                }
                GspElementDataType type = ele.getMDataType();
                String value = ele.getDefaultValue();
                int length = ele.getLength();

                if (!CheckInfoUtil.checkNull(value)) {
                    switch (type) {
                        case String:
                        case Text:
                            break;
                        case Integer:
                            Matcher matcherInt = patternInt.matcher(value);
                            isvalidate = matcherInt.matches();
                            break;
                        case Decimal:
                            String reDecimalString = String.format("^(([0-9]+\\.[0-9]{0,%1$s})|([0-9]*\\.[0-9]{0,%1$s})|([1-9][0-9]+)|([0-9]))$", ele.getPrecision());
                            Pattern patternDecimal = Pattern.compile(reDecimalString);
                            Matcher matcherDecimal = patternDecimal.matcher(value);
                            if (matcherDecimal.matches()) {
                                isvalidate = true;
                            } else {
                                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0052", model.getName(), aObject.getName(), ele.getName()));
                                sb.append("\n");
                                return sb.toString();
                            }
                            break;
                        case Date:
                            try {
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                Date date = sdf.parse(value);
                                isvalidate = true;
                            } catch (java.lang.Exception e) {
                                isvalidate = false;
                            }
                            break;
                        case DateTime:
                            try {
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date date = sdf.parse(value);
                                isvalidate = true;
                            } catch (java.lang.Exception e) {
                                isvalidate = false;
                            }
                            break;
                        case Boolean:
                            isvalidate = value.equalsIgnoreCase("True") || value.equalsIgnoreCase("False");
                            break;
                        case Binary:
                            isvalidate = value.trim().isEmpty();
                            break;
                        default:
                            break;
                    }
                }
                if (!isvalidate) {
                    sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0053", model.getName(), aObject.getName(), ele.getName()));
                    sb.append("\n");
                }
            }
        }
        return sb.toString();
    }

    /**
     * 校验be字段表达式类型默认值校验
     *
     * @param ele         be字段
     * @param sb          错误提示
     * @param modelName   工程名称
     * @param aObjectName be名称
     */
    private void validateElementExpression(IGspCommonElement ele, StringBuilder sb, String modelName,
                                           String aObjectName) {
        String expression = ele.getDefaultValue();
        if (ExpressionVariableCheckUtil.isDateType(expression)) {
            if (ele.getMDataType() == GspElementDataType.Date || ele.getMDataType() == GspElementDataType.DateTime) {
                return;
            }
            sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0027", modelName, aObjectName, ele.getName()));
            sb.append("\n");
        }
        if (ExpressionVariableCheckUtil.isTextType(expression)) {
            if (ele.getMDataType() == GspElementDataType.String || ele.getMDataType() == GspElementDataType.Text) {
                return;
            }
            sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0028", modelName, aObjectName, ele.getName()));
            sb.append("\n");
        }
    }

    private String validateElementLabelID(GspBusinessEntity model) {
        java.util.ArrayList<IGspCommonElement> allElementList = model.getAllElementList(true);

        if (allElementList != null && !allElementList.isEmpty()) {
            for (int i = 0; i < allElementList.size() - 1; i++) {
                IGspCommonElement firstElement = allElementList.get(i);
                for (int j = i + 1; j < allElementList.size(); j++) {
                    IGspCommonElement secondElement = allElementList.get(j);
                    if (firstElement.getLabelID().equals(secondElement.getLabelID()) && firstElement.getBelongObject().getID().equals(secondElement.getBelongObject().getID())) {
                        String message = MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0103", model.getName(), firstElement.getBelongObject().getName(), firstElement.getName(), secondElement.getName());
                        return message;
                    }
                }
            }
        }
        return "";
    }

    private String validateElementCode(GspBusinessEntity model) {
        java.util.ArrayList<IGspCommonElement> allElementList = model.getAllElementList(false);
        if (allElementList != null && !allElementList.isEmpty()) {
            for (int i = 0; i < allElementList.size() - 1; i++) {
                IGspCommonElement firstElement = allElementList.get(i);
                for (int j = i + 1; j < allElementList.size(); j++) {
                    IGspCommonElement secondElement = allElementList.get(j);
                    if (firstElement.getCode().equals(secondElement.getCode()) && firstElement.getBelongObject().getID().equals(secondElement.getBelongObject().getID())) {
                        String message = MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0104", model.getName(), firstElement.getBelongObject().getName(), firstElement.getName(), secondElement.getName());
                        return message;
                    }
                }
            }
        }
        return "";
    }

    // 关联关系
    private String validateChildRelation(GspBusinessEntity model) {
        StringBuilder sb = new StringBuilder();
        GspBizEntityObject mainObject = model.getMainObject();
        if (mainObject != null) {
            for (IGspCommonObject cObject : mainObject.getContainChildObjects()) {
                String message;
                if (cObject.getKeys().isEmpty()) {
                    sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0096", model.getName(), cObject.getName(), cObject.getParentObject().getName()));
                    sb.append("\n");
                }

                //验证ID字段不能既是主键又是外键
                String cID = cObject.getColumnGenerateID().getElementID();
                GspAssociationKey ak = cObject.getKeys().get(0);
                if (cID.equals(ak.getSourceElement())) {
                    sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0097", model.getName(), cObject.getName(), cObject.getParentObject().getName(), ak.getSourceElementDisplay()));
                    sb.append("\n");
                }

                //ParentID字段应为ID类型
                if (!validateIDElementTypeAndLength(cObject.getParentObject().findElement(ak.getSourceElement()))) {
                    sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0098", model.getName(), cObject.getName(), cObject.getParentObject().getName(), ak.getSourceElementDisplay()));
                    sb.append("\n");
                }

                if ((mainObject.getCode().trim() + "LIST").equalsIgnoreCase(cObject.getCode().trim())) {
                    sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0099", model.getName(), cObject.getName(), cObject.getParentObject().getName(), ak.getSourceElementDisplay()));
                    sb.append("\n");
                }

                message = validateChildrenRelation(model, cObject);
                if (!message.isEmpty()) {
                    return message;
                }
            }
        }
        return sb.toString();
    }

    /**
     * 逻辑删除字段
     *
     * @param model
     * @return
     */
    private String validateLogicDeleteElement(GspBusinessEntity model) {
        StringBuilder sb = new StringBuilder();
        java.util.ArrayList<IGspCommonObject> collection = model.getAllObjectList();

        for (IGspCommonObject cObject : collection) {
            GspBizEntityObject bizEntityObject = (GspBizEntityObject) cObject;
            LogicDeleteControlInfo info = bizEntityObject.getLogicDeleteControlInfo();
            if (!info.getEnableLogicDelete()) {
                continue;
            }
            String logicDeleteElementId = info.getLogicDeleteControlElementId();
            if (CheckInfoUtil.checkNull(logicDeleteElementId)) {
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0100", model.getName(), cObject.getName()));
                sb.append("\n");
                continue;
            }

            IGspCommonElement ele = bizEntityObject.findElement(logicDeleteElementId);
            if (ele == null) {
                // 配置，但是字段没了
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0101", model.getName(), cObject.getName()));
                sb.append("\n");
                continue;
            }
            if (ele.getMDataType() != GspElementDataType.Boolean) {
                // 配置，但是字段数据类型错误
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0102", model.getName(), cObject.getName()));
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    /**
     * 校验ID类型字段类型字符串且长度为22
     * ID字段，ParentID字段，关联字段
     * TODO:被关联字段暂不考虑
     *
     * @param element
     * @return
     */
    private boolean validateIDElementTypeAndLength(IGspCommonField element) {
        if (element == null) {
            return true;
        }
        return element.getMDataType() == GspElementDataType.String && element.getLength() >= 36;
    }

    private String validateChildrenRelation(GspBusinessEntity model, IGspCommonObject cObject) {
        StringBuilder sb = new StringBuilder();
        if (cObject.getContainChildObjects() != null && !cObject.getContainChildObjects().isEmpty()) {
            for (int i = 0; i < cObject.getContainChildObjects().size(); i++) {
                IGspCommonObject ccObject = cObject.getContainChildObjects().get(i);
                if (ccObject.getKeys().isEmpty()) {
                    sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0096", model.getName(), ccObject.getName(), ccObject.getParentObject().getName()));
                    sb.append("\n");
                }

                String cID = ccObject.getColumnGenerateID().getElementID();

                GspAssociationKey ak = ccObject.getKeys().get(0);
                if (cID.equals(ak.getSourceElement())) {
                    sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0097", model.getName(), ccObject.getName(), ccObject.getParentObject().getName(), ak.getSourceElementDisplay()));
                    sb.append("\n");
                }

                String message = validateChildrenRelation(model, ccObject);
                if (!message.isEmpty()) {
                    sb.append(String.format(message));
                    sb.append("\n");
                    break;
                }
            }
        }
        return sb.toString();
    }

    private void validateElementBasicInfo(IGspCommonField element, StringBuilder sb, String modelName, String objName, String objCode, boolean modelIsVirtual, String errorMessage) {
        String msgCode;
        if (errorMessage.equals("Field")) {
            msgCode = "GSP_BEMODEL_INFO_0003";
        } else {
            msgCode = "GSP_BEMODEL_INFO_0004";
        }
        if (CheckInfoUtil.checkNull(element.getCode())) {
            sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0056", modelName, objName, element.getName()));
            sb.append("\n");
        } else {
            // 字段Code限制非法字符
            if (!ConformToNaminGspecification(element.getCode())) {
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0057", modelName, objName, element.getName()));
                sb.append("\n");
            }
            // 字段Code限制数据库保留字
            ReserveResult codeResult = getReservedResult(element.getCode(), msgCode, modelName, objName, MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0058"), element);
            if (codeResult.isKeyWords) {
                sb.append(codeResult.message);
                sb.append("\n");
            } else {
                ReserveResult labelIdResult = getReservedResult(element.getLabelID(), msgCode, modelName, objName, MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0061"), element);
                if (labelIdResult.isKeyWords) {
                    sb.append(labelIdResult.message);
                    sb.append("\n");
                }
            }

            // 字段Code限制数据库对象保留字（包括1.时间戳字段；2.租户字段TenantID；）
            if (isDatabaseObjectReservedWords(element.getCode())) {
                String newErrorMessage = MessageI18nUtils.getMessage(msgCode, modelName, objName, element.getName());
                sb.append(newErrorMessage).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0062", element.getCode()));
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0063"));
                sb.append("\n");
            }
            // 字段LabelID限制数据库对象保留字（包括1.时间戳字段；2.租户字段TenantID；）
            else if (isDatabaseObjectReservedWords(element.getLabelID())) {
                String newErrorMessage = MessageI18nUtils.getMessage(msgCode, modelName, objName, element.getName());
                sb.append(newErrorMessage).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0064", element.getLabelID()));
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0063"));
                sb.append("\n");
            }
        }
        if (CheckInfoUtil.checkNull(element.getName())) {
            sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0065", modelName, objName, element.getCode()));
            sb.append("\n");
        }

        if (CheckInfoUtil.checkNull(element.getLabelID()) || CheckInfoUtil.checkNull(element.getLabelID().trim())) {
            if (modelIsVirtual) {
                element.setLabelID(element.getCode());
            } else {
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0066", modelName, objName, element.getName()));
                sb.append("\n");
            }
        } else {
            if (Objects.equals(element.getLabelID(), objCode + "_Id")) {
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0067", modelName, objName, element.getName())).append("\r\n");
            }
            if (element.getLabelID().length() > 30) {
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0068", modelName, objName, element.getName()));
                sb.append("\n");
            }

            // LabelID在表单和报表中用作XML的标签，要限制非法字符
            if (isFrameworkReservedWords(element.getLabelID())) {
                String newErrorMessage = MessageI18nUtils.getMessage(msgCode, modelName, objName, element.getName());
                sb.append(newErrorMessage).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0070", element.getLabelID()));
                sb.append("\n");
            }
        }
        if (element.getObjectType() == GspElementObjectType.Association) {
            if (!element.getHasAssociation()) {
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0071", modelName, objName, element.getName()));
                sb.append("\n");
            } else {
                for (GspAssociation association : element.getChildAssociations()) {
                    if (association == null || association.getKeyCollection().isEmpty()) {
                        sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0071", modelName, objName, element.getName()));
                        sb.append("\n");
                    }
                    if (StringUtils.isEmpty(association.getRefModelID())) {
                        sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0072", modelName, objName, element.getName()));
                        sb.append("\n");
                    }
                    if (StringUtils.isEmpty(association.getRefObjectID())) {
                        sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0073", modelName, objName, element.getName()));
                        sb.append("\n");
                    }
                    //处理有时关联字段会丢失的问题
                    String sourceElement = "";
                    for (GspAssociationKey associationKey : association.getKeyCollection()) {
                        if (associationKey.getSourceElement().isEmpty() || associationKey.getTargetElement().isEmpty()) {
                            sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0074", modelName, objName, element.getName()));
                            sb.append("\n");
                        } else {
                            sourceElement = associationKey.getSourceElement();
                            String targetElement = associationKey.getTargetElement();
                            if (!element.getID().equalsIgnoreCase(targetElement)) {
                                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0075", modelName, objName, element.getName()));
                                sb.append("\n");
                            }
                        }
                    }

                    GspMetadata refMetadata = getMetadataService().getRefMetadata(association.getRefModelID());
                    if (refMetadata == null) {
                        sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0076", modelName, objName, element.getName()));
                        sb.append("\n");
                    } else {
                        if (refMetadata.getContent() instanceof GspBusinessEntity) {
                            GspBusinessEntity refBusinessEntity = (GspBusinessEntity) refMetadata.getContent();
                            IGspCommonObject refObject = refBusinessEntity.findObjectById(association.getRefObjectID());
                            if (refObject == null) {
                                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0077", modelName, objName, element.getName()));
                                sb.append("\n");
                            } else {
                                if (refObject.findElement(sourceElement) == null) {
                                    sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0078", modelName, objName, element.getName()));
                                    sb.append("\n");
                                }
                                for (IGspCommonField commonField : association.getRefElementCollection()) {
                                    IGspCommonElement refElement = refObject.findElement(commonField.getRefElementId());

                                    if (refElement == null) {
                                        String newErrorMessage = MessageI18nUtils.getMessage(msgCode, modelName, element.getName(), objName);
                                        sb.append(newErrorMessage).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0079", commonField.getLabelID()));
                                        sb.append("\n");
                                    } else {
                                        if (commonField.getRefElementId().equalsIgnoreCase(element.getID()) && element.getBelongObject().getID().equals(refObject.getID())) {
                                            //if太长了 model的判断放到方法内部
                                            GspCommonObject currentObj = null;
                                            if (element.getBelongObject() instanceof GspCommonObject) {
                                                currentObj = (GspCommonObject) element.getBelongObject();
                                            }
                                            if (currentObj != null && currentObj.getBelongModel().getID().equals(association.getRefModelID())) {
                                                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0080", modelName, objName, element.getName()));
                                                sb.append("\n");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else if (element.getObjectType() == GspElementObjectType.Enum) {
            //todo 暂时屏蔽、后续要加上，前端放开
            // 枚举类型字段，校验默认值为枚举编号
//            if (!CheckInfoUtil.checkNull(element.getDefaultValue()))
//            {
//                String defaultValue = element.getDefaultValue();
//                boolean isEnumKey = false;
//                for (GspEnumValue enumValue : element.getContainEnumValues())
//                {
//                    if (defaultValue.equals(enumValue.getValue()))
//                    {
//                        isEnumKey = true;
//                    }
//                }
//                if (!isEnumKey)
//                {
//                    sb.append(String.format(errorMessage + "的 [对象类型] 属性为枚举, [默认值] 应为枚举编号。 ", modelName,objName, element.getName()));
//                }
//            }
            // 枚举类型字段，非必填
            if (element.getIsRequire()) {
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0081", modelName, objName, element.getName()));
                sb.append("\n");
            }

            if (element.getContainEnumValues() == null || element.getContainEnumValues().isEmpty()) {
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0082", modelName, objName, element.getName()));
                sb.append("\n");
            }

            //处理Enum值必须为string的问题
            for (GspEnumValue ev : element.getContainEnumValues()) {
                String _value = ev.getValue();
                if (element.getMDataType() == GspElementDataType.Boolean) {
                    try {
                        boolean temp = Boolean.parseBoolean(_value);
                    } catch (java.lang.Exception e) {
                        sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0083", modelName, objName, element.getName(), _value));
                        sb.append("\n");
                    }
                } else if (element.getMDataType() == GspElementDataType.String) {
                    int indexLength = String.valueOf(ev.getIndex()).length();
                    int eleLength = element.getLength();
                    if (eleLength < indexLength) {
                        sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0084", modelName, objName, element.getName(), ev.getName()));
                        sb.append("\n");
                    }
                }
            }
            if (element.getContainEnumValues() != null && !element.getContainEnumValues().isEmpty()) {
                for (int i = 0; i < element.getContainEnumValues().size() - 1; i++) {

                    GspEnumValue enumValue1 = element.getContainEnumValues().get(i);
                    for (int j = i + 1; j < element.getContainEnumValues().size(); j++) {

                        GspEnumValue enumValue2 = element.getContainEnumValues().get(j);
                        if (Objects.equals(enumValue1.getName(), enumValue2.getName())) {
                            String message = MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0085", modelName, objName, element.getName(), enumValue1.getName(), enumValue1.getName());
                            sb.append(message);
                            sb.append("\n");
                        }
                    }
                }
            }
        } else if (element.getObjectType() == GspElementObjectType.DynamicProp) {
            if (element.getDynamicPropSetInfo() != null && element.getDynamicPropSetInfo().getDynamicPropSerializerComp() != null && CheckInfoUtil.checkNull(element.getDynamicPropSetInfo().getDynamicPropSerializerComp().getId())) {
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0086", modelName, objName, element.getName()));
            }
        }

        if (element.getMDataType() == GspElementDataType.Decimal) {
            if (element.getLength() == 0) {
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0087", modelName, objName, element.getName()));
                sb.append("\n");
            }

            if (element.getPrecision() == 0) {
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0088", modelName, objName, element.getName()));
                sb.append("\n");
            }

        }
        if (element.getMDataType() == GspElementDataType.String) {
            if (!element.getIsVirtual() && element.getLength() < 1) {
                sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0089", modelName, objName, element.getName()));
                sb.append("\n");
            }
        }
        String errorMessageString = MessageI18nUtils.getMessage(msgCode, modelName, objName, element.getName());
        if (element.getIsUdt() && CheckInfoUtil.checkNull(element.getUdtID())) {
            sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0054", modelName, objName, element.getName()));
            sb.append("\n");
        }
        //20190918盘，临时添加检查：
        //Bug296009,按udtID加载出udt元数据来，看是否为多列的多值udt
        //Bug296448,udt的带出字段包含[普通关联]类型的
        if (element.getObjectType() == GspElementObjectType.Association) {

            for (IGspCommonField refEle : element.getChildAssociations().get(0).getRefElementCollection()) {
                if (refEle.getIsUdt()) {

                    String refEleUdtId = refEle.getUdtID();
                    if (CheckInfoUtil.checkNull(refEleUdtId)) {
                        throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0022, errorMessageString, refEle.getLabelID());
                    }

                    GspMetadata refEleUdtMeta = getMetadataService().getRefMetadata(refEleUdtId);
                    if (refEleUdtMeta == null) {
                        throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0023, errorMessageString, refEle.getLabelID(), refEleUdtId);
                    }
                }
            }
        }
        // 20191128-N版不支持此种场景，但去掉保存前校验
        //      if (element.IsUdt)
        //{
        //	string udtId = element.UdtID;
        //	var udtMeta= MetadataService.GetRefMetadata(udtId);
        //	if (udtMeta == null)
        //	{
        //		throw new Exception($"{FIELD}对应的udt元数据加载识别，udtId={udtId}。");
        //	}

        //	var udtContent = udtMeta.Content as UnifiedDataTypeDef;
        //	if (udtContent is SimpleDataTypeDef sUdt)
        //	{
        //		if (sUdt.ObjectType == GspElementObjectType.Association)
        //		{
        //			foreach (var refEle in sUdt.ChildAssociations[0].RefElementCollection)
        //			{
        //				if (refEle.ObjectType == GspElementObjectType.Association&&!refEle.IsUdt)
        //				{
        //					sb.append(String.format(FIELD + "的数据类型为关联类型的[业务字段]，且关联带出字段{3}为普通关联类型，暂不支持引用此类关联业务字段。", modelName, objName, element.Name,
        //						refEle.LabelID);
        //					sb.AppendLine();
        //				}
        //			}
        //		}
        //	}
        //}
    }

    //标签唯一性校验
    private String validateBizTags(GspBusinessEntity model) {
        String sb = "";
        java.util.ArrayList<IGspCommonObject> aObjectList = model.getAllObjectList();
        for (IGspCommonObject aObject : aObjectList) {
            List<List<String>> tagLists = aObject.getContainElements().stream().filter(item -> item.getBizTagIds() != null && !item.getBizTagIds().isEmpty())
                    .map(IGspCommonField::getBizTagIds).collect(Collectors.toList());
            List<String> tempList = new ArrayList<>();
            if (tagLists != null && !tagLists.isEmpty()) {
                for (List<String> tag : tagLists) {
                    tempList.addAll(tag);
                }
                Map<String, Long> map = tempList.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
                for (Map.Entry<String, Long> entry : map.entrySet()) {
                    if (entry.getValue() > 1) {
                        BizTagService bizTagService = SpringBeanUtils.getBean(BizTagService.class);
                        BizTag bizTag = bizTagService.getBizTagById(entry.getKey());
                        if (bizTag.isUnique())
                            return MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0105", bizTag.getName());
                    }
                }
            }
        }
        return sb;
    }

    //字段
    private String validateElementBasicInfo(GspBusinessEntity model) {
        StringBuilder sb = new StringBuilder();
        java.util.ArrayList<IGspCommonObject> aObjectList = model.getAllObjectList();
        for (IGspCommonObject aObject : aObjectList) {
            java.util.ArrayList<IGspCommonElement> allElementList = aObject.getAllElementList(false);
            for (IGspCommonElement element : allElementList) {
                validateElementBasicInfo(element, sb, model.getName(), aObject.getName(), aObject.getCode(), model.getIsVirtual(), FIELD);
            }
        }
        return sb.toString();
    }

    /**
     * 是否符合命名规范
     * 字母数字下划线组成,字母下划线开头
     *
     * @param text
     * @return
     */
    private boolean ConformToNaminGspecification(String text) {
        String regex = "^[A-Za-z_][A-Za-z_0-9]*$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        return matcher.matches();
    }

    //TODO：不校验字段的数据类型转换
    private String validateElementDataType(GspBusinessEntity model) {
        return "";
        //  StringBuilder sb = new StringBuilder();
        //  List<IGspCommonObject> aObjectList = model.GetAllObjectList();
        //  foreach (IGspCommonObject aObject in aObjectList)
        //  {
        //   string tableName = aObject.RefObjectName;
        //   DatabaseObjectTable dtab = ServiceManager.GetService<IDatabaseObjectService>()
        //    .GetDatabaseObject(tableName, DatabaseObjectType.Table) as DatabaseObjectTable;

        //List<IGspCommonElement> elementList = aObject.GetAllElementList(false);
        //   foreach (IGspCommonElement ele in elementList)
        //   {
        //    GspElementDataType type = ele.MDataType;
        //    string baseType = string.Empty;

        //    if (dtab != null && dtab.Columns.Count > 0)
        //    {
        //	    foreach (DatabaseObjectColumn column in dtab.Columns)
        //	    {
        //		    if (ele.ColumnID == column.ID)
        //		    {
        //			    if (!this.CompareDataType(ele, column))
        //			    {
        //				    sb.append(String.format(FIELD + "的 [数据类型] 与数据对象中数据类型不能做匹配转换! ", model.Name, aObject.Name, ele.Name);
        //				    sb.AppendLine();
        //			    }
        //			    break;
        //		    }
        //	    }
        //    }
        //   }
        //  }
        //  return sb.ToString();
    }

    /**
     * [字段] 校验业务字段相关属性
     *
     * @param model
     * @return
     */

    private String validateElementUdt(GspBusinessEntity model) {
        StringBuilder sb = new StringBuilder();
        java.util.ArrayList<IGspCommonObject> aObjectList = model.getAllObjectList();
        for (IGspCommonObject aObject : aObjectList) {
            java.util.ArrayList<IGspCommonElement> allElementList = aObject.getAllElementList(false);
            for (IGspCommonElement element : allElementList) {
                GspBizEntityElement bizElement = (GspBizEntityElement) element;
                if (bizElement.getIsUdt() && CheckInfoUtil.checkNull(bizElement.getUdtID())) {
                    sb.append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0054", model.getName(), aObject.getName(), element.getName()));
                    sb.append("\n");
                } else if (bizElement.getIsUdt()) {
                    String result = ValidateElementUdt(model, aObject, bizElement);
                    if (!result.isEmpty()) {
                        sb.append(result).append("\r\n");
                    }
                }
            }
        }
        return sb.toString();
    }

    private String ValidateElementUdt(GspBusinessEntity model, IGspCommonObject aObject, GspBizEntityElement bizElement) {
        if (bizElement.getObjectType() != GspElementObjectType.Association) {
            return "";
        }


        GspMetadata udtMetadata = getMetadataService().getRefMetadata(bizElement.getUdtID());
        if (udtMetadata == null) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0024, bizElement.getLabelID(), bizElement.getUdtID());
        }
        SimpleDataTypeDef udt = (SimpleDataTypeDef) ((udtMetadata.getContent() instanceof SimpleDataTypeDef) ? udtMetadata.getContent() : null);
        if (udt == null) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0024, bizElement.getLabelID(), bizElement.getUdtID());
        }
        if (!udt.getCode().equals(bizElement.getLabelID())) {
            return "";
        }


        for (IGspCommonField refElement : bizElement.getChildAssociations().get(0).getRefElementCollection()) {
            if (!refElement.getIsFromAssoUdt()) {
                return MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0055", model.getName(), aObject.getName(), bizElement.getName());
            }
        }

        return "";
    }

    ///#endregion


    ///#region Operation

    /**
     * [操作]联动计算
     *
     * @param model
     * @return
     */
    private String validateDeterminations(GspBusinessEntity model) {
        StringBuilder sb = new StringBuilder();
        ArrayList<IGspCommonObject> aObjectList = model.getAllObjectList();
        for (IGspCommonObject aObject : aObjectList) {
            CommonDtmCollection collection = ((GspBizEntityObject) aObject).getBizCommonDtms();
            ArrayList<CommonOperation> actions = new ArrayList<>(collection);

            String errorMessage = MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0008", model.getName(), aObject.getName());
            //①必填项
            sb.append(String.format(validateCommonOperationBasicInfo(errorMessage, actions)));
            if (sb.length() > 0) {
                return sb.toString();
            }
            //②编号重复
            sb.append(String.format(validateCommonOperationCodeRepeat(errorMessage, actions)));

            //③校验数据状态
//            sb.append(String.format(validateDeterminationSelfProperty(errorMessage, actions)));

            sb.append(String.format(validateDeterminationRequestElements(aObject, errorMessage, actions)));
        }
        return sb.toString();
    }

    /**
     * [操作]校验规则
     *
     * @param model
     * @return
     */
    private String validateValidations(GspBusinessEntity model) {
        StringBuilder sb = new StringBuilder();
        java.util.ArrayList<IGspCommonObject> aObjectList = model.getAllObjectList();
        for (IGspCommonObject aObject : aObjectList) {
            CommonValCollection collection = ((GspBizEntityObject) aObject).getBizCommonVals();
            ArrayList<CommonOperation> actions = new ArrayList<>(collection);
            String errorMessage = MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0009", model.getName(), aObject.getName());
            //①必填项
            sb.append(String.format(validateCommonOperationBasicInfo(errorMessage, actions)));
            if (sb.length() > 0) {
                return sb.toString();
            }
            //②编号重复
            sb.append(String.format(validateCommonOperationCodeRepeat(errorMessage, actions)));
            //③触发时机至少选一个
            sb.append(String.format(validateValidationRequestElements(aObject, errorMessage, actions)));
        }
        return sb.toString();
    }

    /**
     * 联动计算至少选择一个数据状态
     *
     * @param errorMessage
     * @param operations
     * @return
     */
    private String validateDeterminationSelfProperty(String errorMessage, java.util.ArrayList<BizOperation> operations) {
        StringBuilder sb = new StringBuilder();
        if (operations != null && !operations.isEmpty()) {
            for (BizOperation operation : operations) {
                if (operation instanceof Determination) {
                    Determination dtm = (Determination) operation;
                    if (dtm.getTriggerTimePointType().equals(EnumSet.of(BETriggerTimePointType.AfterModify)) || dtm.getTriggerTimePointType().equals(EnumSet.of(BETriggerTimePointType.BeforeCheck))) {
                        if (dtm.getRequestNodeTriggerType().equals(EnumSet.of(RequestNodeTriggerType.None))) {
                            String message = String.format(errorMessage + dtm.getName() + "的 [数据状态] (创建、更新、删除)至少选择一种。");
                            sb.append(message);
                            sb.append("\n");
                        }
                    }
                } else {
                    throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0025, errorMessage, operation.getName(), "Validation");
                }
            }
        }
        return sb.toString();
    }

    /**
     * 校验触发字段是否存在
     *
     * @param errorMessage
     * @param operations
     * @return
     */
    private String validateDeterminationRequestElements(IGspCommonObject gspCommonObject, String errorMessage, java.util.ArrayList<CommonOperation> operations) {
        StringBuilder sb = new StringBuilder();
        if (operations != null && !operations.isEmpty()) {
            for (CommonOperation operation : operations) {
                if (!(operation instanceof BizCommonDetermination)) {
                    throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0025, errorMessage, operation.getName(), "Determination");
                }
                BizCommonDetermination dtm = (BizCommonDetermination) operation;
                DtmElementCollection dtmElementCollection = dtm.getRequestElements();
                if (dtmElementCollection != null && !dtmElementCollection.isEmpty()) {
                    for (String element : dtmElementCollection) {
                        if (gspCommonObject.findElement(element) == null) {
                            String message = errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0032", dtm.getName(), element);
                            sb.append(message);
                            sb.append("\n");
                        }
                    }
                }
                HashMap<String, DtmElementCollection> childDtmElements = dtm.getRequestChildElements();
                if (childDtmElements == null || childDtmElements.isEmpty())
                    continue;
                for (Map.Entry<String, DtmElementCollection> entry : childDtmElements.entrySet()) {
                    String key = entry.getKey();
                    DtmElementCollection value = entry.getValue();
                    IObjectCollection childObjects = gspCommonObject.getContainChildObjects();
                    if (childObjects == null || childObjects.isEmpty()) {
                        String message = errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0033", dtm.getName(), key);
                        sb.append(message);
                        sb.append("\n");
                        continue;
                    }
                    IGspCommonObject reqChildObj = null;
                    for (IGspCommonObject childObj : childObjects) {
                        if (childObj.getID().equals(key)) {
                            reqChildObj = childObj;
                            break;
                        }
                    }
                    if (reqChildObj == null) {
                        String message = errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0034", dtm.getName(), key);
                        sb.append(message);
                        sb.append("\n");
                        continue;
                    }
                    if (value == null || value.getCount() == 0) {
                        String message = errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0035", dtm.getName(), key);
                        sb.append(message);
                        sb.append("\n");
                        continue;
                    }
                    for (String childElement : value) {
                        if (reqChildObj.findElement(childElement) == null) {
                            String message = errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0036", dtm.getName(), childElement);
                            sb.append(message);
                            sb.append("\n");
                        }
                    }
                }
            }
        }
        return sb.toString();
    }

    private String validateValidationRequestElements(IGspCommonObject gspCommonObject, String errorMessage, java.util.ArrayList<CommonOperation> operations) {
        StringBuilder sb = new StringBuilder();
        if (operations != null && !operations.isEmpty()) {
            for (CommonOperation operation : operations) {
                if (!(operation instanceof CommonValidation)) {
                    throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0025, errorMessage, operation.getName(), "Validation");
                }
                BizCommonValdation val = (BizCommonValdation) operation;
                ValElementCollection valElementCollection = val.getRequestElements();
                if (valElementCollection != null && !valElementCollection.isEmpty()) {
                    for (String element : valElementCollection) {
                        if (gspCommonObject.findElement(element) == null) {
                            String message = errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0032", val.getName(), element);
                            sb.append(message);
                            sb.append("\n");
                        }
                    }
                }
                HashMap<String, ValElementCollection> childValElements = val.getRequestChildElements();
                if (childValElements == null || childValElements.isEmpty())
                    continue;
                for (Map.Entry<String, ValElementCollection> entry : childValElements.entrySet()) {
                    String key = entry.getKey();
                    ValElementCollection value = entry.getValue();
                    IObjectCollection childObjects = gspCommonObject.getContainChildObjects();
                    if (childObjects == null || childObjects.isEmpty()) {
                        String message = errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0033", val.getName(), key);
                        sb.append(message);
                        sb.append("\n");
                        continue;
                    }
                    IGspCommonObject reqChildObj = null;
                    for (IGspCommonObject childObj : childObjects) {
                        if (childObj.getID().equals(key)) {
                            reqChildObj = childObj;
                            break;
                        }
                    }
                    if (reqChildObj == null) {
                        String message = errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0034", val.getName(), key);
                        sb.append(message);
                        sb.append("\n");
                        continue;
                    }
                    if (value == null || value.getCount() == 0) {
                        String message = errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0035", val.getName(), key);
                        sb.append(message);
                        sb.append("\n");
                        continue;
                    }
                    for (String childElement : value) {
                        if (reqChildObj.findElement(childElement) == null) {
                            String message = errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0036", val.getName(), childElement);
                            sb.append(message);
                            sb.append("\n");
                        }
                    }
                }
            }
        }
        return sb.toString();
    }

    /**
     * 校验规则至少选择一个执行时机/一个数据状态
     *
     * @param errorMessage
     * @param operations
     * @return
     */
    private String validateValidationSelfProperty(String errorMessage, java.util.ArrayList<BizOperation> operations) {
        StringBuilder sb = new StringBuilder();
        if (operations != null && !operations.isEmpty()) {
            for (BizOperation operation : operations) {
                if (operation instanceof Validation) {
                    Validation val = (Validation) operation;
                    if (val.getValidationTriggerPoints() == null || val.getValidationTriggerPoints().isEmpty()) {
                        String message = String.format(errorMessage + val.getName() + "的 [执行时机] 至少选择一种。");
                        sb.append(message);
                        sb.append("\n");
                    } else {
                        boolean hasTimePoint = false;

                        for (Map.Entry<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> point : val.getValidationTriggerPoints().entrySet()) {
                            if (!(point.getValue().equals(EnumSet.of(RequestNodeTriggerType.None)))) {
                                hasTimePoint = true;
                            }
                        }
                        if (!hasTimePoint) {
                            String message = String.format(errorMessage + val.getName() + "的 [执行时机] 至少选择一种。");
                            sb.append(message);
                            sb.append("\n");
                        }
                    }
                } else {
                    throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0025, errorMessage, operation.getName(), "Validation");
                }
            }
        }
        return sb.toString();
    }

    /**
     * [操作]业务实体动作
     *
     * @param model
     * @return
     */
    private String validateBeActions(GspBusinessEntity model) {
        StringBuilder sb = new StringBuilder();
        ArrayList<IGspCommonObject> aObjectList = model.getAllObjectList();
        for (IGspCommonObject aObject : aObjectList) {
            java.util.ArrayList<BizOperation> actions = ((GspBizEntityObject) aObject).getBizActions();

            String errorMessage = MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0006", model.getName(), aObject.getName());
            //①必填项
            sb.append(String.format(validateOperationBasicInfo(errorMessage, actions)));
            if (sb.length() > 0) {
                return sb.toString();
            }
            //②编号重复
            sb.append(String.format(validateOperationCodeRepeat(errorMessage, actions)));
            if (sb.length() > 0) {
                return sb.toString();
            }
            //③参数及返回值校验
            sb.append(String.format(validateActionParas(errorMessage, actions)));
        }
        return sb.toString();
    }

    /**
     * [操作]自定义动作
     *
     * @param model
     * @return
     */
    private String validateMgrActions(GspBusinessEntity model) {
        String errorMessage = MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0007", model.getName());
        StringBuilder sb = new StringBuilder();
        java.util.ArrayList<BizOperation> actions = model.getBizMgrActions();
        //①必填项
        sb.append(String.format(validateOperationBasicInfo(errorMessage, actions)));
        if (sb.length() > 0) {
            return sb.toString();
        }
        //②编号重复
        sb.append(String.format(validateOperationCodeRepeat(errorMessage, actions)));
        if (sb.length() > 0) {
            return sb.toString();
        }
        //③参数及返回值校验
        sb.append(String.format(validateActionParas(errorMessage, actions)));
        return sb.toString();
    }

    /**
     * [操作]编号重复
     *
     * @param errorMessage
     * @param operations
     * @return
     */
    private String validateOperationCodeRepeat(String errorMessage, java.util.ArrayList<BizOperation> operations) {
        StringBuilder sb = new StringBuilder();
        if (operations != null && !operations.isEmpty()) {
            for (int i = 0; i < operations.size() - 1; i++) {
                BizOperation firstOperation = operations.get(i);
                for (int j = i + 1; j < operations.size(); j++) {
                    BizOperation secondOperation = operations.get(j);
                    if (firstOperation.getCode().equals(secondOperation.getCode())) {
                        String message = String.format(errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0026", firstOperation.getName(), secondOperation.getName(), firstOperation.getCode()));
                        sb.append("\n");
                        return message;
                    }
                }
            }
        }
        return sb.toString();
    }

    /**
     * [操作]编号名称为空
     *
     * @param errorMessage
     * @param oprs
     * @return
     */
    private String validateOperationBasicInfo(String errorMessage, java.util.ArrayList<BizOperation> oprs) {
        if (oprs == null || oprs.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();

        for (BizOperation opr : oprs) {
            if (CheckInfoUtil.checkNull(opr.getID())) {
                opr.setID(UUID.randomUUID().toString());
            }
            if (CheckInfoUtil.checkNull(opr.getCode())) {
                sb.append(String.format(errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0029", opr.getName())));
                sb.append("\n");
            } else {
                // 操作Code限制非法字符
                if (!ConformToNaminGspecification(opr.getCode())) {
                    sb.append(String.format(errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0030", opr.getName())));
                    sb.append("\n");
                }
            }
            if (CheckInfoUtil.checkNull(opr.getName())) {
                sb.append(String.format(errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0031", opr.getCode())));
                sb.append("\n");
            }
        }
        return sb.toString();
    }


    /**
     * [操作]编号重复
     *
     * @param errorMessage
     * @param operations
     * @return
     */
    private String validateCommonOperationCodeRepeat(String errorMessage, java.util.ArrayList<CommonOperation> operations) {
        StringBuilder sb = new StringBuilder();
        if (operations != null && !operations.isEmpty()) {
            for (int i = 0; i < operations.size() - 1; i++) {

                CommonOperation firstOperation = operations.get(i);
                for (int j = i + 1; j < operations.size(); j++) {

                    CommonOperation secondOperation = operations.get(j);
                    if (firstOperation.getCode().equals(secondOperation.getCode())) {
                        String message = String.format(errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0026", firstOperation.getName(), secondOperation.getName(), firstOperation.getCode()));
                        sb.append("\n");
                        return message;
                    }
                }
            }
        }
        return sb.toString();
    }

    /**
     * [操作]编号名称为空
     *
     * @param errorMessage
     * @param oprs
     * @return
     */
    private String validateCommonOperationBasicInfo(String errorMessage, java.util.ArrayList<CommonOperation> oprs) {
        if (oprs == null || oprs.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();

        for (CommonOperation opr : oprs) {
            if (CheckInfoUtil.checkNull(opr.getID())) {
                opr.setID(UUID.randomUUID().toString());
            }
            if (CheckInfoUtil.checkNull(opr.getCode())) {
                sb.append(String.format(errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0029", opr.getName())));
                sb.append("\n");
            } else {
                // 操作Code限制非法字符
                if (!ConformToNaminGspecification(opr.getCode())) {
                    sb.append(String.format(errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0030", opr.getName())));
                    sb.append("\n");
                }
            }
            if (CheckInfoUtil.checkNull(opr.getName())) {
                sb.append(String.format(errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0031", opr.getCode())));
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    /**
     * [操作]-action-参数
     *
     * @param opr
     * @param errorMessage
     * @return
     */
    private String validateActionReturnValue(BizActionBase opr, String errorMessage) {
        StringBuilder sb = new StringBuilder();
        BizReturnValue returnValue = opr.getReturnValue();
        if (returnValue != null && returnValue.getParameterType() == BizParameterType.Custom) {
            if (BeManagerService.CreatJavaModule(metaPath)) {
                if (opr.getIsGenerateComponent() && CheckInfoUtil.checkNull(returnValue.getClassName())) {
                    sb.append(String.format(errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0045", opr.getName())));
                    sb.append("\n");
                }
            }
        }
        return sb.toString();
    }


    /**
     * 校验动作参数及返回值
     *
     * @param errorMessage
     * @param actions
     * @return
     */
    private String validateActionParas(String errorMessage, java.util.ArrayList<BizOperation> actions) {
        StringBuilder sb = new StringBuilder();
        if (actions != null && !actions.isEmpty()) {

            for (BizOperation action : actions) {
                sb.append(String.format(validateActionParas((BizActionBase) action, errorMessage)));
                sb.append(String.format(validateActionComponent((BizActionBase) action, errorMessage)));
                sb.append(String.format(validateActionReturnValue((BizActionBase) action, errorMessage)));
            }
        }
        return sb.toString();
    }

    private String validateActionParas(BizActionBase opr, String message) {
        StringBuilder sb = new StringBuilder();
        //参数编号名称不可为空

        for (Object para : opr.getParameters()) {
            if (CheckInfoUtil.checkNull(((IBizParameter) para).getParamCode())) {
                sb.append(message).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0037", opr.getName(), ((IBizParameter) para).getParamName()));
                sb.append("\n");
            } else {
                // 参数编号Code限制非法字符
                if (!ConformToNaminGspecification(((IBizParameter) para).getParamCode())) {
                    sb.append(message).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0038", opr.getName(), ((IBizParameter) para).getParamName()));
                    sb.append("\n");
                }
            }
            if (CheckInfoUtil.checkNull(((IBizParameter) para).getParamName())) {
                sb.append(message).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0039", opr.getCode(), ((IBizParameter) para).getParamCode()));
                sb.append("\n");
            }
            if (((IBizParameter) para).getParameterType() == BizParameterType.Custom) {
                if (BeManagerService.CreatJavaModule(metaPath)) {
                    if (opr.getIsGenerateComponent() && CheckInfoUtil.checkNull(((IBizParameter) para).getClassName())) {
                        sb.append(message).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0040", opr.getName(), ((IBizParameter) para).getParamName()));
                        sb.append("\n");
                    }
                }
            }
        }
        if (sb.length() > 0) {
            return sb.toString();
        }

        //参数编号不可重复
        if (opr.getParameters() != null && opr.getParameters().getCount() > 0) {
            for (int i = 0; i < opr.getParameters().getCount() - 1; i++) {
                IBizParameter firstPara = opr.getParameters().getItem(i);
                for (int j = i + 1; j < opr.getParameters().getCount(); j++) {
                    IBizParameter secondPara = opr.getParameters().getItem(j);
                    if (firstPara.getParamCode().equals(secondPara.getParamCode())) {
                        sb.append(message).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0041", opr.getName(), firstPara.getParamName(), secondPara.getParamName(), firstPara.getParamCode()));
                        sb.append("\n");
                    }
                }
            }
        }
        return sb.toString();
    }


    private String validateActionComponent(BizActionBase opr, String message) {
        //内置实体动作和自定义动作不用校验
        if (opr instanceof IInternalMgrAction || opr instanceof IInternalBEAction)
            return "";
        StringBuilder sb = new StringBuilder();
        //参数编号名称不可为空
        ICompParameterCollection compParameterCollection = null;
        if (!opr.getIsGenerateComponent() && StringUtils.isEmpty(opr.getComponentId())) {
            sb.append(message).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0042", opr.getName()));
            sb.append("\n");
            return sb.toString();
        }
        //新增的时候，构件ID是空的
        if (StringUtils.isEmpty(opr.getComponentId())) {
            return sb.toString();
        }
        RefCommonService metadataService = SpringBeanUtils.getBean(RefCommonService.class);
        GspMetadata metadata = metadataService.getRefMetadata(opr.getComponentId());
        if (metadata == null) {
            sb.append(message).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0043", opr.getName()));
            sb.append("\n");
            return sb.toString();
        }
        if (metadata.getContent() instanceof GspComponent) {
            if (!opr.getIsGenerateComponent()) {
                GspComponent component = (GspComponent) metadata.getContent();
                compParameterCollection = component.getMethod().getCompParameterCollection();
                int oprParSize = opr.getParameters() == null ? 0 : opr.getParameters().getCount();
                int compParSize = compParameterCollection == null ? 0 : compParameterCollection.getCount();
                if (oprParSize != compParSize) {
                    sb.append(message).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0044", opr.getName(), oprParSize, compParSize));
                    sb.append("\n");
                    return sb.toString();
                }
            }
        }
        if (!opr.getIsGenerateComponent()) {
            for (int i = 0; i < opr.getParameters().getCount(); i++) {
                IBizParameter bizParameter = opr.getParameters().getItem(i);
                ICompParameter compParameter = compParameterCollection.getItem(i);
                //引用构件的才校验
                sb.append(validBizCompParameter(bizParameter, compParameter, opr, message));
            }
        }

        if (sb.length() > 0) {
            return sb.toString();
        }

        //参数编号不可重复
        if (opr.getParameters() != null && opr.getParameters().getCount() > 0) {
            for (int i = 0; i < opr.getParameters().getCount() - 1; i++) {
                IBizParameter firstPara = opr.getParameters().getItem(i);
                for (int j = i + 1; j < opr.getParameters().getCount(); j++) {
                    IBizParameter secondPara = opr.getParameters().getItem(j);
                    if (firstPara.getParamCode().equals(secondPara.getParamCode())) {
                        sb.append(message).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0041", opr.getName(), firstPara.getParamName(), secondPara.getParamName(), firstPara.getParamCode()));
                        sb.append("\n");
                    }
                }
            }
        }
        return sb.toString();
    }

    private String validBizCompParameter(IBizParameter bizParameter, ICompParameter compParameter, BizActionBase bizActionBase, String message) {
        StringBuilder sb = new StringBuilder();
        if (!bizParameter.getParamCode().equalsIgnoreCase(compParameter.getParamCode())) {
            sb.append(message).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0106", bizActionBase.getName(), bizParameter.getParamCode(), compParameter.getParamCode()));
        }
        return sb.toString();
    }

    /**
     * [操作]业务实体动作
     *
     * @param model * @return
     */
    private String validateTccSettingCollection(GspBusinessEntity model) {
        StringBuilder sb = new StringBuilder();
        ArrayList<IGspCommonObject> objectList = model.getAllObjectList();
        for (IGspCommonObject object : objectList) {
            String errorMessage = MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0002", model.getName(), object.getName());
            TccSettingCollection settings = ((GspBizEntityObject) object).getTccSettings();
            // ①TCC配置字段校验
            for (TccSettingElement ele : settings) {
                sb.append(validateTccElement(errorMessage, ele));
            }
            // ②TCC字段Code重复性校验
            String repeatMessage = validateTccSettingCollectionCodeRepeat(errorMessage, ((GspBizEntityObject) object).getTccSettings());
            if (!StringUtils.isEmpty(repeatMessage)) {
                sb.append(validateTccSettingCollectionCodeRepeat(errorMessage, ((GspBizEntityObject) object).getTccSettings()));
            }
        }
        return sb.toString();
    }

    private String validateTccElement(String errorMessage, TccSettingElement element) {
        StringBuilder sb = new StringBuilder();
        if (CheckInfoUtil.checkNull(element.getCode())) {
            sb.append(errorMessage).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0021", element.getName()));
            sb.append("\n");
        } else {
            // 字段Code限制非法字符
            if (!ConformToNaminGspecification(element.getCode())) {
                sb.append(errorMessage).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0022", element.getName()));
                sb.append("\n");
            }
            // 字段Code限制数据库保留字
            else if (isDatabaseReservedWords(element.getCode())) {
                sb.append(errorMessage).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0023", element.getName(), element.getCode()));
                sb.append("\n");
            } else if (isDatabaseObjectReservedWords(element.getCode())) {
                sb.append(errorMessage).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0024", element.getName(), element.getCode()));
                sb.append("\n");
            }
        }
        if (CheckInfoUtil.checkNull(element.getName())) {
            sb.append(errorMessage).append(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0025", element.getCode()));
            sb.append("\n");
        }

        String validateTccActionResult = validateTccAction(errorMessage, element);
        if (!CheckInfoUtil.checkNull(validateTccActionResult)) {
            sb.append(validateTccActionResult);
        }
        return sb.toString();
    }

    private String validateTccAction(String errorMessage, TccSettingElement element) {
        ArrayList<BizOperation> actions = new ArrayList<>();
        actions.add(element.getTccAction());
        return validateOperationBasicInfo(errorMessage, actions);
    }

    private String validateTccSettingCollectionCodeRepeat(String errorMessage, TccSettingCollection collection) {
        if (collection != null && !collection.isEmpty()) {
            for (int i = 0; i < collection.size() - 1; i++) {
                TccSettingElement firstEle = collection.get(i);
                for (int j = i + 1; j < collection.size(); j++) {
                    TccSettingElement secondEle = collection.get(j);
                    if (firstEle.getCode().equals(secondEle.getCode())) {
                        return errorMessage + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0026", firstEle.getName(), secondEle.getName(), firstEle.getCode());
                    }
                }
            }
        }
        return null;
    }
    ///#endregion


    ///#region Util

    private boolean isDatabaseReservedWords(String word) {
        return DatabaseReservedWords.isReservedWord(word);
    }

    /*
    是否Farris前端关键字
     */
    private boolean isFarrisReservedWords(String word) {
        if (word == null || word.isEmpty())
            return false;
        return farrisKeyWords.contains(word.toLowerCase());
    }

    private ReserveResult getReservedResult(String words, String errorMessage, String modelName, String objName, String propName, IGspCommonField element) {
        ReserveResult reserveResult = new ReserveResult();
        String Message = MessageI18nUtils.getMessage(errorMessage, modelName, element.getName(), objName);
        if (isDatabaseReservedWords(words)) {
            reserveResult.isKeyWords = true;
            reserveResult.message = Message + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0059", propName, element.getCode());
            return reserveResult;
        }
        if (isFarrisReservedWords(words)) {
            reserveResult.isKeyWords = true;
            reserveResult.message = Message + MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0060", propName, element.getCode());
            return reserveResult;
        }
        return reserveResult;
    }

    private boolean isDatabaseObjectReservedWords(String word) {
        //todo:[启用时间戳]方案成熟前，先隐藏
        //return DatabaseObjectReservedWords.IsReservedWord(word);
        return false;
    }

    private static final java.util.ArrayList<String> fwkReserveds = new java.util.ArrayList<String>(java.util.Arrays.asList(new String[]{"DynamicPropSet"}));

    private static boolean isFrameworkReservedWords(String word) {
        return fwkReserveds.contains(word);
    }

    class ReserveResult {
        private boolean isKeyWords = false;
        private String message = "";
    }
    ///#endregion
}
