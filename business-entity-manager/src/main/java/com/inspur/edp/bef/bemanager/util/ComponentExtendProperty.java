package com.inspur.edp.bef.bemanager.util;

public class ComponentExtendProperty {
    private CefConfigBeanGenCode cefConfigBeanGenCode;

    public ComponentExtendProperty() {
        this.cefConfigBeanGenCode = new CefConfigBeanGenCode();
    }

    public CefConfigBeanGenCode getCefConfigBeanGenCode() {
        return cefConfigBeanGenCode;
    }

    public void setCefConfigBeanGenCode(
            CefConfigBeanGenCode cefConfigBeanGenCode) {
        this.cefConfigBeanGenCode = cefConfigBeanGenCode;
    }
}
