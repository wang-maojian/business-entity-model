package com.inspur.edp.bef.bemanager.service.config;

import com.inspur.edp.bef.bemanager.service.BeMetadataElementService;
import com.inspur.edp.lcm.metadata.spi.MetadataElementService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeMetadataElementServiceConfig {
    @Bean(name = "com.inspur.edp.bef.bemanager.service.config.BeMetadataElementService")
    public MetadataElementService getBeMetadataElementService() {
        return new BeMetadataElementService();
    }
}
