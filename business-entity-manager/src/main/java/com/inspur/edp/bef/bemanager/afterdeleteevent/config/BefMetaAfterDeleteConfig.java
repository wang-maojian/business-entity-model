package com.inspur.edp.bef.bemanager.afterdeleteevent.config;

import com.inspur.edp.bef.bemanager.afterdeleteevent.BefMetaDataAfterDeleteEvent;
import com.inspur.edp.metadata.rtcustomization.spi.MetadataRtSpi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BefMetaAfterDeleteConfig {
    @Bean("com.inspur.edp.bef.bemanager.afterdeleteevent.BefMetaDataAfterDeleteEvent")
    public MetadataRtSpi getMetadataRtSpiService() {
        return new BefMetaDataAfterDeleteEvent();
    }
}
