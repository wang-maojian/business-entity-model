package com.inspur.edp.bef.bemanager.validate.operation;

import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.operation.BizActionBase;
import com.inspur.edp.bef.bizentity.operation.componentinterface.IBizParameter;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;
import org.codehaus.plexus.util.StringUtils;

import static com.inspur.edp.cef.designtime.api.validate.common.CheckUtil.isLegality;


public abstract class BizActionBaseChecker extends BizOperationChecker {
    //BE参数校验
    private void checkParameters(BizActionBase para) {
        for (Object par : para.getParameters()) {
            par = par instanceof IBizParameter ? (IBizParameter) par : null;
            if (par == null)
                return;
            if (StringUtils.isEmpty(((IBizParameter) par).getParamCode())) {
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0048, para.getCode(), ((IBizParameter) par).getParamName());
            }
            if (isLegality(((IBizParameter) par).getParamCode()))
                continue;
            opException(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0110", para.getCode(), ((IBizParameter) par).getParamCode()));
        }
    }

    protected final void checkBizActionBase(BizActionBase actionBase) {
        checkBizOperation(actionBase);
        checkParameters(actionBase);
        checkBizActionExtended(actionBase);
    }

    protected void checkBizActionExtended(BizActionBase actionBase) {
    }
}
