package com.inspur.edp.bef.bemanager.expression.entity;

public class NavigationProperty {

    private String name;
    private String navigationId;
    private NavigationType type;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNavigationId() {
        return navigationId;
    }

    public void setNavigationId(String navigationId) {
        this.navigationId = navigationId;
    }

    public NavigationType getType() {
        return type;
    }

    public void setType(NavigationType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public NavigationProperty clone() {
        NavigationProperty newProp = new NavigationProperty();
        newProp.setNavigationId(getNavigationId());
        newProp.setName(getName());
        newProp.setType(getType());
        newProp.setDescription(getDescription());
        return newProp;
    }
}
