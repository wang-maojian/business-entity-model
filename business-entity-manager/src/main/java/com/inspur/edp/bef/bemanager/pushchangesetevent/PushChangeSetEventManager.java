package com.inspur.edp.bef.bemanager.pushchangesetevent;

import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.pushchangesetargs.PushChangeSetArgs;
import com.inspur.edp.bef.bizentity.pushchangesetlistener.IPushChangeSetListener;
import io.iec.edp.caf.commons.event.EventManager;
import io.iec.edp.caf.commons.event.IEventListener;

public class PushChangeSetEventManager extends EventManager {
    @Override
    public String getEventManagerName() {
        return "PushChangeSetEventManager";
    }

    /**
     * 当前的事件管理者是否处理输入的事件监听者对象
     *
     * @param listener 事件监听者
     * @return 若处理，则返回true,否则返回false
     */
    @Override
    public boolean isHandlerListener(IEventListener listener) {
        return (listener instanceof IPushChangeSetListener);
    }

    /**
     * 注册事件
     *
     * @param listener 监听者
     */
    @Override
    public void addListener(IEventListener listener) {
        if (!(listener instanceof IPushChangeSetListener)) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0056, "IPushChangeSetListener");
        }
        IPushChangeSetListener pushChangeSetListener = (IPushChangeSetListener) listener;
        this.addEventHandler(PushChangeSetType.bePushChangeSet, pushChangeSetListener, "bePushChangeSet");

    }

    /***
     * 注销事件
     * @param listener  监听者
     */
    @Override
    public void removeListener(IEventListener listener) {
        if (!(listener instanceof IPushChangeSetListener)) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0056, "IPushChangeSetListener");
        }
        IPushChangeSetListener pushChangeSetListener = (IPushChangeSetListener) listener;
        this.removeEventHandler(PushChangeSetType.bePushChangeSet, pushChangeSetListener, "bePushChangeSet");
    }

    // fire
    public final void firePushChangeSet(PushChangeSetArgs args) {
        this.fire(PushChangeSetType.bePushChangeSet, args);
    }
}
