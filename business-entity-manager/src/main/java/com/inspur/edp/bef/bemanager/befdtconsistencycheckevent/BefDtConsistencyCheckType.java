package com.inspur.edp.bef.bemanager.befdtconsistencycheckevent;

public enum BefDtConsistencyCheckType {
    removingEntity,
    removingField,
    deletingAction;
}
