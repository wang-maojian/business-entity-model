package com.inspur.edp.bef.bemanager.validate.operation;

import com.inspur.edp.bef.bizentity.operation.BizMgrAction;

public class BizMgrActionChecker extends BizActionBaseChecker {
    private static BizMgrActionChecker mgr;

    public final static BizMgrActionChecker getInstance() {
        if (mgr == null) {
            mgr = new BizMgrActionChecker();
        }
        return mgr;
    }

    public final void checkMgrAciton(BizMgrAction action) {
        checkBizActionBase(action);
    }
}
