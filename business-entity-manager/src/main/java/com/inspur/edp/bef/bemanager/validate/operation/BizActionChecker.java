package com.inspur.edp.bef.bemanager.validate.operation;

import com.inspur.edp.bef.bizentity.operation.BizActionBase;

public class BizActionChecker extends BizActionBaseChecker {
    private static BizActionChecker bizAction;

    public final static BizActionChecker getInstance() {
        if (bizAction == null) {
            bizAction = new BizActionChecker();
        }
        return bizAction;
    }

    public final void checkBizAction(BizActionBase para) {
        checkBizActionBase(para);
    }
}
