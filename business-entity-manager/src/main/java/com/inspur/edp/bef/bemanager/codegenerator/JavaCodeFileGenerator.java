/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.bemanager.codegenerator;


import com.inspur.edp.bef.bemanager.codegenerator.actions.JavaBaseCommonCompCodeGen;
import com.inspur.edp.bef.bemanager.generatecomponent.ComponentGenUtil;
import com.inspur.edp.bef.bemanager.util.CheckInfoUtil;
import com.inspur.edp.bef.bemanager.util.ComponentExtendProperty;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.common.OperationConvertUtils;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.Determination;
import com.inspur.edp.bef.bizentity.operation.TccAction;
import com.inspur.edp.bef.bizentity.operation.Validation;
import com.inspur.edp.bef.bizentity.operation.internalmgraction.TccSettingElement;
import com.inspur.edp.cef.designtime.api.collection.CommonDtmCollection;
import com.inspur.edp.cef.designtime.api.collection.CommonValCollection;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;
import com.inspur.edp.cef.variable.dtgenerator.vardtmGenerate.javageneratecmpcode.JavaCommonDeterminationGenerator;
import com.inspur.edp.cef.variable.dtgenerator.vardtmGenerate.javageneratecmpcode.JavaIBaseCompCodeGen;
import com.inspur.edp.das.commonmodel.util.MetadataProjectUtil;
import com.inspur.edp.jittojava.context.GenerateService;
import com.inspur.edp.lcm.fs.api.IFsService;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.FileService;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.commons.utils.StringUtils;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * BE触发代码生成的统一入口类，在此编排元数据中不同类型的构件代码生成
 */
public class JavaCodeFileGenerator {
    private static final String javaCodeFileExtension = ".java";
    private String relativePath;
    private GspBusinessEntity be;

    /**
     * 对应元数据工程的namespace（dotnet中对应程序集名称）
     */
    private String compAssemblyName;

    /**
     * 包路径前缀，默认值为com
     */
    private String packagePrefix;

    /**
     * 对应构件工程下的Java路径
     * 举例：D:\projects\Scm\OrderMrg\SaleOrder\bo-saleorder-back\java\code\comp\src\main\java\com\inspur\gs\scm\ordermrg\saleorder\saleorder\back
     */
    private String compProjectJavaPath;
    private IFsService iFsService;
    private FileService fsService;
    private ArrayList<ComponentExtendProperty> extendProperties;
    /**
     * 创建文件路径
     *
     * @param metadata
     */
    public JavaCodeFileGenerator(GspMetadata metadata) {
        this(metadata.getRelativePath());
        this.be = (GspBusinessEntity) metadata.getContent();
    }

    public JavaCodeFileGenerator(String path, GspBusinessEntity be) {
        this(path);
        this.be = be;
    }

    private JavaCodeFileGenerator(String path) {
        this.relativePath = path;
        this.iFsService = SpringBeanUtils.getBean(IFsService.class);
        this.fsService = SpringBeanUtils.getBean(FileService.class);
        this.extendProperties = new ArrayList<>();
        //根据元数据相对路径获取工程的namespace（dotnet中对应程序集名称）
        this.compAssemblyName = ComponentGenUtil.GetComponentNamespace(relativePath);
        //获取包路径前缀
        this.packagePrefix = MetadataProjectUtil.getPackagePrefix(relativePath);
    }

    /**
     * 根据方法列表，统一生成构件代码
     * @param actionList
     */
    public void generate(ArrayList<String> actionList) {
        //自定义实体动作（主节点）
        JavaGenerateBizMgrActions(actionList);
        //实体动作（各节点均可）
        JavaGenerateBizActions(actionList);
        //联动计算
        JavaGenerateDeterminations(actionList);
        //校验规则
        JavaGenerateValidations(actionList);
        //变量
        JavaGenerateVariableDtms(actionList);
        //tcc动作
        javaGenerateTccActions(actionList);
        //config
        javaGenerateBeanConfigFile();
    }

    private static final String mgrActionDirName = "BizActions";

    private void JavaGenerateBizMgrActions(ArrayList<String> actionList) {
        String path = JavaPrepareBizActionsDir(mgrActionDirName);

        for (BizOperation action : be.getCustomMgrActions()) {
            if(actionList.contains(action.getCode())){
                JavaCodeGenerate(action, path, null);
            }
        }
    }

    private static final String JavaBizActionDirName = "EntityActions";

    private void JavaGenerateBizActions(ArrayList<String> actionList) {
        //var path = JavaPrepareBizActionsDir(JavaBizActionDirName);
        java.util.ArrayList<GspBizEntityObject> nodes = be.getAllNodes();

        for (GspBizEntityObject node : nodes) {
            for (BizOperation action : node.getCustomBEActions()) {
                if (actionList.contains(action.getCode())) {
                    String path = JavaPrepareDir(JavaBizActionDirName, node.getCode());
                    //todo be.getMainObject应该为node
                    JavaCodeGenerate(action, path, be.getMainObject());
                }
            }
        }
    }

    private static final String JavaTccActionDirName = "TccActions";

    private void javaGenerateTccActions(ArrayList<String> actionList) {
        java.util.ArrayList<GspBizEntityObject> nodes = be.getAllNodes();
        for (GspBizEntityObject node : nodes) {
            for (TccSettingElement ele : node.getTccSettings()) {
                TccAction action = ele.getTccAction();
                if (actionList.contains(action.getCode())) {
                    String path = JavaPrepareDir(JavaTccActionDirName, node.getCode());
                    JavaCodeGenerate(action, path, node);
                }
            }
        }
    }

    private static final String JavaDtmDirName = "Determinations";

    private void JavaGenerateDeterminations(ArrayList<String> actionList) {
        for (GspBizEntityObject node : be.getAllNodes()) {
            //todo node.getDeterminations应该可以去掉
            for (BizOperation action : node.getDeterminations()) {
                if (actionList.contains(action.getCode())) {
                    String path = JavaPrepareDir(JavaDtmDirName, node.getCode());
                    JavaCodeGenerate(action, path, node);
                }
            }
            generateCommonDtm(actionList, node, node.getDtmAfterCreate(), CefNames.DtmAfterCreate);
            generateCommonDtm(actionList, node, node.getAllDtmAfterModify(), CefNames.DtmAfterModify);
            generateCommonDtm(actionList, node, node.getAllDtmBeforeSave(), CefNames.DtmBeforeSave);
            generateCommonDtm(actionList, node, node.getDtmAfterSave(), CefNames.DtmAfterSave);
            generateCommonDtm(actionList, node, node.getDtmBeforeQuery(), BizEntityJsonConst.B4QueryDtm);
            generateCommonDtm(actionList, node, node.getDtmAfterQuery(), BizEntityJsonConst.AftQueryDtm);
            generateCommonDtm(actionList, node, node.getDtmCancel(), BizEntityJsonConst.DtmCancel);
            generateCommonDtm(actionList, node, node.getDtmBeforeRetrieve(), BizEntityJsonConst.B4RetrieveDtm);
            generateCommonDtm(actionList, node, node.getDtmAfterLoading(), BizEntityJsonConst.AftLoadingDtm);
        }
    }

    private void generateCommonDtm(List<String> actionList, GspBizEntityObject obj,
                                   CommonDtmCollection dtmCollection, String commonDtmType) {
        if (dtmCollection == null) {
            return;
        }
        for (CommonDetermination dtm : dtmCollection) {
            if (!actionList.contains(dtm.getCode()) || dtm.getIsRef() || !dtm.getIsGenerateComponent()
                    || obj.getDeterminations().findById(dtm.getID()) != null) {
                continue;
            }
            String path = JavaPrepareDir(JavaDtmDirName, obj.getCode());
            Determination action = OperationConvertUtils.convertToDtm(dtm, commonDtmType, obj);
            JavaCodeGenerate(action, path, obj);
        }
    }

    private static final String JavaValidationDirName = "Validations";

    private void JavaGenerateValidations(ArrayList<String> actionList) {

        for (GspBizEntityObject node : be.getAllNodes()) {

            for (BizOperation action : node.getValidations()) {
                if (actionList.contains(action.getCode())) {
                    String path = JavaPrepareDir(JavaValidationDirName, node.getCode());
                    JavaCodeGenerate(action, path, node);
                }
            }

            generateCommonVal(actionList, node, node.getValAfterSave(), BizEntityJsonConst.ValidationAfterSave);
            generateCommonVal(actionList, node, node.getallValBeforeSave(), CefNames.ValBeforeSave);
            generateCommonVal(actionList, node, node.getAllValAfterModify(), CefNames.ValAfterModify);
        }
    }

    private void generateCommonVal(List<String> actionList, GspBizEntityObject obj,
                                   CommonValCollection collection, String commonType) {
        if (collection == null) {
            return;
        }
        for (CommonValidation val : collection) {
            if (!actionList.contains(val.getCode()) || val.getIsRef() || !val.getIsGenerateComponent()
                    || obj.getValidations().findById(val.getID()) != null) {
                continue;
            }
            String path = JavaPrepareDir(JavaValidationDirName, obj.getCode());
            Validation action = OperationConvertUtils.convertToVal(val, commonType, obj);
            JavaCodeGenerate(action, path, obj);
        }
    }

    private static final String determinationDirName = "VarDeterminations";

    private void JavaGenerateVariableDtms(ArrayList<String> actionList) {
        if (be.getVariables() == null) {
            return;
        }
        String path = JavaPrepareDir(determinationDirName, be.getCode());

        JavaGenerateVariableDtms(be.getVariables().getDtmAfterCreate(), path, actionList);
        JavaGenerateVariableDtms(be.getVariables().getDtmAfterModify(), path, actionList);
        JavaGenerateVariableDtms(be.getVariables().getDtmBeforeSave(), path, actionList);
    }


    /**
     * 获取构件工程目录
     * 例如 D:\projects\Scm\OrderMrg\SaleOrder\bo-saleorder-back\java\code\comp\src\main\java\com\inspur\gs\scm\ordermrg\saleorder\saleorder\back
     * @return
     */
    private String getJavaCompModulePath() {
        //如果工程下Java路径为空，则获取一下
        if(StringUtils.isEmpty(this.compProjectJavaPath)) {
            String generatingAssembly = be.getDotnetGeneratingAssembly().replace(".", "\\");
            //获取ava工程路径下java文件夹路径
            String compModulePath = MetadataProjectUtil.getJavaCompProjectPath(relativePath);

            //TODO toLowerCase 需要修改
            this.compProjectJavaPath = CheckInfoUtil.getCombinePath(compModulePath, this.packagePrefix, generatingAssembly.toLowerCase());
        }
//		String compositePath = String.Concat(compModulePath, "\\com\\", generatingAssembly.toLowerCase());
        return this.compProjectJavaPath;

    }

    private String JavaPrepareDir(String actionDirName, String codeDir) {
        String path = CheckInfoUtil.getCombinePath(getJavaCompModulePath(), codeDir.toLowerCase(), actionDirName.toLowerCase());
//		String path = Path.Combine(getJavaCompModulePath(), codeDir.toLowerCase(), actionDirName.toLowerCase());
        if (!fsService.isDirectoryExist(path)) {
            fsService.createDirectory(path);
        }
        return path;
    }

    private String JavaPrepareBizActionsDir(String actionDirName) {
        String path = CheckInfoUtil.getCombinePath(getJavaCompModulePath(), actionDirName.toLowerCase());

//		var path = Path.Combine(getJavaCompModulePath(), actionDirName.toLowerCase());
        if (!fsService.isDirectoryExist(path)) {
            fsService.createDirectory(path);
        }
        return path;
    }

    /**
     * 根据动作、路径及节点生成java文件
     * @param op
     * @param dirPath
     * @param belongNode
     */
    private void JavaCodeGenerate(BizOperation op, String dirPath, GspBizEntityObject belongNode) {
        if (!op.getIsRef() && op.getIsGenerateComponent()) {
            //是否子节点
            boolean isChild = belongNode!=null && !belongNode.getIsRootNode();
            String nodeCode = belongNode==null? null : belongNode.getCode();
            JavaGenerateSingleFile(op, dirPath, isChild, nodeCode);
        }
    }

    //ORIGINAL LINE: private void JavaGenerateSingleFile(BizOperation op, string dirPath,GspBizEntityObject belongNode = null)
    private void JavaGenerateSingleFile(BizOperation op, String dirPath, boolean isChild, String nodeCode) {
        JavaBaseCommonCompCodeGen codeGen = !isChild ?
                JavaCmpCodeGeneratorFactory.JavaGetGenerator(be, op, compAssemblyName, relativePath) :
                JavaCmpCodeGeneratorFactory.JavaGetChildNodeGenerator(be, nodeCode, op, compAssemblyName, relativePath);

        String filePathExe = CheckInfoUtil.getCombinePath(dirPath, codeGen.GetCompName() + javaCodeFileExtension);
        if ((fsService.isFileExist(filePathExe))) {
            JavaReplace(filePathExe, codeGen);
            return;
        }
        String codeContent = codeGen.Generate();
        iFsService.createFile(filePathExe, codeContent, StandardCharsets.UTF_8);
        ComponentExtendProperty extendProp = codeGen.getExtendProperty();
        if (extendProp != null && extendProp.getCefConfigBeanGenCode() != null) {
            this.extendProperties.add(extendProp);
        }
    }

    public void createVarDtmJavaCodeFile(CommonDetermination dtm) {
        String path = JavaPrepareDir(determinationDirName, be.getCode());
        JavaGenerateVariableDtm(dtm, path);
    }

    public void createJavaCodeFile(BizOperation op, boolean isChild, String nodeCode) {
        String path;
        switch (op.getOpType()) {
            case BizMgrAction:
                path = JavaPrepareBizActionsDir(mgrActionDirName);
                break;
            case BizAction:
                path = JavaPrepareDir(JavaBizActionDirName, nodeCode);
                break;
            case TccAction:
                path = JavaPrepareDir(JavaTccActionDirName, nodeCode);
                break;
            case Validation:
                path = JavaPrepareDir(JavaValidationDirName, nodeCode);
                break;
            case Determination:
                path = JavaPrepareDir(JavaDtmDirName, nodeCode);
                break;
            default:
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0009, op.getOpType().toString());
        }
        JavaGenerateSingleFile(op, path, isChild, nodeCode);
        javaGenerateBeanConfigFile();
    }

    private void JavaGenerateVariableDtmFile(JavaIBaseCompCodeGen codeGen, String dirPath) {
        String originalFilePath = CheckInfoUtil.getCombinePath(relativePath, ComponentGenUtil.ComponentDir, codeGen.getCompName() + javaCodeFileExtension);
        String filePathExe = CheckInfoUtil.getCombinePath(dirPath, codeGen.getCompName() + javaCodeFileExtension);
        String filePathCommon = CheckInfoUtil.getCombinePath(dirPath, codeGen.getCompName() + "Ctor" + javaCodeFileExtension);

        if (fsService.isFileExist(originalFilePath)) {
            return;
        }
        //此处判断用来兼容文件夹下已经有对应的生成文件情形
        if ((fsService.isFileExist(filePathExe)) && (!fsService.isFileExist(filePathCommon))) {
            return;
        }
        if (!fsService.isFileExist(filePathExe)) {
            iFsService.createFile(filePathExe, codeGen.generateExecute());
        }
        // be动作执行GenerateCommon(),commonDtm不执行
        if (codeGen.getIsCommonGenerate()) {
            iFsService.createFile(filePathExe, codeGen.generateCommon());
        }
    }

    /**
     * 兼容原来情况，修改引用的程序集
     *
     * @param filePath 待更新文件路径
     * @param codeGen  更新文件使用的代码生成器
     */
    private void JavaReplace(String filePath, JavaBaseCommonCompCodeGen codeGen) {
        try {
            String content = fsService.fileRead(filePath);
            String codeContent = codeGen.update(content);
            if (codeContent != null) {
                fsService.fileUpdate(filePath, codeContent, false);
            }
        } catch (IOException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0084, e, filePath);
        }
    }

    private void JavaGenerateVariableDtms(CommonDtmCollection dtms, String dirPath, ArrayList<String> actionList) {

        for (CommonDetermination dtm : dtms) {
            if (dtm.getIsRef() || !dtm.getIsGenerateComponent() || !actionList.contains(dtm.getCode())) {
                continue;
            }
            JavaGenerateVariableDtm(dtm, dirPath);
        }
    }

    private void JavaGenerateVariableDtm(CommonDetermination dtm, String dirPath) {
        JavaCommonDeterminationGenerator gen = new JavaCommonDeterminationGenerator(be, dtm, compAssemblyName, relativePath);
        JavaGenerateVariableDtmFile(gen, dirPath);
    }

    private void javaGenerateBeanConfigFile() {
        if (this.extendProperties.isEmpty()) {
            return;
        };
        String filePath = CheckInfoUtil.getCombinePath(getJavaCompModulePath(), be.getCode()+ JavaCompCodeNames.CefConfigFileSuffix + javaCodeFileExtension);
        JavaBeanConfigGenerator beanConfigGen = new JavaBeanConfigGenerator(be, this.extendProperties, this.packagePrefix, compAssemblyName);
        if (fsService.isFileExist(filePath)) {
            String content = null;
            try {
                content = fsService.fileRead(filePath);
            } catch (IOException e) {
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0084, e, filePath);
            }
            fsService.fileUpdate(filePath, beanConfigGen.update(content), false);
        } else {
            iFsService.createFile(filePath, beanConfigGen.generate());
            addSpringFactoriesProperty();
        }
    }

    private void addSpringFactoriesProperty() {
        GenerateService service = SpringBeanUtils.getBean(GenerateService.class);
        MetadataProjectService servicePath = SpringBeanUtils.getBean(MetadataProjectService.class);
        String compModulePath = servicePath.getJavaCompProjectPath(relativePath);
        String beanConfigPath = JavaCodeGeneratorUtil.ConvertImportPackage(this.packagePrefix, be.getComponentAssemblyName())+"."+be.getCode()+JavaCompCodeNames.CefConfigFileSuffix;
        service.addProperties(compModulePath, JavaCompCodeNames.SPRING_FACTORIES_PROPERTYKEY,beanConfigPath);
    }
}