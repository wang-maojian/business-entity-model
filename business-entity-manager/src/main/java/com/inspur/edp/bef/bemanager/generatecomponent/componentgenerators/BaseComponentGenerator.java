/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators;

import com.inspur.edp.bef.bemanager.codegenerator.JavaCodeGeneratorUtil;
import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bemanager.generatecomponent.ComponentClassNameGenerator;
import com.inspur.edp.bef.bemanager.generatecomponent.GspMetadataExchangeUtil;
import com.inspur.edp.bef.bemanager.util.CheckInfoUtil;
import com.inspur.edp.bef.bizentity.beenum.BEOperationType;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.component.ICompMethod;
import com.inspur.edp.bef.component.ICompParameter;
import com.inspur.edp.bef.component.ICompParameterCollection;
import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.bef.component.base.ReturnValue;
import com.inspur.edp.das.commonmodel.util.MetadataProjectUtil;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataReference;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Iterator;
import java.util.Locale;
import java.util.Objects;

/**
 * 构件元数据生成器基类
 */
public abstract class BaseComponentGenerator {
    /**
     * 业务实体编号
     */
    protected String bizEntityCode;
    /**
     * 业务实体程序集名称
     */
    protected String assemblyName;
    protected String nameSpace;
    /**
     * 是否当前生成的构件为子节点的构件
     */
    protected boolean isChildObj;
    /**
     * 节点编号
     */
    protected String objCode;
    protected String bizObjectID;
    /**
     * 原始构件
     */
    protected GspComponent originalComponent;
    /**
     * 包路径前缀，默认值为com
     */
    protected String packagePrefix;

    /**
     * 生成构件元数据
     *
     * @param operation     业务操作
     * @param path          生成指定路径
     * @param bizEntityCode 业务实体编号
     * @param assemblyName  业务实体程序集名称
     * @param isChildObj    是否子节点
     * @param objCode       节点对象编号
     * @param bizObjectID   业务对象ID
     * @param metadata      元数据
     */
    public final String generateComponent(BizOperation operation, String path, String bizEntityCode,
                                          String assemblyName, boolean isChildObj, String objCode, String bizObjectID,
                                          GspMetadata metadata, String defaultNameSpace) {
        this.bizEntityCode = bizEntityCode;
        this.assemblyName = assemblyName;
        this.nameSpace = defaultNameSpace;
        this.isChildObj = isChildObj;
        this.objCode = objCode;
        this.bizObjectID = bizObjectID;
        //获取包路径前缀
        this.packagePrefix = MetadataProjectUtil.getPackagePrefix(path);

        if (CheckInfoUtil.checkNull(operation.getComponentId())) {
            GspMetadata compMetadata = createComponent(operation, path);
            operation.setComponentId(((GspComponent) compMetadata.getContent()).getComponentID());
            operation.setComponentName(compMetadata.getHeader().getCode());
            addReference(metadata, compMetadata);
            return operation.getCode();
        } else {
            // 修改构件时,若构件方法内容发生变化,则记录动作编号,后续执行更新操作.
            if (modifyComponent(operation, path)) {
                return operation.getCode();
            }
            return null;
        }
    }

    public GspMetadata createComponent(BizOperation operation, String path, String bizEntityCode,
                                       String assemblyName, boolean isChildObj, String objCode, String bizObjectID,
                                       String defaultNameSpace) {
        Objects.requireNonNull(operation, "operation");
        Objects.requireNonNull(path, "path");
        Objects.requireNonNull(bizEntityCode, "bizEntityCode");
        Objects.requireNonNull(assemblyName, "assemblyName");
        Objects.requireNonNull(objCode, "objCode");
        Objects.requireNonNull(bizObjectID, "bizObjectID");
        Objects.requireNonNull(defaultNameSpace, "defaultNameSpace");

        this.bizEntityCode = bizEntityCode;
        this.assemblyName = assemblyName;
        this.nameSpace = defaultNameSpace;
        this.isChildObj = isChildObj;
        this.objCode = objCode;
        this.bizObjectID = bizObjectID;
        //获取包路径前缀
        this.packagePrefix = MetadataProjectUtil.getPackagePrefix(path);
        return createComponent(operation, path);
    }

    /**
     * 新建构件
     */
    private GspMetadata createComponent(BizOperation operation, String path) {
        //1、构建实体
        GspComponent component = buildComponent();
        //2、赋值
        evaluateComponentInfo(component, operation, null);
        //3、生成构件
        GspMetadata compMetadata = establishComponent(component, path);

        return compMetadata;
    }

    private void addReference(GspMetadata refSrc, GspMetadata refTarget) {
        MetadataReference compReference = new MetadataReference();
        compReference.setMetadata(refSrc.getHeader());
        compReference.setDependentMetadata(refTarget.getHeader());
        refSrc.getRefs().add(compReference);
    }

    /**
     * 获得Java模版Package名称
     *
     * @param
     * @return
     */
    protected final String JavaModuleImportPackage(String nameSpace) {
        return JavaCodeGeneratorUtil.ConvertImportPackage(this.packagePrefix, nameSpace);
    }

    /**
     * 将.NET的AssemblyName 转换为 Java的包名
     * @param packagePrefix
     * @param dotnetClassName
     * @return
     */
    protected final String handleJavaPackageName(String packagePrefix, String dotnetClassName) {
        if (dotnetClassName == null || dotnetClassName.isEmpty()) {
            return dotnetClassName;
        }
        String result = "";
        String[] list = dotnetClassName.split("\\.");
        for (int i = 0; i < list.length; i++) {
            String lowerCase = list[i].toLowerCase(Locale.ROOT);
            if (i == 0 && !lowerCase.equals(packagePrefix)) {
                result = result.concat(packagePrefix);
            }
            result = result.concat("." + lowerCase);
        }
        if (result.startsWith("com.inspur.gsp.common.commonudt")) {
            result = result.replace("com.inspur.gsp.common.commonudt", "com.inspur.edp.common.commonudt");
        }
        return result;
    }

    /**
     * 获得Java模版类的名称
     *
     * @param classNamestr
     * @param packageNameStr
     * @return
     */
    protected final String JavaModuleClassName(String classNamestr, String packageNameStr) {
        String connections = "";
        if (!CheckInfoUtil.checkNull(classNamestr)) {
            connections = classNamestr.substring(classNamestr.lastIndexOf('.'));
            connections = String.format("%1$s%2$s", packageNameStr, connections);
        }

        return (connections);
    }

    /**
     * 构造构件实体类
     *
     * @return 构件实体类
     * <see cref="GspComponent"/>
     */
    protected abstract GspComponent buildComponent();

    /**
     * 为构件赋值
     *
     * @param component 构件实体信息
     * @param operation 动作信息
     */
    protected void evaluateComponentInfo(GspComponent component, BizOperation operation, GspComponent originalComponent){
        this.originalComponent = originalComponent;
        // 基本信息（没有参数信息）
        evaluateComponentBasicInfo(component, operation);
    }

    protected void evaluateComponentBasicInfo(GspComponent component, BizOperation bizAction) {
        if (!CheckInfoUtil.checkNull(bizAction.getComponentId())) {
            component.setComponentID(bizAction.getComponentId());
        }
        component.setComponentCode(bizAction.getCode());
        component.setComponentName(bizAction.getName());
        component.setComponentDescription(bizAction.getDescription());

        ICompMethod method = component.getMethod();
        method.setDotnetAssembly(this.assemblyName);
        method.setAssembly(this.handleJavaPackageName(this.packagePrefix,this.assemblyName));

        String packageName = JavaModuleImportPackage(this.nameSpace);

        packageName = String.format("%1$s%2$s%3$s", packageName, ".",  getActionNameSpaceSuffix(bizAction));

        if (this.originalComponent != null) {
            method.setDotnetClassName(this.originalComponent.getMethod().getDotnetClassName());
            method.setClassName(JavaModuleClassName(this.originalComponent.getMethod().getDotnetClassName(), packageName));
        } else {
            method.setDotnetClassName(ComponentClassNameGenerator.generateBEMgrComponentClassName(this.nameSpace, this.objCode + bizAction.getCode()));
            method.setClassName(JavaModuleClassName(ComponentClassNameGenerator.generateBEMgrComponentClassName(this.nameSpace, this.objCode + bizAction.getCode()), packageName));
        }
        // component.GetMethod().JavaPackageName = packageName;
    }

    /**
     * 获取动作对应命名空间的后缀
     * @return
     */
    protected abstract String getActionNameSpaceSuffix(BizOperation bizAction);

    /**
     * 生成构件实体对应的构件元数据
     *
     * @param component 构件实体
     * @param path      生成构件元数据指定路径
     * @return 生成的构件元数据名称
     * <see cref="string"/>
     */
    private GspMetadata establishComponent(GspComponent component, String path) {
        return GspMetadataExchangeUtil.getInstance().establishGspMetdadata(component, path, this.bizEntityCode, this.bizObjectID, this.nameSpace);
    }

    /**
     * 修改构件
     *
     * @param operation
     * @param path
     */
    private boolean modifyComponent(BizOperation operation, String path) {
        //1、构建实体
        GspComponent component = buildComponent();

        //3、获取要修改的构件元数据完整路径（包括后缀）
        String fullPath;

        ///#region ----------- 兼容旧版本命名规则（此种情况适用于构件名称未修改）--------------
        MetadataService metadataService = SpringBeanUtils.getBean(MetadataService.class);


        // 得到具体类型的构件扩展名
        String cmpExtendName = evaluateCompTypeExtendNameByOpType(operation.getOpType());
        // 带针对不同类型构件的扩展名的文件全名
        String metadataFileNameWithExtendName = operation.getComponentName() + JavaCompCodeNames.ComponentExtensionName;

        if (metadataService.isMetadataExist(path, metadataFileNameWithExtendName)) {
            //TODO
            String[] arrName = operation.getComponentName().split("[_]", -1);
            String strName = "";
            if ("cmp".equalsIgnoreCase(arrName[0])) {
                strName = operation.getComponentName().substring(4);
            }
            String oldFileNameWithExtendName = metadataFileNameWithExtendName;
            String newFileNameWithExtendName = metadataFileNameWithExtendName = strName + cmpExtendName;

            metadataService.renameMetadata(oldFileNameWithExtendName, newFileNameWithExtendName, path);
            operation.setComponentName(strName);
            fullPath = path + "\\" + newFileNameWithExtendName;
        } else {
            metadataFileNameWithExtendName = operation.getComponentName() + cmpExtendName;
            if (!metadataService.isMetadataExist(path, metadataFileNameWithExtendName)) {
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0001, metadataFileNameWithExtendName);
            }
            fullPath = path + "\\" + operation.getComponentName() + cmpExtendName;
        }

        ///#endregion ---------------------------兼容旧版本---------------------------

        GspComponent originalComponent = getOriginalComponent(path, metadataFileNameWithExtendName);
        //2、赋值
        evaluateComponentInfo(component, operation, originalComponent);
        //component.GetMethod().JavaPackageName = ;
        //component.GetMethod().JavaClassName = ;
        //4、修改更新构件元数据
        GspMetadataExchangeUtil.getInstance().UpdateGspMetadata(component, fullPath, this.bizEntityCode, this.bizObjectID, this.nameSpace);

        // 对比新旧构件参数\出参类型是否发生变化
        return isComponentMethodChanged(originalComponent, component);
    }

    /**
     * 对比新旧构件参数\出参类型是否发生变化
     *
     * @param originalComponent 原始构件
     * @param component         新构件
     * @return 是否发生变化, 若发生变化返回TRUE
     */
    private boolean isComponentMethodChanged(GspComponent originalComponent, GspComponent component) {
        // 异常场景处理:新构件方法为空 兼容为未发生变化
        if (component == null || component.getCompMethod() == null) {
            return false;
        }
        // 异常场景处理:原始构件方法为空 兼容为发生变化
        if (originalComponent == null || originalComponent.getCompMethod() == null) {
            return true;
        }
        // 对比构件返回值/参数是否发生变更
        return isComponentMethodReturnTypeChanged(originalComponent.getCompMethod().getReturnValue(), component.getCompMethod().getReturnValue())
                || isComponentMethodParameterCollectionChanged(originalComponent.getCompMethod().getCompParameterCollection(),
                component.getCompMethod().getCompParameterCollection());
    }

    /**
     * 对比两构件方法返回类型是否发生变化
     *
     * @param originalReturnValue 原始返回类型
     * @param returnValue         新返回类型
     * @return 是否发生变化, 发生变化返回True
     */
    private boolean isComponentMethodReturnTypeChanged(ReturnValue originalReturnValue, ReturnValue returnValue) {
        // 两对象相等或均为null则未发生变化
        if (originalReturnValue == returnValue) {
            return false;
        }

        // 其中一个对象为null则发生变化
        if (originalReturnValue == null || returnValue == null) {
            return true;
        }

        // 对比返回集合类型、返回类型等属性是否一致，若不一致则为发生变化
        return originalReturnValue.getClass() != returnValue.getClass()
                || originalReturnValue.getParameterCollectionType() != returnValue.getParameterCollectionType()
                || originalReturnValue.getParameterTypeEnum() != returnValue.getParameterTypeEnum()
                || !StringUtils.equals(originalReturnValue.getAssembly(), returnValue.getAssembly())
                || !StringUtils.equals(originalReturnValue.getDotnetClassName(), returnValue.getDotnetClassName());
    }

    /**
     * 对比两构件方法参数集合是否发生变化
     *
     * @param originalParameters 原始参数集合
     * @param parameters         现构件参数集合
     * @return 是否发生变化, 发生变化返回True
     */
    private boolean isComponentMethodParameterCollectionChanged(ICompParameterCollection<ICompParameter> originalParameters, ICompParameterCollection<ICompParameter> parameters) {
        // 两对象相等或均为null则未发生变化
        if (originalParameters == parameters) {
            return false;
        }
        // 两集合其中一个为null, 或参数数量不同则发生变化
        if (originalParameters == null || parameters == null || originalParameters.getCount() != parameters.getCount()) {
            return true;
        }

        Iterator<ICompParameter> originalIter = originalParameters.iterator();
        Iterator<ICompParameter> iterator = parameters.iterator();
        // 遍历对比各个参数
        while (originalIter.hasNext() && iterator.hasNext()) {
            ICompParameter originalParam = originalIter.next();
            ICompParameter parameter = iterator.next();
            // 两对象相等或均为null则未发生变化
            if (originalParam == parameter) {
                continue;
            }
            // 其中一个对象为null则认为发生变化
            if (originalParam == null || parameter == null) {
                return true;
            }
            // 对比参数的编号\参数类型\集合类型等属性,若存在不一致则认为发生变化
            if (!StringUtils.equals(originalParam.getParamCode(), parameter.getParamCode())
                    || originalParam.getParameterCollectionType() != parameter.getParameterCollectionType()
                    || originalParam.getParameterTypeEnum() != parameter.getParameterTypeEnum()) {
                return true;
            }
        }

        return false;
    }

    /**
     * 获取修改元数据之前的元数据信息
     *
     * @param path
     * @param
     * @param metadataFileNameWithSuffix
     * @return
     */
    private GspComponent getOriginalComponent(String path, String metadataFileNameWithSuffix) {
        MetadataService metadataService = SpringBeanUtils.getBean(MetadataService.class);
        GspMetadata metadata = metadataService.loadMetadata(metadataFileNameWithSuffix, path);
        GspComponent originalComponent = (GspComponent) ((metadata.getContent() instanceof GspComponent) ? metadata.getContent() : null);
        return originalComponent;
    }

    private String evaluateCompTypeExtendNameByOpType(BEOperationType opType) {
        switch (opType) {
            case BizAction:
                return JavaCompCodeNames.BECmpExtendName;
            case BizMgrAction:
                return JavaCompCodeNames.BEMgrCmpExtendName;
            case Determination:
                return JavaCompCodeNames.DtmCmpExtendName;
            case Validation:
                return JavaCompCodeNames.ValCmpExtendName;
            default:
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0007, opType.getClass().getSimpleName());
        }
    }
}
