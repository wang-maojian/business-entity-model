package com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators;


import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bemanager.generatecomponent.ComponentClassNameGenerator;
import com.inspur.edp.bef.bemanager.util.CheckInfoUtil;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.Validation;
import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.bef.component.base.VoidReturnType;
import com.inspur.edp.bef.component.detailcmpentity.validation.ValidationComponent;

public class ValidationComponentGenerator extends BaseComponentGenerator {
    public static ValidationComponentGenerator getInstance() {
        return new ValidationComponentGenerator();
    }

    private ValidationComponentGenerator() {
    }

    @Override
    protected GspComponent buildComponent() {
        return new ValidationComponent();
    }

    @Override
    protected void evaluateComponentInfo(GspComponent component, BizOperation validation, GspComponent originalComponent) {
        //1、基本信息
        super.evaluateComponentInfo(component, validation, originalComponent);
        //2、返回值信息
        ((ValidationComponent) component).getValidationMethod().setReturnValue(new VoidReturnType());
    }

    @Override
    protected String getActionNameSpaceSuffix(BizOperation bizAction){
        return String.format("%1$s%2$s%3$s", bizAction.getOwner().getCode().toLowerCase(), '.', JavaCompCodeNames.ValidationNameSpaceSuffix.toLowerCase());
    }
}