package com.inspur.edp.bef.bemanager.service;

public class BeMetadataElementConst {
    public static final String BE_ELEMENT = "BeElement";

    public static final String BE_ENTITY = "BeEntity";

    public static final String BE_ACTION = "BeMgrAction";
}
