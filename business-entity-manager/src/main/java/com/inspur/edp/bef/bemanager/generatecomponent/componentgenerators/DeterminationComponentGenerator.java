package com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators;


import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.bef.component.base.VoidReturnType;
import com.inspur.edp.bef.component.detailcmpentity.determination.DeterminationComponent;

public class DeterminationComponentGenerator extends BaseComponentGenerator {
    public static DeterminationComponentGenerator getInstance() {
        return new DeterminationComponentGenerator();
    }

    private DeterminationComponentGenerator() {
    }

    @Override
    protected GspComponent buildComponent() {
        return new DeterminationComponent();
    }


    ///#region 将Determination中信息赋值给DeterminationComponent
    @Override
    protected void evaluateComponentInfo(GspComponent component, BizOperation determination, GspComponent originalComponent) {
        //1、基本信息
        super.evaluateComponentInfo(component,determination,originalComponent);
        // 返回值信息
        ((DeterminationComponent) component).getDeterminationMethod().setReturnValue(new VoidReturnType());

    }

    @Override
    protected String getActionNameSpaceSuffix(BizOperation bizAction){
        return String.format("%1$s%2$s%3$s", bizAction.getOwner().getCode().toLowerCase(), '.', JavaCompCodeNames.DeterminationNameSpaceSuffix.toLowerCase());
    }
}