package com.inspur.edp.bef.bemanager.codegenerator.genutils;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.caf.generator.baseInfo.CompilationUnitInfo;
import com.inspur.edp.caf.generator.item.ClassGenerator;
import com.inspur.edp.caf.generator.item.ClassGeneratorContext;
import org.eclipse.jdt.core.dom.Modifier;

import java.util.ArrayList;

public class BeMetadataUtilsGenerator extends ClassGenerator {
    private final GspBusinessEntity businessEntity;

    public BeMetadataUtilsGenerator(CompilationUnitInfo projectInfo, GspBusinessEntity businessEntity, String compAssemblyName) {
        super(projectInfo);
        this.businessEntity = businessEntity;
    }

    @Override
    protected ClassGeneratorContext createClassInfoContext() {
        return new ClassGeneratorContext();
    }

    @Override
    protected ArrayList<Modifier.ModifierKeyword> getAccessModifier() {
        ArrayList<Modifier.ModifierKeyword> list = new ArrayList<>();
        list.add(Modifier.ModifierKeyword.STATIC_KEYWORD);
        list.add(Modifier.ModifierKeyword.PUBLIC_KEYWORD);
        return list;
    }

    @Override
    protected String getName() {
        return businessEntity.getCode() + "Utils";
    }

}
