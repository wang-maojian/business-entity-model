package com.inspur.edp.bef.bemanager.util;

import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;

import java.io.File;
import java.nio.file.Paths;


public class CheckInfoUtil {
    /**
     * 检查必填项是否为空
     *
     * @param propName
     * @param propValue
     */
    public static void checkNessaceryInfo(String propName, Object propValue) {
        if (checkNull(propValue)) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_COMMON_0003, propName);
        }
    }

    /**
     * 检查空对象，空字符串
     *
     * @param propValue
     * @return
     */
    public static boolean checkNull(Object propValue) {
        if (propValue == null) {
            return true;
        }
        if (propValue.getClass().isAssignableFrom(String.class)) {
            String stringValue = (String) propValue;
            return stringValue.isEmpty();
        }

        return false;
    }

    public static String getCombinePath(String path1, String path2) {
        String path = Paths.get(path1).resolve(path2).toString();
        return handlePath(path);
    }

    public static String getCombinePath(String path1, String path2, String path3) {
        String path = Paths.get(path1).resolve(path2).resolve(path3).toString();
        return handlePath(path);
    }

    public static String handlePath(String path) {
        return path.replace("\\", File.separator);
    }
}
