package com.inspur.edp.bef.bemanager.lowcode;

import com.inspur.edp.metadata.rtcustomization.spi.event.ExtMdSavedArgs;
import com.inspur.edp.metadata.rtcustomization.spi.event.IMetadataDeployEventListener;
import com.inspur.edp.metadata.rtcustomization.spi.event.MdPkgDeployedEventArgs;

public class MdPkgToolsDeployListener implements IMetadataDeployEventListener {


//    private void handleMdPkg(MetadataPackage metadataPackage) {
//        try {
////            if (metadataPackage.getHeader().getProcessMode() != ProcessMode.interpretation) {
////                //如果不是无需生成代码的元数据包，直接忽略
////                return;
////            }
//
//            List<GspMetadata> packageMetadataList = metadataPackage.getMetadataList();
//            if (packageMetadataList == null || packageMetadataList.isEmpty()) {
//                //如果元数据包下不存在元数据
//                return;
//            }
//            List<GspMetadata> metadatas = packageMetadataList.stream()
//                    .filter(item -> item.getHeader().getType().equals("GSPBusinessEntity"))
//                    .collect(Collectors.toList());
//            if (metadatas.isEmpty()) {
//                return;
//            }
//            ProcessMode processMode = Optional.ofNullable(metadataPackage.getHeader().getProcessMode()).orElse(ProcessMode.generation);
//            BSessionUtil.wrapFirstTenantBSession(tenant -> {
//                CustomizationService metadataService = SpringBeanUtils.getBean(CustomizationService.class);
//                IGspBeExtendInfoRpcService beInfoService = SpringBeanUtils.getBean(IGspBeExtendInfoRpcService.class);
//                //IGspBeExtendInfoService beInfoService = SpringBeanUtils.getBean(IGspBeExtendInfoService.class);
//                List<GspBeExtendInfo> infos = new ArrayList(metadatas.size());
//                for (GspMetadata metadata : metadatas) {
//                    GspBeExtendInfo info = handelMetadata(metadata,processMode);
//                    if(info != null)
//                        infos.add(info);
//                }
//                if (!infos.isEmpty()) {
//                    beInfoService.saveGspBeExtendInfos(infos);
//                }
//            });
//        } catch (Exception e) {
//            System.out.println("部署解析BE元数据出错：" + e.getMessage());
//
//        }
//    }

    @Override
    public void fireMdPkgDeployedEvent(MdPkgDeployedEventArgs mdPkgDeployedEventArgs) {
//        MetadataPackage metadataPackage = mdPkgDeployedEventArgs.getPcakage();
//        handleMdPkg(metadataPackage);
    }

    @Override
    public void fireExtMdSavedEvent(ExtMdSavedArgs extMdSavedArgs) {
//        System.out.println("fireExtMdSavedEvent 补丁工具监听节点进去啦！");
//
//        GspMetadata metadata = extMdSavedArgs.getExtMetadata();
//        IMetadataDeployService metadataService = SpringBeanUtils.getBean(IMetadataDeployService.class);
//        IGspBeExtendInfoService beInfoService = SpringBeanUtils.getBean(IGspBeExtendInfoService.class);
//        GspBeExtendInfo info = handelMetadata(metadataService,beInfoService,metadata);
//        if(info == null)
//            return;
//        List<GspBeExtendInfo> infos = new ArrayList(1);
//        infos.add(info);
//        beInfoService.saveGspBeExtendInfos(infos);
    }

//    private GspBeExtendInfo handelMetadata(GspMetadata metadata, ProcessMode processMode){
////        GspBusinessEntity be = (GspBusinessEntity) metadataService.getMetadata(metadata.getHeader().getId()).getContent();
//        CustomizationServerService serverService= SpringBeanUtils.getBean(CustomizationServerService.class);
//        GspBusinessEntity be = (GspBusinessEntity)serverService.getMetadata(metadata.getHeader().getId()).getContent();
//        if (be.getGeneratedConfigID() == null) {
//            return null;
//        }
//            //SpringBeanUtils.getBean(com.inspur.edp.bef.extendinfo.server.api.IGspBeExtendInfoRpcService.class)
//        //GspBeExtendInfo existing = beInfoService.getBeExtendInfo(be.getID());
//        //        GspBeExtendInfo info = new GspBeExtendInfo();
////        if (existing == null || !be.getGeneratedConfigID().equals(existing.getConfigId())) {
////            info.setId(be.getID());
////            info.setConfigId(be.getGeneratedConfigID());
////            info.setLastChangedOn(new Date());
////            return info;
////        }
//        return buildBeExtendInfo(processMode, be);
//    }
//
//    private GspBeExtendInfo buildBeExtendInfo(ProcessMode processMode, GspBusinessEntity be) {
//        GspBeExtendInfo info = new GspBeExtendInfo();
//        BeConfigCollectionInfo beConfigCollectionInfo = getBefConfigCollectionInfo(be);
//        beConfigCollectionInfo.setProjectType(processMode);
//        info.setId(be.getID());
//        info.setConfigId(be.getGeneratedConfigID());
//        info.setLastChangedOn(new Date());
//        info.setBeConfigCollectionInfo(beConfigCollectionInfo);
//        return info;
//    }
//
//    private BeConfigCollectionInfo getBefConfigCollectionInfo(GspBusinessEntity be) {
//        CefConfig cefConfig = new CefConfig();
//        cefConfig.setID(be.getGeneratedConfigID());
//        cefConfig.setBEID(be.getID());
//        String nameSpace = be.getCoreAssemblyInfo().getDefaultNamespace();
//        cefConfig.setDefaultNamespace(nameSpace.toLowerCase());
////      MgrConfig mgrConfig = new MgrConfig();
////      cefConfig.setMgrConfig(mgrConfig);
//        BeConfigCollectionInfo beConfigCollectionInfo = new BeConfigCollectionInfo();
//        beConfigCollectionInfo.setConfig(cefConfig);
//        return beConfigCollectionInfo;
//    }
}
