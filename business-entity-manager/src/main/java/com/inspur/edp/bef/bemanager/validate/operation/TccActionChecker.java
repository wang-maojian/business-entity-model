package com.inspur.edp.bef.bemanager.validate.operation;

import com.inspur.edp.bef.bizentity.operation.TccAction;

public class TccActionChecker extends BizOperationChecker {
    private static TccActionChecker tccActionChecker;

    public final static TccActionChecker getInstance() {
        if (tccActionChecker == null) {
            tccActionChecker = new TccActionChecker();
        }
        return tccActionChecker;
    }

    public final void checkTccAction(TccAction action) {
        checkBizOperation(action);
    }
}
