package com.inspur.edp.bef.bemanager.expression.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.bef.bemanager.expression.entity.NavigationType;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

public class NavigationTypeJsonSerializer extends JsonSerializer<NavigationType> {

    @Override
    public void serialize(NavigationType value, JsonGenerator jsonGenerator,
                          SerializerProvider serializers) {
        SerializerUtils.writePropertyValue_Integer(jsonGenerator, value.getValue());
    }
}
