package com.inspur.edp.bef.bemanager.codegenerator.actions;


import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.BizOperation;

public class JavaB4RetrieveDeterminationGenerator extends JavaDeterminationGenerator {
    public JavaB4RetrieveDeterminationGenerator(GspBusinessEntity be, BizOperation operation, String nameSpace, String path) {
        super(be, operation, nameSpace, path);
    }

    @Override
    protected String getBaseClassName() {
        return "AbstractB4RetrieveDetermination";
    }

    @Override
    protected void JavaGenerateConstructor(StringBuilder result) {
        result.append(GetIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ").append(GetCompName()).append("(IBeforeRetrieveDtmContext context)").append(" ").append("{").append(getNewline());
        result.append(GetIndentationStr()).append(GetIndentationStr()).append("super(context)").append(";").append(getNewline()); //添加基类构造函数
        result.append(GetIndentationStr()).append("}");
    }
}