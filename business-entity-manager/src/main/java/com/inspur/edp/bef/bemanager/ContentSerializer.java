package com.inspur.edp.bef.bemanager;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.bef.bemanager.util.BefFileUtil;
import com.inspur.edp.bef.bizentity.BeThreadLocal;
import com.inspur.edp.bef.bizentity.Context;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.api.service.FileService;
import com.inspur.edp.lcm.metadata.spi.MetadataContentSerializer;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;

@Slf4j
public class ContentSerializer implements MetadataContentSerializer {
    @Override
    public JsonNode Serialize(IMetadataContent iMetadataContent) {
        Context context = new Context();
        context.setfull(false);
        BeThreadLocal.set(context);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);

        try {
            String jsonResult = mapper.writeValueAsString(iMetadataContent);
            JsonNode jsonNode = mapper.readTree(jsonResult);
            return jsonNode;
        } catch (IOException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0004, e, "GspBusinessEntity");
        } finally {
            BeThreadLocal.unset();
        }
    }

    @Override
    public IMetadataContent DeSerialize(JsonNode jsonNode) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(handleJsonString(jsonNode.toString()), GspBusinessEntity.class);
        } catch (IOException | RuntimeException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, "GspBusinessEntity");
        }
    }

    private void writeLog(String text) {
        FileService fileService = BefFileUtil.getService(FileService.class);
//		String devPath = fileService.getDevRootPath();
//		String path = devPath + "\\jstack\\log\\";
        // 添加没有资源文件报空指针的问题
        if (this.getClass().getResource(File.separator + "BefProjectMavenDependency.xml") == null) {
            return;
        }
        String path =
                this.getClass().getResource(File.separator + "BefProjectMavenDependency.xml").toString().split(":file:" + File.separator)[1].split(
                        File.separator + "platform")[0] +
                        File.separator + "log" + File.separator;
        String fileName = "BefSerializerLog.txt";
        if (!fileService.isFileExist(path + fileName)) {
            try {
                fileService.createFile(path, fileName);
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
        fileService.fileUpdate(path + fileName, text);
    }

    private static String handleJsonString(String contentJson) {
        if (contentJson.startsWith("\"")) {
            contentJson = contentJson.replace("\\\"", "\"");
            while (contentJson.startsWith("\"")) {
                contentJson = contentJson.substring(1, contentJson.length() - 1);
            }
        }
        return contentJson;
    }
}
