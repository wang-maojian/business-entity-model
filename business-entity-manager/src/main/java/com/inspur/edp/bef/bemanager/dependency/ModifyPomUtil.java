package com.inspur.edp.bef.bemanager.dependency;

import com.inspur.edp.bef.bemanager.util.BefFileUtil;
import org.dom4j.Document;
import org.dom4j.Element;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ModifyPomUtil {
    private ModifyPomUtil() {

    }

    public static ModifyPomUtil getInstance() {
        return new ModifyPomUtil();
    }

    /**
     * 修改{projectPath}pom文件
     *
     * @param projectPath
     * @param dependencyArrayList
     */
    public void modifyPom(String projectPath, ArrayList<MavenDependency> dependencyArrayList) {
        if (dependencyArrayList == null || dependencyArrayList.isEmpty()) {
            return;
        }
        String pomPath = BefFileUtil.getPomFilePath(projectPath);
        Document doc = readDocument(pomPath);
        Element projectElement = doc.getRootElement();
        Element dependenciesElement = projectElement.element(MavenDependencyConst.dependencies);

        if (dependenciesElement == null) {
            dependenciesElement = projectElement.addElement(MavenDependencyConst.dependencies);
        }
        List<Element> existValues = dependenciesElement.elements();
        ArrayList<MavenDependency> existDependencies = readExistDependencies(existValues);
        for (MavenDependency newDependency : dependencyArrayList) {
            if (checkContainDependency(newDependency, existDependencies)) {
                continue;
            }
            addDependencyDom(newDependency, dependenciesElement);
        }
        saveDocument(doc, new File(pomPath));
    }

    /**
     * 读取默认的依赖配置
     *
     * @return
     */
    public ArrayList<MavenDependency> getDefaultDependency() {
        String defaultDependencyPath =
                this.getClass().getResource("/BefProjectMavenDependency.xml").toString();
        return readInternalDependency(defaultDependencyPath);
    }

    /**
     * 读取扩展的依赖配置
     *
     * @return
     */
    public ArrayList<MavenDependency> getExtendDependency() {
        String extendDependencyPath =
                this.getClass().getResource("/BefExtendMavenDependency.xml").toString();
        return readInternalDependency(extendDependencyPath);
    }

    /**
     * 读取Sgf扩展的依赖配置
     *
     * @return
     */
    public ArrayList<MavenDependency> getSgfExtendDependency() {
        String extendDependencyPath =
                this.getClass().getResource("/SgfExtendMavenDependency.xml").toString();
        return readInternalDependency(extendDependencyPath);
    }

    private ArrayList<MavenDependency> readInternalDependency(String defaultDependencyPath) {
        ArrayList<MavenDependency> result = new ArrayList<>();
        Document doc = readDocument(defaultDependencyPath);
        Element rootElement = doc.getRootElement();
        List<Element> list = rootElement.elements();
        for (Element element : list) {
            result.add(new MavenDependency(getTagValue(element, MavenDependencyConst.groupId), getTagValue(element, MavenDependencyConst.artifactId),
                    getTagValue(element, MavenDependencyConst.version), getTagValue(element, MavenDependencyConst.file)));
        }
        return result;
    }

    // region 私有方法
    private ArrayList<MavenDependency> readExistDependencies(List<Element> existValues) {
        ArrayList<MavenDependency> list = new ArrayList<>();
        if (existValues == null || existValues.isEmpty()) {
            return list;
        }
        for (Element existValue : existValues) {
            list.add(new MavenDependency(getTagValue(existValue, MavenDependencyConst.groupId), getTagValue(existValue, MavenDependencyConst.artifactId),
                    getTagValue(existValue, MavenDependencyConst.version)));
        }
        return list;
    }

    private boolean checkContainDependency(MavenDependency dependency, ArrayList<MavenDependency> dependencies) {
        for (MavenDependency item : dependencies) {
            if (item.equals(dependency)) {
                return true;
            }
        }
        return false;
    }

    private void addDependencyDom(MavenDependency dependency, Element parentElement) {
        Element element = parentElement.addElement(MavenDependencyConst.dependency);
        Element groupId = element.addElement(MavenDependencyConst.groupId);
        Element artifactId = element.addElement(MavenDependencyConst.artifactId);
        groupId.setText(dependency.getGroupId());
        artifactId.setText(dependency.getArtifactId());
        if (dependency.getVersion() != null && !dependency.getVersion().isEmpty()) {
            Element version = element.addElement(MavenDependencyConst.version);
            version.setText(dependency.getVersion());
        }
    }

    private String getTagValue(Element ele, String tagName) {
        return BefFileUtil.getTagValue(ele, tagName);
    }

    private Document readDocument(String filePath) {
        return BefFileUtil.readDocument(filePath);
    }

    private void saveDocument(Document document, File xmlFile) {
        BefFileUtil.saveDocument(document, xmlFile);
    }
    // endregion
}
