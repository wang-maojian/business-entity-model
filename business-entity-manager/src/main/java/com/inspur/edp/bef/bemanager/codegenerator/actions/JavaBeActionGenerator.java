/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.bemanager.codegenerator.actions;

import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;
import com.inspur.edp.das.commonmodel.util.generate.ComponentCodeUpdater;

public class JavaBeActionGenerator extends JavaBaseActionGenerator {
    private final boolean isRoot;

    ///#region 字段和属性
    @Override
    protected String getBaseClassName() {
        if (isRoot) {
            return "RootAbstractAction<" + ReturnTypeName + ">";
        }
        return "AbstractAction<" + ReturnTypeName + ">";
    }


    ///#endregion


    ///#region 构造函数
    public JavaBeActionGenerator(GspBusinessEntity be, BizOperation operation, String nameSpace, String path) {
        super(be, operation, nameSpace, path);
        isRoot = be.getMainObject().getCode().equals(operation.getOwner().getCode());
        //  this.isRoot = isRoot;
    }

    @Override
    protected String GetNameSpaceSuffix() {
        return BizOperation.getOwner().getCode() + "." + JavaCompCodeNames.ActionNameSpaceSuffix;
    }


    ///#endregion


    ///#region GenerateConstructor

    @Override
    protected void JavaGenerateConstructor(StringBuilder result) {
        result.append(GetIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ").append(GetCompName()).append("(");
        //result.append(GetDoubleIndentationStr()).append(GetIndentationStr()).append("IBENodeEntityContext beContext");
        result.append("IBENodeEntityContext beContext");

        if (JavaHasCustomConstructorParams()) {
            //result.appendLine(",");
            result.append(",");
            JavaGenerateConstructorParams(result);
        } else {
            result.append("");
        }

        //result.append(GetDoubleIndentationStr()).append(GetIndentationStr()).appendLine(") : base(beContext)");
        result.append(")");

        result.append(" {").append(getNewline());
        result.append(GetIndentationStr()).append(GetIndentationStr()).append("super(beContext);").append(getNewline()); //新加项
        JavaGenerateConstructorContent(result);
        result.append(GetIndentationStr()).append("}").append(getNewline()).append(getNewline());
    }

    @Override
    protected void JavaGenerateExtendUsing(StringBuilder result) {
        super.JavaGenerateExtendUsing(result);
        result.append(GetImportStr(JavaCompCodeNames.AbstractActionNameSpace)).append(GetImportStr(JavaCompCodeNames.IEntityDataNameSpace));
    }


    ///#endregion


    ///#region GenerateExtendMethod
    @Override
    protected void JavaGenerateExtendMethod(StringBuilder result) {
        if (this.isInterpretation) {
            JavaInterpretationBeActionExtendMethod(result);
        } else {
            JavaBeActionExtendMethod(result);
        }
    }

    ///#endregion

    protected void JavaBeActionExtendMethod(StringBuilder result) {
        result.append(GetIndentationStr()).append(JavaCompCodeNames.KeywordPrivate).append(" ").append(EntityClassName).append(" getData() ");
        result.append("{").append(getNewline());

        result.append(GetIndentationStr()).append(GetIndentationStr()).append("return ").append("(").append(EntityClassName).append(")").append("getBEContext().getCurrentData()").append(";").append(getNewline());

        result.append(GetIndentationStr()).append("}").append(getNewline());
    }

    protected void JavaInterpretationBeActionExtendMethod(StringBuilder result) {
        result.append(GetIndentationStr()).append(JavaCompCodeNames.KeywordPrivate).append(" ").append(JavaCompCodeNames.IEntityData).append(" getData() ");
        result.append("{").append(getNewline());

        result.append(GetIndentationStr()).append(GetIndentationStr()).append("return ").append("(").append(JavaCompCodeNames.IEntityData).append(")").append("getBEContext().getCurrentData()").append(";").append(getNewline());

        result.append(GetIndentationStr()).append("}");
    }

    @Override
    protected String GetInitializeCompName() {
        return String.format("%1$s%2$s%3$s", getChildCode(), BizOperation.getCode(), "Action");
    }

    @Override
    public String update(String originalContent) {
        // 组装idelog,输出同步检查目标元数据\动作信息.
        String prefix = MessageI18nUtils.getMessage("GSP_COMPONENT_BEACTION_PREFIX",
                beCode,
                beName,
                BizOperation.getCode(),
                BizOperation.getName());
        // 对比当前文件内容和新生成代码模板, 更新当前文件内容
        return ComponentCodeUpdater.codeUpdate(originalContent, Generate(), GetCompName(), prefix);
    }
}