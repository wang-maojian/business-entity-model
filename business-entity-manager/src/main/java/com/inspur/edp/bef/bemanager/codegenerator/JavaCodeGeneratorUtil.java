package com.inspur.edp.bef.bemanager.codegenerator;

public class JavaCodeGeneratorUtil {

    @Deprecated
    public static String ConvertImportPackage(String readPropertyValue_string) {
        String[] strArray = readPropertyValue_string.split("[.]", -1);
        String str = "com.";
        int i;

        for (i = 0; i < strArray.length - 1; i++) {
            str += strArray[i].toLowerCase() + ".";
        }
        str += strArray[i].toLowerCase();
        return str;
    }

    public static String ConvertImportPackage(String packagePrefix, String namespace) {
        String[] strArray = namespace.split("[.]", -1);
        //namespace前面添加包前缀
        StringBuilder str = new StringBuilder(packagePrefix + ".");
        int i;

        for (i = 0; i < strArray.length - 1; i++) {
            str.append(strArray[i].toLowerCase()).append(".");
        }
        str.append(strArray[i].toLowerCase());
        return str.toString();
    }

    public static String getNewline() {
        return "\r\n";
    }

    public static String GetIndentationStr() {
        return "\t";
    }

    public static String GetDoubleIndentationStr() {
        return "\t\t";
    }
}
