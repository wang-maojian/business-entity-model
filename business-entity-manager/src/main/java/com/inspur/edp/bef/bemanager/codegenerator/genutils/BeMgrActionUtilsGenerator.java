package com.inspur.edp.bef.bemanager.codegenerator.genutils;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.collection.BizMgrActionCollection;
import com.inspur.edp.cef.designtime.core.utilsgenerator.JavaAccessModifier;
import com.inspur.edp.cef.designtime.core.utilsgenerator.JavaClassInfo;

public final class BeMgrActionUtilsGenerator {
    private final GspBusinessEntity be;
    private final String packageName;
    private final JavaClassInfo classInfo = new JavaClassInfo();

    public BeMgrActionUtilsGenerator(GspBusinessEntity be, String packageName, String basePath) {
        this.be = be;
        classInfo.setFilePath(basePath);
        this.packageName = packageName;
    }

    private String getClassName() {
        return this.be.getCode() + "MgrActionUtils";
    }

    public final void generate() {
        classInfo.setPackageName(packageName);
        classInfo.setClassName(getClassName());
        classInfo.getAccessModifiers().add(JavaAccessModifier.Public);
        classInfo.getAccessModifiers().add(JavaAccessModifier.Final);

        generateMgrMethod(be.getCustomMgrActions());
        classInfo.write2File();
    }

    private void generateMgrMethod(BizMgrActionCollection mgrActions) {
        if (mgrActions == null || mgrActions.isEmpty())
            return;
        for (BizOperation mrAction : mgrActions) {
            new BeMgrActionMethodInvocationGenerator(this.be, (BizMgrAction) mrAction, classInfo).generate();
        }
    }
}
