package com.inspur.edp.bef.bemanager.util;

import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;

public class DataValidatorUtil {

    public static void CheckForNullReference(Object value, String propName) {
        if (value == null) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_COMMON_0003, propName);
        }
    }

    public static void CheckForEmptyString(Object value, String propName) {
        if (!(value instanceof String) || CheckInfoUtil.checkNull(propName)) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_COMMON_0003, propName);
        }
    }
}
