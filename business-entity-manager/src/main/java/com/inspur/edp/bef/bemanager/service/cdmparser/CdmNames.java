package com.inspur.edp.bef.bemanager.service.cdmparser;

public class CdmNames {

    ///#region 数据类型

    //Binary
    public static final String CdmDataType_Image = "PIC";
    public static final String CdmDataType_LongBinary = "LBIN";
    public static final String CdmDataType_Ole = "OLE"; // 文档图片
    public static final String CdmDataType_Multibyte = "MBT";
    public static final String CdmDataType_VarMultibyte = "VMBT";
    public static final String CdmDataType_Binary = "BIN";
    public static final String CdmDataType_Bitmap = "BMP";
    public static final String CdmDataType_VariableBinary = "VBIN";

    //Boolean
    public static final String CdmDataType_Boolean = "BL";

    //Date
    public static final String CdmDataType_Date = "D";

    //DateTime
    public static final String CdmDataType_DateTime = "DT";

    //包含精度-decimal
    public static final String CdmDataType_Float = "F";
    public static final String CdmDataType_LongFloat = "LF";
    public static final String CdmDataType_ShortFloat = "SF";
    public static final String CdmDataType_Decimal = "DC";
    public static final String CdmDataType_Money = "MN";
    public static final String CdmDataType_Number = "N";

    //Int
    public static final String CdmDataType_Integer = "I";
    public static final String CdmDataType_LongInteger = "LI";
    public static final String CdmDataType_ShortInteger = "SI";
    public static final String CdmDataType_Serial = "NO"; // 自增序列
    public static final String CdmDataType_Byte = "BT";

    //String
    public static final String CdmDataType_Char = "A";

    //Text
    public static final String CdmDataType_LongVarCha = "LVA";
    public static final String CdmDataType_Text = "TXT";
    public static final String CdmDataType_VarCha = "VA";
    public static final String CdmDataType_LongCha = "LA";

    //Time
    public static final String CdmDataType_Time = "T";

    //TimeStamp
    public static final String CdmDataType_TimeStamp = "TS";


    ///#endregion


    ///#region 序列化常量
    public static final String cDataItems = "c:DataItems";
    public static final String cEntities = "c:Entities";

    public static final String aObjectID = "a:ObjectID";
    public static final String aName = "a:Name";
    public static final String aCode = "a:Code";
    public static final String aCreationDate = "a:CreationDate";
    public static final String aCreator = "a:Creator";
    public static final String aModificationDate = "a:ModificationDate";
    public static final String aModifier = "a:Modifier";
    public static final String aComment = "a:Comment";

    public static final String aDataType = "a:DataType";
    public static final String aLength = "a:Length";
    public static final String aPrecision = "a:Precision";
    public static final String aIdentity = "a:Identity";
    public static final String aMandatory = "a:Mandatory";
    public static final String aPhysicalOptions = "a:PhysicalOptions";
    public static final String aExtendedAttributesText = "a:ExtendedAttributesText";
    public static final String cDataItem = "c:DataItem";

    public static final String cIdentifierAttributes = "c:Identifier.Attributes";
    public static final String cAttributes = "c:Attributes";
    public static final String aIdentifiers = "a:Identifiers";
    public static final String cPrimaryIdentifier = "c:PrimaryIdentifier";


    ///#endregion


}