package com.inspur.edp.bef.bemanager.codegenerator.actions;


import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;

public abstract class JavaBaseCompCodeGenerator extends JavaBaseCommonCompCodeGen {

    protected JavaBaseCompCodeGenerator(GspBusinessEntity be,
                                        com.inspur.edp.bef.bizentity.operation.BizOperation operation, String nameSpace,
                                        String path) {
        super(be, operation, nameSpace, path);
    }

    protected String getClassNameFromComp(IMetadataContent content) {
        String fullClassName;
        if (content instanceof GspComponent) {
            GspComponent component = (GspComponent) content;
            fullClassName = component.getMethod().getClassName();
        } else {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0051);
        }
        String[] sections = fullClassName.split("[.]", -1);

        return sections[sections.length - 1];
    }

    protected void JavaGenerateExecute(StringBuilder result) {
        result.append(GetIndentationStr()).append("@").append(JavaCompCodeNames.KeywordOverride)
                .append(getNewline());
        result.append(GetIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ")
                .append(" ").append(JavaCompCodeNames.KeywordVoid).append(" ").append("execute()")
                .append(" ").append("{").append(getNewline());

        result.append(GetIndentationStr()).append("}");
    }

    @Override
    protected void generateBeanAnnotation(StringBuilder result) {
        return;
    }
}
