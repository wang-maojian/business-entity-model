package com.inspur.edp.bef.bemanager.generatecomponent;


import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;

public class ComponentClassNameGenerator {
    private static final String classNameFormat = "%1$s.%2$s.%3$s%4$s";

    public static String generateBEComponentClassName(String assembly, String compCode) {
        return String.format(classNameFormat, assembly, JavaCompCodeNames.ActionNameSpaceSuffix, compCode, "Action");
    }

    public static String generateBEMgrComponentClassName(String assembly, String compCode) {
        return String.format(classNameFormat, assembly, JavaCompCodeNames.MgrActionNameSpaceSuffix, compCode, "MgrAction");
    }

    public static String generateDeterminationComponentClassName(String assembly, String compCode) {
        return String.format(classNameFormat, assembly, JavaCompCodeNames.DeterminationNameSpaceSuffix, compCode, "Determination");
    }

    public static String generateValidationComponentClassName(String assembly, String compCode) {
        return String.format(classNameFormat, assembly, JavaCompCodeNames.ValidationNameSpaceSuffix, compCode, "Validation");
    }
}