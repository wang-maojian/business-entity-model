package com.inspur.edp.bef.bemanager.config;

import com.inspur.edp.bef.bemanager.commonstructure.BEComStructureSchemaExtension;
import com.inspur.edp.bef.bemanager.service.BeManagerService;
import com.inspur.edp.caf.cef.dt.spi.CommonStructureSchemaExtension;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration("com.inspur.edp.bef.bemanager.config.BefManagerConfig")
public class BefManagerConfig {
    @Bean("com.inspur.edp.bef.bemanager.config.BefManagerConfig.BEComStructureSchemaExtension")
    public CommonStructureSchemaExtension getBEComStructureSchemaExtension() {
        return new BEComStructureSchemaExtension();
    }

    @Bean
    @ConditionalOnClass(name = {"com.inspur.edp.jittojava.context.GenerateService"})
//TODO: tools目录中没有GenerateService,应拆分出去
    public BeManagerService getBizEntityDtService() {
        return new BeManagerService();
    }
}
