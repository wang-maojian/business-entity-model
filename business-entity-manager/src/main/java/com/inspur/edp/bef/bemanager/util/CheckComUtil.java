package com.inspur.edp.bef.bemanager.util;

import lombok.Data;

@Data
public class CheckComUtil {

    private String type = "";
    private String action = "";
    private String changeInfo = "";

}
