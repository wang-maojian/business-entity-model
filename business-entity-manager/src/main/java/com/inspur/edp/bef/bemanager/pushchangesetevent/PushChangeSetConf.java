package com.inspur.edp.bef.bemanager.pushchangesetevent;

import io.iec.edp.caf.commons.event.config.EventListenerSettings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PushChangeSetConf {
    @Bean(name = "PushChangeSetEventBroker")
    public PushChangeSetEventBroker setEventBroker(EventListenerSettings settings, PushChangeSetEventManager eventManager) {
        return new PushChangeSetEventBroker(settings, eventManager);
    }

    @Bean(name = "PushChangeSetEventManager")
    public PushChangeSetEventManager setEventManager() {
        return new PushChangeSetEventManager();
    }
}
