package com.inspur.edp.bef.bemanager.befdtconsistencycheckevent;


import com.inspur.edp.bef.bizentity.bizentitydtevent.IBizEntityActionDTEventListener;
import com.inspur.edp.bef.bizentity.bizentitydtevent.IBizEntityDTEventListener;
import com.inspur.edp.bef.bizentity.bizentitydtevent.IBizEntityFieldDTEventListener;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.actioneventargs.DeletingActionEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.entityeventargs.RemovingEntityEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.fieldeventargs.RemovingFieldEventArgs;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import io.iec.edp.caf.commons.event.EventManager;
import io.iec.edp.caf.commons.event.IEventListener;

public class BefDtConsistencyCheckEventManager extends EventManager {

    /**
     * Consistency check 实现抽象方法getCode(),获取事件管理对象的编码
     *
     * @return 事件管理对象的编码
     */
    @Override
    public String getEventManagerName() {
        return "BefDtConsistencyCheckEventManager";
    }

    /**
     * 当前的事件管理者是否处理输入的事件监听者对象
     *
     * @param listener 事件监听者
     * @return 若处理，则返回true,否则返回false
     */
    @Override
    public boolean isHandlerListener(IEventListener listener) {
        return (listener instanceof IBizEntityDTEventListener
                || listener instanceof IBizEntityFieldDTEventListener
                || listener instanceof IBizEntityActionDTEventListener);
    }

    /**
     * 注册事件
     *
     * @param listener 监听者
     */
    @Override
    public void addListener(IEventListener listener) {
        if (!(listener instanceof IBizEntityDTEventListener
                || listener instanceof IBizEntityFieldDTEventListener
                || listener instanceof IBizEntityActionDTEventListener)) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0002);
        }
        //IBizEntityEventListener
        if (listener instanceof IBizEntityDTEventListener) {
            IBizEntityDTEventListener bizEntityDTEventListener = (IBizEntityDTEventListener) listener;
            this.addEventHandler(BefDtConsistencyCheckType.removingEntity, bizEntityDTEventListener,
                    "removingEntity");
        }

        //IBizEntityFieldEventListener
        if (listener instanceof IBizEntityFieldDTEventListener) {
            IBizEntityFieldDTEventListener bizEntityFieldDTEventListener = (IBizEntityFieldDTEventListener) listener;
            this.addEventHandler(BefDtConsistencyCheckType.removingField, bizEntityFieldDTEventListener,
                    "removingField");
        }
        //IBizEntityActionDTEventListener
        if (listener instanceof IBizEntityActionDTEventListener) {
            IBizEntityActionDTEventListener bizEntityActionDTEventListener = (IBizEntityActionDTEventListener) listener;
            this.addEventHandler(BefDtConsistencyCheckType.deletingAction,
                    bizEntityActionDTEventListener, "deletingAction");
        }


    }

    /***
     * 注销事件
     * @param listener  监听者
     */
    @Override
    public void removeListener(IEventListener listener) {
        if (!(listener instanceof IEventListener)) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0003);
        }
        //IBizEntityEventListener
        if (listener instanceof IBizEntityDTEventListener) {
            IBizEntityDTEventListener bizEntityDTEventListener = (IBizEntityDTEventListener) listener;
            this.removeEventHandler(BefDtConsistencyCheckType.removingEntity, bizEntityDTEventListener,
                    "removingEntity");
        }

        //IBizEntityFieldEventListener
        if (listener instanceof IBizEntityFieldDTEventListener) {
            IBizEntityFieldDTEventListener bizEntityFieldDTEventListener = (IBizEntityFieldDTEventListener) listener;
            this.removeEventHandler(BefDtConsistencyCheckType.removingField,
                    bizEntityFieldDTEventListener, "removingField");
        }
        //IBizEntityActionDTEventListener
        if (listener instanceof IBizEntityActionDTEventListener) {
            IBizEntityActionDTEventListener bizEntityActionDTEventListener = (IBizEntityActionDTEventListener) listener;
            this.removeEventHandler(BefDtConsistencyCheckType.deletingAction,
                    bizEntityActionDTEventListener, "deletingAction");
        }
    }

    //entity

    public final void fireRemovingEntity(RemovingEntityEventArgs args) {
        this.fire(BefDtConsistencyCheckType.removingEntity, args);
    }

    //field

    public final void fireRemovingField(RemovingFieldEventArgs args) {
        this.fire(BefDtConsistencyCheckType.removingField, args);
    }

    //action

    public final void fireDeletingAction(DeletingActionEventArgs args) {
        this.fire(BefDtConsistencyCheckType.deletingAction, args);
    }
}
