package com.inspur.edp.bef.bemanager.codegenerator.actions;

import com.inspur.edp.bef.bemanager.codegenerator.JavaCodeGeneratorUtil;
import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bemanager.util.CheckInfoUtil;
import com.inspur.edp.bef.bemanager.util.ComponentExtendProperty;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.beenum.BEOperationType;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.cef.variable.dtgenerator.vardtmGenerate.javageneratecmpcode.JavaIBaseCompCodeGen;
import com.inspur.edp.das.commonmodel.util.MetadataProjectUtil;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public abstract class JavaBaseCommonCompCodeGen implements JavaIBaseCompCodeGen {

    private String entityNamespace;
    private String apiNamespace;
    private String path;

  protected com.inspur.edp.bef.bizentity.operation.BizOperation BizOperation;

  /**
   * 包前缀，默认为com
   */
  protected String packagePrefix;

  protected String NameSpace;
  protected String EntityClassName;
  protected ComponentExtendProperty extendProperty;

    protected String beCode;
    protected String beName;

    /**
     * 获取生成构件代码扩展属性
     *
     * @return
     */
    public ComponentExtendProperty getExtendProperty() {
        return extendProperty;
    }

    private String privateChildCode;

    protected String getChildCode() {
        return privateChildCode;
    }

    private void setChildCode(String value) {
        privateChildCode = value;
    }


    protected abstract String getBaseClassName();

    public String getPath() {
        return path;
    }

    public void setPath(String value) {
        path = value;
    }

    //解析型标志
    protected boolean isInterpretation;

    ///#endregion


  ///#region 构造函数
  protected JavaBaseCommonCompCodeGen(GspBusinessEntity be, BizOperation operation, String nameSpace, String path) {
    setPath(path);
    BizOperation = operation;
    //根据relativePath获取下包前缀
    this.packagePrefix = MetadataProjectUtil.getPackagePrefix(path);
    NameSpace = GetNameSpace(nameSpace);
    //compName = GetCompName();
    apiNamespace = be.getApiNamespace().getDefaultNamespace();
    entityNamespace = be.getMainObject().getGeneratedEntityClassInfo().getClassNamespace();
    //  EntityClassName = be.MainObject.GetGeneratedEntityClassInfo().ClassName;
    if (operation.getOpType() == BEOperationType.BizMgrAction) {
      EntityClassName = be.getMainObject().getGeneratedEntityClassInfo().getClassName();
    } else {
      EntityClassName = operation.getOwner().getGeneratedEntityClassInfo().getClassName();
    }
    setChildCode("");
    //解析型工程标志
    this.isInterpretation = SpringBeanUtils.getBean(MetadataProjectService.class).isInterpretation(path);
    beCode = be.getCode();
    beName = be.getName();
  }


    private String GetNameSpace(String baseNameSpace) {
        String nameSpace = "%1$s.%2$s";
        return String.format(nameSpace, baseNameSpace, GetNameSpaceSuffix());
    }

    protected abstract String GetNameSpaceSuffix();

    public String GetCompName() {
        if (CheckInfoUtil.checkNull(BizOperation.getComponentId())) {
            return GetInitializeCompName();
        }

        GspMetadata metadata = SpringBeanUtils.getBean(MetadataService.class).getRefMetadata(getPath(), BizOperation.getComponentId());

        if (metadata == null) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0050, ExceptionLevel.Info, getPath(), BizOperation.getComponentName());
        }
        return getClassNameFromComp(metadata.getContent());
    }

    protected abstract String getClassNameFromComp(IMetadataContent content);

    public void SetChild(String childCode) {
        this.setChildCode(childCode);
    }

    ///#endregion

  public String Generate() {
    StringBuilder result = new StringBuilder();
    ///#region package
    NameSpace = JavaCodeGeneratorUtil.ConvertImportPackage(packagePrefix,NameSpace);
    result.append(JavaCompCodeNames.KeywordPackage).append(" ").append(NameSpace).append(";").append(getNewline());
    ///#endregion
    ///#region import
    GenerateImport(result); //这里是最终调用的子类
    result.append(getNewline());
    ///#endregion

        // add Bean Annotation
        generateBeanAnnotation(result);
        ///#region ClassStart
        result.append(JavaCompCodeNames.KeywordPublic).append(" ").append(JavaCompCodeNames.KeywordClass).append(" ").append(GetCompName()).append(" ").append(JavaCompCodeNames.KeywordExtends).append(" ").append(getBaseClassName());
        result.append(" {");
        ///#endregion
        ///#region Field
        JavaGenerateField(result);
        result.append(getNewline());
        ///#endregion
        ///#region Constructor
        JavaGenerateConstructor(result);
        result.append(getNewline());
        ///#endregion
        ///#region ExecuteMethod

        JavaGenerateExecute(result);

        result.append(getNewline());
        JavaGenerateExtendMethod(result);

        result.append(getNewline()).append("}");

        ///#endregion

        return result.toString();
    }

    ///#region 生成方法
    //TODO 此方法没有使用，是不是可以去掉？
    public String generateCommon() {
        StringBuilder result = new StringBuilder();

    ///#region package
    NameSpace = JavaCodeGeneratorUtil.ConvertImportPackage(packagePrefix,NameSpace);
    result.append(JavaCompCodeNames.KeywordImport).append(" ").append(getNewline()).append(NameSpace);

        ///#endregion
        ///#region import
        GenerateImport(result);
        result.append("\n");

        ///#endregion


        ///#region ClassStart
        result.append(JavaCompCodeNames.KeywordPublic).append(" ").append(JavaCompCodeNames.KeywordClass).append(" ").append(GetCompName()).append(" ").append(JavaCompCodeNames.KeywordExtends).append(" ").append(getNewline()).append(getBaseClassName());
        result.append(getNewline()).append("{");

        ///#endregion


        ///#region Field

        JavaGenerateField(result);
        result.append("\n");

        ///#endregion


        ///#region Constructor

        JavaGenerateConstructor(result);
        result.append("\n");

        ///#endregion


        ///#region ExtendMethod

        JavaGenerateExtendMethod(result);

        ///#endregion


        ///#region ExecuteMethod

        result.append(GetIndentationStr()).append("@").append(JavaCompCodeNames.KeywordOverride).append(getNewline());
        result.append(GetIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ").append(" ").append(JavaCompCodeNames.KeywordVoid).append(" ").append("execute() ");

        result.append(GetIndentationStr()).append("{").append(getNewline());
        result.append(GetIndentationStr()).append("}").append(getNewline());


        ///#endregion


        ///#region ClassEnd
        result.append(getNewline()).append("}");

        ///#endregion

        ///#region NameSpaceEnd
        //result.appendLine("}");
        ///#endregion

        return result.toString();
    }

    public boolean getIsCommonGenerate() {
        return true;
    }

    private void GenerateImport(StringBuilder result) {
        //解析性功能不需要Api的引用
        if (!isInterpretation) {
            //此处不调用ConvertImportPackage()方法,entityNamespace已经java包名
            entityNamespace = String.format("%1$s%2$s", entityNamespace, ".*");
            result.append(GetImportStr(entityNamespace));
            apiNamespace = String.format("%1$s%2$s", apiNamespace, ".*");
            result.append(GetImportStr(apiNamespace));
        }
        result.append(GetImportStr(JavaCompCodeNames.MessageNameSpace));
        JavaGenerateExtendUsing(result);
    }

    protected abstract void JavaGenerateExecute(StringBuilder result);

    protected abstract void JavaGenerateExtendUsing(StringBuilder result);

    protected abstract void JavaGenerateConstructor(StringBuilder result);

    protected void JavaGenerateExtendMethod(StringBuilder result) {

    }

    protected void JavaGenerateField(StringBuilder result) {

    }

    ///#endregion

    ///#region 通用方法

    protected String GetImportStr(String value) {
        return new StringBuilder(JavaCompCodeNames.KeywordImport).append(" ").append(value).append(";").append(getNewline()).toString();
    }

    protected String getNewline() {
        return "\r\n";
    }

    protected String GetIndentationStr() {
        return "\t";
        //return "    ";
    }

    protected String GetDoubleIndentationStr() {
        return "\t\t";

    }
    ///#endregion

    ///#region 生成Execute方法

    public String getCompName() {
        return GetCompName();
    }

    //TODO 此方法没有使用，是不是可以去掉？
    public String generateExecute() {
        StringBuilder result = new StringBuilder();

        ///#region NameSpaceStart

        result.append(JavaCompCodeNames.KeywordPackage).append(" ").append(getNewline()).append(NameSpace);
        result.append(getNewline()).append("{");

        ///#endregion
        ///#region ClassStart
        result.append(GetIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ").append("partial").append(" ").append(JavaCompCodeNames.KeywordClass).append(" ").append(GetCompName()).append(getNewline());
        result.append(GetIndentationStr()).append("{").append(getNewline());

        ///#endregion
        ///#region ExecuteMethod
        result.append(GetDoubleIndentationStr()).append(JavaCompCodeNames.KeywordProtected).append(" ").append(JavaCompCodeNames.KeywordOverride).append(" ").append(JavaCompCodeNames.KeywordVoid).append(" ").append("execute()");
        result.append(GetDoubleIndentationStr()).append(getNewline()).append("{");
        result.append(GetDoubleIndentationStr()).append(getNewline()).append("}");
        ///#endregion

        ///#region ClassEnd
        result.append(GetIndentationStr()).append(getNewline()).append("}");

        ///#endregion

        ///#region NameSpaceEnd
        result.append(getNewline()).append("}");
        ///#endregion

        return result.toString();
    }
    ///#endregion

    protected abstract String GetInitializeCompName();

    protected abstract void generateBeanAnnotation(StringBuilder result);

}
