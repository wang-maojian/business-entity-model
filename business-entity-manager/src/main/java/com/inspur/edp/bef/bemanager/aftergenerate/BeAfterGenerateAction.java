package com.inspur.edp.bef.bemanager.aftergenerate;

import com.inspur.edp.bef.bemanager.dependency.ManageProjectDependencyUtil;
import com.inspur.edp.bef.bemanager.util.BefFileUtil;
import com.inspur.edp.jittojava.spi.AfterGeneratorAction;

public class BeAfterGenerateAction implements AfterGeneratorAction {
    @Override
    public void afterGenerator(String s) {
        ManageProjectDependencyUtil util = ManageProjectDependencyUtil.getInstance();
        util.addDefaultProjectDependency(BefFileUtil.getJavaProjPath(s));
    }
}
