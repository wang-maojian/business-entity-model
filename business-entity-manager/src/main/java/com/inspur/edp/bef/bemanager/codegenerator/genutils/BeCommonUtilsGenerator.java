package com.inspur.edp.bef.bemanager.codegenerator.genutils;

import com.inspur.edp.bef.bemanager.codegenerator.JavaCodeGeneratorUtil;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.collection.BizMgrActionCollection;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;
import com.inspur.edp.das.commonmodel.util.MetadataProjectUtil;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.ProcessMode;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.io.File;

/**
 * 生成工具集class
 */
public class BeCommonUtilsGenerator {
    private final GspBusinessEntity businessEntity;
    private GspMetadata metadata;
    private final String relativePath;
    private final String compAssemblyName;

    public BeCommonUtilsGenerator(GspBusinessEntity businessEntity, GspMetadata metadata) {
        this.relativePath = metadata.getRelativePath();
        this.compAssemblyName = businessEntity.getGeneratedConfigID().toLowerCase() + ".common";
        this.businessEntity = businessEntity;
        this.metadata = metadata;
    }

    public void generateCommonUtils() {
        MetadataProjectService service = SpringBeanUtils.getBean(MetadataProjectService.class);
        ProcessMode mode = service.getProcessMode(relativePath);
        if (mode != ProcessMode.interpretation)
            return;
        String compModulePath = service.getJavaCompProjectPath(relativePath);
        File folder = new File(compModulePath);
        if (!folder.exists())
            return;
        for (GspBizEntityObject entityObject:businessEntity.getAllNodes())
        {
            if(isHasComponent(entityObject)){
                new BeEntityUtilsGenerator(entityObject,compAssemblyName,relativePath).generate();
            }
        }
        BizMgrActionCollection bizMgrAction = businessEntity.getCustomMgrActions();
        if(bizMgrAction != null && !bizMgrAction.isEmpty()){
            new BeMgrActionUtilsGenerator(businessEntity,compAssemblyName,relativePath).generate();
        }
    }

    /**
     * 当存在联动计算、校验规则、内部方法时，需要生成DataUtils工具类，返回true
     * 1.以上类型的构件中可以直接操作setValue方法，其余需要使用变更集
     * @param bizEntityObject
     * @return
     */
    private boolean isHasComponent(GspBizEntityObject bizEntityObject) {
        if(bizEntityObject.getBizActions() != null && !bizEntityObject.getBizActions().isEmpty()){
            for (BizOperation bizOperation : bizEntityObject.getBizActions()){
                if (bizOperation.getIsGenerateComponent())
                    return true;
            }
        }

        // 旧版联动计算规则 新版的事件
        if (bizEntityObject.getBizCommonDtms() != null && !bizEntityObject.getBizCommonDtms().isEmpty()){
            for (CommonDetermination commonDetermination : bizEntityObject.getBizCommonDtms()) {
                if (commonDetermination.getIsGenerateComponent())
                    return true;
            }
        }
        if (bizEntityObject.getBizCommonVals() != null && !bizEntityObject.getBizCommonVals().isEmpty()){
            for (CommonValidation commonDetermination : bizEntityObject.getBizCommonVals()) {
                if (commonDetermination.getIsGenerateComponent())
                    return true;
            }
        }
        return false;
    }

}
