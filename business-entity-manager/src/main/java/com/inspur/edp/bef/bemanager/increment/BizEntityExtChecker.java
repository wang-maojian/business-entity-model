package com.inspur.edp.bef.bemanager.increment;

import com.inspur.edp.metadata.rtcustomization.spi.CustomizationExtChecker;
import com.inspur.edp.metadata.rtcustomization.spi.args.ConflictCheckArgs;
import com.inspur.edp.metadata.rtcustomization.spi.args.MdConflictCheckArgs;

public class BizEntityExtChecker implements CustomizationExtChecker {

    @Override
    public void checkMergeConflict(ConflictCheckArgs conflictCheckArgs) {

    }

    @Override
    public void checkMergeConflict(MdConflictCheckArgs mdConflictCheckArgs) {

    }
}
