package com.inspur.edp.bef.bemanager.gspbusinessentity.config;

import com.inspur.edp.bef.bemanager.gspbusinessentity.GspBeExtendInfoService;
import com.inspur.edp.bef.bemanager.gspbusinessentity.cache.GspBeExtendInfoCacheManager;
import io.iec.edp.caf.caching.api.CacheManager;
import io.iec.edp.caf.rpc.api.service.RpcClient;
import io.iec.edp.caf.tenancy.api.ITenantService;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan({"com.inspur.edp.bef.bizentity.gspbusinessentity.entity"})
@EnableJpaRepositories({"com.inspur.edp.bef.bemanager.gspbusinessentity.repository"})
public class GspBeExtendInfoConfig {

    @Bean("com.inspur.edp.bef.bemanager.gspbusinessentity.config.GspBeExtendInfoConfig.getBeExtendInfoService")
    public GspBeExtendInfoService getBeExtendInfoService(GspBeExtendInfoCacheManager gspBeExtendInfoCacheManager,
                                                         RpcClient rpcClient) {
        return new GspBeExtendInfoService(gspBeExtendInfoCacheManager, rpcClient);
    }

    @Bean("com.inspur.edp.bef.bemanager.gspbusinessentity.config.GspBeExtendInfoConfig.gspBeExtendInfoCacheManager")
    public GspBeExtendInfoCacheManager gspBeExtendInfoCacheManager(CacheManager cacheManager, ITenantService tenantService) {
        return new GspBeExtendInfoCacheManager(cacheManager, tenantService);
    }

}
