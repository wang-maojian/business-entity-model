package com.inspur.edp.udt.designtime.api.extension;

public class UdtExtensionConfig {
    private String extensiontype;

    private String serclass;


    private String deserclass;

    public String getExtensiontype() {
        return extensiontype;
    }

    public void setExtensiontype(String extensiontype) {
        this.extensiontype = extensiontype;
    }

    public String getSerclass() {
        return serclass;
    }

    public void setSerclass(String serclass) {
        this.serclass = serclass;
    }

    public String getDeserclass() {
        return deserclass;
    }

    public void setDeserclass(String deserclass) {
        this.deserclass = deserclass;
    }
}
