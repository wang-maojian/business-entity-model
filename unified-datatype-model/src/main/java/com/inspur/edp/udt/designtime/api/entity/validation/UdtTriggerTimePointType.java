package com.inspur.edp.udt.designtime.api.entity.validation;

/**
 * Determination触发时机
 */
public enum UdtTriggerTimePointType {
    /**
     * 不执行,用于判断时机比较结果
     */
    None(0),

    /**
     * 数据更新后
     */
    AfterModify(1),

    /**
     * 保存前
     */
    BeforeSave(2);

    private final int intValue;
    private static java.util.HashMap<Integer, UdtTriggerTimePointType> mappings;

    private synchronized static java.util.HashMap<Integer, UdtTriggerTimePointType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, UdtTriggerTimePointType>();
        }
        return mappings;
    }

    private UdtTriggerTimePointType(int value) {
        intValue = value;
        UdtTriggerTimePointType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static UdtTriggerTimePointType forValue(int value) {
        return getMappings().get(value);
    }
}