package com.inspur.edp.udt.designtime.api.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
import com.inspur.edp.cef.designtime.api.element.GspEnumValue;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.element.GspAssociationSerializer;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.utils.UdtThreadLocal;
import lombok.var;

public class SimpleDataTypeSerializer extends UdtSerializer {


    public SimpleDataTypeSerializer() {
        if (UdtThreadLocal.get() != null)
            isFull = UdtThreadLocal.get().getfull();
    }

    public SimpleDataTypeSerializer(boolean full) {
        super(full);
        isFull = full;
    }

    @Override
    protected void writeUdtExtendInfo(JsonGenerator writer, UnifiedDataTypeDef dataType) {
        SimpleDataTypeDef sUdt = (SimpleDataTypeDef) dataType;
        SerializerUtils.writePropertyValue(writer, UdtNames.DataType, sUdt.getMDataType().toString());//不变
        if (isFull || sUdt.getLength() != 0)
            SerializerUtils.writePropertyValue(writer, UdtNames.Length, sUdt.getLength());
        if (isFull || sUdt.getPrecision() != 0)
            SerializerUtils.writePropertyValue(writer, UdtNames.Precision, sUdt.getPrecision());
        if (isFull || (sUdt.getDefaultValue() != null && !"".equals(sUdt.getDefaultValue())))
            SerializerUtils.writePropertyValue(writer, UdtNames.DefaultValue, sUdt.getDefaultValue());
        if (isFull || sUdt.getIsUnique())
            SerializerUtils.writePropertyValue(writer, UdtNames.IsUnique, sUdt.getIsUnique());
        if (isFull || sUdt.getIsRequired())
            SerializerUtils.writePropertyValue(writer, UdtNames.IsRequired, sUdt.getIsRequired());
        if (isFull || (sUdt.getObjectType() != null && sUdt.getObjectType() != GspElementObjectType.None))
            SerializerUtils.writePropertyValue(writer, UdtNames.ObjectType, sUdt.getObjectType());
        if (isFull || sUdt.isEnableRtrim())
            SerializerUtils.writePropertyValue(writer, UdtNames.EnableRtrim, sUdt.isEnableRtrim());
        if (this.isFull || sUdt.getEnumIndexType() != null)
            SerializerUtils.writePropertyValue(writer, UdtNames.EnumIndexType, sUdt.getEnumIndexType());
        if (sUdt.getObjectType().equals(GspElementObjectType.Enum)) {
            writeEnumValueList(writer, sUdt);
        } else if (sUdt.getObjectType().equals(GspElementObjectType.Association)) {
            writeAssociationCollection(writer, sUdt);
        }
    }

    private void writeAssociationCollection(JsonGenerator writer, SimpleDataTypeDef sUdt) {
        if (isFull || (sUdt.getChildAssociations() != null && !sUdt.getChildAssociations().isEmpty())) {
            SerializerUtils.writePropertyName(writer, UdtNames.ChildAssociations);
            SerializerUtils.WriteStartArray(writer);
            if (!sUdt.getChildAssociations().isEmpty()) {
                GspAssociationSerializer serializer = this.getAssoConvertor();
                for (var asso : sUdt.getChildAssociations()) {
                    serializer.serialize(asso, writer, null);
                }
            }
            SerializerUtils.WriteEndArray(writer);
        }
    }

    private GspAssociationSerializer getAssoConvertor() {
        return new GspAssociationSerializer(isFull, new UdtElementSerializer(isFull));
    }

    private void writeEnumValueList(JsonGenerator writer, SimpleDataTypeDef field) {
        if (isFull || (field.getContainEnumValues() != null && !field.getContainEnumValues().isEmpty())) {
            SerializerUtils.writePropertyName(writer, UdtNames.ContainEnumValues);
            SerializerUtils.WriteStartArray(writer);
            if (field.getContainEnumValues() != null && !field.getContainEnumValues().isEmpty()) {
                for (int index = 0; index < field.getContainEnumValues().size(); ++index)
                    writeEnumValue(writer, field.getContainEnumValues().get(index));
                SerializerUtils.WriteEndArray(writer);
            }
        }
    }

    private void writeEnumValue(JsonGenerator writer, GspEnumValue enumValue) {
        SerializerUtils.writeStartObject(writer);
        SerializerUtils.writePropertyValue(writer, CefNames.I18nResourceInfoPrefix, enumValue.getI18nResourceInfoPrefix());
        SerializerUtils.writePropertyValue(writer, CefNames.Index, enumValue.getIndex());
        SerializerUtils.writePropertyValue(writer, CefNames.IsDefaultEnum, enumValue.getIsDefaultEnum());
        SerializerUtils.writePropertyValue(writer, CefNames.StringIndex, enumValue.getStringIndex());
        SerializerUtils.writePropertyValue(writer, CefNames.Value, enumValue.getValue());
        SerializerUtils.writePropertyValue(writer, CefNames.Name, enumValue.getName());
        // 启用元数据精简
        if (isFull || enumValue.getEnumItemDisabled()) {
            SerializerUtils.writePropertyValue(writer, CefNames.EnumItemDisabled, enumValue.getEnumItemDisabled());
        }
        SerializerUtils.writeEndObject(writer);
    }

}
