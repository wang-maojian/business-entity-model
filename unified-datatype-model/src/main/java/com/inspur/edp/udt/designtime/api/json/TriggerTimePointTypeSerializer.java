package com.inspur.edp.udt.designtime.api.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.udt.designtime.api.entity.validation.UdtTriggerTimePointType;

import java.util.EnumSet;

public class TriggerTimePointTypeSerializer extends JsonSerializer<EnumSet<UdtTriggerTimePointType>> {
    @Override
    public void serialize(EnumSet<UdtTriggerTimePointType> value, JsonGenerator writer, SerializerProvider serializers) {
        int intValue = 0;
        for (UdtTriggerTimePointType timePointType : value) {
            intValue += timePointType.getValue();
        }
        SerializerUtils.writePropertyValue_Integer(writer, intValue);
    }
}
