package com.inspur.edp.udt.designtime.api.entity;

public enum TransmitType {

    ValueChanged,

    Always;
}
