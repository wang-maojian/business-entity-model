package com.inspur.edp.udt.designtime.api;

import com.inspur.edp.udt.designtime.api.extension.UdtExtensionConfigs;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.inspur.edp.udt.designtime.api"})
public class UdtDesigntimeAutoConfig {
    @Bean
    public UdtExtensionConfigs GetUdtExtensionConfigs() {
        return new UdtExtensionConfigs();
    }
}
