package com.inspur.edp.udt.designtime.api.i18n;

import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;
import com.inspur.edp.udt.designtime.api.i18n.names.UdtResourceDescriptionNames;

public class ComplexUdtReourceExtractor extends BaseUdtResourceExtractor {

    public ComplexUdtReourceExtractor(ComplexDataTypeDef cUdt, ICefResourceExtractContext context) {
        super(cUdt, context, getParentResourcePrefixInfo(cUdt));
    }

    static CefResourcePrefixInfo getParentResourcePrefixInfo(ComplexDataTypeDef cUdt) {
        CefResourcePrefixInfo info = new CefResourcePrefixInfo();
        info.setDescriptionPrefix(UdtResourceDescriptionNames.ComplexUnifiedDataType);
        info.setResourceKeyPrefix(cUdt.getDotnetAssemblyName());
        return info;
    }
}
