package com.inspur.edp.udt.designtime.api.i18n;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.cef.designtime.api.i18n.extractor.AssoRefFieldResourceExtractor;
import com.inspur.edp.cef.designtime.api.i18n.extractor.AssoResourceExtractor;
import com.inspur.edp.udt.designtime.api.entity.element.UdtElement;

class UdtAssoResourceExtractor extends AssoResourceExtractor {

    public UdtAssoResourceExtractor(GspAssociation asso, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(asso, context, parentResourceInfo);
    }

    @Override
    protected final AssoRefFieldResourceExtractor getAssoRefFieldResourceExtractor(
            ICefResourceExtractContext context,
            CefResourcePrefixInfo assoPrefixInfo,
            IGspCommonField field) {
        return new UdtAssoRefEleResourceExtractor((UdtElement) field, context, assoPrefixInfo);
    }

}
