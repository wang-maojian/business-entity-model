package com.inspur.edp.udt.designtime.api.exception;

import lombok.Getter;

public enum UdtModelErrorCodeEnum {
    /* ---TEMPLATE ERROR START ---**/
    /**
     * [{0}]
     */
    GSP_BEMODEL_TEMPLATE_ERROR(true),
    /*--- TEMPLATE ERROR END ---**/

    /*--- JSON SERIALIZE ERROR START ---**/
    /**
     * [{0}]反序列化失败
     */
    GSP_BEMODEL_JSON_0001(false),
    /**
     * [{0}]:[{1}]反序列化失败
     */
    GSP_BEMODEL_JSON_0002(false),
    /**
     * JSON解析失败,检查Json结构
     */
    GSP_BEMODEL_JSON_0003(false),
    /**
     * [{0}]序列化失败
     */
    GSP_BEMODEL_JSON_0004(false),
    /**
     * [{0}]:[{1}]序列化失败
     */
    GSP_BEMODEL_JSON_0005(false),
    /*--- JSON SERIALIZE ERROR END ---**/

    /*--- CLASS CLONE ERROR START ---**/
    /**
     * [{0}]克隆失败
     */
    GSP_BEMODEL_CLONE_0001(false),
    /*--- CLASS CLONE ERROR END ---**/

    /*--- ENUM NOT SUPPORT ERROR START ---**/
    /**
     * 不支持的数据类型:[{0}]
     * default
     */
    GSP_BEMODEL_ENUM_NOTSUPPORT_0002(true),
    /**
     * 不支持当前类型[{0}]的业务字段
     */
    GSP_BEMODEL_ENUM_NOTSUPPORT_0005(true),
    /**
     * 未知构件类型:[{0}]
     */
    GSP_BEMODEL_ENUM_NOTSUPPORT_0006(false),

    /*--- ENUM NOT SUPPORT ERROR END ---**/

    /*--- COMMON EXCEPTION ERROR START ---**/
    /**
     * [{0}]的[{1}]方法未被继承实现
     */
    GSP_BEMODEL_COMMON_0009(false),
    /**
     * [{0}]实例化失败
     */
    GSP_BEMODEL_COMMON_0010(false),
    /**
     * 参数[{0}]不能为空
     */
    GSP_BEMODEL_COMMON_0003(false),
    /*--- COMMON EXCEPTION ERROR END ---**/

    /* --- UDT MODEL ERROR START ---**/
    /**
     * 工程路径[{0}]下构件元数据[{1}]不存在，请检查
     */
    GSP_BEMODEL_UDT_MODEL_0001(true),
    /**
     * [{0}]未定义的属性名[{1}]
     * default
     */
    GSP_BEMODEL_UDT_MODEL_0002(false),
    /**
     * 字段映射关系中的id=[{0}]的字段在当前要更新的元数据上不存在，请检查
     */
    GSP_BEMODEL_UDT_MODEL_0003(true),
    /**
     * 请先完善当前字段的[编号]及[标签]。
     */
    GSP_BEMODEL_UDT_MODEL_0004(true),

    /**
     * 没有生成构件，无法继续生成代码
     */
    GSP_BEMODEL_UDT_MODEL_0005(true),

    /**
     * 当前元数据不属于构件元数据
     */
    GSP_BEMODEL_UDT_MODEL_0006(true),

    /**
     * 暂不支持当前操作类型生成构件的java文件
     * default
     */
    GSP_BEMODEL_UDT_MODEL_0007(false),

    /**
     * 根据ID[{0}]未找到对应的关联元数据，请确认该元数据是否存在
     */
    GSP_BEMODEL_UDT_MODEL_0008(true),

    /**
     * 当前值存储方式为[存为一列]，暂不支持设置字段关联，但是当前字段{0}的对象类型为关联，请修改。
     */
    GSP_BEMODEL_UDT_MODEL_0009(true),

    /**
     * 当前值存储方式为[存为多列]，暂不支持设置字段数据类型为[业务字段]，但是当前字段{0}引用了UDT，请修改。
     */
    GSP_BEMODEL_UDT_MODEL_0010(true),

    /**
     * 根据ID[{1}]加载关联业务实体[{0}]失败
     */
    GSP_BEMODEL_UDT_MODEL_0011(true),

    /**
     * 已存在名称为[{0}]的属性。
     */
    GSP_BEMODEL_UDT_MODEL_0012(true),

    /**
     * 根据udtId[{0}]加载引用的UDT元数据失败
     */
    GSP_BEMODEL_UDT_MODEL_0013(true),

    /**
     * 业务字段中无法赋值属性[{0}]
     */
    GSP_BEMODEL_UDT_MODEL_0014(true),

    /**
     * 不存在类型为[{0}]的[{1}]
     */
    GSP_BEMODEL_UDT_MODEL_0015(true);
    /* --- UDT MODEL ERROR END ---**/

    /**
     * 是否业务异常
     */
    @Getter
    private final boolean bizException;

    UdtModelErrorCodeEnum(boolean bizException) {
        this.bizException = bizException;
    }
}
