package com.inspur.edp.udt.designtime.api.i18n;

import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.cef.designtime.api.i18n.extractor.AssoRefFieldResourceExtractor;
import com.inspur.edp.cef.designtime.api.i18n.extractor.AssoResourceExtractor;
import com.inspur.edp.udt.designtime.api.entity.element.UdtElement;

class UdtAssoRefEleResourceExtractor extends AssoRefFieldResourceExtractor {

    public UdtAssoRefEleResourceExtractor(UdtElement field, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {

        super(field, context, parentResourceInfo);
    }

    @Override
    protected final AssoResourceExtractor getAssoResourceExtractor(
            ICefResourceExtractContext context,
            CefResourcePrefixInfo fieldPrefixInfo,
            GspAssociation asso) {
        return new UdtAssoResourceExtractor(asso, context, fieldPrefixInfo);
    }
}
