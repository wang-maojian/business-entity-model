package com.inspur.edp.udt.designtime.api.commonstructure;

import com.inspur.edp.caf.cef.schema.structure.CommonStructure;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
import com.inspur.edp.cef.designtime.api.entity.commonstructure.CefCommonStructureUtil;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.entity.GspCommonModel;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.MetadataRTService;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.exception.UdtModelErrorCodeEnum;
import com.inspur.edp.udt.designtime.api.exception.UdtModelException;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.ArrayList;
import java.util.List;

public class UdtCommonStructureUtil extends CefCommonStructureUtil {

    public static UdtCommonStructureUtil getInstance() {
        return new UdtCommonStructureUtil();
    }

    protected UdtCommonStructureUtil() {
    }

    @Override
    protected CommonStructure getRefBeCommonStructure(GspAssociation asso) {
        MetadataRTService service = SpringBeanUtils.getBean(MetadataRTService.class);
        GspMetadata sourceBeMeta = service.getMetadata(asso.getRefModelID());
        if (sourceBeMeta == null) {
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_TEMPLATE_ERROR, getExpressionMessage(asso));
        }
        GspCommonModel sourceBe = (GspCommonModel) sourceBeMeta
                .getContent();
        if (sourceBe == null) {
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_TEMPLATE_ERROR, getExpressionMessage(asso));
        }
        GspCommonModel be = sourceBe.clone();
        IGspCommonObject refObj = be.findObjectById(asso.getRefObjectID());
        refObj.setID(getRefObjId(asso));
        IGspCommonObject refObjCopy = refObj.clone(refObj.getParentObject());
        IGspCommonField parentElement = asso.getBelongElement();
        refObj.getContainElements().clear();

        // ID字段
        IGspCommonField idElement = refObjCopy
                .findElement(refObjCopy.getColumnGenerateID().getElementID());
        IGspCommonField newIdElement = idElement
                .clone(idElement.getBelongObject(), idElement.getParentAssociation());
        newIdElement
                .setLabelID(parentElement != null ? parentElement.getLabelID() : newIdElement.getLabelID());
        refObj.getContainElements().add(newIdElement);
        // 关联带出字段
        for (IGspCommonField refEle : asso.getRefElementCollection()) {
            IGspCommonElement targetElement = refObjCopy.findElement(refEle.getRefElementId());
            IGspCommonField newRefElement = targetElement.clone(targetElement.getBelongObject(), targetElement.getParentAssociation());
            newRefElement.setLabelID(getRefElementLabelId(targetElement.getLabelID(), parentElement));
            refObj.getContainElements().add(newRefElement);
        }
        return be;
    }

    private String getRefElementLabelId(String currentLabelId, IGspCommonField belongElement) {

        String labelIdPrefix = belongElement != null ? belongElement.getLabelID() + "_" : "";
        return labelIdPrefix + currentLabelId;
    }

    @Override
    protected CommonStructure getRefUdtCommonStructure(String udtId) {
        MetadataRTService service = SpringBeanUtils.getBean(MetadataRTService.class);
        GspMetadata udtMeta = service.getMetadata(udtId);
        if (udtMeta == null) {
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_UDT_MODEL_0013, udtId);
        }
        UnifiedDataTypeDef udt = (UnifiedDataTypeDef) udtMeta.getContent();
        return udt;
    }

    public List<CommonStructure> getElementsRefStructures(List<IGspCommonField> fields) {
        List<CommonStructure> list = new ArrayList<CommonStructure>();
        for (IGspCommonField field : fields) {
            if (field.getIsUdt()) {
                list.add(getRefUdtCommonStructure(field.getUdtID()));
                continue;
            }
            if (field.getObjectType() == GspElementObjectType.Association) {
                for (GspAssociation asso : field.getChildAssociations()) {
                    list.add(getRefBeCommonStructure(asso));
                }
                continue;
            }
        }
        return list;
    }

    private String getExpressionMessage(GspAssociation asso) {
        return MessageI18nUtils.getMessage("GSP_UDT_MESSAGE_0001",
                asso.getRefModelPkgName(), asso.getRefModelName(), asso.getRefModelID());
    }
}
