package com.inspur.edp.udt.designtime.api.i18n;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.cef.designtime.api.i18n.extractor.AssoResourceExtractor;
import com.inspur.edp.cef.designtime.api.i18n.extractor.CefFieldResourceExtractor;
import com.inspur.edp.udt.designtime.api.entity.element.UdtElement;

class UdtEleResourceExtractor extends CefFieldResourceExtractor {

    public UdtEleResourceExtractor(UdtElement field, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(field, context, parentResourceInfo);
    }

    @Override
    protected void extractExtendProperties(IGspCommonField commonField) {
    }

    @Override
    protected AssoResourceExtractor getAssoResourceExtractor(
            ICefResourceExtractContext context,
            CefResourcePrefixInfo fieldPrefixInfo,
            GspAssociation asso) {
        return new UdtAssoResourceExtractor(asso, context, fieldPrefixInfo);
    }

}
