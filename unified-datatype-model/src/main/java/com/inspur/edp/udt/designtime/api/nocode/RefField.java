package com.inspur.edp.udt.designtime.api.nocode;

public class RefField {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRefElementId() {
        return refElementId;
    }

    public void setRefElementId(String refElementId) {
        this.refElementId = refElementId;
    }

    public String getLabelCode() {
        return labelCode;
    }

    public void setLabelCode(String labelCode) {
        this.labelCode = labelCode;
    }

    private String id;
    private String refElementId;
    private String labelCode;
}
