package com.inspur.edp.udt.designtime.api.extension;

import com.fasterxml.jackson.databind.JsonSerializer;

public abstract class BaseUdtExtensionSerializer<T extends BaseUdtExtension> extends JsonSerializer<T> {

}
