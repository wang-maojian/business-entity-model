package com.inspur.edp.udt.designtime.api.i18n.merger;

import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;

public class SimpleUdtResourceMerger extends BaseUdtResourceMerger {

    public SimpleUdtResourceMerger(
            SimpleDataTypeDef commonDataType,
            ICefResourceMergeContext context) {
        super(commonDataType, context);
    }
}
