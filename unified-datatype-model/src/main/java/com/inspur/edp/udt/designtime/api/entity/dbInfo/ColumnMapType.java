package com.inspur.edp.udt.designtime.api.entity.dbInfo;

/**
 * 数据库值存储方式
 */
public enum ColumnMapType {
    /**
     * 存为一列
     */
    SingleColumn(0),

    /**
     * 存为多列
     */
    MultiColumns(1);

    private final int intValue;
    private static java.util.HashMap<Integer, ColumnMapType> mappings;

    private synchronized static java.util.HashMap<Integer, ColumnMapType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, ColumnMapType>();
        }
        return mappings;
    }

    private ColumnMapType(int value) {
        intValue = value;
        ColumnMapType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static ColumnMapType forValue(int value) {
        return getMappings().get(value);
    }
}