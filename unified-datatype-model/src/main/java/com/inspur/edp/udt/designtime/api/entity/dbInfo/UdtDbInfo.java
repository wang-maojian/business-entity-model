package com.inspur.edp.udt.designtime.api.entity.dbInfo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inspur.edp.udt.designtime.api.json.UdtNames;

/**
 * 数据库配置信息
 */
public class UdtDbInfo {
    /**
     * 值存储方式
     */
    private ColumnMapType privateMappingType = ColumnMapType.SingleColumn;//ColumnMapType.forValue(0);

    @JsonProperty(UdtNames.MappingType)
    public final ColumnMapType getMappingType() {
        return privateMappingType;
    }

    public final void setMappingType(ColumnMapType value) {
        privateMappingType = value;
    }
//	@Override
//	public String toString() {
//		return "Usr{" +
//				"id='" + getMappingType() +
//
//				'}';
//	}

}