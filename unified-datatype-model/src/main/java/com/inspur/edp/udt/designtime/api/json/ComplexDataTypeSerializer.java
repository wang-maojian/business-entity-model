package com.inspur.edp.udt.designtime.api.json;


import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.designtime.api.collection.CommonDtmCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.operation.CommonDtmSerializer;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.TransmitType;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.utils.UdtThreadLocal;

;

public class ComplexDataTypeSerializer extends UdtSerializer {

    public ComplexDataTypeSerializer() {
        if (UdtThreadLocal.get() != null)
            isFull = UdtThreadLocal.get().getfull();
    }

    public ComplexDataTypeSerializer(boolean full) {
        super(full);
        isFull = full;
    }

    @Override
    protected void writeUdtExtendInfo(JsonGenerator writer, UnifiedDataTypeDef dataType) {
        ComplexDataTypeDef cUdt = (ComplexDataTypeDef) dataType;
        if (isFull || cUdt.getDbInfo() != null) {
            SerializerUtils.writePropertyName(writer, UdtNames.DbInfo);
            SerializerUtils.writePropertyValue_Object(writer, cUdt.getDbInfo());
        }
        writeElements(writer, cUdt);
        if (isFull || (cUdt.getTransmitType() != null && cUdt.getTransmitType() != TransmitType.ValueChanged))
            SerializerUtils.writePropertyValue(writer, UdtNames.TransmitType, cUdt.getTransmitType());
        if (isFull || (cUdt.getDtmBeforeSave() != null && !cUdt.getDtmBeforeSave().isEmpty())) {
            SerializerUtils.writePropertyName(writer, UdtNames.DtmBeforeSave);
            writeDtms(writer, cUdt.getDtmBeforeSave());
        }
        if (isFull || (cUdt.getDtmAfterCreate() != null && !cUdt.getDtmAfterCreate().isEmpty())) {
            SerializerUtils.writePropertyName(writer, UdtNames.DtmAfterCreate);
            writeDtms(writer, cUdt.getDtmAfterCreate());
        }
        if (isFull || (cUdt.getDtmAfterModify() != null && !cUdt.getDtmAfterModify().isEmpty())) {
            SerializerUtils.writePropertyName(writer, UdtNames.DtmAfterModify);
            writeDtms(writer, cUdt.getDtmAfterModify());
        }

    }

    private void writeDtms(JsonGenerator writer, CommonDtmCollection dtms) {
        CommonDtmSerializer serializer = new CommonDtmSerializer(isFull);
        SerializerUtils.writeArray(writer, serializer, dtms);
    }

    private void writeElements(JsonGenerator writer, ComplexDataTypeDef cUdt) {
        if (isFull || (cUdt.getElements() != null && !cUdt.getElements().isEmpty())) {
            SerializerUtils.writePropertyName(writer, UdtNames.Elements);
            UdtElementSerializer ser = new UdtElementSerializer(isFull);
            SerializerUtils.writeArray(writer, ser, cUdt.getElements());
        }
    }
}