package com.inspur.edp.udt.designtime.api.i18n.merger;

import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;

public class ComplexUdtResourceMerger extends BaseUdtResourceMerger {

    public ComplexUdtResourceMerger(
            ComplexDataTypeDef commonDataType,
            ICefResourceMergeContext context) {
        super(commonDataType, context);
    }
}
