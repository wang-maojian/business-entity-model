package com.inspur.edp.udt.designtime.api.entity.dbInfo;

import com.inspur.edp.cef.designtime.api.util.DataValidator;

public class ColumnCollection extends java.util.ArrayList<ColumnInfo> {
    /**
     * 创建字段集合
     */
    public ColumnCollection() {
    }

    /**
     * 移动字段
     *
     * @param toIndex 目的索引为止
     * @param element 需要移动的字段
     */
    public final void move(int toIndex, ColumnInfo element) {
        if (contains(element)) {

            super.remove(element);
            //super.insert(toIndex, element);
            super.add(toIndex, element);

        }
    }

    private void innerAdd(ColumnInfo element) {
        super.add(element);
    }

    /**
     * 克隆
     * <p>
     * // @param absObject
     * //@param parentAssociation
     *
     * @return
     */
    public ColumnCollection clone() {
        ColumnCollection newCollection = new ColumnCollection();
        for (ColumnInfo item : this) {
            Object tempVar = item.clone();
            newCollection.add((ColumnInfo) ((tempVar instanceof ColumnInfo) ? tempVar : null));
        }
        return newCollection;
    }

    /**
     * Index[string]
     */
    public final ColumnInfo getItem(String id) {
        for (ColumnInfo item : this) {
            if (item.getID().equals(id)) {
                return item;
            }
        }
        return null;
    }

    /**
     * 添加一个新字段
     *
     * @param element
     */
    @Override
    public final boolean add(ColumnInfo element) {
        DataValidator.checkForNullReference(element, "element");
        boolean result = super.add(element);
        return result;
    }
}