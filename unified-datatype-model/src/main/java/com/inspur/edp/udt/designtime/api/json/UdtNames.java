package com.inspur.edp.udt.designtime.api.json;

import org.apache.commons.lang3.time.FastDateFormat;

import java.text.SimpleDateFormat;

public class UdtNames {
    public static final String UdtType = "UdtType";
    public static final String Id = "Id";
    public static final String Code = "Code";
    public static final String Name = "Name";
    public static final String LabelId = "LabelId";
    public static final String Description = "Description";
    public static final String Creator = "Creator";
    public static final String CreatedDate = "CreatedDate";
    public static final String Modifier = "Modifier";
    public static final String ModifiedDate = "ModifiedDate";
    public static final String AssemblyName = "AssemblyName";
    public static final String PropertyUseTypeInfos = "PropertyUseTypeInfos";
    public static final String Propertys = "Propertys";
    public static final String ValidationInfos = "Validations";
    public static final String Validation = "Validation";
    public static final String CmpId = "CmpId";
    public static final String CmpPkgName = "CmpPkgName";
    public static final String CmpName = "CmpName";
    public static final String Order = "Order";
    public static final String RequestElements = "RequestElements";
    public static final String TriggerTimePointType = "TriggerTimePointType";
    public static final String IsGenerateComponent = "IsGenerateComponent";
    public static final String BeLabel = "BeLabel";
    public static final String Extensions = "Extensions";

    public static final String MDataType = "MDataType";
    public static final String DataType = "DataType";
    public static final String Length = "Length";
    public static final String Precision = "Precision";
    public static final String DefaultValue = "DefaultValue";
    public static final String IsUnique = "IsUnique";
    public static final String IsRequired = "IsRequired";
    public static final String EnableRtrim = "EnableRtrim";
    public static final String ObjectType = "ObjectType";
    public static final String ChildAssociations = "ChildAssociations";
    public static final String ContainEnumValues = "ContainEnumValues";
    public static final String EnumIndexType = "EnumIndexType";


    public static final String RefEntityId = "RefEntityId";
    public static final String RefElementId = "RefElementId";
    public static final String IsRef = "IsRef";
    public static final String IsRefElement = "IsRefElement";
    public static final String RefEntityPkgName = "RefEntityPkgName";
    public static final String RefEntityCode = "RefEntityCode";
    public static final String RefEntityName = "RefEntityName";
    public static final String KeyCollection = "KeyCollection";
    public static final String RefElementCollection = "RefElementCollection";


    public static final String RefObjectID = "RefObjectID";
    public static final String RefObjectCode = "RefObjectCode";
    public static final String RefObjectName = "RefObjectName";
    public static final String RefModelID = "RefModelID";
    public static final String RefModelCode = "RefModelCode";
    public static final String RefModelName = "RefModelName";
    public static final String RefModelPkgName = "RefModelPkgName";
    public static final String Where = "Where";
    public static final String AssSendMessage = "AssSendMessage";
    public static final String ForeignKeyConstraintType = "ForeignKeyConstraintType";
    public static final String DeleteRuleType = "DeleteRuleType";

    public static final String PropertyName = "PropertyName";
    public static final String PropertyUseType = "PropertyUseType";
    public static final String CanEdit = "CanEdit";


    public static final String DbInfo = "DbInfo";
    public static final String MappingType = "MappingType";
    public static final String Elements = "Elements";
    public static final String IsRequire = "IsRequire";
    public static final String IsVirtual = "IsVirtual";
    public static final String IsMultiLanguage = "IsMultiLanguage";
    public static final String IsUdt = "IsUdt";
    public static final String UdtID = "UdtID";
    public static final String UdtPkgName = "UdtPkgName";
    public static final String UdtName = "UdtName";
    public static final String UnifiedDataType = "UnifiedDataType";

    // determination
    public static final String ID = "ID";
    public static final String ComponentId = "ComponentId";
    public static final String ComponentPkgName = "ComponentPkgName";
    public static final String ComponentName = "ComponentName";
    public static final String DtmBeforeSave = "DtmBeforeSave";
    public static final String DtmAfterSave = "DtmAfterSave";
    public static final String DtmAfterModify = "DtmAfterModify";
    public static final String DtmAfterCreate = "DtmAfterCreate";
    public static final String ValAfterModify = "ValAfterModify";
    public static final String ValBeforeSave = "ValBeforeSave";
    public static final String GetExecutingDataStatus = "GetExecutingDataStatus";
    public static final String TransmitType = "TransType";

    public static FastDateFormat DateTimeFormat = FastDateFormat.getInstance("yyyy年MM月dd日 HH:mm:ss");
    public static ThreadLocal<SimpleDateFormat> local = new ThreadLocal<SimpleDateFormat>() {
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");

        }
    };

}
