package com.inspur.edp.udt.designtime.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inspur.edp.udt.designtime.api.entity.enumtype.UseType;

/**
 * 属性使用方式信息
 */
public class UseTypeInfo {
    /**
     * 属性名称
     * <see cref="string"/>
     */
    private String privatePropertyName;

    @JsonProperty("PropertyName")
    public final String getPropertyName() {
        return privatePropertyName;
    }

    public final void setPropertyName(String value) {
        privatePropertyName = value;
    }

    /**
     * 属性使用方式
     * <see cref="UseType"/>
     */
    private UseType privatePropertyUseType = UseType.AsTemplate;

    @JsonProperty("PropertyUseType")
    public final UseType getPropertyUseType() {
        return privatePropertyUseType;
    }

    public final void setPropertyUseType(UseType value) {
        privatePropertyUseType = value;
    }

    ;
    /**
     * 是否可以编辑
     * <see cref="bool"/>
     */
    private boolean privateCanEdit = true;

    @JsonProperty("CanEdit")
    public final boolean getCanEdit() {
        return privateCanEdit;
    }

    public final void setCanEdit(boolean value) {
        privateCanEdit = value;
    }


}